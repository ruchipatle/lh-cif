package com.lh.cif.core.exception;

import lombok.Data;

public @Data class CustomGenericException {

	private String errCode;

	private String errMsg;

}
