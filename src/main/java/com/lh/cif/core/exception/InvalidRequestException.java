package com.lh.cif.core.exception;

import org.springframework.http.HttpStatus;

public class InvalidRequestException extends RestException {

	private static final long serialVersionUID = 1L;

	public InvalidRequestException(final HttpStatus status, final String message) {
		super(status, message);
	}

	public InvalidRequestException(final HttpStatus status, final Exception cause, final String message) {
		super(status, cause, message);
	}
}
