package com.lh.cif.core.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class RestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private HttpStatus status;

	public RestException(final String message) {
		super(message);
	}

	public RestException(final HttpStatus status, final String message) {
		super(message);
		this.status = status;
	}

	public RestException(final HttpStatus status, final Exception cause) {
		super(cause);
		this.status = status;
	}

	public RestException(final HttpStatus status, final Exception cause, final String message) {
		super(message, cause);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return this.status;
	}
}
