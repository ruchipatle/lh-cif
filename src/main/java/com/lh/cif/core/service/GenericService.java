package com.lh.cif.core.service;

import java.util.List;
import java.util.Map;

public interface GenericService<T> {

	/** Persist the newInstance object into database */
	T saveOrUpdate(T newInstance);

	/**
	 * Retrieve an object that was previously persisted to the database using the
	 * indicated id as primary key
	 */
	T find(Long id);

	T find(String id);

	List<T> findAll();
	
	List<T> findAll(List<Long> ids);

	/** Remove an object from persistent storage in the database */
	void delete(Long id);

	void delete(String id);

	T getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap);

	List<T> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap);

}
