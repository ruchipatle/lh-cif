package com.lh.cif.core.repository;

import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.QueryResultIterator;

public interface GenericDao<T> {

	/** Persist the newInstance object into database */
	public T saveOrUpdate(T newInstance);

	/**
	 * Retrieve an object that was previously persisted to the database using the
	 * indicated id as primary key
	 */
	public T find(Long id);

	public T find(String id);

	public List<T> findAll();
	
	public List<T> findAll(List<Long> ids);

	/** Remove an object from persistent storage in the database */
	public void delete(Long id);

	public void delete(String id);

	public T getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap);

	public List<T> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap);

	public QueryResultIterator<T> findByColumnAndValuePaginated(Map<String, Object> columnValueMap, Integer range, String startPosition);

}
