package com.lh.cif.core.web;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.ui.Model;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.lh.cif.core.domain.dto.ErrorMessage;
import com.lh.cif.core.exception.CustomGenericException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ankit Bharti- 680806 TCS
 *
 */

@Slf4j
@ControllerAdvice
public class GlobalExceptionControllerAdvice extends ResponseEntityExceptionHandler {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
		List<String> errors = new ArrayList<>(fieldErrors.size() + globalErrors.size());
		String error;
		for (FieldError fieldError : fieldErrors) {
			error = fieldError.getField() + ":" + fieldError.getDefaultMessage();
			errors.add(error);
		}
		for (ObjectError objectError : globalErrors) {
			error = objectError.getObjectName() + ": " + objectError.getDefaultMessage();
			errors.add(error);
		}
		ErrorMessage errorMessage = new ErrorMessage(errors);
		return new ResponseEntity(errorMessage, headers, status);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		String unsupported = "Unsupported content type: " + ex.getContentType();
		String supported = "Supported content types: " + MediaType.toString(ex.getSupportedMediaTypes());
		ErrorMessage errorMessage = new ErrorMessage(unsupported, supported);
		return new ResponseEntity(errorMessage, headers, status);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		Throwable mostSpecificCause = ex.getMostSpecificCause();
		ErrorMessage errorMessage;
		if (mostSpecificCause != null) {
			String exceptionName = mostSpecificCause.getClass().getName();
			String message = mostSpecificCause.getMessage();
			errorMessage = new ErrorMessage(exceptionName, message);
		} else {
			errorMessage = new ErrorMessage(ex.getMessage());
		}
		return new ResponseEntity(errorMessage, headers, status);
	}

	@ExceptionHandler(Exception.class)
	public String handleAllException(final Model map, final Exception ex) {
		CustomGenericException expResponse = new CustomGenericException();
		expResponse.setErrCode("GW500");
		expResponse.setErrMsg("\n Time: " + new Date() + "|\n Message:" + (ex.getMessage() != null ? ex.getMessage() : "Internal Server Error") + "|\n Cause: " + ex.getCause());
		ex.printStackTrace();
		printException(ex, "");
		// return new ResponseEntity<CustomGenericException>(expResponse,
		// HttpStatus.INTERNAL_SERVER_ERROR);
		return "redirect:/ims/handleException";
	}

	protected void printException(final Exception ex, final String info) {
		String errorMessage = ex.getMessage();
		if (null == errorMessage || errorMessage.isEmpty()) {
			StringWriter sw = new StringWriter();
			ex.printStackTrace(new PrintWriter(sw));
			log.error("\n Global Exception Handler \n " + info + "\n" + sw.toString());
		} else {
			log.error("\n Error \n " + info + "\n" + ex.getMessage() + ex.getCause());
		}
		ex.printStackTrace();
	}
}
