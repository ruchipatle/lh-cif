//package com.lh.cif.core.web;
//
//import java.io.PrintWriter;
//import java.io.StringWriter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.json.JSONException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.google.api.client.repackaged.com.google.common.base.Throwables;
//import com.google.appengine.api.taskqueue.Queue;
//import com.google.appengine.api.taskqueue.QueueFactory;
//import com.google.appengine.api.taskqueue.RetryOptions;
//import com.google.appengine.api.taskqueue.TaskOptions;
//import com.lh.cif.core.domain.dto.GenericRequestDto;
//import com.lh.cif.core.domain.dto.Response;
//import com.lh.cif.core.service.ExportToCSVService;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//@RestController
//public class ExportMasterDataRest {
//
//	@Autowired
//	ExportToCSVService exportToCSVService;
//
//	@RequestMapping(value = "/ims/opco/{countryCode}/locale/{locale}/api/admin/export", method = RequestMethod.POST)
//	public Response addTaskToQueueForExport(@PathVariable("countryCode") String countryCode, @PathVariable("locale") String locale, final HttpServletRequest request, final HttpServletResponse response,
//			final HttpSession session, Model map, @RequestBody GenericRequestDto genericRequestDto) throws JSONException {
//		Response resp = new Response();
//		if (genericRequestDto.getInput() != null) {
//			try {
//				final Queue queue = QueueFactory.getDefaultQueue();
//				RetryOptions retry = RetryOptions.Builder.withTaskRetryLimit(1);
//				queue.add(TaskOptions.Builder.withUrl("/exportMasterToCSV").retryOptions(retry).param("countryCode", countryCode).param("receiverId", genericRequestDto.getUserEmail()).param("userLocale", locale)
//						.param("ids", genericRequestDto.getInput()));
//
//				resp.setStatus(HttpStatus.OK);
//				resp.setFlag(true);
//				resp.setCountryCode(countryCode);
//				
//			} catch (Exception e) {
//				log.error("addTaskToQueue for ExportToCSV exception..." + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
//
//				resp.setFlag(false);
//				resp.setMessage("Export Fail");
//				resp.setStatus(HttpStatus.BAD_REQUEST);
//				resp.setCountryCode(countryCode);
//			}
//		}
//		return resp;
//	}
//
//	@RequestMapping(value = "/exportMasterToCSV", method = RequestMethod.POST)
//	public void executeTaskToExportToCSV(HttpServletRequest req, HttpServletResponse resp, @RequestParam("countryCode") String countryCode, @RequestParam("receiverId") String receiverId, @RequestParam("ids") String ids,
//			@RequestParam("userLocale") String userLocale) {
//
//		try {
//			exportToCSVService.masterDataExportToCSV(resp, countryCode, receiverId, ids, userLocale);
//		} catch (Exception e) {
//			log.error("executeTaskToExportToCSV exception..." + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
//		}
//	}
//
//	public void printException(String callerFunction, Exception e) {
//		String errorMessage = e.getMessage();
//		if (null == errorMessage || errorMessage.isEmpty()) {
//			StringWriter sw = new StringWriter();
//			e.printStackTrace(new PrintWriter(sw));
//			log.error("\n " + callerFunction + "\n" + sw.toString());
//		} else {
//			log.error("\n " + callerFunction + "\n" + e.getMessage());
//		}
//	}
//}
