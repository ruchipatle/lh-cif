package com.lh.cif.core.domain.dto;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Response {

	private String id;

	private HttpStatus status;

	private boolean flag;

	private String message;

	private String countryCode;

}
