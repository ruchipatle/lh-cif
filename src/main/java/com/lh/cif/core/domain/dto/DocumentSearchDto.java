package com.lh.cif.core.domain.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DocumentSearchDto {

	private String indexName;

	private String queryString;

	private String cursorWebSafeString;

	private int limit;

	private String sortColumn;

	private String sortDirection;

}
