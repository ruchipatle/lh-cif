package com.lh.cif.core.domain.dto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.Data;

public @Data class ErrorMessage {

	private List<String> errors;

	public ErrorMessage() {
	}

	public ErrorMessage(final List<String> errors) {
		this.errors = errors;
	}

	public ErrorMessage(final String error) {
		this(Collections.singletonList(error));
	}

	public ErrorMessage(final String... errors) {
		this(Arrays.asList(errors));
	}
}
