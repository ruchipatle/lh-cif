package com.lh.cif.core.domain.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GenericRequestDto {

	private String countryCode;

	private String locale;

	private String userEmail;

	private String input;
}
