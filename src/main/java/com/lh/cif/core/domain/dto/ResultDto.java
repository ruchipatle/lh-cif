package com.lh.cif.core.domain.dto;

import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ResultDto {

	private HttpStatus status;

	private List aaData;

	private String searchNextCursor;

	private int count;

	private String countryCode;
}
