package com.lh.cif.core.domain.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LegacyRoutingDecision {

	boolean recordExistInCurrentCountryCode;
	
	boolean recordFoundInUserCountryCodeList;
	
	boolean recordFoundInMultipleCountryCode;
	
	//IFF record not found in multiple country code
	String recordFoundInCountryCode;
	
	List<String> recordFoundInSessionCountryCodeList;
	
}
