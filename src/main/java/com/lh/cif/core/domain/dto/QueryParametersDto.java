package com.lh.cif.core.domain.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class QueryParametersDto {

	private String freeSearch;

	private String deptVal;

	private String opCoIdVal;

	private String imsKeywordVal;

	private String sectionVal;

	private String folderVal;

	private String linkTypeVal;

	private String validFromDate;

	private String validToDate;

	private String pubFromDate;

	private String pubToDate;

	private String nextRevFrom;

	private String nextRevTo;

	private String status;

	private boolean directSearch;

	private String countryCode;

	private String locale;

	private String userEmail;

	private String searchNextCursor;

}
