package com.lh.cif.core.interceptor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SessionInterceptor implements HandlerInterceptor {

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object arg2, Exception arg3) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView arg3) throws Exception {
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {

        Map<String, Object> pathVariables = (HashMap<String, Object>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        String uri = request.getRequestURI();
        String uriMethod = request.getMethod();
        log.info("preHandle Role Page - \n URI : " + uri + "_ Method : " + uriMethod);

        if (null != pathVariables) {

            String countryCode = (String) pathVariables.get("countryCode");
            String locale = (String) pathVariables.get("locale");

            HttpSession session = request.getSession();
            long sessionDuration = TimeUnit.MILLISECONDS.toMinutes((System.currentTimeMillis() - session.getCreationTime()));
            log.info("*************************** Session duration(min) " + sessionDuration);
            if (sessionDuration >= 49) {
                /*
                 * NO NEED TO EXPLICIT STOP USER AFTER 50 MINS.. DRIVE PICKER TOKEN WILL
                 * AUTOMATICALLY LOAD AFTER 30 MINS
                 * log.info("*************************** Session Expired");
                 * session.invalidate(); response.sendRedirect(errorRedirectUrl + "C100");
                 */
                return true;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }

}
