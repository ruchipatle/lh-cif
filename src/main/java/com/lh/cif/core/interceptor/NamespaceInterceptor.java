package com.lh.cif.core.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lh.cif.core.util.NameSpaceUtil;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.service.RoleVsActionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NamespaceInterceptor implements HandlerInterceptor {

    @Autowired
    RoleVsActionService imsRoleVsActionService;

    @Autowired
    NameSpaceUtil nameSpaceUtil;

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object arg2, Exception arg3) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView arg3) throws Exception {
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {

        log.info("preHandle");

        Map<String, Object> pathVariables = (HashMap<String, Object>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        if (null != pathVariables) {

            String requestUrl = request.getRequestURI();
            String countryCode = (String) pathVariables.get("countryCode");
            String locale = (String) pathVariables.get("locale");

            UserInfoSession userInfoSession = (UserInfoSession) request.getSession().getAttribute("userInfoSession");

            log.info("Reaching ::::::: " + userInfoSession);

//			if (userInfoSession.getCountryCodeLocaleMap().size() > 0) {
//
//				if (userInfoSession.getCountryCodeLocaleMap().containsKey(countryCode)) {
//					
//					if(!userInfoSession.getCountryCode().equalsIgnoreCase(countryCode)) {
//					log.info("Direct link hasbeen accessed and opco has been changed, transferring to nameSpaceUtil");				
//						 nameSpaceUtil.changeOpco(requestUrl, userInfoSession, countryCode, request);	
//					} 
//
//					if (userInfoSession.getCountryCodeLocaleMap().get(countryCode).containsValue(locale)) {
//						log.info("************* Locale for opco is  FOUND " + locale);
//					} else {
//						log.info("************* Locale for opco is  NOT FOUND " + locale);
//						String[] tempUrl = requestUrl.split("\\/");
//						locale = userInfoSession.getCountryCodeLocaleMap().get(countryCode).get("defaultLocale");
//						// locale position in url is 5th
//						tempUrl[5] = locale;
//						response.sendRedirect(StringUtils.join(tempUrl, "/"));
//						return false;
//					}
//				/*	}*/
//					log.info(" ****************** User Role ************** " + userInfoSession.getRole());
//				} else {
//					log.error(" ****************** INCORRECT COUNTRY CODE IN URL ************** " + countryCode);
//					countryCode = userInfoSession.getCountryCodeLocaleMap().entrySet().stream().findFirst().get().getKey();
//					String requestLocale = request.getLocale().getLanguage();
//					if (userInfoSession.getCountryCodeLocaleMap().get(countryCode).containsValue(locale)) {
//						log.info("************* Locale for opco is  FOUND");
//						locale = requestLocale;
//					} else {
//						log.info("************* Locale for opco is  NOT FOUND");
//						locale = userInfoSession.getCountryCodeLocaleMap().get(countryCode).get("defaultLocale");
//					}
//
//					String[] tempUrl = requestUrl.split("\\/");
//					tempUrl[3] = countryCode;
//					tempUrl[5] = locale;
//					log.error(" ****************** NEW URL ************** " + StringUtils.join(tempUrl, "/"));
//					response.sendRedirect(StringUtils.join(tempUrl, "/"));
//					return false;
//				}
//			} else {
//				log.error(" ****************** OPCO ACCESS DENIED ************** " + countryCode);
//				// SEND 403 exception here
//			}

        }
        return true;

    }

}
