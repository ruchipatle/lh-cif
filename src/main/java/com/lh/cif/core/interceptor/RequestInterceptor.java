package com.lh.cif.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.lh.cif.iam.domain.dto.UserInfoSession;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestInterceptor implements HandlerInterceptor {

	// SET OF FLAGS
	private boolean AIUK = false;
	private boolean faqAccess = false;

	private boolean createAccess = false;
	private boolean searchAccess = false;
	private boolean manageAccess = false;
	private boolean masterdataMgmtAccess = false;
	private boolean dashboardAccess = false;
	private boolean roleMgmtAccess = false;
	private boolean adminAccess = false;
	
	private boolean workflowApproverAccess = false;

	private boolean roleAdmin = false;
	private boolean roleAdminApprover = false;
	private boolean roleCE = false;
	private boolean roleCEApprover = false;
	private boolean roleApprover = false;
	private boolean roleReader = false;

	private String _ICO;
	private String _LOGO_BIG;
	private String _LOGO_SMALL;
	private String _THEME;

	@Override
	public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object arg2, Exception arg3) throws Exception {
		log.info("afterCompletion");

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView arg3) throws Exception {
		log.info("postHandle");
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {

		log.info("preHandle");

		// Reset Flags
//		resetFlags();
//
//		Map<String, Object> pathVariables = (HashMap<String, Object>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
//
//		if (null != pathVariables) {
//
//			String countryCode = (String) pathVariables.get("countryCode");
//			String locale = (String) pathVariables.get("locale");
//
//			request.setAttribute("COUNTRY_CODE", countryCode);
//			request.setAttribute("LOCALE", locale);
//
//			// Language for all pages
//			HolcimConfigHelper configUtils = ConfigUtils.getLocaleInstance(locale);
//			request.setAttribute("langConfig", configUtils);
//			
			UserInfoSession userInfoSession = (UserInfoSession) request.getSession().getAttribute("userInfoSession");
//			String userRole = userInfoSession.getOpcoRoleMap().get(countryCode);
//			
			request.setAttribute("USER_EMAIL", userInfoSession.getUserEmail());
//			request.setAttribute("USER_ROLE", userRole);
			request.setAttribute("accessToken", (String)request.getSession().getAttribute("userAccessToken"));
//			
//			setUserAccess(userRole, countryCode , request);
//		}
		
		return true;
	}

	private void resetFlags() {
		AIUK = false;
		faqAccess = false;
		createAccess = false;
		searchAccess = false;
		manageAccess = false;
		masterdataMgmtAccess = false;
		dashboardAccess = false;
		roleMgmtAccess = false;
		adminAccess = false;
		workflowApproverAccess = false;
		roleAdmin = false;
		roleAdminApprover = false;
		roleCE = false;
		roleCEApprover = false;
		roleApprover = false;
		roleReader = false;
		_ICO = "";
		_LOGO_BIG = "";
		_LOGO_SMALL = "";
		_THEME = "";

	}

	// TODO Needs to be enhanced better
	private HttpServletRequest setUserAccess(String role, String countryCode, HttpServletRequest req) {

		if (role.equalsIgnoreCase("ADMIN")) {
			roleAdmin = true;
			adminAccess = true;
			searchAccess = true;
			manageAccess = true;
			masterdataMgmtAccess = true;
			dashboardAccess = true;
			roleMgmtAccess = true;
			createAccess = true;
			workflowApproverAccess = true;
		}

		if (role.equalsIgnoreCase("ADMIN-APPROVER")) {
			roleAdminApprover = true;
			adminAccess = true;
			searchAccess = true;
			manageAccess = true;
			dashboardAccess = true;
			masterdataMgmtAccess = true;
			roleMgmtAccess = true;
			createAccess = true;
			workflowApproverAccess = true;
		}

		if (role.equalsIgnoreCase("CE")) {
			roleCE = true;
			searchAccess = true;
			createAccess = true;
			manageAccess = true;
			workflowApproverAccess = true;
		}

		if (role.equalsIgnoreCase("CE-APPROVER")) {
			roleCEApprover = true;
			searchAccess = true;
			createAccess = true;
			manageAccess = true;
			workflowApproverAccess = true;
		}

		if (role.equalsIgnoreCase("APPROVER")) {
			roleApprover = true;
			searchAccess = true;
			manageAccess = true;
			workflowApproverAccess = true;
		}

		if (role.equalsIgnoreCase("READER")) {
			roleReader = true;
			searchAccess = true;
		}

		// AIUK
		if (countryCode.equalsIgnoreCase("AIUK")) {
			AIUK = true;
			faqAccess = true;
			_ICO = "/LHassets/core/latest/img/AI_UK/favicon_AIUK.ico";
			_LOGO_BIG = "/LHassets/core/latest/img/AI_UK/AIUK_logo.jpg";
			_LOGO_SMALL = "/LHassets/core/latest/img/AI_UK/AIUK_logo.jpg";
			_THEME = "skin-blue";
		} else {
			_ICO = "/LHassets/core/latest/img/favicon/favicon.ico";
			_LOGO_BIG = "/LHassets/core/latest/img/logo.png";
			_LOGO_SMALL = "/LHassets/core/latest/img/48x48.png";
			_THEME = "skin-brown";
		}
		

		return setFlagsInRequest(req);

	}

	// TODO Needs to be enhanced better
	private HttpServletRequest setFlagsInRequest(HttpServletRequest req) {

		req.setAttribute("AIUK", AIUK);
		req.setAttribute("faqAccess", faqAccess);
		req.setAttribute("createAccess", createAccess);
		req.setAttribute("searchAccess", searchAccess);
		req.setAttribute("manageAccess", manageAccess);
		req.setAttribute("masterdataMgmtAccess", masterdataMgmtAccess);
		req.setAttribute("dashboardAccess", dashboardAccess);
		req.setAttribute("roleMgmtAccess", roleMgmtAccess);
		req.setAttribute("adminAccess", adminAccess);
		req.setAttribute("roleAdmin", roleAdmin);
		req.setAttribute("roleAdminApprover", roleAdminApprover);
		req.setAttribute("roleCE", roleCE);
		req.setAttribute("roleCEApprover", roleCEApprover);
		req.setAttribute("roleApprover", roleApprover);
		req.setAttribute("roleReader", roleReader);
		
		req.setAttribute("workflowApproverAccess", workflowApproverAccess);

		req.setAttribute("_ICO", _ICO);
		req.setAttribute("_LOGO_BIG", _LOGO_BIG);
		req.setAttribute("_LOGO_SMALL", _LOGO_SMALL);
		req.setAttribute("_THEME", _THEME);

		return req;
	}

}
