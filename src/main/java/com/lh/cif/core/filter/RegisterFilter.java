package com.lh.cif.core.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.googlecode.objectify.ObjectifyService;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.DocumentComments;
import com.lh.cif.search.model.entity.DocumentLikes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("registerFilter")
public class RegisterFilter extends GenericFilterBean {

	public RegisterFilter() {
		ObjectifyService.register(CIFDocument.class);
		ObjectifyService.register(DocumentComments.class);
		ObjectifyService.register(DocumentLikes.class);
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		//
	}
}
