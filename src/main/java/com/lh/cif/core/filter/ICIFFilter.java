package com.lh.cif.core.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.lh.cif.iam.domain.dto.UserInfoSession;

import lombok.extern.slf4j.Slf4j;

@Slf4j
/* @Component */
public class ICIFFilter extends GenericFilterBean  {
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();

        try {
            UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");
            if (null == userInfoSession) {
                String userName = request.getParameter("name");
                log.info("USERNAME -- "+userName);
                String email = request.getParameter("email");
                log.info("EMAIL -- "+email);
                userInfoSession =  new  UserInfoSession();
                userInfoSession.setUserName(userName);
                userInfoSession.setUserEmail(email);
                session.setAttribute("userInfoSession", userInfoSession);
            }
            
            chain.doFilter(req, response);
        }catch(Exception e) {

        }
    }

}
