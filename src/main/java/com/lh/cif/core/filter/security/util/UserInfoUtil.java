package com.lh.cif.core.filter.security.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Groups;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.directory.DirectoryService;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.domain.entity.MetaDataAccessControl;
import com.lh.cif.iam.domain.entity.OpcoLocalDetails;
import com.lh.cif.iam.service.IMSGroupService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserInfoUtil {

	public static Map<String, List<String>> getIMSCompanyGroupList(final List<OpcoLocalDetails> entityList) {

		Map<String, List<String>> companyGroupMap = new HashMap<String, List<String>>();

		for (OpcoLocalDetails details : entityList) {
			companyGroupMap.put(details.getOpco() + "~" + details.getCountryCode(), details.getCompanyGroupName());
			if (null != details.getCustomCompanyGroupEmails()) {
				List<String> temp = companyGroupMap.get(details.getOpco() + "~" + details.getCountryCode());
				temp.addAll(details.getCustomCompanyGroupEmails());
				companyGroupMap.put(details.getOpco() + "~" + details.getCountryCode(), temp);
			}
		}

		return companyGroupMap;

	}

	public static void setUserCompanyAndCompanyGroup(final List<String> userGroups, final UserInfoSession userInfoSession, final Map<String, List<String>> companyGroupMap) {
		for (Entry<String, List<String>> entry : companyGroupMap.entrySet()) {
			for (String userGroupEmail : userGroups) {
				if (entry.getValue().contains(userGroupEmail)) {
					userInfoSession.setUserCompanyGroupEmailId(userGroupEmail);
					userInfoSession.setOpcoName(entry.getKey().split("~")[0]); // opco as in Brasil
					userInfoSession.setCountryCode(entry.getKey().split("~")[1]); // code as in BR
				}
			}

		}
	}

	public static void calculateAllowedOpcos(List<String> userGroupEmailIds, UserInfoSession userInfoSession, Map<String, List<String>> companyGroupMap) {
		List<String> authorizedOpcos = new ArrayList<String>();
		Map<String, String> authorizedOpcoMap = new HashMap<String, String>();
		for (Entry<String, List<String>> entry : companyGroupMap.entrySet()) {
			for (String userGroupEmail : userGroupEmailIds) {
				if (entry.getValue().contains(userGroupEmail) || entry.getValue().contains(userInfoSession.getUserEmail())) {
					authorizedOpcos.add(entry.getKey().split("~")[1]);
					authorizedOpcoMap.put(entry.getKey().split("~")[1], entry.getKey().split("~")[0]);
				}
			}

		}
		userInfoSession.setAuthorizedOpcos(authorizedOpcos);
		userInfoSession.setAuthorizedOpcoMap(authorizedOpcoMap);
	}

	public static void findRoleInEachOpco(List<OpcoLocalDetails> opcoEntityList, UserInfoSession userInfoSession, List<String> userGroupEmailIds) {

		List<String> allowedOpcos = userInfoSession.getAuthorizedOpcos();
		Map<String, String> opcoRoleMap = new HashMap<String, String>();
		for (String opcoTemp : allowedOpcos) {
			for (OpcoLocalDetails opcoLocalDetails : opcoEntityList) {
				if (opcoTemp.equals(opcoLocalDetails.getCountryCode())) {
					String userRoleTemp = UserRoleDecisionUtil.getUserRole(userGroupEmailIds, opcoLocalDetails);
					opcoRoleMap.put(opcoTemp, userRoleTemp);
				}
			}
		}
		userInfoSession.setOpcoRoleMap(opcoRoleMap);
	}

	public static void setUserOpco(UserInfoSession userInfoSession, List<OpcoLocalDetails> entityList) {

		List<String> allowedOpcos = userInfoSession.getAuthorizedOpcos();
		boolean flag = false;
		for (OpcoLocalDetails opcoLocalDetails : entityList) {
			if (null != allowedOpcos && allowedOpcos.size() == 1 && opcoLocalDetails.getCountryCode().equals(allowedOpcos.get(0))) {// single opco- as is behaviour
				// userInfoSession.setUserCompanyGroupEmailId(opcoLocalDetails.get);
				userInfoSession.setOpcoName(opcoLocalDetails.getOpco()); // opco as in Brasil
				userInfoSession.setCountryCode(allowedOpcos.get(0)); // code as in BR
				userInfoSession.setRole(userInfoSession.getOpcoRoleMap().get(allowedOpcos.get(0)));
				flag = true;
			} else if (null != allowedOpcos && allowedOpcos.size() > 1) {
				for (String temp : allowedOpcos) {
					if (temp.equals(opcoLocalDetails.getCountryCode()) && opcoLocalDetails.isParent()) {// Lab and CREST
						userInfoSession.setOpcoName(opcoLocalDetails.getOpco()); // opco as in Brasil
						userInfoSession.setCountryCode(temp); // code as in BR
						userInfoSession.setRole(userInfoSession.getOpcoRoleMap().get(temp));
						flag = true;
					}
				}

			}
		}
		if (!flag) {// exception case - adhoc access is given to different opco
			for (OpcoLocalDetails opcoLocalDetails : entityList) {
				if (null != allowedOpcos && allowedOpcos.contains(opcoLocalDetails.getCountryCode())) {
					userInfoSession.setOpcoName(opcoLocalDetails.getOpco()); // opco as in Brasil
					userInfoSession.setCountryCode(opcoLocalDetails.getCountryCode()); // code as in BR
					userInfoSession.setRole(userInfoSession.getOpcoRoleMap().get(opcoLocalDetails.getCountryCode()));
					break;
				}
			}
		}

	}
	
	
	/** It select master id & restricted data type ( Department etc) in user session 
	 * @param userInfoSession
	 * @param userGroups
	 * @param restrictedMetaData
	 */
	public static void setRestrictedMetaData (UserInfoSession userInfoSession, List<String> userGroups ,List<MetaDataAccessControl> restrictedMetaData) {
		log.info("Google groups of user:  "+userInfoSession.getUserEmail()+" is: "+userGroups);
		Map <String, String> restrictedMap = new HashMap<String, String>();
		for (MetaDataAccessControl entry :restrictedMetaData ) {
			if (userGroups.contains(entry.getGroupId()))
				restrictedMap.put(entry.getMasterId(), entry.getMetaDataType());
		}
		log.info("UserInfoUtil class :: restrictedMap "+restrictedMap);
		
		userInfoSession.setRestrictedMetaData(restrictedMap);
	}
	
	/**
	 * It provides all the google groups of the user
	 * @param userEmail
	 * @param gsuiteManager
	 * @param googleGroupService
	 * @return
	 * @throws Throwable
	 */
	public static List<String> getUserGroups(String userEmail, GsuiteManager gsuiteManager,
			IMSGroupService googleGroupService) {
		List<String> userGroupEmailIds = new ArrayList<String>();
		try {
			Credential credential = gsuiteManager.getAdminOauth();
			DirectoryService adminDirectory = gsuiteManager.getDirectory(credential);
			Groups userGroups = googleGroupService.getGroupsByUserEmail(userEmail, adminDirectory);
			List<Group> userGroupsList = userGroups.getGroups();
			for (Group gp : userGroupsList) {
				userGroupEmailIds.add(gp.getEmail());
			}
		} catch (Throwable e) {
			log.error("Can not find user groups, making the list as null so to break further wrong flow "+e.getMessage());
			userGroupEmailIds = null;
			e.printStackTrace();
		}
		return userGroupEmailIds;
	}
}
