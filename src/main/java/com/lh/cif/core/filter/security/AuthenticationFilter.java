package com.lh.cif.core.filter.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.google.api.client.auth.oauth2.Credential;
import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.openid_connect.utils.OpenIdConnectConstants;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.service.UserInfoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthenticationFilter extends GenericFilterBean {

    /* private String userEmail; */
    private UserInfoService userInfoService;
    private String authMode;
    private HolcimConfigHelper configUtilsDefault;
    private String locale;

    public void setUserInfoService(final UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public void setAuthMode(final String authMode) {
        this.authMode = authMode;
    }

    /**
     * Prepare UserInfoSession and insert into attribute, if not found originally
     *
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        log.info("doFilter AuthenticationFilter");

        String userEmail = null;
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        locale = request.getLocale().getLanguage();

        switch (this.authMode) {
        case "cloud":
            userEmail = (String) session.getAttribute(OpenIdConnectConstants.SESSION_EMAIL_ATTRIBUTE);
            break;
        case "local":
            userEmail = "admin1.gr@dev.holcim.com";
            break;
        default:
            log.error("\n Errror Auth Mode not found " + this.authMode);
        }

        try {
            UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");

            if (null == userInfoSession) {
                // check email and get user
                userInfoSession = userInfoService.getUserInfo(userEmail, locale);
                session.setAttribute("userInfoSession", userInfoSession);
                // below is the potential fix for some other user drive picker
                Credential cred = userInfoService.getUserCredential(userInfoSession.getUserEmail());

                session.setAttribute("userAccessToken", cred.getAccessToken());
                session.setAttribute("userTokenExpirationInSeconds", cred.getExpiresInSeconds());
                Long accessTokenStartTime = System.currentTimeMillis();
                session.setAttribute("accessTokenStartTime", accessTokenStartTime);
            }
            tokenRefreshDecision(userInfoSession, session);

//			if (!userInfoSession.isAppAccess()) {
//				log.warn("Access denied to IMS to " + userInfoSession.getUserEmail());
//				req.setAttribute("userInfoSession", userInfoSession);
//				req.setAttribute("noPermissionMsg", "Access denied to IMS");
//				request.getRequestDispatcher("/error/a01").forward(request,response);
//			} else {
//				// Default Config
//				configUtilsDefault = ConfigUtils.getDefaultConfigInstance();
//				req.setAttribute("defaultConfig", configUtilsDefault);

            log.info("User logged in : " + userInfoSession.getUserEmail());

            chain.doFilter(req, response);
//			}
            // chain.doFilter(req, response);
        } catch (Exception e) {
            log.error("Exception occured in Auth filter:: " + e.getMessage());
            request.getRequestDispatcher("/error/g01").forward(request, response);
        }

    }

    protected void tokenRefreshDecision(UserInfoSession userInfoSession, HttpSession session) {

        Long currentTime = System.currentTimeMillis();
        Long userTokenExpiration = 1801L;
        if (session.getAttribute("accessTokenStartTime") == null) {
            session.setAttribute("accessTokenStartTime", currentTime);
        } else {
            Long accessTokenStartTime = (Long) session.getAttribute("accessTokenStartTime");
            userTokenExpiration = (currentTime - accessTokenStartTime) / 1000;
        }

        // If Oauth2 user token is about to expire, refresh it before 5min. Although
        // session timeout is set at 50min. ... lets check back logic again
        if (userTokenExpiration > 1800) {
            try {
                Credential cred = userInfoService.getUserCredential(userInfoSession.getUserEmail());

                userTokenExpiration = cred.getExpiresInSeconds();

                session.setAttribute("userAccessToken", cred.getAccessToken());
                session.setAttribute("userTokenExpirationInSeconds", userTokenExpiration);

                session.setAttribute("accessTokenStartTime", System.currentTimeMillis());
            } catch (Exception e) {
                log.error("Error in token ", e);
            }
        } else {
            log.info("Token has life more than 30 mins left, no refresh needed.");
        }

    }

    @Override
    public void destroy() {
        //
    }

    protected void sendForbidden(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.sendError(403);
    }

    protected void sendUnauthorized(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

        response.sendError(401);
    }

    protected void sendForbiddenApplicationAccess(final String application, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.sendError(403, "No access rights for " + application);
    }

    protected String getRequestUri(final HttpServletRequest request) {
        String contextPath = request.getContextPath();
        return request.getRequestURI().substring(contextPath.length());
    }

}
