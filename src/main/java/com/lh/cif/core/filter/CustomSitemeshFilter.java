package com.lh.cif.core.filter;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomSitemeshFilter extends ConfigurableSiteMeshFilter {

	@Override
	protected void applyCustomConfiguration(final SiteMeshFilterBuilder builder) {

//		builder.addDecoratorPath("/cif/*", "/decorators/framework.jsp").addExcludedPath("*/api/*").addExcludedPath("*/search-table").addExcludedPath("*/manage-table").addExcludedPath("*/keywords/manage");
	}
}
