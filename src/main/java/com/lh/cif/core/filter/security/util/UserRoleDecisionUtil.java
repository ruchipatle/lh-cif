package com.lh.cif.core.filter.security.util;

import java.util.ArrayList;
import java.util.List;

import com.lh.cif.iam.domain.entity.OpcoLocalDetails;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserRoleDecisionUtil {

	public static String getUserRole(final List<String> userGroupEmailIds, final OpcoLocalDetails opcoDetails) {
		String role = "";
		List<String> adminGrouplist = opcoDetails.getAdminGroupName();
		List<String> approverGrouplist = opcoDetails.getApproverGroupName();
		List<String> ceGrouplist = opcoDetails.getCeGroupName();
		List<String> tempList = new ArrayList<String>();

		// retain only IMS related groups
		for (String group : userGroupEmailIds) {
			if (!group.trim().contains("ims") && !group.trim().contains("companygroup")) {
				tempList.add(group);
			}
		}
		userGroupEmailIds.removeAll(tempList);

		/*
		 * ADMIN and ADMIN-APPROVER check iterate through the user's IMS related groups
		 * only
		 */
		for (String userGroupEmail : userGroupEmailIds) {
			if (adminGrouplist.contains(userGroupEmail.trim())) {
				role = "ADMIN";
				for (String approverGroupName : approverGrouplist) {
					if (userGroupEmailIds.contains(approverGroupName)) {
						role = "ADMIN-APPROVER";
						break;
					}
				}
				break;
			}
		}
		/*
		 * CE and CE-APPROVER check iterate through the user's IMS related groups only
		 */
		if (role.equals("")) {
			for (String userGroupEmail : userGroupEmailIds) {
				if (ceGrouplist.contains(userGroupEmail.trim())) {
					role = "CE";
					for (String approverGroupName : approverGrouplist) {
						if (userGroupEmailIds.contains(approverGroupName)) {
							role = "CE-APPROVER";
							break;
						}
					}
					break;
				}
			}
		}
		/*
		 * APPROVER only check iterate through the user's IMS related groups only
		 */
		if (role.equals("")) {
			for (String userGroupEmail : userGroupEmailIds) {
				if (approverGrouplist.contains(userGroupEmail.trim())) {
					role = "APPROVER";
					break;
				}
			}
		}

		/*
		 * READER only check
		 */
		if (role.equals("")) {
			role = "READER";
		}
		return role;
	}

}
