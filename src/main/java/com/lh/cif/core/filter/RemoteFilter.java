package com.lh.cif.core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

//import com.lh.cif.core.util.RemoteApiUtil;

public class RemoteFilter implements Filter {

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		//
	}

	@Override
	public void init(final FilterConfig arg0) throws ServletException {
		//RemoteApiUtil.installRemoteOnAll();
	}

}
