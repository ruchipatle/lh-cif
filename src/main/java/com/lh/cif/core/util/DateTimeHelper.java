package com.lh.cif.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateTimeHelper {

	private static final int SECOND = 1000;

	private static final int MINUTE = 60 * SECOND;

	private static final int HOUR = 60 * MINUTE;

	private static final int DAY = 24 * HOUR;

	public static String calcDuration(long duration) {

		StringBuffer text = new StringBuffer("");
		if (duration > DAY) {
			text.append(duration / DAY).append(" days ");
			duration %= DAY;
		}
		if (duration > HOUR) {
			text.append(duration / HOUR).append(" hours ");
			duration %= HOUR;
		}
		if (duration > MINUTE) {
			text.append(duration / MINUTE).append(" minutes ");
			duration %= MINUTE;
		}
		if (duration > SECOND) {
			text.append(duration / SECOND).append(" seconds ");
			duration %= SECOND;
		}
		// text.append(duration + " ms");

		return text.toString();
	}

	public static Date calcDate(final String timeStamp) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		try {
			Date timestamp = simpleDateFormat.parse(timeStamp);
			return timestamp;
		} catch (ParseException e) {
			log.error("calcDate Exception :" + e.getMessage());
		}
		return null;

	}

	public static String getDateQuery(final String inputDate) {
		Pattern p = Pattern.compile("^[0-9 -]*$");
		String query = "";

		if (!p.matcher(inputDate).matches()) {
			return query;
		}

		String year = "";
		String month = "";
		String day = "";

		int index = inputDate.indexOf(":");
		String[] dateEntered;

		if (index < 0) {
			dateEntered = inputDate.trim().split("-");
		} else {
			dateEntered = inputDate.substring(0, index).trim().split("-");
		}

		int length = dateEntered.length;

		if (length == 3) {
			String tempYear = dateEntered[2];
			if (tempYear.length() > 3) {
				year = tempYear.substring(0, 4);
			} else {
				year = tempYear;
			}
			month = dateEntered[0];
			day = dateEntered[1];
		} else if (length == 2) {
			String tempYear = dateEntered[1];
			if (tempYear.length() == 3) {
				year = tempYear;
			} else if (tempYear.length() > 3) {
				year = tempYear.substring(0, 4);
			} else {
				day = tempYear;
			}
			month = dateEntered[0];
		} else if (length == 1) {
			String tempYear = dateEntered[0];
			if (tempYear.startsWith("0") || tempYear.startsWith("1")) {
				if (tempYear.length() > 2) {
					month = tempYear.substring(0, 2);
				} else {
					month = tempYear;
				}
			} else {
				if (tempYear.length() == 3) {
					year = tempYear;
				} else if (tempYear.length() > 3) {
					year = tempYear.substring(0, 4);
				} else {
					month = tempYear;
				}

			}
		}
		if (year.length() != 4) {
			query += "%" + year + "%-";
		} else {
			query += year + "-";
		}

		if (month.length() != 2) {
			query += "%" + month + "%-";
		} else {
			query += month + "-";
		}
		if (day.length() != 2) {
			query += "%" + day + "%";
		} else {
			query += day;
		}
		return query += "%";
	}

	public static Date getDate(String stringDate) {
		Date convertedDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			convertedDate = formatter.parse(stringDate);
			return convertedDate;
		} catch (ParseException e) {
			log.info("Error in date parser" + e.getMessage());
		}
		return null;
	}

	public static String getDateString(Date validTo) {
		String convertedDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			convertedDate = formatter.format(validTo);
			log.info(convertedDate);
			return convertedDate;
		} catch (Exception e) {
			log.info("Error in date parser" + e.getMessage());
		}
		return null;

	}

	public static String getDateString(Date date, String format) {
		String convertedDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		try {
			convertedDate = formatter.format(date);
			log.info(convertedDate);
			return convertedDate;
		} catch (Exception e) {
			log.info("Error in date parser" + e.getMessage());
		}
		return null;

	}
	// mehod to compare only dates excluding time
	public static boolean isSameDay(Date  d1, Date d2) {
		//	 log.info("New method called");
			 Calendar cal1 = Calendar.getInstance();
		        cal1.setTime(d1);
		        Calendar cal2 = Calendar.getInstance();
		        cal2.setTime(d2);
			 
		        if (cal1 == null || cal2 == null) {
		            throw new IllegalArgumentException("The dates must not be null");
		        }
		        return (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
		                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
		                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
		    }
}
