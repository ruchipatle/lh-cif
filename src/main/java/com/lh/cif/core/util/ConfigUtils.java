/*
 * This Class gives the instance of the config
 */

package com.lh.cif.core.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.appengine.api.NamespaceManager;
import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.cc.framework.config.HolcimConfigRetrievalException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigUtils {

    public static HolcimConfigHelper getDefaultConfigInstance() {

        String oldNameSpace = NamespaceManager.get();
        HolcimConfigHelper holcimConfigHelper = null;
        try {
            NamespaceManager.set("");
            holcimConfigHelper = HolcimConfigHelper.getInstance("DEFAULT_CONFIG_3.X");
        } catch (HolcimConfigRetrievalException e) {
            log.error(e.getMessage() + " " + Throwables.getStackTraceAsString(e));
        } finally {
            NamespaceManager.set(oldNameSpace);
        }

        return holcimConfigHelper;

    }

    public static HolcimConfigHelper getFileUploadConfigInstance() {

        String oldNameSpace = NamespaceManager.get();
        HolcimConfigHelper holcimConfigHelper = null;

        try {
            NamespaceManager.set("");
            holcimConfigHelper = HolcimConfigHelper.getInstance("FILE_UPLOAD_CONFIG_3.X");
        } catch (HolcimConfigRetrievalException e) {
            log.error(e.getMessage() + " " + Throwables.getStackTraceAsString(e));
        } finally {
            NamespaceManager.set(oldNameSpace);
        }
        return holcimConfigHelper;
    }

    public static HolcimConfigHelper getLocaleInstance(final String locale) {

        String oldNameSpace = NamespaceManager.get();
        HolcimConfigHelper holcimConfigHelper = null;
        try {
            NamespaceManager.set("");
            holcimConfigHelper = HolcimConfigHelper.getInstance("ALL_CONFIGS_3.X");
            holcimConfigHelper = HolcimConfigHelper.getInstance(holcimConfigHelper.getValue("CONFIGNAME_" + locale));
        } catch (HolcimConfigRetrievalException e) {
            log.error(e.getMessage() + " " + Throwables.getStackTraceAsString(e));
        } finally {
            NamespaceManager.set(oldNameSpace);
        }
        return holcimConfigHelper;
    }

    public static HolcimConfigHelper getAllConfigInstance() {
        String oldNameSpace = NamespaceManager.get();
        HolcimConfigHelper hAllConfigInstance = null;
        try {
            NamespaceManager.set("");
            hAllConfigInstance = HolcimConfigHelper.getInstance("ALL_CONFIGS_3.X");
        } catch (HolcimConfigRetrievalException e) {
            log.error("Exception in getting config details:::" + e.getMessage());
        } finally {
            NamespaceManager.set(oldNameSpace);
        }
        return hAllConfigInstance;
    }

    public static String getStackTrace(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

}
