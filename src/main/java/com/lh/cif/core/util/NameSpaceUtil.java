package com.lh.cif.core.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.lh.cif.core.filter.security.util.UserInfoUtil;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.domain.entity.MetaDataAccessControl;
import com.lh.cif.iam.service.IMSGroupService;
import com.lh.cif.iam.service.MetaDataAccessControlService;
import com.lh.cif.iam.service.OpcoLocaleService;
import com.lh.cif.iam.service.RoleVsActionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NameSpaceUtil {
	
	@Autowired
	OpcoLocaleService opcoLocaleService;

	@Autowired
	RoleVsActionService imsRoleVsActionService;
	
	@Autowired
	MetaDataAccessControlService restrictedMetaDataService ;
	
	@Autowired
	GsuiteManager gsuiteManager;
	@Autowired
	IMSGroupService googleGroupService;

	public String changeOpco (String pageURL,UserInfoSession userInfoSession , String selectedOpco , HttpServletRequest request ) {

		log.info("==============================================================================");
		log.info("pageURL" + pageURL);
		try {

			//UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");

			if (null != selectedOpco && null != userInfoSession.getCountryCodeLocaleMap().get(selectedOpco)) {

				String locale = "";
				String requestLocale = request.getLocale().getLanguage();
				if (userInfoSession.getCountryCodeLocaleMap().get(selectedOpco).containsValue(requestLocale)) {
					locale = requestLocale;
				} else {
					locale = userInfoSession.getCountryCodeLocaleMap().get(selectedOpco).get("defaultLocale");
				}

				// set country code
				userInfoSession.setCountryCode(selectedOpco);
				userInfoSession.setLocale(locale);

				// set role
				userInfoSession.setRole(userInfoSession.getOpcoRoleMap().get(selectedOpco));

				// get the actions allowed for the user
				List<String> userActions = imsRoleVsActionService.getActions(userInfoSession.getRole());
				userInfoSession.setActions(userActions);

				// IMS -Zimbabwe changes
				boolean isRestrictedOpco = opcoLocaleService.getOpcoRecordByCompany(userInfoSession.getCountryCode()).isMetaDataAccessControl();
				
				if(isRestrictedOpco) {
					log.info("Restriction is enabled for changed opco: "+userInfoSession.getCountryCode());
					userInfoSession.setRestricted(isRestrictedOpco);
					List<MetaDataAccessControl> restrictedMetaData = restrictedMetaDataService.getAllRestrictedMetaData(userInfoSession.getCountryCode());		
					List<String> userGroups = UserInfoUtil.getUserGroups(userInfoSession.getUserEmail(),gsuiteManager,googleGroupService);
					UserInfoUtil.setRestrictedMetaData(userInfoSession, userGroups , restrictedMetaData);
				 } else {
					log.info("Restriction not enabled for changed opco:: "+userInfoSession.getCountryCode());
					 userInfoSession.setRestricted(false);
					 userInfoSession.setRestrictedMetaData(null);
				 }		
				request.getSession().setAttribute("userInfoSession", userInfoSession);
				request.getSession().setAttribute("role", userInfoSession.getRole());

				if (pageURL.contains("/ims/view") || pageURL.contains("/ims/edit")) {
					return "OK_VIEW";
				} else {
					return "OK";
				}
			} else {
				return "OK";
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("set opco Exception :" + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
			return "NOT_OK";
		}
	
	}
}
