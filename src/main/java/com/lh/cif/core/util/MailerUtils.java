/*
 * This Class has methods to send an email
 */
package com.lh.cif.core.util;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.cc.framework.config.HolcimConfigRetrievalException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MailerUtils {

	public static void sendEmail(final String fromEmail, final String toEmail, final String subject, final String msgBody) {

		final Properties props = new Properties();
		final Session session = Session.getDefaultInstance(props, null);
		final MimeMessage msg = new MimeMessage(session);
		final Multipart multiPart = new MimeMultipart();
		final MimeBodyPart htmlPart = new MimeBodyPart();
		try {
			HolcimConfigHelper config = HolcimConfigHelper.getInstance();
			htmlPart.setContent(msgBody, "text/html");
			multiPart.addBodyPart(htmlPart);
			msg.setSubject(subject, "UTF-8");
			msg.setContent(multiPart);
			msg.setFrom(new InternetAddress(fromEmail, config.getValue("EMAIL_SENDER_NAME")));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail, ""));
			Transport.send(msg);
		} catch (IOException e) {
			log.error(e.getMessage());
			// TODO Auto-generated catch block
		} catch (MessagingException e) {
			log.error(e.getMessage());
			// TODO Auto-generated catch block
		} catch (HolcimConfigRetrievalException e) {
			// TODO Auto-generated catch block
			log.error("HolcimConfigRetrievalException ", e.getMessage());
		}

	}

	public static void sendEmailWithAttachment(final String fromEmail, final String fromName, final String userEmail, final String subject, final String msgBody, final byte[] attachData, final String attachMimeType,
			final String attachName) throws Exception {

		// Get system properties
		Properties properties = new Properties();
		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties, null);
		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(fromEmail, fromName));

		message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));
		message.setSubject(subject);

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();

		// Fill the message
		messageBodyPart.setText(msgBody);

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();

		DataSource source = new ByteArrayDataSource(attachData, attachMimeType);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(attachName);
		multipart.addBodyPart(messageBodyPart);

		message.setContent(multipart);
		Transport.send(message);
	}

}
