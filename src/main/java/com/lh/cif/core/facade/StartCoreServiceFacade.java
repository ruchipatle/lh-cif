//package com.lh.cif.core.facade;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class StartCoreServiceFacade {
//
//	@Autowired
//	private CoreServiceManagerFacade coreServiceManagerFacade;
//
//	public void start(Object object) {
//
//		coreServiceManagerFacade.assignVersionTask(object);
//		coreServiceManagerFacade.assignWorkflowTask(object);
//		coreServiceManagerFacade.assignRecordTask(object);
//		coreServiceManagerFacade.assignIndexTask(object);
//		coreServiceManagerFacade.assignSearchTagsTask(object);
//		coreServiceManagerFacade.assignAuditTask(object);
//		coreServiceManagerFacade.assignDriveTask(object);
//		coreServiceManagerFacade.assignNotificationTask(object);
//		coreServiceManagerFacade.assignCloudTask(object);
//		// acknowledge feature
//		coreServiceManagerFacade.assignAcknowledgeTask(object);
//		
//	}
//}
