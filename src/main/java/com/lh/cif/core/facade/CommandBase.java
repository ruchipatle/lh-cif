package com.lh.cif.core.facade;

/**
 * A command controller which should be implemented by all controllers
 */
public interface CommandBase {

	public Object execute(Object ob, String command);

	public void execute(String command);

	public boolean canHandle(String command);
}
