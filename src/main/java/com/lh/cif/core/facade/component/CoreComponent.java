//package com.lh.cif.core.facade.component;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//import com.holcim.cc.framework.config.HolcimConfigHelper;
//import com.lh.cif.record.domain.entity.IMSLinks;
//
//import lombok.Data;
//import lombok.ToString;
//
///**
// * Business Domain Object on which rules will be applied and final action will
// * be executed
// *
// * @author anbharti
// *
// */
//@Data
//@ToString
//public class CoreComponent {
//
//	private String status;
//
//	private double versionNumber;
//
//	private String userRole;
//
//	private Long id;
//
//	private String userAction;
//
//	private Date publishedDate;
//
//	private String createdBy;
//
//	private String modifiedBy;
//
//	private Date createdDate;
//
//	private Date modifiedDate;
//
//	private Date archiveDate;
//
//	private String archivedBy;
//
//	private String ownerId;
//
//	private String ownerName;
//
//	private String ownerOpco;
//
//	private Date nextReviewDate;
//
//	private Date validTo;
//
//	private String driveFileObject;
//
//	private List<String> department;
//
//	private List<String> section;
//
//	private List<String> folder;
//
//	private List<String> keywordTags;
//
//	private String comments;
//
//	private String auditComments;
//
//	private Map<String, List<String>> approvers;
//
//	private Map<String, Boolean> approvalFlag;
//
//	private List<String> distributionList;
//
//	private String finalApprovers;
//
//	private Date oldNextReviewDate;
//
//	private Date oldValidToDate;
//
//	private String oldOwnerId;
//
//	private List<IMSLinks> imsLinks;
//
//	private String locale;
//
//	private String countryCode;
//
//	private boolean rejected;
//
//	private double oldVersionNumber;
//
//	private Boolean isDriveUpdated;
//
//	private Boolean isDriveUpdatedArchive;
//
//	private Boolean isDriveUpdatedPublish;
//
//	private List<String> oldDepartment;
//
//	private List<String> oldSection;
//
//	private List<String> oldFolder;
//
//	private List<String> oldKeywordTags;
//	
//	private HolcimConfigHelper configLocale;
//	
//	//////minor version
//	private String commentsForSkip;
//	private boolean allowMinorVersion;
//	
//	// Restricted Record flag IMS Zimbabwe requirement
//	private Boolean isRestricted = false ;
//	
//	private boolean oldRestriction ; 
//	private boolean restrictionChanged ;
//
//}
