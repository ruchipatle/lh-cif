//package com.lh.cif.core.facade;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.lh.cif.acknowledge.process.AcknowledgeManager;
//import com.lh.cif.audit.process.AuditManager;
//import com.lh.cif.keywords.process.SearchTagsManager;
//import com.lh.cif.notification.NotificationManager;
//import com.lh.cif.record.process.RecordManager;
//import com.lh.cif.search.process.IndexManager;
//import com.lh.cif.storage.process.cloud.CloudManager;
//import com.lh.cif.storage.process.drive.DriveManager;
//import com.lh.cif.version.process.VersionManager;
//import com.lh.cif.workflow.process.WorkflowManager;
//
//import lombok.Data;
//import lombok.ToString;
//
//@Data
//@ToString
//@Service
//public class CoreServiceManagerFacade {
//
//	@Autowired
//	private DriveManager driveManager;
//
//	@Autowired
//	private RecordManager recordManager;
//
//	@Autowired
//	private IndexManager indexManager;
//
//	@Autowired
//	private AuditManager auditManager;
//
//	@Autowired
//	private NotificationManager notificationManager;
//
//	@Autowired
//	private VersionManager versionManager;
//
//	@Autowired
//	private WorkflowManager workflowManager;
//
//	@Autowired
//	private SearchTagsManager searchTagsManager;
//
//	@Autowired
//	private CloudManager cloudManager;
//	
//	@Autowired
//	private AcknowledgeManager acknowledgeManager ;
//
//	private boolean isDriveTaskComplete;
//
//	private boolean isRecordTaskComplete;
//
//	private boolean isAuditTaskComplete;
//
//	private boolean isIndexTaskComplete;
//
//	private boolean isVersionTaskComplete;
//
//	private boolean isWorkflowTaskComplete;
//
//	private boolean isNotificationTaskComplete;
//
//	private boolean isSearchTagsTaskComplete;
//
//	private boolean isCloudTaskComplete;
//
//	private boolean isAcknowledgeTaskComplete;
//	public CoreServiceManagerFacade() {
//
//		/*
//		 * this.driveManager = new DriveManager(); //this.recordManager = new
//		 * RecordManager(); this.indexManager = new IndexManager();
//		 * this.notificationManager = new NotificationManager(); this.versionManager =
//		 * new VersionManager(); this.workflowManager = new WorkflowManager();
//		 * this.auditManager = new AuditManager(); this.searchTagsManager = new
//		 * SearchTagsManager();
//		 */
//	}
//
//	public void assignDriveTask(Object ob) {
//		this.isDriveTaskComplete = this.driveManager.manageDrive(ob);
//	}
//
//	public void assignRecordTask(Object ob) {
//		this.isRecordTaskComplete = this.recordManager.manageRecord(ob);
//	}
//
//	public void assignIndexTask(Object ob) {
//		this.isIndexTaskComplete = this.indexManager.manageIndex(ob);
//	}
//
//	public void assignAuditTask(Object ob) {
//		this.isAuditTaskComplete = this.auditManager.manageAudit(ob);
//	}
//
//	public void assignVersionTask(Object ob) {
//		this.isVersionTaskComplete = this.versionManager.manageVersion(ob);
//	}
//
//	public void assignNotificationTask(Object ob) {
//		this.isNotificationTaskComplete = this.notificationManager.manageNotification(ob);
//	}
//
//	public void assignWorkflowTask(Object ob) {
//		this.isWorkflowTaskComplete = this.workflowManager.manageWorkflow(ob);
//	}
//
//	public void assignSearchTagsTask(Object ob) {
//		this.isSearchTagsTaskComplete = this.searchTagsManager.manageSearchTags(ob);
//	}
//
//	public void assignCloudTask(Object ob) {
//		this.isCloudTaskComplete = this.cloudManager.manageCloud(ob);
//	}
//	
//	public void assignAcknowledgeTask(Object ob) {
//		this.isAcknowledgeTaskComplete =  this.acknowledgeManager.manageAcknowledge(ob);
//	}
//}
