//package com.lh.cif.core.facade;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.lh.cif.admin.domain.entity.MasterData;
//import com.lh.cif.admin.service.MasterDataService;
//import com.lh.cif.constant.IMSCoreEngineUserActionConstants;
//import com.lh.cif.constant.IMSStatusConstants;
//import com.lh.cif.core.facade.component.CoreComponent;
//import com.lh.cif.core.util.ConfigUtils;
//import com.lh.cif.core.util.DateTimeHelper;
//import com.lh.cif.core.util.StringUtil;
//import com.lh.cif.record.domain.dto.ArchiveDto;
//import com.lh.cif.record.domain.dto.DirectPublishSDto;
//import com.lh.cif.record.domain.dto.SaveIMSRecordDto;
//import com.lh.cif.record.domain.dto.UpdateIMSRecord;
//import com.lh.cif.record.domain.entity.IMSLinks;
//import com.lh.cif.record.domain.entity.IMSRecord;
//import com.lh.cif.record.service.RecordService;
//import com.lh.cif.workflow.domain.dto.ApproveDto;
//import com.lh.cif.workflow.domain.dto.CancelWorkflowDto;
//import com.lh.cif.workflow.domain.dto.RejectDto;
//import com.lh.cif.workflow.domain.dto.SubmitApprovalDto;
//import com.lh.cif.workflow.domain.entity.SaveApproversData;
//import com.lh.cif.workflow.service.WorkflowService;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//@Service
//public class CoreComponentBuilderService {
//
//	@Autowired
//	MasterDataService masterDataService;
//
//	@Autowired
//	RecordService recordService;
//
//	@Autowired
//	WorkflowService workflowService;
//
//	/* save draft-initial */
//	public CoreComponent buildCoreEngineComponent(SaveIMSRecordDto imsRecordDTO) throws JSONException {
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setDepartment(StringUtil.splitToListValues(imsRecordDTO.getDeptVal()));
//		imsRecordComponent.setSection(getSectionMasterIds(StringUtil.splitToListValues(imsRecordDTO.getSectionVal())));
//		imsRecordComponent.setFolder(StringUtil.splitToListValues(imsRecordDTO.getFolderVal()));
//		imsRecordComponent.setOwnerOpco(imsRecordDTO.getImsOpCo());
//		imsRecordComponent.setNextReviewDate(DateTimeHelper.getDate(imsRecordDTO.getNextRevDate()));
//		imsRecordComponent.setValidTo(DateTimeHelper.getDate(imsRecordDTO.getValidToDate()));
//
//		List<IMSLinks> links = buildAttachmentObject(imsRecordDTO.getLinkJson());
//		imsRecordComponent.setImsLinks(links);
//		imsRecordComponent.setOwnerId(imsRecordDTO.getImsCreator());
//		imsRecordComponent.setOwnerName(imsRecordDTO.getImsOwner());
//		if (null != imsRecordDTO.getImsKeyVal()) {
//			imsRecordComponent.setKeywordTags(StringUtil.splitToListValues(imsRecordDTO.getImsKeyVal()));
//		}
//        imsRecordComponent.setCreatedBy(imsRecordDTO.getUserEmail());
//		imsRecordComponent.setCreatedDate(new Date());
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(imsRecordDTO.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(imsRecordDTO.getLocale());
//		imsRecordComponent.setCountryCode(imsRecordDTO.getCountryCode());
//		imsRecordComponent.setStatus(IMSStatusConstants.DRAFT_INITIAL);  //////////////minor version///////////////
//		log.info("skipped version"+imsRecordDTO.getComments());
//		if(imsRecordDTO.getComments()!=null){
//	    log.info("skipped version"+imsRecordDTO.getComments());
//	    imsRecordComponent.setCommentsForSkip(imsRecordDTO.getComments());
//	   }
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.SAVE_NEW_IMSRECORD);
//		imsRecordComponent.setIsDriveUpdated(false);
//		imsRecordComponent.setIsDriveUpdatedArchive(false);
//		imsRecordComponent.setIsDriveUpdatedPublish(false);
//		if (imsRecordDTO.getVersionNumber() > 1.0) {
//			if ((int) (long) imsRecordDTO.getImsRecordId()!=0){
//				imsRecordComponent.setId(imsRecordDTO.getImsRecordId());
//			}
//			imsRecordComponent.setVersionNumber(imsRecordDTO.getVersionNumber());
//		}
//		
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(imsRecordDTO.getLocale()));
//		
//		//IMS ZIMBABWE
//		imsRecordComponent.setIsRestricted(imsRecordDTO.isRestricted());
//		
//		return imsRecordComponent;
//	}
//
//	/* submitforapproval */
//	public CoreComponent buildCoreEngineComponent(SubmitApprovalDto submitApprovalDTO) {
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(submitApprovalDTO.getImsRecordId());
//		imsRecordComponent.setVersionNumber(submitApprovalDTO.getVersionNumber());
//		imsRecordComponent.setAuditComments(submitApprovalDTO.getComments());
//
//		Map<String, List<String>> apprMap = new HashMap<String, List<String>>();
//		Map<String, Boolean> apprFlag = new HashMap<String, Boolean>();
//		prepareApproversMap(submitApprovalDTO.getApproversIds(), apprMap, apprFlag);
//		imsRecordComponent.setApprovers(apprMap);
//		imsRecordComponent.setApprovalFlag(apprFlag);
//		if (null != submitApprovalDTO.getEmailIdList()) {
//			//imsRecordComponent.setDistributionList(prepareEmailList(submitApprovalDTO.getEmailIdList()));
//			imsRecordComponent.setDistributionList(prepareEmailList(submitApprovalDTO.getEmailIdList()));
//			
//		}
//
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(submitApprovalDTO.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(submitApprovalDTO.getLocale());
//		imsRecordComponent.setCountryCode(submitApprovalDTO.getCountryCode());
//		IMSRecord imsRecord = recordService.find(submitApprovalDTO.getImsRecordId());
//		List<IMSLinks> links = imsRecord.getImsLinks();
//		imsRecordComponent.setImsLinks(links);
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.SUBMIT_FOR_APPROVAL);
//
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(submitApprovalDTO.getLocale()));
//				
//		return imsRecordComponent;
//	}
//
//	/* directPublish */
//	public CoreComponent buildCoreEngineComponent(DirectPublishSDto directPublishDTO) {
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(directPublishDTO.getImsRecordId());
//		imsRecordComponent.setVersionNumber(directPublishDTO.getVersionNumber());
//		if (directPublishDTO.getComments() != null) {
//			imsRecordComponent.setComments(directPublishDTO.getComments());
//			imsRecordComponent.setAuditComments(directPublishDTO.getComments());
//		} else {
//			imsRecordComponent.setComments("");
//		}
//
//		if (null != directPublishDTO.getEmailIdList()) {
//			/*String[] distArray = directPublishDTO.getEmailIdList().split(",");
//			List<String> distList = new ArrayList<String>();
//			for (int i = 0; i < distArray.length; i++) {
//				distList.add(distArray[i].trim());
//			}*/
//
//			imsRecordComponent.setDistributionList(prepareEmailList(directPublishDTO.getEmailIdList()));
//		}
//
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(directPublishDTO.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(directPublishDTO.getLocale());
//		imsRecordComponent.setCountryCode(directPublishDTO.getCountryCode());
//		IMSRecord imsRecord = recordService.find(directPublishDTO.getImsRecordId());
//		List<IMSLinks> links = imsRecord.getImsLinks();
//		imsRecordComponent.setImsLinks(links);
//		imsRecordComponent.setIsRestricted(imsRecord.isRestricted());
//		log.debug("CORE COMPONENT BUILDER SERVICE :: --  corecomponent value:: -->> "+imsRecordComponent.getIsRestricted());
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.DIRECT_PUBLISHED);
//
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(directPublishDTO.getLocale()));
//
//		return imsRecordComponent;
//	}
//
//	private List<String> prepareEmailList(String emailIdList) {
//		List<String> emailIds = new ArrayList<String>();
//		String[] emailArray = emailIdList.split(",");
//		for (String email : emailArray) {
//			emailIds.add(email.trim());
//		}
//		return getOnlyEmailAddress(emailIds);
//	}
//
//	private void prepareApproversMap(String approversIds, Map<String, List<String>> apprMap, Map<String, Boolean> apprFlag) {
//		String[] approvers = approversIds.split("\\|");
//		int i = 1;
//		for (String approver : approvers) {
//			List<String> approverList = new ArrayList<String>();
//			String[] approverNames = approver.split(",");
//			for (String name : approverNames) {
//				approverList.add(name);
//			}
//
//			apprMap.put("approversLevel_" + i, approverList);
//			apprFlag.put("approvalFlag_" + i, false);
//
//			i++;
//		}
//
//	}
//
//	private List<String> getSectionMasterIds(List<String> splitToListValues) {
//		List<String> masterIds = new ArrayList<String>();
//		for (String val : splitToListValues) {
//			masterIds.add("" + masterDataService.getMasterIdByDisplayValue(val, "Section").getMasterId());
//		}
//		return masterIds;
//	}
//
//	public List<IMSLinks> buildAttachmentObject(String linkJson) {
//		
//
//		final List<IMSLinks> linkList = new ArrayList<IMSLinks>();
//		JSONObject linkJSON = null;
//		int id = 0;
//		IMSLinks imsLinks = new IMSLinks();
//		try {
//			final JSONArray linkJSONArray = new JSONArray(linkJson);
//			for (int j = 0; j < linkJSONArray.length(); j++) {
//				linkJSON = (JSONObject) linkJSONArray.get(j);
//				imsLinks = new IMSLinks();
//				id++;
//				imsLinks.setId(Long.parseLong(String.valueOf(id)));
//				imsLinks.setName(linkJSON.getString("Name"));
//				imsLinks.setUrl(linkJSON.getString("URL"));
//			//	9898
//				log.info("linkJSON::"+linkJSON);
//				log.info( "**************linkJSON is:"+linkJSON.toString()  );
//				//MasterData linkIDData = masterDataService.getMasterIdByDisplayValue(linkJSON.getString("Type"));
//				List<MasterData> linkIDDataList = masterDataService.getMasterRowsByDisplayValue(linkJSON.getString("Type"));
//				MasterData linkIDData = null;
//				
//				for (int i=0 ;i<linkIDDataList.size();i++) {
//					if ("Format".equalsIgnoreCase(linkIDDataList.get(i).getLookupType()) && 
//								linkJSON.getString("Type").equalsIgnoreCase(linkIDDataList.get(i).getDisplayValues()) ) {
//						linkIDData  = linkIDDataList.get(i);
//						break;
//					}
//				}
//				
//				log.info("String values: "+String.valueOf(linkIDData.getMasterId()));
//				imsLinks.setType(String.valueOf(linkIDData.getMasterId()));
//				if (linkJSON.getString("ID") != "" && !linkJSON.getString("ID").equalsIgnoreCase("")) {
//					imsLinks.setFileId(linkJSON.getString("ID"));
//				} else if (imsLinks.getUrl().contains("storage.cloud.google.com") && imsLinks.getUrl().contains("~")) {
//					imsLinks.setFileId("0");
//				} else {
//					imsLinks.setFileId("external");
//				}
//				linkList.add(imsLinks);
//			}
//		} catch (JSONException e) {
//			log.error(e.getMessage());
//		} catch (Exception e1) {
//			log.error(e1.getMessage());
//		}
//
//		return linkList;
//		
//		/*
//		final List<IMSLinks> linkList = new ArrayList<IMSLinks>();
//		JSONObject linkJSON = null;
//		int id = 0;
//		IMSLinks imsLinks = new IMSLinks();
//		try {
//			final JSONArray linkJSONArray = new JSONArray(linkJson);
//			for (int j = 0; j < linkJSONArray.length(); j++) {
//				linkJSON = (JSONObject) linkJSONArray.get(j);
//				imsLinks = new IMSLinks();
//				id++;
//				imsLinks.setId(Long.parseLong(String.valueOf(id)));
//				imsLinks.setName(linkJSON.getString("Name"));
//				imsLinks.setUrl(linkJSON.getString("URL"));
//				MasterData linkIDData = masterDataService.getMasterIdByDisplayValue(linkJSON.getString("Type"));
//				imsLinks.setType(String.valueOf(linkIDData.getMasterId()));
//				if (linkJSON.getString("ID") != "" && !linkJSON.getString("ID").equalsIgnoreCase("")) {
//					imsLinks.setFileId(linkJSON.getString("ID"));
//				} else if (imsLinks.getUrl().contains("storage.cloud.google.com") && imsLinks.getUrl().contains("~")) {
//					imsLinks.setFileId("0");
//				} else {
//					imsLinks.setFileId("external");
//				}
//				linkList.add(imsLinks);
//			}
//		} catch (JSONException e) {
//			log.error(e.getMessage());
//		} catch (Exception e1) {
//			log.error(e1.getMessage());
//		}
//
//		return linkList;
//	*/
//		}
//
//	public CoreComponent buildCoreEngineComponent(ApproveDto approvalDto) {
//
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(approvalDto.getImsRecordId());
//		imsRecordComponent.setAuditComments(approvalDto.getComments());
//
//		IMSRecord imsRecord = recordService.find(approvalDto.getImsRecordId());
//		Long imsRecordId = imsRecord.getId();
//		if (imsRecordId < 0) {
//			imsRecordId = imsRecordId * -1;
//		}
//		imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//		SaveApproversData saveApproversData = workflowService.getApproverByRecordIdAndVersionNumber(imsRecordId, imsRecord.getVersionNumber());
//
//		imsRecordComponent.setApprovers(saveApproversData.getApprovers());
//		imsRecordComponent.setApprovalFlag(saveApproversData.getApprovedFlag());
//		imsRecordComponent.setImsLinks(imsRecord.getImsLinks());
//		imsRecordComponent.setRejected(saveApproversData.isRejected());
//
//		Map<String, List<String>> apprMap = saveApproversData.getApprovers();
//		Map<String, Boolean> apprFlag = saveApproversData.getApprovedFlag();
//		for (int i = 1; i <= apprMap.size(); i++) {
//			if (apprFlag.get("approvalFlag_" + i) == false) {
//				if (apprMap.get("approversLevel_" + i).contains(approvalDto.getUserEmail())) {
//					if (apprMap.size() == i) {
//						imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.APPROVED_AT_LAST_LEVEL);
//						// setting restriction flag in Core Component when record is approved at last level
//						imsRecordComponent.setIsRestricted(imsRecord.isRestricted());
//						if (null != saveApproversData.getEmailIdList()) {
//							//imsRecordComponent.setDistributionList(saveApproversData.getEmailIdList());	
//							// this will correct old records as well if acted upon, can be cleaned 
//							imsRecordComponent.setDistributionList(getOnlyEmailAddress(saveApproversData.getEmailIdList()));	
//						}
//						break;
//					} else if (apprMap.size() > i) {
//						imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.APPROVED_AT_LEVEL_X);
//						break;
//					}
//				}
//			}
//		}
//		imsRecordComponent.setFinalApprovers(saveApproversData.getFinalApprovers());
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(approvalDto.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(approvalDto.getLocale());
//		imsRecordComponent.setCountryCode(approvalDto.getCountryCode());
//
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(approvalDto.getLocale()));
//
//		return imsRecordComponent;
//	}
//
//	public CoreComponent buildCoreEngineComponent(RejectDto rejectDto) {
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(rejectDto.getImsRecordId());
//		imsRecordComponent.setAuditComments(rejectDto.getComments());
//
//		IMSRecord imsRecord = recordService.find(rejectDto.getImsRecordId());
//		Long imsRecordId = imsRecord.getId();
//		if (imsRecordId < 0) {
//			imsRecordId = imsRecordId * -1;
//		}
//		imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//		SaveApproversData saveApproversData = workflowService.getApproverByRecordIdAndVersionNumber(imsRecordId, imsRecord.getVersionNumber());
//
//		imsRecordComponent.setApprovers(saveApproversData.getApprovers());
//		imsRecordComponent.setApprovalFlag(saveApproversData.getApprovedFlag());
//		imsRecordComponent.setImsLinks(imsRecord.getImsLinks());
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.REJECTED_AT_LEVEL_X);
//		imsRecordComponent.setRejected(saveApproversData.isRejected());
//
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(rejectDto.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(rejectDto.getLocale());
//		imsRecordComponent.setCountryCode(rejectDto.getCountryCode());
//
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(rejectDto.getLocale()));
//
//		return imsRecordComponent;
//	}
//
//	public CoreComponent buildCoreEngineComponent(ArchiveDto archiveDto) {
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(archiveDto.getImsRecordId());
//		imsRecordComponent.setAuditComments(archiveDto.getComments());
//		IMSRecord imsRecord = recordService.find(archiveDto.getImsRecordId());
//		imsRecordComponent.setOldValidToDate(imsRecord.getValidTo());
//		imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//		imsRecordComponent.setImsLinks(imsRecord.getImsLinks());
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.MANUALLY_ARCHIVED);
//		imsRecordComponent.setStatus(imsRecord.getStatus());
//
//		imsRecordComponent.setModifiedBy(archiveDto.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(archiveDto.getLocale());
//		imsRecordComponent.setCountryCode(archiveDto.getCountryCode());
//
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(archiveDto.getLocale()));
//		imsRecordComponent.setIsRestricted(imsRecord.isRestricted());
//		return imsRecordComponent;
//	}
//
//	public CoreComponent buildCoreEngineComponent(UpdateIMSRecord updateIMSRecordDTO) throws JSONException {
//		log.info("updateIMSRecordDTO============>" + updateIMSRecordDTO);
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(updateIMSRecordDTO.getImsRecordId());
//		imsRecordComponent.setAllowMinorVersion(updateIMSRecordDTO.isAllowMinorVersion());
//		imsRecordComponent.setOldDepartment(StringUtil.splitToListValues(updateIMSRecordDTO.getDeptVal()));
//	    imsRecordComponent.setOldSection(getSectionMasterIds(StringUtil.splitToListValues(updateIMSRecordDTO.getSectionVal())));
//	    imsRecordComponent.setOldFolder(StringUtil.splitToListValues(updateIMSRecordDTO.getFolderVal()));
//		imsRecordComponent.setOwnerOpco(updateIMSRecordDTO.getImsOpCo());
//		
//	/////for minor version commented
//	/*	imsRecordComponent.setNextReviewDate(DateTimeHelper.getDate(updateIMSRecordDTO.getNextRevDate()));
//		imsRecordComponent.setValidTo(DateTimeHelper.getDate(updateIMSRecordDTO.getValidToDate()));*/
//
//		List<IMSLinks> links = new ArrayList<IMSLinks>();
//		if (null != updateIMSRecordDTO.getLinkJson() && !updateIMSRecordDTO.getLinkJson().equals("")) {
//			links = buildAttachmentObject(updateIMSRecordDTO.getLinkJson());
//		}
//		imsRecordComponent.setImsLinks(links);
//		imsRecordComponent.setOwnerId(updateIMSRecordDTO.getImsCreator());
//		imsRecordComponent.setOwnerName(updateIMSRecordDTO.getImsOwner());
//		imsRecordComponent.setVersionNumber(updateIMSRecordDTO.getVersionNumber());
//		imsRecordComponent.setOldVersionNumber(updateIMSRecordDTO.getOldVersionNumber());
//		log.info("initial version"+updateIMSRecordDTO.getVersionNumber());
//		log.info("old version"+updateIMSRecordDTO.getOldVersionNumber());
//		log.info("version skip***************"+updateIMSRecordDTO.getCommentForSkip());
//		imsRecordComponent.setKeywordTags(StringUtil.splitToListValues(updateIMSRecordDTO.getImsKeyVal()));
//
//		// setGenericProperties(userInfoSession, imsRecordComponent);
//		imsRecordComponent.setModifiedBy(updateIMSRecordDTO.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(updateIMSRecordDTO.getLocale());
//		imsRecordComponent.setCountryCode(updateIMSRecordDTO.getCountryCode());
//		/* crucial for core engine */
//		 
//		if (updateIMSRecordDTO.getStatusVal().equals(IMSStatusConstants.DRAFT_INITIAL)) {
//			imsRecordComponent.setNextReviewDate(DateTimeHelper.getDate(updateIMSRecordDTO.getNextRevDate()));
//			imsRecordComponent.setValidTo(DateTimeHelper.getDate(updateIMSRecordDTO.getValidToDate()));
//			imsRecordComponent.setIsDriveUpdated(false);
//			imsRecordComponent.setIsDriveUpdatedArchive(false);
//			imsRecordComponent.setIsDriveUpdatedPublish(false);
//			//minor version
//			imsRecordComponent.setDepartment(StringUtil.splitToListValues(updateIMSRecordDTO.getDeptVal()));
//		    imsRecordComponent.setSection(getSectionMasterIds(StringUtil.splitToListValues(updateIMSRecordDTO.getSectionVal())));
//		    imsRecordComponent.setFolder(StringUtil.splitToListValues(updateIMSRecordDTO.getFolderVal()));
//			imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_AND_SAVE_DRAFT_INTITIAL_IMSRECORD);
//		} else if (updateIMSRecordDTO.getStatusVal().equals(IMSStatusConstants.DRAFT_UNDER_REVIEW)) {
//			imsRecordComponent.setNextReviewDate(DateTimeHelper.getDate(updateIMSRecordDTO.getNextRevDate()));
//			imsRecordComponent.setValidTo(DateTimeHelper.getDate(updateIMSRecordDTO.getValidToDate()));
//			imsRecordComponent.setIsDriveUpdated(false);
//			imsRecordComponent.setIsDriveUpdatedArchive(false);
//			imsRecordComponent.setIsDriveUpdatedPublish(false);
//			//minor version
//			imsRecordComponent.setDepartment(StringUtil.splitToListValues(updateIMSRecordDTO.getDeptVal()));
//		    imsRecordComponent.setSection(getSectionMasterIds(StringUtil.splitToListValues(updateIMSRecordDTO.getSectionVal())));
//		    imsRecordComponent.setFolder(StringUtil.splitToListValues(updateIMSRecordDTO.getFolderVal()));
//			if (updateIMSRecordDTO.getCommentForSkip()!= null) {
//			imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//		        log.info("skip comments**************"+updateIMSRecordDTO.getComments());
//		      }
//			imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.SAVE_DRAFT_UNDER_REVIEW);
//			
//		} else if (updateIMSRecordDTO.getStatusVal().equals(IMSStatusConstants.PUBLISHED)) {
//			
//			
//			IMSRecord imsRecord = recordService.find(updateIMSRecordDTO.getImsRecordId());
//			imsRecordComponent.setAuditComments(updateIMSRecordDTO.getComments());
//			imsRecordComponent.setNextReviewDate(DateTimeHelper.getDate(updateIMSRecordDTO.getNextRevDate()));
//			imsRecordComponent.setValidTo(DateTimeHelper.getDate(updateIMSRecordDTO.getValidToDate()));
//			log.info("ims record version"+imsRecord.getVersionNumber());
//			  int diff=Double.compare(imsRecordComponent.getVersionNumber(),imsRecord.getVersionNumber()); 
//			 
//			   log.info("difference**************"+diff);
//			   
//			   imsRecordComponent.setOldRestriction(imsRecord.isRestricted());
//			imsRecordComponent.setIsRestricted(updateIMSRecordDTO.isRestricted());
//			imsRecordComponent.setOldDepartment(imsRecord.getDepartment());
//			/* CHG0116569 - sprint 1 : allow metadata edit in published status */
//			
//			if (updateIMSRecordDTO.isMetaDataFlag()) {	
//				imsRecordComponent.setOldSection(imsRecord.getSection());
//				imsRecordComponent.setOldFolder(imsRecord.getFolder());
//				imsRecordComponent.setOldKeywordTags(imsRecord.getKeywordTags());
//				imsRecordComponent.setOldNextReviewDate(imsRecord.getNextReviewDate());
//				imsRecordComponent.setOldValidToDate(imsRecord.getValidTo());
//				imsRecordComponent.setOldOwnerId(imsRecord.getOwnerId());
//				//minor version/////
//				imsRecordComponent.setDepartment(StringUtil.splitToListValues(updateIMSRecordDTO.getDeptVal()));
//			    imsRecordComponent.setSection(getSectionMasterIds(StringUtil.splitToListValues(updateIMSRecordDTO.getSectionVal())));
//			    imsRecordComponent.setFolder(StringUtil.splitToListValues(updateIMSRecordDTO.getFolderVal()));
//			    boolean isMetaDataChanged = recordService.isMetaDataChanged(imsRecordComponent);
//				 //////////////minor version///////////////
//			   if(imsRecordComponent.isAllowMinorVersion()) {
//					log.info("minor version inside IF***********************" +imsRecordComponent.isAllowMinorVersion());
//					log.info("minor version inside IF***********************" +isMetaDataChanged);
//				if (isMetaDataChanged || updateIMSRecordDTO.getCommentForSkip()!=null) {
//					 log.info("meta data changed**************" + isMetaDataChanged);
//			    imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//				imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_METADATA);
//				  }
//				}
//				else {
//					log.info("minor version else ***********************" +imsRecordComponent.isAllowMinorVersion());
//					 imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_METADATA_NOTMINOR);
//					 imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//					
//				}
//				
//				// changed accidently but needed though check again please
//			} else if ((! DateTimeHelper.isSameDay(imsRecord.getNextReviewDate(), imsRecordComponent.getNextReviewDate()) || !DateTimeHelper.isSameDay( imsRecord.getValidTo(), imsRecordComponent.getValidTo()))
//					&& !imsRecord.getOwnerId().equals(imsRecordComponent.getOwnerId())) {
//				log.info("old version for action"+imsRecord.getVersionNumber());
//				
//				log.info("keywords are**********"+imsRecord.getKeywordTags());
//				imsRecordComponent.setOldVersionNumber(imsRecord.getVersionNumber());
//				
//				if(imsRecordComponent.isAllowMinorVersion()) {
//					log.info("minor version if ***********************" +imsRecordComponent.isAllowMinorVersion());
//					imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//					imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_MINOR_RECORD);
//					imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//					
//					}else {
//						log.info("minor version else ***********************" +imsRecordComponent.isAllowMinorVersion());
//						 imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_RECORD);
//						 imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//						
//					}
//                imsRecordComponent.setOldNextReviewDate(imsRecord.getNextReviewDate());
//				imsRecordComponent.setOldValidToDate(imsRecord.getValidTo());
//				imsRecordComponent.setOldOwnerId(imsRecord.getOwnerId());
//				//imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//				
//				//////for opco specific minor version//////
//				log.info("minor version ***********************" +imsRecordComponent.isAllowMinorVersion());
//			
//		
//		//	} else if (!imsRecord.getNextReviewDate().equals(imsRecordComponent.getNextReviewDate()) || !imsRecord.getValidTo().equals(imsRecordComponent.getValidTo())) {
//			
//			} else if (! DateTimeHelper.isSameDay(imsRecord.getNextReviewDate(), imsRecordComponent.getNextReviewDate()) || !DateTimeHelper.isSameDay( imsRecord.getValidTo(), imsRecordComponent.getValidTo())) {
//				
//				imsRecordComponent.setOldNextReviewDate(imsRecord.getNextReviewDate());
//				imsRecordComponent.setOldValidToDate(imsRecord.getValidTo());
//				
//				if(imsRecordComponent.isAllowMinorVersion()) {
//					imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//					imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//					imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_RECORD_DATES);
//					}else {
//						//added
//						 imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//						 imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//						 imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_RECORD_DATES_NOTMINOR);	
//					}
//			} else if (!imsRecord.getOwnerId().equals(imsRecordComponent.getOwnerId())) {
//			
//				
//				imsRecordComponent.setOldOwnerId(imsRecord.getOwnerId());
//				 imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//
//					if(imsRecordComponent.isAllowMinorVersion()) {
//						imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//						imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//						imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_RECORD_OWNERSHIP);
//						}else {
//							//added
//							 imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//							 imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//							 imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_PUBLISHED_RECORD_OWNERSHIP_NOTMINOR);
//						}
//			}
//			else if (!String.valueOf(imsRecord.getVersionNumber()).equals(String.valueOf(imsRecordComponent.getVersionNumber()))) {
//				log.info("audit comments*************"+updateIMSRecordDTO.getComments());
//				imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//				imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_ARCHIVED_RECORD_VERSION);
//				imsRecordComponent.setOldOwnerId(imsRecord.getOwnerId());
//
//				if(imsRecordComponent.isAllowMinorVersion()) {
//					
//					imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//					}
//                else {
//						 imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//					}
//			}
//	
//			// below is the case when  restriction was changed 
//			boolean isRestrictionChanged = false;
//			if(imsRecord.isRestricted()!=updateIMSRecordDTO.isRestricted()) {
//				log.debug("Restriction has been changed::--- "+imsRecord.isRestricted()+"  and new is:-- ");
//				imsRecordComponent.setRestrictionChanged(true);
//				isRestrictionChanged = true;
//			}
//			// below is the case when only restriction was changed 
//			if(imsRecordComponent.getUserAction()==null && isRestrictionChanged) {
//				log.info("Case where only restriction has been changed for a publushed record, old is  "+imsRecord.isRestricted()+ "  and new is: "+updateIMSRecordDTO.isRestricted());
//				imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.RESTRICTION_CHANGED);
//			}
//			
//			
//			
//		}
//		/////////////for minor version///////////////////
//		 else if (updateIMSRecordDTO.getStatusVal().equals(IMSStatusConstants.ARCHIVED)) {
//				IMSRecord imsRecord = recordService.find(updateIMSRecordDTO.getImsRecordId());
//				imsRecordComponent.setAuditComments(updateIMSRecordDTO.getComments());
//				imsRecordComponent.setNextReviewDate(imsRecord.getNextReviewDate());
//				imsRecordComponent.setValidTo(imsRecord.getValidTo());
//				imsRecordComponent.setDepartment(imsRecord.getDepartment());
//				log.info("archive ims record dept"+imsRecord.getDepartment());
//				log.info("archive ims record comments"+updateIMSRecordDTO.getComments());
//				log.info("archive ims record version"+imsRecord.getVersionNumber());
//				log.info("skip for archive comments"+updateIMSRecordDTO.getCommentForSkip());
//				imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_AND_SAVE_ARCHIVE_RECORD);
//			    imsRecordComponent.setOldVersionNumber(imsRecord.getVersionNumber());
//				log.info("ims record version"+imsRecord.getVersionNumber());
//				int diff=Double.compare(imsRecordComponent.getVersionNumber(),imsRecord.getVersionNumber()); 
//				 log.info("difference**************"+diff);
//				 // to be check
//				 if(diff!=0 &&(imsRecord.getVersionNumber()!=imsRecordComponent.getVersionNumber())||updateIMSRecordDTO.getCommentForSkip()!=null){
//					 imsRecordComponent.setCommentsForSkip(updateIMSRecordDTO.getCommentForSkip());
//					 imsRecordComponent.setKeywordTags(imsRecord.getKeywordTags());
//					 imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.EDIT_ARCHIVED_RECORD_VERSION);
//				 }
//		   }
//		//////////////////////////////////////////////////
//		// Added on 24.01.2018
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(updateIMSRecordDTO.getLocale()));
//		// IMS Zimbabwe
//		imsRecordComponent.setIsRestricted(updateIMSRecordDTO.isRestricted());
//		
//	    return imsRecordComponent;
//	}
//	
//	public CoreComponent buildCoreEngineComponent(CancelWorkflowDto cancelWorkflowDto) {
//
//		CoreComponent imsRecordComponent = new CoreComponent();
//		imsRecordComponent.setId(cancelWorkflowDto.getImsRecordId());
//		
//		
//		IMSRecord imsRecord = recordService.find(cancelWorkflowDto.getImsRecordId());
//		Long imsRecordId = imsRecord.getId();
//		if (imsRecordId < 0) {
//			imsRecordId = imsRecordId * -1;
//		}
//		imsRecordComponent.setVersionNumber(imsRecord.getVersionNumber());
//		SaveApproversData saveApproversData = workflowService.getApproverByRecordIdAndVersionNumber(imsRecordId, imsRecord.getVersionNumber());
//
//		imsRecordComponent.setApprovers(saveApproversData.getApprovers());
//		imsRecordComponent.setRejected(saveApproversData.isRejected());
//
//		/*Map<String, Boolean> apprFlag = saveApproversData.getApprovedFlag();
//		for (int i = 1; i <= apprFlag.size(); i++) {
//			apprFlag.put("approvalFlag_" + i, false);
//		}*/
//		imsRecordComponent.setModifiedBy(cancelWorkflowDto.getUserEmail());
//		imsRecordComponent.setModifiedDate(new Date());
//		imsRecordComponent.setLocale(cancelWorkflowDto.getLocale());
//		imsRecordComponent.setCountryCode(cancelWorkflowDto.getCountryCode());
//		imsRecordComponent.setApprovalFlag(saveApproversData.getApprovedFlag());
//		imsRecordComponent.setFinalApprovers(null);
//		imsRecordComponent.setAuditComments(cancelWorkflowDto.getComments());
//		imsRecordComponent.setStatus(IMSStatusConstants.DRAFT_UNDER_REVIEW);
//		imsRecordComponent.setUserAction(IMSCoreEngineUserActionConstants.CANCEL_WORKFLOW);
//		//setGenericProperties(userInfoSession, imsRecordComponent);
//		
//		imsRecordComponent.setConfigLocale(ConfigUtils.getLocaleInstance(cancelWorkflowDto.getLocale()));
//		
//		return imsRecordComponent;
//	}
//	
//	
//	/**
//	 * This method will return list of only email address like if Input has "SomeOne LastNme <someone@mail.com>", it will return "someone@mail.com".
//	 * @param distList
//	 * @return
//	 */
//	private List<String> getOnlyEmailAddress(List<String> distList){
//		String mixedEmail = distList.toString();
//		List<String> emails = new ArrayList<String>();
//		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(mixedEmail);
//	    while (m.find()) {
//	       emails.add(m.group());
//	    }
//		return emails;
//		
//	}
//	 
//}