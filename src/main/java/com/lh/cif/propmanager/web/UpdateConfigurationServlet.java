package com.lh.cif.propmanager.web;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.propmanager.config.PropManagerConfig;
import com.lh.cif.propmanager.service.SynchronizeSheets;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UpdateConfigurationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// launch cron
		// Read the google sheets
		try {
			this.updateConfigurationSheets(req, resp);
		} catch (HolcimGoogleCredentialRetrievalException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	private void updateConfigurationSheets(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, HolcimGoogleCredentialRetrievalException {
		log.info("PropConfigSpreadSheetId = " + PropManagerConfig.Keys.spreadSheetId.get());
		try {
			SynchronizeSheets ss = new SynchronizeSheets();
			ss.synchronize();
		} catch (GeneralSecurityException | URISyntaxException e) {
			log.error("Exception loading spreadsheet: " + e.getMessage());
			e.printStackTrace();
		}

		resp.getWriter().print("Job Completed Successfully");
	}
}
