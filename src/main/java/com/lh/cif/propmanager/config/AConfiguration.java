package com.lh.cif.propmanager.config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

import lombok.Data;

/**
 * Created by Ankit Bharti 11-June-2016
 */
@Data
public class AConfiguration {
	private Credential credential;

	private WorksheetEntry worksheetEntry;

	private SpreadsheetService spreadsheetService;

	private DatastoreService dataStoreService;

	private static int CONNECTION_TIMEOUT = 60 * 1000;

	private static Logger LOG = Logger.getLogger(AConfiguration.class.getName());

	public static String CONFIG_DATASTORE_KIND = "application_config";

	public AConfiguration() {
	}

	public AConfiguration(Credential credential, WorksheetEntry worksheetEntry) {
		this.credential = credential;
		this.worksheetEntry = worksheetEntry;
		spreadsheetService = new SpreadsheetService("CIF");
		spreadsheetService.setOAuth2Credentials(this.credential);
		spreadsheetService.setConnectTimeout(CONNECTION_TIMEOUT);
		dataStoreService = DatastoreServiceFactory.getDatastoreService();
	}

	public void synchronize() throws IOException, MalformedURLException, URISyntaxException, ServiceException {
		LOG.log(Level.FINE, "Method to implement");
	}

}
