package com.lh.cif.propmanager.config;

import java.util.logging.Logger;

import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.common.collect.ImmutableSet;
import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.cc.framework.config.HolcimConfigRetrievalException;

/**
 * Created by Ankit Bharti 11-June-2016
 */
public class PropManagerConfig {

	private static Logger LOG = Logger.getLogger(PropManagerConfig.class.getName());

	private HolcimConfigHelper holcimConfigHelper;

	private static PropManagerConfig config = null;

	public PropManagerConfig() {
		try {
			holcimConfigHelper = HolcimConfigHelper.getInstance("PROP_MANAGER_CONFIG");
		} catch (HolcimConfigRetrievalException e) {
			LOG.severe(e.getMessage() + " " + Throwables.getStackTraceAsString(e));
		}
	}

	public static PropManagerConfig getConfig() {
		return getConfig(false);
	}

	public static PropManagerConfig getConfig(Boolean force) {
		if (config != null && force == false)
			return config;
		return new PropManagerConfig();
	}

	public enum Keys {
		spreadSheetId, driveUser;

		public String get() {
			return getConfig().holcimConfigHelper.getValue(this.name());
		}

		public boolean isTrue() {
			return getConfig().holcimConfigHelper.getValue(this.name()).equals("true");
		}

		public Integer getInt() {
			return Integer.parseInt(getConfig().holcimConfigHelper.getValue(this.name()));
		}

		public ImmutableSet<String> getSet() {
			return ImmutableSet.copyOf(getConfig().holcimConfigHelper.getValue(this.name()).split(","));
		}
	}

}
