package com.lh.cif.propmanager.service;

/**
 * Created by Ankit Bharti 11-June-2016
 */
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import com.lh.cif.propmanager.config.AConfiguration;

public class SyncWorkerTask extends AConfiguration {

	private static Logger LOG = Logger.getLogger(SyncWorkerTask.class.getName());

	public SyncWorkerTask(Credential credential, WorksheetEntry worksheetEntry) {
		super(credential, worksheetEntry);
	}

	@Override
	public void synchronize() throws IOException, MalformedURLException, URISyntaxException, ServiceException {

		URL cellFeedUrl = new URI(this.getWorksheetEntry().getCellFeedUrl().toString() + "?min-row=2&min-col=1&max-col=2").toURL();

		CellFeed cellFeed = this.getSpreadsheetService().getFeed(cellFeedUrl, CellFeed.class);
		List<CellEntry> cellEntries = cellFeed.getEntries();

		String worksheetTitle = this.getWorksheetEntry().getTitle().getPlainText();

		if (cellEntries.size() > 0) {

			Key parentKey = KeyFactory.createKey(AConfiguration.CONFIG_DATASTORE_KIND, worksheetTitle);

			Entity en = new Entity(parentKey); // Holcim Commons
												// application_config

			// Create Properties
			for (int i = 0; i < cellEntries.size(); i = i + 2) {
				String configKey = cellEntries.get(i).getCell().getValue();
				String configValue = cellEntries.get(i + 1).getCell().getValue();
				if (configValue.length() > 499) {
					// use Text because String has limit 1500 bytes
					Text text = new Text(configValue);
					en.setProperty(configKey, text);
				} else {
					en.setUnindexedProperty(configKey, configValue);
				}

			}

			this.getDataStoreService().put(en);

			LOG.info(String.format("Worksheet Config %s sync completed", worksheetTitle));

		}
	}
}
