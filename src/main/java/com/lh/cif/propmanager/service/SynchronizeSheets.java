package com.lh.cif.propmanager.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;

import com.google.api.client.auth.oauth2.Credential;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.gsuite.auth.OAuth2Service;
import com.lh.cif.gsuite.spreadsheet.SpreadsheetReader;
import com.lh.cif.propmanager.config.PropManagerConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SynchronizeSheets {

	private Credential credential;

	public SynchronizeSheets() throws GeneralSecurityException, IOException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		SpreadsheetService service = new SpreadsheetService("CIF");
		credential = OAuth2Service.getCredential(PropManagerConfig.Keys.driveUser.get());
		service.setOAuth2Credentials(this.credential);
		service.setProtocolVersion(SpreadsheetService.Versions.V3);
	}

	public void synchronize() {
		try {
			this.synchronizeGlobalSettings();
		} catch (ServiceException | IOException se) {
			log.error(se.getMessage());
		}
	}

	public void synchronizeGlobalSettings() throws ServiceException, IOException {
		String id = PropManagerConfig.Keys.spreadSheetId.get();
		SpreadsheetReader ssr = new SpreadsheetReader(this.credential, id);

		log.info("Global settings file id: " + id);
		try {
			for (WorksheetEntry wse : ssr.listSheets()) {
				// Heavy Job will scroll through all sheets and process Sync
				SyncWorkerTask toct = new SyncWorkerTask(this.credential, wse);
				toct.synchronize();
			}

			MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
			memcache.clearAll();
			log.info(String.format("All Worksheet sync completed. Flushing Memcache"));

		} catch (Exception e) {
			log.error("Exception while syncing configuration sheets: " + e.getMessage());
			throw new ServiceException(e);
		}
	}

}
