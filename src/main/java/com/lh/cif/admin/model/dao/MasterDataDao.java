package com.lh.cif.admin.model.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.core.repository.GenericDao;

public interface MasterDataDao extends GenericDao<MasterData> {
    
    public List<MasterData> findMaxMasterId();

	public Map<String, MasterData> getMasterDataByIdsInBulk(Set<String> allIds);

	public Map<String, MasterData> getMasterDataByIdsInBulkMap(final Set<String> allIds);

	@Override
	public MasterData find(String id);

	public Collection<MasterData> findAllByIdName(List<String> ids);

}
