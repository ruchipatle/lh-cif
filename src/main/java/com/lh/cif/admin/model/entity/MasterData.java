package com.lh.cif.admin.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.Cacheable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

@ToString
@Entity
@Cache
@Cacheable
@Data
public class MasterData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Index
    private Long id;

    @Index
    private int lookupId = 0;

    @Index
    private String lookupType = null;

    @Index
    private String displayValue = null;

    @Index
    private int sortOrder = 0;

    private String createdBy = null;

    private Date createdDate = null;

    private String modifiedBy = null;

    private Date modifiedDate = null;

    public int compare(final MasterData m1, final MasterData m2) {
        int compare = (m1.getSortOrder() > m2.getSortOrder()) ? 1 : 0;
        if (compare == 0) {
            compare = (m1.getSortOrder() == m2.getSortOrder()) ? 0 : -1;
        }
        return compare;
    }

}
