package com.lh.cif.admin.model.dao;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class MasterDataDTO {
	private String id;
	private String lookupId;
	private String lookupType;
	private String displayValue;
	private Integer sortOrder;
	private String createdBy;
	private String modifiedBy;
	private Date createdDate;
	private Date modifiedDate;
}