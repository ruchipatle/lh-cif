package com.lh.cif.admin.model.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.QueryResultIterator;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.gsuite.datastore.DatastoreWorker;

@Repository
public class MasterDataDaoImpl implements MasterDataDao {

	private DatastoreWorker<MasterData> masterData;

	public MasterDataDaoImpl() {
		super();
		this.masterData = new DatastoreWorker<>(MasterData.class);
	}

	@Override
	public MasterData saveOrUpdate(MasterData newInstance) {
		return this.masterData.save(newInstance);
	}
	
	public List<MasterData> findMaxMasterId(){
	    return this.masterData.getBy("-id", 1);
	}

	@Override
	public MasterData find(Long id) {
		return this.masterData.get(id);
	}

	@Override
	public void delete(Long id) {
	    this.masterData.deleteById(id);
	}

	@Override
	public List<MasterData> findAll() {
		return this.masterData.list();
	}

	@Override
	public List<MasterData> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.masterData.list(columnValueMap);
	}

	@Override
	public Map<String, MasterData> getMasterDataByIdsInBulk(Set<String> allIds) {
		return (Map<String, MasterData>) masterData.getAll(allIds);
	}

	@Override
	public QueryResultIterator<MasterData> findByColumnAndValuePaginated(Map<String, Object> columnValueMap, Integer range, String startPosition) {
		return this.masterData.listEntitiesPaginated(columnValueMap, range, startPosition);
	}

	@Override
	public MasterData find(final String id) {
		return this.masterData.get(id);
	}

	@Override
	public Map<String, MasterData> getMasterDataByIdsInBulkMap(final Set<String> allIds) {
		return this.masterData.getAllMap(allIds);
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public MasterData getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.masterData.get(columnValueMap);
	}

	@Override
	public List<MasterData> findAll(List<Long> ids) {
		return (List<MasterData>) this.masterData.getAllByIds(ids);
	}
	
	@Override
	public Collection<MasterData> findAllByIdName(List<String> ids){
		return this.masterData.getAllByIdNames(ids);
	}
	
	

}
