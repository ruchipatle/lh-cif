package com.lh.cif.admin.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.dao.MasterDataDTO;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.core.domain.dto.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/cif/api/admin")
public class AdminRest {

    @Autowired
    MasterDataService masterDataService;

    /*
     * gets data from UI and saves the record in DB
     */
    @RequestMapping(value = "/masterdata", method = RequestMethod.POST)
    public Response saveMasterRecord(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @RequestBody MasterDataDTO masterDataUI,
            Model map) {

        NamespaceManager.set("CIF");

        boolean flag = masterDataService.saveMasterData(masterDataUI);
        Response resp = new Response();
        if (flag) {
            resp.setMessage("SUCCESS");
            resp.setStatus(HttpStatus.OK);
            resp.setFlag(true);
        } else {
            resp.setFlag(false);
            resp.setMessage("FAIL");
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }

}
