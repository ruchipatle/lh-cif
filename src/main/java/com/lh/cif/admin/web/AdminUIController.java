package com.lh.cif.admin.web;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/cif/admin")
public class AdminUIController {

    @Autowired
    MasterDataService masterDataService;

    /*
     * redirects to the home page of masterdata with all master data listed in 
     */
    @RequestMapping(value = "/masterdata/manage", method = RequestMethod.GET)
    public String goToMasterDataCreationUI(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, Model map) {

        NamespaceManager.set("CIF");

        // load the master data UI with all the master data
        List<MasterData> masterDataList = masterDataService.getAllData();
        Set<MasterData> masterDataSet = new LinkedHashSet<MasterData>(masterDataList);

        map.addAttribute("masterDataList", masterDataSet);

        return "/WEB-INF/views/admin/masterdata/manage.jsp";
    }

    /*
     * redirects user to create UI, where he can create new meta data
     */
    @RequestMapping(value = "/masterdata/create", method = RequestMethod.GET)
    public String createMasterDataUI(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, Model map) {

        NamespaceManager.set("CIF");

        final Map<String, Integer> lookTypeMap = masterDataService.getLookupIdMap();

        // create empty object of masterdata
        map.addAttribute("action", "CREATE");
        map.addAttribute("lookTypeList", lookTypeMap.keySet());

        return "/WEB-INF/views/admin/masterdata/create.jsp";
    }

    /*
     * user is redirected to create UI again with pre loaded data of the record for
     * edit purpose. get record specific details and load in the request
     */
    @RequestMapping(value = "/masterdata/{masterRecordId}", params = { "mode=edit" }, method = RequestMethod.GET)
    public String editMasterData(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @PathVariable("masterRecordId") Long id,
            Model map) {

        NamespaceManager.set("CIF");

        MasterData masterRecord = masterDataService.find(id);

        map.addAttribute("action", "EDIT");
        map.addAttribute("masterRecord", masterRecord);

        return "/WEB-INF/views/admin/masterdata/create.jsp";
    }

}