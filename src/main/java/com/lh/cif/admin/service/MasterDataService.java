package com.lh.cif.admin.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lh.cif.admin.model.dao.MasterDataDTO;
import com.lh.cif.admin.model.dao.MasterDataDao;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.core.domain.dto.GenericRequestDto;
import com.lh.cif.core.util.ConfigUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MasterDataService {

    @Autowired
    private MasterDataDao masterDataDao;

    public static final String LOOKUP_TYPE_MAIN = "Main";
    public static final String LOOKUP_TYPE_PLANT = "Plant";
    public static final String LOOKUP_TYPE_SECTION = "Section";
    public static final String LOOKUP_TYPE_KEY_TOPIC = "Key Topic";
    public static final String LOOKUP_TYPE_TAG = "Tag";
    public static final String LOOKUP_TYPE_DOC_TYPE = "Document Type";
    public static final String LOOKUP_TYPE_DATASOURCE = "DataSource";

    public List<MasterData> getMasterDataByLookupType(final String lookupType) {

        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("lookupType", lookupType);

        return sortList(this.masterDataDao.getResultListfindByColumnAndValue(columnValueMap));
    }

    public List<MasterData> getMasterDataByUI(final String lookupType, final String locale, final String targetUIAction) {
        return sortList(this.masterDataDao.getResultListfindByColumnAndValue(prepareMapByAction(lookupType)));
    }

    public Map<String, Integer> getLookupIdMap() {
        Map<String, Integer> columnValueMap = new HashMap<>();
        columnValueMap.put(LOOKUP_TYPE_MAIN, 0);
        columnValueMap.put(LOOKUP_TYPE_PLANT, 1);
        columnValueMap.put(LOOKUP_TYPE_SECTION, 2);
        columnValueMap.put(LOOKUP_TYPE_KEY_TOPIC, 3);
        columnValueMap.put(LOOKUP_TYPE_TAG, 4);
        columnValueMap.put(LOOKUP_TYPE_DOC_TYPE, 5);
        columnValueMap.put(LOOKUP_TYPE_DATASOURCE, 6);
        return columnValueMap;
    }

    private Integer getLookupId(final String lookupType) {
        Map<String, Integer> columnValueMap = getLookupIdMap();
        return columnValueMap.get(lookupType);
    }

    public Long findMaxMasterId() {
        Long maxMasterId = 0L;
        List<MasterData> masterDataList = masterDataDao.findMaxMasterId();
        if (masterDataList == null || masterDataList.isEmpty()) {
            maxMasterId = 1L;
        } else {
            maxMasterId = masterDataList.get(0).getId() + 1;
        }
        return maxMasterId;
    }

    public List<MasterData> getMasterDataByLookUpID(final String lookupType, final String locale, final String targetUIAction, final int lookupId) {
        Map<String, Object> columnValueMap = prepareMapByAction(lookupType);
        columnValueMap.put("lookupId", lookupId);
        return sortList(this.masterDataDao.getResultListfindByColumnAndValue(columnValueMap));
    }

    public List<MasterData> getDisplayValueByMasterId(final int id) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("masterId", id);
        return this.masterDataDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    public MasterData getMasterIdByDisplayValue(final String displayValue) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("displayValues", displayValue);
        return this.masterDataDao.getSingleResultfindByColumnAndValue(columnValueMap);
    }

    public MasterData getMasterIdByDisplayValue(final String displayValue, final String lookupType) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("displayValues", displayValue);
        columnValueMap.put("lookupType", lookupType);
        return this.masterDataDao.getSingleResultfindByColumnAndValue(columnValueMap);
    }

    private Map<String, Object> prepareMapByAction(final String lookupType) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("lookupType", lookupType);
        return columnValueMap;
    }

    private List<MasterData> sortList(final List<MasterData> masterData) {
        Collections.sort(masterData, new Comparator<MasterData>() {
            @Override
            public int compare(final MasterData md1, final MasterData md2) {
                return md1.getSortOrder() - md2.getSortOrder();
            }
        });
        return masterData;
    }

    public Map<String, MasterData> getMasterDataByIdsInBulk(Set<String> allIds) {
        return this.masterDataDao.getMasterDataByIdsInBulk(allIds);
    }

    public MasterData find(final Long id) {
        return this.masterDataDao.find(id);
    }

    public void delete(final Long id) {
        this.masterDataDao.delete(id);
    }

    public void delete(final String id) {
        this.masterDataDao.delete(id);
    }

    public MasterData find(final String id) {
        return this.masterDataDao.find(id);
    }

    public Map<String, MasterData> getMasterDataByIdsInBulkMap(final Set<String> allIds) {
        return this.masterDataDao.getMasterDataByIdsInBulkMap(allIds);
    }

    public List<MasterData> getAllData() {
        return masterDataDao.findAll();
    }

    public List<MasterData> getListManageUI(String lookupType) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("lookupType", lookupType);
        return masterDataDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    public List<MasterData> getDisplayValueByMasterId(int values, String locale) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("masterId", values);
        columnValueMap.put("locale", locale);
        return masterDataDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    public boolean saveMasterData(MasterDataDTO masterDataUI) {
        MasterData masterData = createMaterDataObject(masterDataUI, "test.user@dev.lafargeholcim.com");// , email, locale);
        masterDataDao.saveOrUpdate(masterData);
        return true;
    }

    private MasterData createMaterDataObject(MasterDataDTO masterDataUI, String email) {
        MasterData masterData;

        Log.info("ID Coming from UI " + masterDataUI.getId() + ", " + masterDataUI);

        if (masterDataUI.getId() != null && !"".equals(masterDataUI.getId())) {
            masterData = find(Long.parseLong(masterDataUI.getId()));
        } else {
            masterData = new MasterData();
        }

        Log.info("Obj Coming from UI " + masterData);

        masterData.setCreatedBy(email);
        if (null != masterDataUI.getCreatedDate()) {
            masterData.setCreatedDate(masterDataUI.getCreatedDate());
        } else {
            masterData.setCreatedDate(new Date());
        }

        masterData.setModifiedBy(email);
        if (null != masterDataUI.getModifiedDate()) {
            masterData.setModifiedDate(masterDataUI.getModifiedDate());
        } else {
            masterData.setModifiedDate(new Date());
        }

        masterData.setSortOrder(masterDataUI.getSortOrder());
        masterData.setDisplayValue(masterDataUI.getDisplayValue());

        masterData.setLookupType(masterDataUI.getLookupType());
        if (masterDataUI.getLookupId() == null || "".equals(masterDataUI.getLookupId())) {
            masterData.setLookupId(getLookupId(masterDataUI.getLookupType()));
        } else {
            masterData.setLookupId(Integer.parseInt(masterDataUI.getLookupId()));
        }

        return masterData;
    }

    /**
     * Creates the MasterData object and saves the data to datastore
     * 
     * @param displayValue
     * @param lookupType
     * @param email
     * 
     * @return the saved MasterData
     */
    public MasterData createAndSaveMaterData(String displayValue, String lookupType, String email) {

        MasterData masterData = getMasterIdByDisplayValue(displayValue, lookupType);
        if (masterData == null) {
            masterData = new MasterData();

            masterData.setCreatedBy(email);
            masterData.setCreatedDate(new Date());
            masterData.setSortOrder(0);
            masterData.setDisplayValue(displayValue);
            masterData.setLookupType(lookupType);
            masterData.setLookupId(getLookupId(lookupType));

            return masterDataDao.saveOrUpdate(masterData);
        } else {
            return masterData;
        }
    }

    /**
     * Creates the MasterData object and saves the data to datastore
     * 
     * @param displayValue
     * @param lookupType
     * @param email
     * 
     * @return the saved MasterData
     */
    public MasterData createMasterData(String displayValue, String lookupType, String email) {
        try {
            MasterData masterData = new MasterData();

            masterData.setCreatedBy(email);
            masterData.setCreatedDate(new Date());
            masterData.setSortOrder(0);
            masterData.setDisplayValue(displayValue);
            masterData.setLookupType(lookupType);
            masterData.setLookupId(getLookupId(lookupType));

            return masterDataDao.saveOrUpdate(masterData);
        } catch (Exception e) {
            log.error("Exception while inserting master data (Lookup Type : " + lookupType + ", Display Value : " + displayValue);
            log.error(ConfigUtils.getStackTrace(e));
            return null;
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map getJsonOtherTypes(GenericRequestDto genericRequestDto, Map<String, Map<String, String>> countryCodeLocaleMap) {
        int counter = 0;
        String maxSortOrder = "";
        for (String locale : countryCodeLocaleMap.get(genericRequestDto.getCountryCode()).keySet()) {
            if (!locale.equalsIgnoreCase("defaultLocale")) {
                List<Integer> sortList = new ArrayList<Integer>();
                sortList = masterRecordSortOrderList(genericRequestDto.getInput());
                if (sortList.size() > 0) {
                    Collections.sort(sortList, Collections.reverseOrder());
                    // sort order always a multiple of 10
                    maxSortOrder = maxSortOrder + "_" + String.valueOf(sortList.get(0) + (10 - (sortList.get(0) % 10)));
                } else {
                    maxSortOrder = maxSortOrder + "_" + "10"; // initial sort id when no data is present
                }
            }
        }
//        counter = getCounter(genericRequestDto.getLocale());
        // JSONObject obj = new JSONObject();
        Map obj = new HashMap();
        obj.put("counter", counter); // master id will be +1 of its previous data (max master id)
        obj.put("maxSortOrder", maxSortOrder); // gets the max sort id
        return obj;
    }

    public List<Integer> masterRecordSortOrderList(String lookupType) {
        List<MasterData> masterDataList = getListManageUI(lookupType);
        List<Integer> sortList = new ArrayList<Integer>();
        for (MasterData master : masterDataList) {
            sortList.add(master.getSortOrder());
        }
        return sortList;
    }

//    private int getCounter(String locale) {
//        int counter;
//        List<MasterData> masterIdLocaleFilterList = getRecordsLocaleWise(locale);
//        List<Integer> masterIdList = new ArrayList<Integer>();
//        for (int i = 0; i < masterIdLocaleFilterList.size(); i++) {
//            int MasterId = masterIdLocaleFilterList.get(i).getMasterId();
//            masterIdList.add(MasterId);
//        }
//        Collections.sort(masterIdList, Collections.reverseOrder());
//        if (masterIdList == null || masterIdList.isEmpty()) {
//            counter = 1;
//        } else {
//            counter = masterIdList.get(0) + 1;
//        }
//        return counter;
//    }

    private List<MasterData> getRecordsLocaleWise(String locale) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("locale", locale);
        return masterDataDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })

    public Map getJsonForSection(GenericRequestDto genericRequestDto, Map<String, Map<String, String>> countryCodeLocaleMap) {

        String maxSortOrder = "";
        int counter = 0;
        for (String locale : countryCodeLocaleMap.get(genericRequestDto.getCountryCode()).keySet()) {
            if (!locale.equalsIgnoreCase("defaultLocale")) {
                List<Integer> sortList = new ArrayList<Integer>();
                sortList = masterRecordSortOrderList("Folder");
                if (sortList.size() > 0) {
                    Collections.sort(sortList, Collections.reverseOrder());
                    maxSortOrder = maxSortOrder + "_" + String.valueOf(sortList.get(0) + (10 - (sortList.get(0) % 10))); // sort
                                                                                                                         // id
                                                                                                                         // is
                                                                                                                         // a
                                                                                                                         // multiple
                                                                                                                         // of
                                                                                                                         // 10
                } else {
                    maxSortOrder = maxSortOrder + "_" + "10";
                }
            }
        }

//        counter = getCounter(genericRequestDto.getLocale());

        // int lookup =
        // getMasterIdByDisplayValue(genericRequestDto.getInput()).getMasterId();
//        int lookup = getMasterIdByDisplayValue(genericRequestDto.getInput(), "Section").getMasterId();

        // JSONObject obj = new JSONObject();
        Map obj = new HashMap();
        obj.put("counter", counter);
//        obj.put("lookup", lookup);
        obj.put("maxSortOrder", maxSortOrder);
        return obj;
    }

//	public List<IMSLinks> getDisplayValueOfLinks(List<IMSLinks> links, String locale) {
//		List<IMSLinks> newLinkList = new ArrayList<IMSLinks>();
//		for (IMSLinks link : links) {
//			IMSLinks newLink = new IMSLinks();
//			newLink.setFileId(link.getFileId());
//			newLink.setId(link.getId());
//			newLink.setName(link.getName());
//			List<MasterData> masterData = getDisplayValueByMasterId(Integer.parseInt(link.getType()), locale);
//			newLink.setType(masterData.get(0).getDisplayValues());
//			newLink.setUrl(link.getUrl());
//			newLinkList.add(newLink);
//		}
//		return newLinkList;
//	}

    public List<String> getMasterDataValues(List<String> ids, String locale) {
        List<String> values = new ArrayList<String>();
        for (String s : ids) {
            List<MasterData> masterDataList = getDisplayValueByMasterId(Integer.parseInt(s), locale);
            for (MasterData masterData : masterDataList) {
                values.add("\"" + masterData.getDisplayValue() + "\"");
            }
        }
        return values;
    }

    public List<MasterData> getMasterRowsByDisplayValue(String displayValue) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("displayValues", displayValue);
        return this.masterDataDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    public String getMasterIdArrayFromDisplay(String displayValues, String lookUpType) {
        String displayValuesArr[] = displayValues.split("\\|");
        List<Integer> list = new ArrayList<Integer>();
        for (String id : displayValuesArr) {
            list.add(getMasterIdByDisplayValue(id, lookUpType).getLookupId());
        }
        String listS = list.toString();
        listS = listS.replaceAll(",", "\\|");
        listS = listS.replace("[", "").replace("]", "");
        return listS;
    }
}
