package com.lh.cif.gsuite;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.admin.directory.model.User;
import com.google.appengine.api.NamespaceManager;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentials;
import com.holcim.cc.framework.users.UserHelper;
import com.lh.cif.gsuite.auth.OAuth2Service;
import com.lh.cif.gsuite.directory.DirectoryService;
import com.lh.cif.gsuite.drive.DriveService;
import com.lh.cif.gsuite.plus.PlusService;
import com.lh.cif.gsuite.script.ScriptService;
import com.lh.cif.gsuite.search.DocumentSearchService;

import lombok.extern.slf4j.Slf4j;

/**
 * Google Service Manager will expose all base Google Services. *
 *
 * @author anbharti
 *
 */
@Slf4j
@Service
public class GsuiteManager {

	public Credential getOauth(final String userEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		log.info("getting credentials for " + userEmail);
		return OAuth2Service.getCredential(userEmail);
	}

	public Credential getAdminOauth() throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		log.info("getting ADMIN credentials");
		return OAuth2Service.getAdminCredential();
	}

	public DirectoryService getDirectory(Credential credential) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		return new DirectoryService(credential);
	}

	public DriveService getDrive(String userEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		return new DriveService(getOauth(userEmail));
	}

	public ScriptService getScriptService(String userEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		return new ScriptService(getOauth(userEmail));
	}

	public PlusService getPlusService(Credential credential) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		return new PlusService(credential);

	}

	public DocumentSearchService getDocumentSearchService() {
		return new DocumentSearchService();
	}

	// WHY BELOW TWO METHODS ARE ADDED HERE
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public Map<String, List<String>> getInActiveApproversMap(Map<String, List<String>> apprMap) throws HolcimGoogleCredentialRetrievalException, IOException {
		Map<String, List<String>> inactiveApprovers = new java.util.HashMap<String, List<String>>();
		Set<String> apprKeySet = apprMap.keySet();
		for (String level : apprKeySet) {
			List<String> apprList = apprMap.get(level);
			for (String apprMailId : apprList) {
				if (!getActiveUserStatus(apprMailId)) {
					// not an active google email account
					if (null != inactiveApprovers.get(level)) {
						inactiveApprovers.get(level).add(apprMailId);
					} else {
						List<String> tempList = new ArrayList<String>();
						tempList.add(apprMailId);
						inactiveApprovers.put(level, tempList);
					}
				}
			}
		}
		return inactiveApprovers;
	}

	public boolean getActiveUserStatus(String apprMailId) throws HolcimGoogleCredentialRetrievalException, IOException {
		boolean activeUserFlag = false;
		String oldNameSpace = NamespaceManager.get();
		try {
			NamespaceManager.set("");
			User user = UserHelper.getInstance(HolcimGoogleCredentials.getInstance()).getUser(apprMailId);
			if (null != user && !user.getSuspended()) {
				activeUserFlag = true;
				// return activeUserFlag;
			}
		} finally {
			NamespaceManager.set(oldNameSpace);
		}
		return activeUserFlag;
	}

}
