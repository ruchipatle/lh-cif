package com.lh.cif.gsuite.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.api.utils.SystemProperty.Environment.Value;
import com.google.common.base.Strings;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DatastoreWorker<E> {

	private static final int COUNT_MANY_PAGE_SIZE = 100000;

	private final Class<E> c;

	private static Set<String> classes = new HashSet<String>();

	private static DatastoreService datastore;

	public DatastoreWorker(final Class<E> c) {
		this.c = c;
		registerClass(c);
		if (c.isAnnotationPresent(AlsoRegister.class)) {
			AlsoRegister alsoRegister = c.getAnnotation(AlsoRegister.class);
			Class[] additionalClasses = alsoRegister.value();
			for (Class<?> additionalClass : additionalClasses) {
				registerClass(additionalClass);
			}
		}
		//if (SystemProperty.environment.value() != Value.Production) {
			// ObjectifyService.begin(); v5
			ObjectifyService.factory().begin();
		//}
		datastore = DatastoreServiceFactory.getDatastoreService();
	}

	public void registerClass(final Class<?> c) {
		String classId = c.getPackage() + c.getName();
		if (!classes.contains(classId)) {
			synchronized (classes) {
				if (!classes.contains(classId)) {
					ObjectifyService.factory().register(c);
					classes.add(classId);
				}
			}
		}
	}

	public void clearSessionCache() {
		ofy().clear();
	}

	public E save(final E e) {
		ofy().save().entity(e).now();
		return e;

		// in case we need transaction
		/*
		 * Objectify ofy = ObjectifyService.beginTransaction(); try { ofy.put(e);
		 * ofy.getTxn().commit(); } finally { if (ofy.getTxn().isActive()) {
		 * ofy.getTxn().rollback(); } } return e;
		 */
	}

	public void saveAll(final Iterable<? extends E> list) {
		ofy().save().entities(list).now();
	}

	public E get(final Long id) {
		/*
		 * if (SystemProperty.environment.value() != Value.Production) { return (E)
		 * ofy().load().filterKey(Key.create(this.c, id)).first().now(); } else { return
		 * ofy().load().type(this.c).id(id).now(); }
		 */

		return ofy().load().type(this.c).id(id).now();
	}

	public E get(final Map<String, Object> columnValueMap) {
		Query<E> query = ofy().load().type(this.c);
		for (Entry<String, Object> key : columnValueMap.entrySet()) {
			query = query.filter(key.getKey(), key.getValue());

		}
		return query.first().now();
	}

	public E get(final String id) {
		/*
		 * if (SystemProperty.environment.value() != Value.Production) { return (E)
		 * ofy().load().filterKey(Key.create(this.c, id)).first().now(); } else { return
		 * ofy().load().type(this.c).id(id).now(); }
		 */

		return ofy().load().type(this.c).id(id).now();
	}

	public void delete(final E e) {
		ofy().delete().entity(e).now();
	}

	public void deleteAll(final Iterable<E> e) {
		ofy().delete().entities(e).now();
	}

	public Collection<E> getAll(final List<String> ids) {
		return ofy().load().type(this.c).ids(ids).values();
	}

	// overloaded method
	public Collection<E> getAll(final Set<String> ids) {
		return ofy().load().type(this.c).ids(ids).values();
	}

	public Map<String, E> getAllMap(final Set<String> ids) {
		return ofy().load().type(this.c).ids(ids);
	}

	public Collection<E> getAllByIds(final List<Long> ids) {

		/*
		 * if (SystemProperty.environment.value() != Value.Production) { List<Key> keys
		 * = new ArrayList<>(); for (long id : ids) { keys.add(Key.create(this.c, id));
		 * } return ofy().load().type(this.c).filterKey("IN", keys).list(); } else {
		 * return ofy().load().type(this.c).ids(ids).values(); }
		 */

		return ofy().load().type(this.c).ids(ids).values();
	}
	
	public Collection<E> getAllByIdNames(final List<String> ids) { 
		return ofy().load().type(this.c).ids(ids).values();
	}

	public Set<Entry<String, E>> getAllBySet(final List<String> ids) {
		return ofy().load().type(this.c).ids(ids).entrySet();
	}

	public List<E> getEntityList() {
		return ofy().load().type(this.c).list();
	}

	public List<E> getBy(final String order, final int limit) {
		return ofy().load().type(this.c).order(order).limit(limit).list();
	}

	public List<E> getBy(final String byString, final Object byObject) {
		return ofy().load().type(this.c).filter(byString, byObject).list();
	}

	public List<E> getBy(final String byString, final Object byObject, final String order, final int limit) {
		return ofy().load().type(this.c).filter(byString, byObject).order(order).limit(limit).list();
	}

	public List<E> getBy(final String filterColumns[], final Object filterValues[], final String order, final int limit) {
		Query<E> query = ofy().load().type(this.c);
		for (int i = 0; i < filterColumns.length; i++) {
			query = query.filter(filterColumns[i], filterValues[i]);
		}
		return query.order(order).limit(limit).list();
	}

	public int count(final String filterColumns[], final Object filterValues[]) {
		Query<E> query = ofy().load().type(this.c);
		for (int i = 0; i < filterColumns.length; i++) {
			query = query.filter(filterColumns[i], filterValues[i]);
		}
		return query.count();
	}

	public int count(final String byString, final Object byObject) {
		return ofy().load().type(this.c).filter(byString, byObject).count();
	}

	public void deleteById(final long id) {
		ofy().delete().type(this.c).id(id).now();
	}

	public void deleteById(final String id) {
		ofy().delete().type(this.c).id(id).now();
	}

	public List<E> getBy(final String[] filterColumns, final Object[] filterValues, final int limit) {
		Query<E> query = ofy().load().type(this.c);
		for (int i = 0; i < filterColumns.length; i++) {
			query = query.filter(filterColumns[i], filterValues[i]);
		}
		return query.limit(limit).list();
	}

	public E getWithoutCache(final String id) {
		return ofy().cache(false).load().type(this.c).id(id).now();
	}

	public int countMany(final String byString, final Object byObject) {
		return countMany(new String[] { byString }, new Object[] { byObject });
	}

	public int countMany(final String[] byStrings, final Object[] byObject) {
		int total = 0;
		Cursor cursor = null;
		boolean carryOnToNextCursor = false;
		do {
			carryOnToNextCursor = false;
			Query<E> query = ofy().load().type(this.c).limit(COUNT_MANY_PAGE_SIZE);
			for (int i = 0; i < byStrings.length; i++) {
				query = query.filter(byStrings[i], byObject[i]);
			}
			if (cursor != null) {
				query = query.startAt(cursor);
			}

			QueryResultIterator<Key<E>> iterator = query.keys().iterator();
			while (iterator.hasNext()) {
				iterator.next();
				total++;
				carryOnToNextCursor = true;
			}
			cursor = iterator.getCursor();
		} while (carryOnToNextCursor);
		return total;
	}

	public QueryResultIterator<E> iterate(final String[] columns, final Object[] values, final int limit, final Cursor cursor) {
		Query<E> query = ofy().load().type(this.c).limit(limit);
		if (cursor != null) {
			query = query.startAt(cursor);
		}
		for (int i = 0; i < columns.length; i++) {
			query = query.filter(columns[i], values[i]);
		}
		return query.iterator();
	}

	public List<E> list() {
		Query<E> query = ofy().load().type(this.c);
		return query.list();
	}

	public List<E> list(final Map<String, Object> columnValueMap) {
		Query<E> query = ofy().load().type(this.c);
		for (Entry<String, Object> key : columnValueMap.entrySet()) {
			query = query.filter(key.getKey(), key.getValue());
		}
		return query.list();
	}

	public List<E> list(final String[] columns, final Object[] values) {
		Query<E> query = ofy().load().type(this.c);
		for (int i = 0; i < columns.length; i++) {
			query = query.filter(columns[i], values[i]);
		}
		return query.list();
	}

	public List<Key<E>> listKeysForParent(final Object parentKeyOrEntity) {
		return ofy().load().type(this.c).ancestor(parentKeyOrEntity).keys().list();
	}

	public void deleteKeysAsynchronously(final List<Key<E>> keys) {
		ofy().delete().keys(keys);
	}

	public QueryResultIterator<E> listEntitiesForParent(final Object parentKeyOrEntity, final String[] columns, final Object[] values, final int chunkSize) {
		Query<E> query = ofy().load().type(this.c).ancestor(parentKeyOrEntity).chunk(chunkSize);
		for (int i = 0; i < columns.length; i++) {
			query = query.filter(columns[i], values[i]);
		}
		return query.iterator();
	}

	public QueryResultIterator<E> listEntitiesForParentByCursor(final Object parentKeyOrEntity, final String[] columns, final Object[] values, final int limit, final String pCursor) {
		Query<E> query = ofy().load().type(this.c).ancestor(parentKeyOrEntity).limit(limit);

		if (!Strings.isNullOrEmpty(pCursor)) {
			query.startAt(Cursor.fromWebSafeString(pCursor));
		}
		for (int i = 0; i < columns.length; i++) {
			query = query.filter(columns[i], values[i]);
		}
		return query.iterator();

	}

	// paginated
	public QueryResultIterator<E> listEntitiesPaginated(final Map<String, Object> columnValueMap, final int limit, final String pCursor) {
		Query<E> query = ofy().load().type(this.c).limit(limit);
		if (!Strings.isNullOrEmpty(pCursor)) {
			query.startAt(Cursor.fromWebSafeString(pCursor));
		}
		if (!CollectionUtils.isEmpty(columnValueMap)) {
			for (Entry<String, Object> column : columnValueMap.entrySet()) {
				query = query.filter(column.getKey(), column.getValue());
			}
		}
		return query.iterator();
	}

	// PROJECTION QUERY from REPORTS
	public List<Entity> fetchSelectedColumnValue(String kind, Map<String, Class<?>> columnTypeMap, Map<String, Object> columnValueMap, Boolean distinctFlag) {
		com.google.appengine.api.datastore.Query query = new com.google.appengine.api.datastore.Query(kind);
		for (Entry<String, Class<?>> key : columnTypeMap.entrySet()) {
			query.addProjection(new PropertyProjection(key.getKey(), key.getValue()));
		}
		query.setDistinct(distinctFlag);
		if (columnValueMap.size() > 0) {
			for (Entry<String, Object> key : columnValueMap.entrySet()) {
				query = query.setFilter(com.google.appengine.api.datastore.Query.FilterOperator.EQUAL.of(key.getKey(), key.getValue()));

			}
		}
		List<Entity> entities = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());

		return entities;
	}

}
