package com.lh.cif.gsuite.exception;

public class RetryException extends Exception {
	private static final long serialVersionUID = -7016318966461172356L;

	public RetryException(final String string, final Throwable e) {
		super(string, e);
	}
}
