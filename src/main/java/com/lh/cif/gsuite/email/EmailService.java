package com.lh.cif.gsuite.email;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmailService {

	/**
	 * Send an email on behalf of the application
	 *
	 */
	public static void sendMail(final String senderEmail, final String senderName, final List<String> recipientsTO, final List<String> recipientsCC, final List<String> recipientsBCC, final String subject,
			final String htmlBody) throws IOException, MessagingException {

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		Multipart mp = new MimeMultipart();
		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(htmlBody, "text/html");
		mp.addBodyPart(htmlPart);

		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(senderEmail, senderName));

		InternetAddress[] addressesTO = new InternetAddress[recipientsTO.size()];

		int indexTO = 0;
		for (String recipientTO : recipientsTO) {
			addressesTO[indexTO] = new InternetAddress(recipientTO, recipientTO);
			indexTO = indexTO + 1;
		}

		if (recipientsCC != null) {
			InternetAddress[] addressesCC = new InternetAddress[recipientsCC.size()];

			int indexCC = 0;
			for (String recipientCC : recipientsCC) {
				addressesCC[indexCC] = new InternetAddress(recipientCC, recipientCC);
				indexCC = indexCC + 1;
			}
			msg.setRecipients(Message.RecipientType.CC, addressesCC);
		}

		msg.setRecipients(Message.RecipientType.TO, addressesTO);
		String subjectEncoded = MimeUtility.encodeText(subject, StandardCharsets.UTF_8.name(), "Q");
		msg.setSubject(subjectEncoded);
		msg.setContent(mp);
		Transport.send(msg);

		log.info("Sent email: " + msg.getContent().toString());
	}

}
