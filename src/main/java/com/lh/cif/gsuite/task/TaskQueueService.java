package com.lh.cif.gsuite.task;

import java.util.Map;
import java.util.UUID;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskQueueService {

	public static void purgeQueue(final String appQueue) {
		Queue queue = QueueFactory.getQueue(appQueue);
		long numTasks = queue.fetchStatistics().getNumTasks();
		log.info("Purging {} tasks in queue {}", numTasks, appQueue);
		queue.purge();
	}

	public static TaskOptions getTaskPost(final String taskName, final String url, final String payload) {
		TaskOptions taskOptions = TaskOptions.Builder.withUrl(url).method(TaskOptions.Method.POST).taskName(taskName).payload(payload);
		return randomizeTaskName(taskOptions);
	}

	public static TaskOptions getTaskGet(final String taskName, final String url, final String[]... params) {
		TaskOptions taskOptions = TaskOptions.Builder.withUrl(url).method(TaskOptions.Method.GET).taskName(taskName);
		if (params != null && params.length > 0) {
			for (String kv[] : params) {
				if (kv[1] != null)
					taskOptions.param(kv[0], kv[1]);
			}
		}
		return randomizeTaskName(taskOptions);
	}

	public static TaskOptions getTaskGet(final String taskName, final String url, final Map<String, String> params) {
		TaskOptions taskOptions = TaskOptions.Builder.withUrl(url).method(TaskOptions.Method.GET).taskName(taskName);
		if (params != null && params.size() > 0) {
			for (String k : params.keySet()) {
				if (params.get(k) != null)
					taskOptions.param(k, params.get(k));
			}
		}
		return randomizeTaskName(taskOptions);
	}

	public static void runTask(final String queueName, final TaskOptions taskOptions) {
		runTask(queueName, taskOptions, 0);
	}

	public static void runTask(final String queueName, final TaskOptions taskOptions, final long delay) {
		Queue queueDelete = QueueFactory.getQueue(queueName);
		taskOptions.countdownMillis(delay);
		queueDelete.add(taskOptions);
	}

	private static TaskOptions randomizeTaskName(final TaskOptions taskOptions) {
		return taskOptions.taskName("TestTask" + "---RANDOM-" + UUID.randomUUID());
	}
}
