package com.lh.cif.gsuite.task;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;

import com.lh.cif.gsuite.exception.RetryException;

public class RetriableTask<T> implements Callable<T> {
	protected Callable<T> task;

	public static final int DEFAULT_NUMBER_OF_RETRIES = 5;

	public static final long DEFAULT_WAIT_TIME = 2000L;

	private int numberOfRetries;

	private int numberOfTriesLeft;

	private long timeToWait;

	public RetriableTask() {
		this(DEFAULT_NUMBER_OF_RETRIES, DEFAULT_WAIT_TIME);
	}

	public RetriableTask(final int numberOfRetries, final long timeToWait) {
		this.numberOfRetries = numberOfRetries;
		this.numberOfTriesLeft = numberOfRetries;
		this.timeToWait = timeToWait;
	}

	public void setTask(final Callable<T> task) {
		this.task = task;
	}

	public Callable<T> getTask() {
		return this.task;
	}

	@Override
	public T call() throws InterruptedException, CancellationException, RetryException {
		T result = null;
		try {
			result = this.task.call();
		} catch (Throwable e) {
			this.numberOfTriesLeft -= 1;
			if (this.numberOfTriesLeft == 0) {
				throw new RetryException(this.numberOfRetries + " attempts to retry failed at " + this.timeToWait + "ms interval", e);
			}
			Thread.sleep(this.timeToWait);
			result = call();
		}
		return result;
	}

}
