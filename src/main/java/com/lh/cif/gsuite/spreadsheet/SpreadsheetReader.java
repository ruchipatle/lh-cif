package com.lh.cif.gsuite.spreadsheet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpreadsheetReader {
	private final Credential credential;

	private final SpreadsheetService spreadsheetService;

	private final String spreadsheetId;

	private static int CONNECTION_TIMEOUT = 60 * 1000;

	public SpreadsheetReader(final Credential credential, final String spreadsheetId) {
		this.credential = credential;
		this.spreadsheetService = new SpreadsheetService("CIF");
		this.spreadsheetService.setOAuth2Credentials(this.credential);
		this.spreadsheetService.setConnectTimeout(CONNECTION_TIMEOUT);

		// driveService = new Drive()
		this.spreadsheetId = spreadsheetId;
	}

	public List<WorksheetEntry> listSheets() throws IOException, ServiceException {
		URL spreadsheetURL = new URL("https://spreadsheets.google.com/feeds/worksheets/" + this.spreadsheetId + "/private/full");
		WorksheetFeed feed = this.spreadsheetService.getFeed(spreadsheetURL, WorksheetFeed.class);
		return feed.getEntries();
	}

	public List<ListEntry> listRows(final WorksheetEntry entry) throws IOException, ServiceException {
		URL listFeedUrl = entry.getListFeedUrl();
		ListFeed listFeed = this.spreadsheetService.getFeed(listFeedUrl, ListFeed.class);
		return listFeed.getEntries();
	}

	public List<CellEntry> listRows(final WorksheetEntry entry, final int startRowNumber, final int columnNumber) throws IOException, ServiceException, URISyntaxException {
		// Fetch column 4, and every row after row 1.
		URL cellFeedUrl = new URI(entry.getCellFeedUrl().toString() + "?min-row=" + startRowNumber + "&min-col=" + columnNumber + "&max-col=" + columnNumber).toURL();
		CellFeed cellFeed = this.spreadsheetService.getFeed(cellFeedUrl, CellFeed.class);
		return cellFeed.getEntries();
	}

	public static String getCellValue(final ListEntry row, final String columnName) {
		if (row != null && columnName != null) {
			return row.getCustomElements().getValue(columnName);
		}
		return null;
	}
}
