package com.lh.cif.gsuite.drive;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.CommentList;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.google.api.services.drive.model.User;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.gsuite.util.RetrybleService;

import lombok.extern.slf4j.Slf4j;

// TODO : This Class needs to be modified as per functionalities in V3
@Slf4j
public class DriveService extends RetrybleService {

	protected Drive drive;

	public DriveService(final Credential credential) {
		this.drive = DriveBuilder.getInstance().buildDriveService(credential);
	}

	// TODO : Set Visibility based on String value
	public File copy(final File file, final String visibility) throws Throwable {
		return execute(this.drive.files().copy(file.getId(), file));
	}

	public File update(final File file) throws Throwable {
		return execute(this.drive.files().update(file.getId(), file));
	}

	public Void delete(final String fileId) throws Throwable {
		return execute(this.drive.files().delete(fileId));
	}

	public InputStream getInputStream(final String fileId) throws Throwable {
		Callable<InputStream> callable = new Callable<InputStream>() {
			@Override
			public InputStream call() throws Exception {
				return DriveService.this.drive.files().get(fileId).executeMediaAsInputStream();
			}
		};
		return execute(callable);
	}

	public File get(final String fileId) throws Throwable {
		return execute(this.drive.files().get(fileId));
	}

	public File insert(final File file) throws Throwable {
		return execute(this.drive.files().create(file));
	}

	public Permission insertPermissions(final String fileId, final Permission p) throws Throwable {
		return execute(this.drive.permissions().create(fileId, p).setSendNotificationEmail(false));
	}

	public Void deletePermissions(final String fileId, final String permissionId) throws Throwable {
		return execute(this.drive.permissions().delete(fileId, permissionId));
	}

	// TODO Write Logic mimeType = image , image/jpeg,image/png
	// https://developers.google.com/drive/v3/web/search-parameters#value_types
	public FileList getImageDriveFilesByFileNameAndParentFolder(final String title, final String mimeType, final String parentFolderName) throws Throwable {

		String pageToken = null;
		FileList fileList;
		do {
			fileList = execute(this.drive.files().list().setQ("mimeType contains '" + mimeType + "' and title='" + title + "' and '" + parentFolderName + "' in parents").setSpaces("drive")
					.setFields("nextPageToken, files(id, name)").setPageToken(pageToken));
			pageToken = fileList.getNextPageToken();
		} while (pageToken != null);

		return fileList;
	}

	public List<FileList> getDriveFiles(final String mimeType) throws Throwable {
		String pageToken = null;
		FileList fileList;
		List<FileList> allFiles = new ArrayList<>();
		do {
			fileList = execute(this.drive.files().list().setQ("mimeType contains '" + mimeType + "'").setSpaces("drive").setFields("nextPageToken, files(id, name)").setPageToken(pageToken));
			allFiles.add(fileList);
			pageToken = fileList.getNextPageToken();
		} while (pageToken != null);

		return allFiles;
	}

	public List<File> getAllImagesFromDrive() throws Throwable {
		ListIterator<FileList> fileIterator = getDriveFiles("image").listIterator();
		List<File> files = new ArrayList<>();
		while (fileIterator.hasNext()) {
			List<File> fileList = fileIterator.next().getFiles();
			files.addAll(fileList);
		}
		return files;
	}

	// TODO : Write Logic
	public List<String> search(final String parentFolderId, final String[] words, final String next, final int page) throws Throwable {
		return Arrays.asList("Childs List");
	}

	// TODO : Write Logic
	public File removeAllParents(final File file) {
		return new File();
	}

	public List<File> getFiles(final Integer pageSize) throws IOException, Throwable {
		return execute(this.drive.files().list().setPageSize(pageSize)).getFiles();
	}

	// TODO : Write Logic
	public File transferOwnershipFrom(final File file, final DriveService currentUserDriveService, final List<String> parentFolderName) throws Throwable {
		File copy = null;
		try {
			// copy of the file
			log.info("Copying file");
			file.setParents(parentFolderName);
			file.setShared(false);
			copy = currentUserDriveService.copy(file, "PRIVATE");

			log.info("File copied");

			currentUserDriveService.removeAllParents(copy);

			// insert new owner
			log.info("Changing ownership...");

			Permission p = new Permission();
			p.setType("user");
			p.setRole("owner");

			currentUserDriveService.insertPermissions(copy.getId(), p);
			log.info("Ownership changed!");

		} catch (Throwable t) {
			log.error(t.getMessage(), t);
			if (copy != null) {
				log.error("Removing file copy");
				currentUserDriveService.delete(copy.getId());
			}
			throw t;
		}
		return copy;
	}

	public PermissionList getPermissionListByFileId(final String fileId) throws Throwable {
		return execute(this.drive.permissions().list(fileId));
	}

	public PermissionList getPermissionListByFileIdWithEmail(final String fileId) throws Throwable {
		return drive.permissions().list(fileId).setFields("permissions(emailAddress,id,kind,role,type)").execute();
		
	}
	
	public List<String> getPermissionIdByUserEmail(final String userEmail, final String fileId) throws Throwable {

		List<String> userPermissionIds = new ArrayList<>();
		PermissionList existingPermissionList = getPermissionListByFileId(fileId);
		for (Permission permission : existingPermissionList.getPermissions()) {
			if (permission.getEmailAddress().contains(userEmail)) {
				log.info("Current user permission: {}", permission.getId() + " " + permission.getType());
				userPermissionIds.add(permission.getId());
			}
		}
		return userPermissionIds;
	}

	// https://developers.google.com/drive/v3/web/manage-sharing#manipulating_permissions_with_batch_requests

	protected JsonBatchCallback<Permission> addPermissionCallBack() {
		return new JsonBatchCallback<Permission>() {
			@Override
			public void onFailure(final GoogleJsonError e, final HttpHeaders responseHeaders) throws IOException {
				// Handle error
				log.error(e.getMessage());
			}

			@Override
			public void onSuccess(final Permission permission, final HttpHeaders responseHeaders) throws IOException {
				log.info("Permission ID: " + permission.getId());
			}
		};
	}

	protected JsonBatchCallback<Void> deletePermissionCallBack() {
		return new JsonBatchCallback<Void>() {
			@Override
			public void onFailure(final GoogleJsonError e, final HttpHeaders responseHeaders) throws IOException {
				// Handle error
				log.error(e.getMessage());
			}

			@Override
			public void onSuccess(final Void t, final HttpHeaders responseHeaders) throws IOException {
				log.info("Permission Deleted: ");

			}
		};
	}

	private void executeBatch(final String fileId, final List<String> userPermissionIds) throws Throwable {
		JsonBatchCallback<Void> callback = deletePermissionCallBack();
		BatchRequest batchRequest = this.drive.batch();
		for (String permissionId : userPermissionIds) {
			this.drive.permissions().delete(fileId, permissionId).queue(batchRequest, callback);
		}
		execute(batchRequest);
	}

	public static File transferOwnershipByFileId(final File file, final String userEmail, final DriveService techUserDriveService, final String parentFolder) throws Throwable {
		try {

			String fileId = file.getId();

			log.info("Current user: {}", userEmail);

			List<String> userPermissionIds = techUserDriveService.getPermissionIdByUserEmail(userEmail, fileId);

			log.info("Deleting current user permissions ... ");
			techUserDriveService.executeBatch(fileId, userPermissionIds);
			log.info("Deleting permission done!");

			log.info("Adding technical user permissions ... ");
			Permission defaultPermission = getDefaultFilePermission();

			log.info("Default permission value: {}", defaultPermission.getType());

			techUserDriveService.insertPermissions(fileId, defaultPermission);
			log.info("Adding permission done!");
			file.setParents(Arrays.asList(parentFolder));

			techUserDriveService.update(file);

		} catch (Throwable t) {
			log.error(t.getMessage(), t);
			log.error("Removing file copy");
			// techUserDriveService.delete(file.getId());
			throw t;
		}
		return file;
	}

	public File transferOwnershipTo(final File file, final String userEmail, final DriveService techUserDriveService, final String parentFolder) throws Throwable {
		try {

			String fileId = file.getId();

			log.info("Current user: {}", userEmail);

			List<String> userPermissionIds = techUserDriveService.getPermissionIdByUserEmail(userEmail, fileId);

			log.info("Deleting current user permissions ... ");
			techUserDriveService.executeBatch(fileId, userPermissionIds);
			log.info("Deleting permission done!");

			log.info("Adding technical user permissions ... ");
			Permission defaultPermission = getDefaultFilePermission();

			log.info("Default permission value: {}", defaultPermission.getType());

			techUserDriveService.insertPermissions(fileId, defaultPermission);
			log.info("Adding permission done!");
			file.setParents(Arrays.asList(parentFolder));

			techUserDriveService.update(file);

		} catch (Throwable t) {
			log.error(t.getMessage(), t);
			log.error("Removing file copy");
			// techUserDriveService.delete(file.getId());
			throw t;
		}
		return file;
	}

	public static Permission getDefaultFilePermission() {
		Permission p = new Permission();
		p.setRole("reader");
		p.setType("domain");
		return p;
	}

	public File getFile(final String fileId) throws IOException, HolcimGoogleCredentialRetrievalException {
		return this.drive.files().get(fileId).setFields("owners").execute(); // get owners of the file
																				// by
		// setting the field to
		// "owners"
	}

	/* following methods are added for footer update component */
	public Map<String, String> getFileProperties(final String fileId) throws IOException, HolcimGoogleCredentialRetrievalException {
		return this.drive.files().get(fileId).setFields("properties").execute().getProperties();
	}

	public InputStream getFileInputStream(final String fileId, final String mimeType) throws IOException, HolcimGoogleCredentialRetrievalException {
		return this.drive.files().export(fileId, mimeType).executeAsInputStream();
	}

	public File update(String fileId, File file, ByteArrayContent bac) throws IOException {
		return this.drive.files().update(fileId, file, bac).execute();

	}

	public Permission insertOwnerPermission(String fileId, Permission p) throws IOException, Throwable {
		return execute(this.drive.permissions().create(fileId, p).setTransferOwnership(true).setSendNotificationEmail(true));
	}

	public String getPermissionId() throws IOException {
		About about = this.drive.about().get().setFields("user").execute(); // get user info by setting the field "user"
		User user = about.getUser();
		return user.getPermissionId();
	}

	public Permission updatePermission(String fileId, String permissionId, Permission permission) throws IOException {
		return this.drive.permissions().update(fileId, permissionId, permission).setFields("role").execute();
	}
	
	public File getWritercanShare(final String fileId) throws IOException, HolcimGoogleCredentialRetrievalException {
		return this.drive.files().get(fileId).setFields("writersCanShare").execute(); 
	}

	public File getViewersCanCopyContent(final String fileId) throws IOException, HolcimGoogleCredentialRetrievalException {
		return this.drive.files().get(fileId).setFields("viewersCanCopyContent").execute(); }

	public File update(String fileId, File file) throws IOException {
		return this.drive.files().update(fileId, file).execute();		
	}
	
	public String getCommentsForFile(final String fileId) throws IOException {
			String str = this.drive.comments().list(fileId).
				setFields("comments(content ,createdTime,author(displayName,photoLink), replies(author(displayName,photoLink),createdTime,content))").
				//setFields("*").
				execute().toString();
			JSONObject json = new JSONObject(str);	
			JSONArray jarr = json.getJSONArray("comments");
			if(jarr.length()==0)
				str = "";
			return str;
			
	}
	
	public boolean isCommentsPresent(final String fileId) throws IOException {
		boolean res =false;
		CommentList list =	this.drive.comments().list(fileId).setFields("comments(content)").execute();
		JSONObject json = new JSONObject(list.toString());	
		JSONArray jarr = json.getJSONArray("comments");
		if(jarr.length()!=0)
			res = true;
		return res ;
	}
}
