package com.lh.cif.gsuite.drive;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;

public class DriveBuilder {

	private static DriveBuilder instance;

	private static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	private static JsonFactory JSON_FACTORY = new JacksonFactory();

	private DriveBuilder() {
		HTTP_TRANSPORT = new NetHttpTransport();
		JSON_FACTORY = new JacksonFactory();
	}

	public static DriveBuilder getInstance() {
		if (instance == null) {
			instance = new DriveBuilder();
		}
		return instance;
	}

	public Drive buildDriveService(final Credential credential) {
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName("CIF").build();
	}
}
