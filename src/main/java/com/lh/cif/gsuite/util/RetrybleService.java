package com.lh.cif.gsuite.util;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.api.client.auth.oauth2.TokenRequest;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.common.base.Predicate;
import com.lh.cif.gsuite.auth.AuthException;

import lombok.extern.slf4j.Slf4j;

/**
 * Source Reference https://github.com/rholder/guava-retrying
 *
 * @author anbharti
 *
 */
@Slf4j
public class RetrybleService {

	protected int multiplier = 500;

	protected int timeout = 60;

	protected int attempts = 5;

	protected TimeUnit timeUnit = TimeUnit.SECONDS;

	/**
	 * retryier for all google API calls
	 *
	 * @param client
	 * @param <T>
	 * @return
	 * @throws Throwable
	 */
	protected <T> T execute(final AbstractGoogleClientRequest<T> client) throws Throwable {
		Callable<T> callable = new Callable<T>() {
			int tried = 0;

			@Override
			public T call() throws Exception {
				if (this.tried > 0) {
					log.info("Retrying: " + this.tried);
				}
				this.tried++;
				return client.execute();
			}
		};
		try {

			Retryer<T> retryer = getRetryer();
			return retryer.call(callable);

		} catch (ExecutionException e) {
			if (e.getCause() instanceof GoogleJsonResponseException) {
				GoogleJsonResponseException g = (GoogleJsonResponseException) e.getCause();
				if (g.getStatusCode() == 401) {
					throw new AuthException(g);
				}
			}
			throw e.getCause();
		}
	}

	/**
	 * retryier for all google API batch calls
	 *
	 * @param client
	 * @param <T>
	 * @return
	 * @throws Throwable
	 */

	protected <T> T execute(final BatchRequest client) throws Throwable {

		Callable<T> callable = new Callable<T>() {
			int tried = 0;

			@Override
			public T call() throws Exception {
				if (this.tried > 0) {
					log.info("Retrying: " + this.tried);
				}
				this.tried++;
				client.execute();
				return null;
			}
		};
		try {

			Retryer<T> retryer = getRetryer();
			return retryer.call(callable);

		} catch (ExecutionException e) {
			if (e.getCause() instanceof GoogleJsonResponseException) {
				GoogleJsonResponseException g = (GoogleJsonResponseException) e.getCause();
				if (g.getStatusCode() == 401) {
					throw new AuthException(g);
				}
			}
			throw e.getCause();
		}

	}

	/**
	 * retry for request and access token
	 *
	 * @param tokenRequest
	 * @param <T>
	 * @return
	 * @throws Throwable
	 */
	protected <T> T execute(final TokenRequest tokenRequest) throws Throwable {
		Callable<T> callable = new Callable<T>() {
			int tried = 0;

			@Override
			public T call() throws Exception {
				if (this.tried > 0) {
					log.info("Retrying: " + this.tried);
				}
				this.tried++;
				return (T) tokenRequest.execute();
			}
		};
		try {

			Retryer<T> retryer = getRetryer();
			return retryer.call(callable);

		} catch (ExecutionException e) {
			if (e.getCause() instanceof GoogleJsonResponseException) {
				GoogleJsonResponseException g = (GoogleJsonResponseException) e.getCause();
				if (g.getStatusCode() == 401) {
					throw new AuthException(g);
				}
			}

			throw e.getCause();
		}
	}

	/**
	 * generic executor of callable
	 *
	 * @param callable
	 * @param <T>
	 * @return
	 * @throws Throwable
	 */
	protected <T> T execute(final Callable<T> callable) throws Throwable {
		try {

			Retryer<T> retryer = getRetryer();
			return retryer.call(callable);

		} catch (ExecutionException e) {
			if (e.getCause() instanceof GoogleJsonResponseException) {
				GoogleJsonResponseException g = (GoogleJsonResponseException) e.getCause();
				if (g.getStatusCode() == 401) {
					throw new AuthException(g);
				}
			}
			throw e.getCause();
		}
	}

	/*
	 * 400 invalidParameter Indicates that a request parameter has an invalid value.
	 * The locationType and location fields in the error response provide
	 * information as to which value was invalid. Do not retry without fixing the
	 * problem. You need to provide a valid value for the parameter specified in the
	 * error response. 400 badRequest Indicates that the query was invalid. E.g.,
	 * parent ID was missing or the combination of dimensions or metrics requested
	 * was not valid. Do not retry without fixing the problem. You need to make
	 * changes to the API query in order for it to work. 401 invalidCredentials
	 * Indicates that the auth token is invalid or has expired. Do not retry without
	 * fixing the problem. You need to get a new auth token. 403
	 * insufficientPermissions Indicates that the user does not have sufficient
	 * permissions for the entity specified in the query. Do not retry without
	 * fixing the problem. You need to get sufficient permissions to perform the
	 * operation on the specified entity. 403 dailyLimitExceeded Indicates that user
	 * has exceeded the daily quota (either per project or per view (profile)). Do
	 * not retry without fixing the problem. You have used up your daily quota. See
	 * API Limits and Quotas. 403 usageLimits.userRateLimitExceededUnreg Indicates
	 * that the application needs to be registered in the Google Developers Console.
	 * Do not retry without fixing the problem. You need to register in Developers
	 * Console to get the full API quota. 403 userRateLimitExceeded Indicates that
	 * the user rate limit has been exceeded. The maximum rate limit is 10 qps per
	 * IP address. The default value set in Google Developers Console is 1 qps per
	 * IP address. You can increase this limit in the Google Developers Console to a
	 * maximum of 10 qps. Retry using exponential back-off. You need to slow down
	 * the rate at which you are sending the requests. 403 quotaExceeded Indicates
	 * that the 10 concurrent requests per view (profile) in the Core Reporting API
	 * has been reached. Retry using exponential back-off. You need to wait for at
	 * least one in-progress request for this view (profile) to complete. 500
	 * internalServerError Unexpected internal server error occurred. Do not retry
	 * this query more than once. 503 backendError Server returned an error. Do not
	 * retry this query more than once.
	 */

	/**
	 * generic function to manage retry
	 *
	 * @param <T>
	 * @return
	 */
	protected <T> Retryer<T> getRetryer() {
		return RetryerBuilder.<T>newBuilder().withWaitStrategy(WaitStrategies.exponentialWait(this.multiplier, this.timeout, this.timeUnit)).withStopStrategy(StopStrategies.stopAfterAttempt(this.attempts))
				.retryIfException(new Predicate<Throwable>() {
					private int a = 0;

					@Override
					public boolean apply(final Throwable t) {
						this.a++;
						if (t instanceof GoogleJsonResponseException) {
							GoogleJsonResponseException e = (GoogleJsonResponseException) t;
							if ((e.getStatusCode() == 403 && e.getMessage().equals("userRateLimitExceeded")) || e.getStatusCode() == 500 || e.getStatusCode() == 503) {
								log.warn("Retrying: " + e.getMessage(), e);
								return true;
							} else if (e.getStatusCode() == 404) {
								log.warn(e.getMessage());
								return false;
							} else if (e.getStatusCode() == 409) {// already exits
								log.warn(e.getMessage());
								return false;
							}
						} else if (t instanceof TokenResponseException) {
							TokenResponseException e = (TokenResponseException) t;
							log.warn(e.getMessage(), e);
							return false;
						} else if (t instanceof IOException) {
							IOException e = (IOException) t;
							log.warn("Retrying: " + e.getMessage(), e);
							return true;
						} else if (t instanceof RuntimeException) {
							RuntimeException e = (RuntimeException) t;
							log.warn("Retrying: " + e.getMessage(), e);
							return true;
						}
						log.error(t.getMessage(), t);
						return false;
					}
				}).build();
	}

}
