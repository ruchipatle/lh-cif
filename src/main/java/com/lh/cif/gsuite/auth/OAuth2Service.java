package com.lh.cif.gsuite.auth;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import javax.servlet.jsp.jstl.core.Config;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.RefreshTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.extensions.appengine.auth.oauth2.AppEngineCredentialStore;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.appengine.api.NamespaceManager;
import com.google.appengine.repackaged.com.google.api.client.auth.oauth2.TokenRequest;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentials;
import com.lh.cif.gsuite.util.RetrybleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OAuth2Service extends RetrybleService {

    public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    public static final JsonFactory JSON_FACTORY = new JacksonFactory();

    public static HolcimGoogleCredentials holcimGoogleCredentials;

    public static Credential getCredential(final String userEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {

        String ns = NamespaceManager.get();
        try {
            NamespaceManager.set("");
            GoogleCredential credential = null;
            holcimGoogleCredentials = HolcimGoogleCredentials.getInstance();
            credential = holcimGoogleCredentials.getUserCredentials(userEmail);

            log.info("Credential (" + userEmail + ") : " + credential);

            // below check is Fix for exception if userEmail is group email id
            if (null != credential)
                try {
                    credential.refreshToken();
                    log.info("Token Refreshed");
                } catch (Throwable e) {
                    log.warn("passed email ID: " + userEmail + "  is group email id and unable to refresh token for that, It won't break the flow");
                    credential = null;
                }
            return credential;
        } finally {
            NamespaceManager.set(ns);
        }
    }

    public static Credential getAdminCredential() {
        // throws IOException, GeneralSecurityException, URISyntaxException,
        // HolcimGoogleCredentialRetrievalException {
        log.info("getAdminCredential enter---");

        String ns = NamespaceManager.get();
        try {

            NamespaceManager.set("");

            GoogleCredential credential = null;
            holcimGoogleCredentials = HolcimGoogleCredentials.getInstance();

            credential = holcimGoogleCredentials.getAdminCredentials();
            log.info("getAdminCredential token1----" + credential.getAccessToken());
            log.info("getAdminCredential token2----" + credential.getRefreshToken());
            log.info("getAdminCredential token3----" + credential.getServiceAccountId());
            log.info("getAdminCredential token4----" + credential.getServiceAccountPrivateKeyId());
            log.info("getAdminCredential token5----" + credential.getServiceAccountProjectId());
            log.info("getAdminCredential token6----" + credential.getServiceAccountScopesAsString());
            log.info("getAdminCredential token7----" + credential.getServiceAccountUser());
            log.info("getAdminCredential token8----" + credential.getTokenServerEncodedUrl());
            log.info("getAdminCredential token9----" + credential.getExpirationTimeMilliseconds());
            log.info("getAdminCredential token10----" + credential.getExpiresInSeconds());

            log.info("getAdminCredential token11----" + credential.getTransport());
            log.info("getAdminCredential token12----" + credential.getJsonFactory());
            log.info("getAdminCredential token13----" + credential.getTokenServerEncodedUrl());
            log.info("getAdminCredential token14----" + credential.getClientAuthentication());
            log.info("getAdminCredential token15----" + credential.getRequestInitializer());

            credential.refreshToken();
            return credential;
        } catch (Exception ex) {
            log.info("getAdminCredential exception---" + ex.getMessage());
            ex.printStackTrace();
            return null;
        } finally {
            NamespaceManager.set(ns);
        }
    }

    public static String getAccessToken(final String userEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
        return getCredential(userEmail).getAccessToken();
    }

}
