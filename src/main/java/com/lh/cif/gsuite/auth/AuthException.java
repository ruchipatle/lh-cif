package com.lh.cif.gsuite.auth;

import java.io.IOException;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import lombok.Data;

@Data
public class AuthException extends IOException {

	/**
	*
	*/
	private static final long serialVersionUID = -5594649357827429365L;

	private GoogleJsonResponseException googleException;

	public AuthException(final GoogleJsonResponseException googleException) {
		this.googleException = googleException;
	}
}
