package com.lh.cif.gsuite.contacts;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Users;
import com.google.api.services.groupssettings.Groupssettings;
import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.core.util.ConfigUtils;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.directory.DirectoryService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 1380740
 * This class is used to list all the users and the Google groups with Matching criteria
 *
 */
@Slf4j
@Component
public class UserAndGroupsSearch {
	@Autowired
	GsuiteManager gsuiteManager;
	
	Credential adminCredential ;
	Groupssettings gservice ;
	DirectoryService adminDirectory;
	
	/**
	 * Initailizes the Credentials, settingsService and adminDirectory for the First time or if the credentials are expired
	 * @throws IOException
	 * @throws GeneralSecurityException
	 * @throws URISyntaxException
	 * @throws HolcimGoogleCredentialRetrievalException
	 */
	private void populateValues() throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException{
		if(adminCredential == null || adminCredential.getExpiresInSeconds()<100) {
		 adminCredential = gsuiteManager.getAdminOauth();
		 adminDirectory = gsuiteManager.getDirectory(adminCredential);
		 gservice =  new Groupssettings.Builder(new NetHttpTransport(), new JacksonFactory(), adminCredential).setApplicationName("CIF").build();
		} 
	}
	
	/**
	 * Returns Map of Email, Name of users and groups based on the query
	 * @param query
	 * @param maxResults
	 * @param domain
	 * @return
	 * @throws IOException
	 * @throws GeneralSecurityException
	 * @throws URISyntaxException
	 * @throws HolcimGoogleCredentialRetrievalException
	 */
	public Map<String, String> findGroupsAndUsers(String query, int maxResults, String domain) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		populateValues();
		Users userList = adminDirectory.getUsers().getUsers(query, null, maxResults);
		List<User> users = userList.getUsers();
		Map<String, String> userMap = new HashMap<>();
		if(null!=users) {
		for (int i = 0; i < users.size(); i++) {
			User user =(User)users.get(i);
			if(!user.getSuspended()) 
				userMap.put(user.getPrimaryEmail(), user.getName().getFullName());	
			
		 }
		}
		
		Groups groups =	findOnlyGroups( query,  maxResults, domain);
		if(groups!=null) {
			  List<Group> gr = groups.getGroups();
			  if(gr!=null) {
			   for (Group g : gr) {
	        		if(checkDiscovery(g.getEmail())) {
	        			userMap.put(g.getEmail(),g.getName());
	        		}
			   }
	         }
		}
		log.info("Matching contacts and groups from directory for query: "+query+" is --> "+userMap);
		return userMap;
	}
	
	private Groups findOnlyGroups(String query, int maxResults,String domain) throws IOException {
		query = "email:"+query+"*";
		return adminDirectory.getUsers().getMatchingGroups(query, domain,maxResults);
	}
	
	/**
	 * 
	 * @param groupKey  group email address
	 * @return true if Group is to be discover in Global address list else false
	 */
	private boolean checkDiscovery(String groupKey) {
		boolean result = false;
		try {
        	 com.google.api.services.groupssettings.model.Groups pd = gservice.groups().get(groupKey).execute();
        	 result = Boolean.parseBoolean(pd.getIncludeInGlobalAddressList());
        } catch (IOException e) {
        	 log.warn("An unknown error occurred in finding discovery settings for : " +groupKey );
        	 e.printStackTrace();
        }
		return result;
	}
	
	/**
	 * It will create a department Google group, then apply group settings, then add all admin members as OWNERS in the group.
	 * @param groupId
	 * @param groupName
	 * @param opco
	 * @param masterId
	 * @param adminGroupId
	 * @return true or False based on if the process completed successfully or not
	 */
	public boolean startGoogleTaskForDeptGroup(String groupId, String groupName, String opco, String masterId, String adminGroupId) {
		boolean result = false;
		try {
		populateValues();
		createGoogleGroup(groupId,groupName,opco,masterId );
		// sleep of 2 seconds because it take few mili seconds to find newly created group from api 
		Thread.sleep(2000);
		applyGroupSettings(groupId);
		Set<String> adminUsers =  adminDirectory.getMembers().listAll(adminGroupId);
		if(adminUsers !=null) {
			addmembers(groupId,adminUsers);
		}
		result = true;
		} catch (Throwable e ) {
			log.error("Error in the process of making department group");
			e.printStackTrace();
		}
		log.info("returninhg result --> "+result);
		return result;
		
	}
	
	private void createGoogleGroup(String groupId, String groupName, String opco, String masterid) throws IOException, Throwable {
		log.info("Going to create group --> "+groupId);
		Group googleGroup = new Group();
	   	googleGroup.setName(groupName);
	   	googleGroup.setEmail(groupId);
	   	googleGroup.setDescription("IMS dept Group for "+opco+ "  for dept with master id:- "+masterid);  
	   	adminDirectory.getGroups().createGroup(googleGroup); 
	   	log.info("Group created successfully");
	}
	
	private void applyGroupSettings(String groupId) throws IOException {
		 log.info("Going to apply group settings  --> "+groupId);
	     com.google.api.services.groupssettings.model.Groups gsettings = new com.google.api.services.groupssettings.model.Groups();
         gsettings.setWhoCanPostMessage("NONE_CAN_POST");
         gsettings.setWhoCanInvite("NONE_CAN_INVITE");
         gsettings.setIncludeInGlobalAddressList("false");
         gsettings.setWhoCanViewGroup("ALL_MANAGERS_CAN_VIEW");
         gsettings.setWhoCanViewMembership("ALL_IN_DOMAIN_CAN_VIEW");
         gsettings.setWhoCanContactOwner(null);
         gsettings.setWhoCanJoin("INVITED_CAN_JOIN");
         gsettings.setAllowWebPosting("false");
         gsettings.setArchiveOnly("true");
         gservice.groups().update(groupId, gsettings).buildHttpRequest().execute();
         log.info("Group settings applied succedssfully");
	}
	
	private void addmembers(String groupid, Set<String> userEmails) {
		log.info("Going to add members -- > "+userEmails.toString());
		for (String s: userEmails) {
		Member member = new Member();
		member.setEmail(s);
		member.setRole("OWNER");
		member.setType("USER");
		adminDirectory.getMembers().insert(groupid, member);
		
		}
		log.info("members added success ");
}
	
	
	
	/**
	 * It will return the qualified email of group which is being created for IMS departments
	 * @param masterDataService
	 * @param masterId
	 * @param locale
	 * @param countryCode
	 * @return
	 */
//	public String getDeptGroupForMasterId(MasterDataService masterDataService, String masterId, String locale, String countryCode ) {
//		MasterData mdata = 	masterDataService.getDisplayValueByMasterId(Integer.parseInt(masterId), locale).get(0);
//		String deptName = mdata.getDisplayValues();
//		while(deptName.contains(" ")) {
//		deptName = deptName.replace(" " , "");
//		}
//		// As google group does not take foreign characters as email, so if dept name is having foreign character replace it
//		if (!deptName.matches("[a-zA-Z0-9]*")) {
//			deptName = "dept-"+masterId;
//		}else {
//			deptName = "dept-"+deptName;
//		}
//		HolcimConfigHelper defaultConfig = ConfigUtils.getDefaultConfigInstance();
//		String domain = defaultConfig.getValue(IMSUIConstants.DOMAIN_VALUE);
//		String groupId = "app-ims-"+countryCode+"-"+deptName+"@"+domain;
//		groupId = groupId.toLowerCase();
//		return groupId;
//	}
}