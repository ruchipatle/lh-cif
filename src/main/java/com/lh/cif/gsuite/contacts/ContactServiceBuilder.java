package com.lh.cif.gsuite.contacts;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gdata.client.contacts.ContactsService;


/**
 * Returns the instance of ContactService for the Project
 * @author 1380740
 *
 */
@Component
public class ContactServiceBuilder {

	@Bean
	public  ContactsService getAppContactService() {
		ContactsService contactsService = new ContactsService("CIF");
 		//contactsService.setOAuth2Credentials(credential);
 		return contactsService ;
	}
	
}
