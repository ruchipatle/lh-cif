package com.lh.cif.gsuite.contacts;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;

import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.extensions.Name;
import com.google.gdata.util.ServiceException;

import lombok.extern.slf4j.Slf4j;
/**
 * UserContactsService - used to fetch the user's Contact with Oauth 2.0 credentials
 * @author 1380740
 *
 */
@Slf4j
public class UserContactsService {

	/**
	 * It returns all the contacts present in user's 
	 * @param contactsService
	 * @param userEmail
	 * @return
	 * @throws IOException
	 * @throws ServiceException
	 */
	public static ContactFeed getUserMyContacts(ContactsService contactsService, String userEmail) throws IOException, ServiceException {
		Query query = new Query(new URL("https://www.google.com/m8/feeds/contacts/default/full"));
 		query.setMaxResults(10_000);
 		query.setStringCustomParameter("group", "http://www.google.com/m8/feeds/groups/"+userEmail+"/base/6");
 		ContactFeed allContactsFeed = contactsService.getFeed(query, ContactFeed.class);
 		return allContactsFeed;
	}
	
	
	/**
	 * provides matching contacts from the user's My Contact from Google contacts
	 * @param contactsService
	 * @param userEmail
	 * @param query
	 * @param map
	 * @return
	 * @throws IOException
	 * @throws ServiceException
	 */
	public static LinkedHashMap<String, String> getMatchingContacts(ContactsService contactsService , String userEmail , String query , LinkedHashMap<String, String>  map) 
			throws IOException, ServiceException{
		/*LinkedHashMap<String, String>  map = new LinkedHashMap<String, String>();*/
		ContactFeed allContactsFeed = getUserMyContacts(contactsService , userEmail);
		String res = "";
		for(ContactEntry contact : allContactsFeed.getEntries()){
			res = isMatching(query, contact);
			if(res.split("~")[0].equals("true")) {
				map.put(contact.getEmailAddresses().get(0).getAddress(),res.split("~")[1]);
			}
		}

		log.info("matching contacts:  "+map);
		return map;
		
	}
	
	
	private static String isMatching(String query ,ContactEntry entry ) {
		boolean result = false;
		String fullNameToDisplay ="No Name";
		if (entry.hasName()) {
  	      Name name = entry.getName();
  	      if (name.hasFullName()) {
  	         fullNameToDisplay = name.getFullName().getValue();
  	         String [] namesArr = fullNameToDisplay.split(" ");
  	        for (String str : namesArr) {
  	        	if (str.toLowerCase().startsWith(query.toLowerCase())) {
  	        		result = true;
  	        		break;
  	        	}}
  	      	}
  	      }
  	      if(!result && entry.hasEmailAddresses() && entry.getEmailAddresses().get(0).getAddress().startsWith(query.toLowerCase())){
  	        		result = true;
  	        		//break;
  	        	}	
		return result+"~"+fullNameToDisplay;
	}
}
