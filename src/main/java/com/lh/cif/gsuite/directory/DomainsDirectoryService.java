package com.lh.cif.gsuite.directory;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.Domains;
import com.google.api.services.admin.directory.model.Domains2;

public class DomainsDirectoryService extends GenericDirectoryService {

	private final Directory.Domains domains;

	public DomainsDirectoryService(final Credential credential) {
		super(credential);
		this.domains = this.directory.domains();
	}

	public Domains2 list(final String customer) throws Throwable {
		return execute(this.domains.list(customer));
	}

	public Domains get(final String customer, final String domainName) throws Throwable {
		return execute(this.domains.get(customer, domainName));
	}
}
