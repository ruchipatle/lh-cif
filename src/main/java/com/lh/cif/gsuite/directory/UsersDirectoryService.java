package com.lh.cif.gsuite.directory;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpBackOffUnsuccessfulResponseHandler;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserPhoto;
import com.google.api.services.admin.directory.model.Users;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UsersDirectoryService extends GenericDirectoryService {

	private HttpRequest request = null;

	private HttpResponse response = null;

	private static final int MAX_PAGE_RESULTS = 5000;

	private final Directory.Users users;

	public UsersDirectoryService(final Credential credential) {
		super(credential);
		this.users = this.directory.users();
	}

	public Users list(@Nullable final String domain, @Nullable final String pageToken) throws Throwable {
		return list(domain, pageToken, MAX_PAGE_RESULTS);
	}

	public Users list(@Nullable final String domain, @Nullable final String pageToken, final int maxResults) throws Throwable {
		return execute(this.users.list().setDomain(domain).setPageToken(pageToken).setMaxResults(maxResults));
	}

	public User get(@Nullable final String userEmail) throws Throwable {
		return execute(this.users.get(userEmail));
	}

	protected JsonBatchCallback<User> userDetailsCallback() {
		return new JsonBatchCallback<User>() {
			@Override
			public void onFailure(final GoogleJsonError e, final HttpHeaders responseHeaders) throws IOException {
				// Handle error
				log.error(e.getMessage());
			}

			@Override
			public void onSuccess(final User user, final HttpHeaders responseHeaders) throws IOException {
				log.info("User ID: " + user.getEmails());
			}
		};
	}

	private void executeBatch(final List<Member> memberIds) throws Throwable {
		JsonBatchCallback<User> callback = userDetailsCallback();
		BatchRequest batchRequest = this.directory.batch();
		for (Member member : memberIds) {
			this.directory.users().get(member.getEmail()).queue(batchRequest, callback);
		}
		execute(batchRequest);
	}

	public UserPhoto getUserPhoto(final String userEmail) {
		try {
			this.request = this.users.photos().get(userEmail).buildHttpRequest().setUnsuccessfulResponseHandler(new HttpBackOffUnsuccessfulResponseHandler(exponentialBackoff()));
			this.response = this.request.execute();
			if ("OK".equalsIgnoreCase(this.response.getStatusMessage())) {
				return response.parseAs(UserPhoto.class);
			}
		} catch (Exception e) {
			log.warn("Error getting user photo " + e.getMessage());
		}
		return null;
	}

	// Custom Implementation of Retryable
	public User getUserDetails(final String userEmail) {

		try {
			this.request = this.users.get(userEmail).buildHttpRequest().setUnsuccessfulResponseHandler(new HttpBackOffUnsuccessfulResponseHandler(exponentialBackoff()));
			this.response = this.request.execute();
			if ("OK".equalsIgnoreCase(this.response.getStatusMessage())) {
				return response.parseAs(User.class);
			}
		} catch (IOException e) {
			if (e instanceof GoogleJsonResponseException) {
				String ex = googleException((GoogleJsonResponseException) e);
				if (ex != null) {
					String content = parseString(ex, "content");
					String message = parseString(content, "message");
					log.info("Exception Get User details GoogleJsonResponseException " + message);
				} else {
					log.error("Exception Get User details Some Exception " + e);
				}
			} else {
				log.error("Exception Get User details Some Exception " + e);
			}
		}
		return null;
	}

	protected String googleException(final GoogleJsonResponseException ge) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(ge).toString();
		} catch (IOException e) {
			log.error("googleException handler exception " + e);
		}
		return null;
	}

	protected String parseString(final String exJson, final String key) {
		JsonObject jobj = new Gson().fromJson(exJson, JsonObject.class);
		return jobj.get(key) == null ? "" : jobj.get(key).getAsString();
	}

	private ExponentialBackOff exponentialBackoff() {
		return new ExponentialBackOff.Builder().setInitialIntervalMillis(500).setMaxElapsedTimeMillis(900000).setMaxIntervalMillis(6000).setMultiplier(1.5).setRandomizationFactor(0.5).build();
	}
	
	public Users getUsers(final String query, final String nextPageToken, final int maxResults) throws IOException {
		com.google.api.services.admin.directory.Directory.Users.List usersList = 
				this.directory.users().list().setFields("users(name,primaryEmail,suspended)").setCustomer("my_customer").setMaxResults(maxResults);
				//this.directory.users().list().setCustomer("my_customer").setMaxResults(maxResults);

		if(query != null) {
			usersList.setQuery(query);
		}
		if(nextPageToken != null || !"".equals(nextPageToken)) {
			usersList.setPageToken(nextPageToken);
		}
		return usersList.execute();
	} 
	
	/**
	 * Returns all the Groups present in the domain, with matching query
	 * @param query
	 * @param domain
	 * @param maxResult
	 * @return
	 * @throws IOException
	 */
	public Groups  getMatchingGroups(String query, String domain, int maxResult) throws IOException {
		return directory.groups().list().set("query", query).setFields("groups/email").setDomain(domain).setMaxResults(maxResult).execute();
	}
}
