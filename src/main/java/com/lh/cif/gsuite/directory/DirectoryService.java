package com.lh.cif.gsuite.directory;

import com.google.api.client.auth.oauth2.Credential;

public class DirectoryService {

	private final Credential credential;

	private DomainsDirectoryService domains;

	private UsersDirectoryService users;

	private MembersDirectoryService members;

	private GroupsDirectoryService groups;

	public DirectoryService(final Credential credential) {
		this.credential = credential;
	}

	public DomainsDirectoryService getDomains() {
		if (this.domains == null)
			this.domains = new DomainsDirectoryService(this.credential);
		return this.domains;
	}

	public UsersDirectoryService getUsers() {
		if (this.users == null)
			this.users = new UsersDirectoryService(this.credential);
		return this.users;
	}

	public MembersDirectoryService getMembers() {
		if (this.members == null)
			this.members = new MembersDirectoryService(this.credential);
		return this.members;
	}

	public GroupsDirectoryService getGroups() {
		if (this.groups == null)
			this.groups = new GroupsDirectoryService(this.credential);
		return this.groups;
	}

}
