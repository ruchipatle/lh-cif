package com.lh.cif.gsuite.directory;

import java.io.IOException;

import javax.annotation.Nullable;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.admin.directory.Directory.Groups;
import com.google.api.services.admin.directory.model.Group;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GroupsDirectoryService extends GenericDirectoryService {

	private static final int MAX_PAGE_RESULTS = 50;

	private final Groups groups;

	public GroupsDirectoryService(final Credential credential) {
		super(credential);
		this.groups = this.directory.groups();

	}

	public com.google.api.services.admin.directory.model.Groups list(@Nullable final String domain, @Nullable final String pageToken) throws Throwable {
		return list(domain, pageToken, MAX_PAGE_RESULTS);
	}

	public com.google.api.services.admin.directory.model.Groups list(@Nullable final String domain, @Nullable final String pageToken, final int maxResults) throws Throwable {
		return execute(this.groups.list().setDomain(domain).setPageToken(pageToken).setMaxResults(maxResults));
	}

	public com.google.api.services.admin.directory.model.Groups list(@Nullable final String userEmail, @Nullable final String domain, @Nullable final String pageToken, final int maxResults) throws Throwable {
		return execute(this.groups.list().setUserKey(userEmail).setDomain(domain).setPageToken(pageToken).setMaxResults(maxResults));
	}

	public com.google.api.services.admin.directory.model.Groups list(@Nullable final String userEmail) throws Throwable {
		return execute(this.groups.list().setUserKey(userEmail));
	}
	public void createGroup(Group group) throws IOException, Throwable {
		execute(this.groups.insert(group));
	}
}