package com.lh.cif.gsuite.directory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.Directory.Members.List;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.Members;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MembersDirectoryService extends GenericDirectoryService {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final int MAX_PAGE_RESULTS = 500;

	private final Directory.Members members;

	public MembersDirectoryService(final Credential credential) {
		super(credential);
		this.members = this.directory.members();
	}

	@SneakyThrows
	public Member get(@NonNull final String groupEmail, @NonNull final String userEmail) {
		return execute(this.members.get(groupEmail, userEmail));
	}

	@SneakyThrows
	public Members list(final String groupEmail, @Nullable final String pageToken) {
		return list(groupEmail, pageToken, MAX_PAGE_RESULTS);
	}

	public List list(final String groupEmail) throws IOException {
		return this.members.list(groupEmail);
	}

	@SneakyThrows
	public Members list(final String groupEmail, @Nullable final String pageToken, final int maxResults) {

		return execute(this.members.list(groupEmail).setGroupKey(groupEmail).setPageToken(pageToken).setMaxResults(maxResults));
	}

	@SneakyThrows
	public Boolean isMember(@NonNull final String groupEmail, @NonNull final String userEmail) {
		try {

			return execute(this.members.get(groupEmail, userEmail)) != null;
		} catch (Throwable t) {
			if (t instanceof GoogleJsonResponseException) {
				GoogleJsonResponseException g = (GoogleJsonResponseException) t;
				if (g.getStatusCode() == 404) {
					return false;
				}
			}
			throw t;
		}

	}

	@SneakyThrows
	public Void delete(final String groupEmail, final String email) {
		return execute(this.members.delete(groupEmail, email));
	}

	@SneakyThrows
	public Member insert(final String googleGroupId, final Member member) {
		return execute(this.members.insert(googleGroupId, member));
	}

	@SneakyThrows
	public Set<String> listAll(final String groupEmail) {
		Set<String> results = new HashSet<>();
		String next = null;
		do {
			Members members = list(groupEmail, next);
			if (members.getMembers() != null) {
				for (Member member : members.getMembers()) {
					results.add(member.getEmail());
				}
				next = members.getNextPageToken();
			} else {
				next = null;
			}
		} while (next != null);
		return results;
	}
}
