package com.lh.cif.gsuite.directory;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.admin.directory.Directory;
import com.lh.cif.gsuite.util.RetrybleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GenericDirectoryService extends RetrybleService {

	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	protected Directory directory;

	public GenericDirectoryService(final Credential credential) {
		this.directory = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName("corp-search-doc-library").build();
	}

}
