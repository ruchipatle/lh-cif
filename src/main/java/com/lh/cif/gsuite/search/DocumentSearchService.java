/**
 * This class handles all the interaction with Google AppEngine Search API. Currently, it has
 * methods for: 1. Index a document i.e. add a document to the existing index 2. Retrieve a Document
 * from the Index, provided its Document Id (In our case it is the user id) 3. Remove a Document
 * from the Index, provided its Document Id 4. Retrieve a List of Documents from the Index that
 * match the Search Term.
 * 
 * Retrieve the list of Documents that match the SearchText entered. Results<ScoredDocument> results
 * = SearchIndexManager.INSTANCE.retrieveDocuments(indexName,searchText);
 */
package com.lh.cif.gsuite.search;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.GetRequest;
import com.google.appengine.api.search.GetResponse;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortExpression.SortDirection;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.StatusCode;
import com.lh.cif.core.domain.dto.DocumentSearchDto;
import com.lh.cif.gsuite.util.RetrybleService;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class DocumentSearchService extends RetrybleService {

	public DocumentSearchService() {
		super();
	}

	/**
	 * This method is used to add a Document to a particular Index
	 * 
	 * @param indexName
	 *            This is the name of the Index to which the document is to be
	 *            added. An index serves as a logical collection of documents
	 * @param document
	 *            This is the Document instance that needs to be added to the Index
	 */
	public void indexDocument(String indexName, Document document) {

		// Setup the Index
		Index index = indexBuilder(indexName);

		try {
			// Put the Document in the Index. If the document is already existing, it will
			// be overwritten
			index.put(document);
		} catch (PutException e) {
			if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
				// retry putting the document
				indexDocument(indexName, document);
			}
		}
	}

	/**
	 * This method is used to retrieve a particular Document from the Index
	 * 
	 * @param documentId
	 *            This is the key field that uniquely identifies a document in the
	 *            collection i.e. the Index.
	 * @return An instance of the Document object from the Index.
	 */
	public Document retrieveDocument(String indexName, String documentId) {
		// Setup the Index
		Index index = indexBuilder(indexName);

		// Retrieve the Record from the Index
		return index.get(documentId);
	}

	/**
	 * This method is used to retrieve a list of documents from the Index that match
	 * the Search Term.
	 * 
	 * @param searchText
	 *            The search term to find matching documents. By default, if you do
	 *            not use the Search Language Syntax, it will retrieve all the
	 *            records that contain a partial or full text match for all
	 *            attributes of a document
	 * @return A collection of Documents that were found
	 */
	public Results<ScoredDocument> retrieveAllDocument(DocumentSearchDto documentSearchDto) {

		// Setup Index
		Index index = indexBuilder(documentSearchDto.getIndexName());
         log.info("index**************************"+index);
		// Build Cursor
		Cursor cursor = cursorBuilder(documentSearchDto.getCursorWebSafeString());
		log.info("cursor in Document **********"+cursor.toString());
		// Build Sort Options
		SortOptions sortOptions = sortOptionsBuilder(documentSearchDto.getSortColumn(), documentSearchDto.getSortDirection(), documentSearchDto.getLimit());

		// Build Query
		Query query = queryBuilder(documentSearchDto.getQueryString(), cursor, sortOptions);

		// Retrieve the Documents from the Index
		return index.search(query);

	}

	/**
	 * This method is used to delete a document from the Index
	 * 
	 * @param documentId
	 *            This is the key field that uniquely identifies a document in the
	 *            collection i.e. the Index.
	 */

	public void deleteDocumentFromIndex(String indexName, String documentId) {
		// Setup the Index
		Index index = indexBuilder(indexName);
		// Delete the Records from the Index
		index.delete(documentId);
	}

	/**
	 * This method is used to delete all document from an Index
	 * 
	 * @param documentId
	 *            This is the key field that uniquely identifies a document in the
	 *            collection i.e. the Index.
	 */

	public void deleteAllDocumentFromIndex(String indexName) {
		Index index = indexBuilder(indexName);
		while (true) {
			List<String> docIds = new ArrayList<String>(); // Return a set of doc_ids.
			GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
			GetResponse<Document> response = index.getRange(request);
			if (response.getResults().isEmpty()) {
				break;
			}
			for (Document newDoc : response) {
				docIds.add(newDoc.getId());
			}
			index.delete(docIds);
		}
	}

	protected Cursor cursorBuilder(String cursorWebSafeString) {

		if (cursorWebSafeString != null && !cursorWebSafeString.isEmpty()) {
			return Cursor.newBuilder().build(cursorWebSafeString);
		} else {
			return Cursor.newBuilder().build();
		}
	}

	protected SortExpression sortExpressionBuilder(String sortColumn, String sortDirection) {
		SortDirection direction;
		if (sortDirection.equalsIgnoreCase("desc")) {
			direction = SortExpression.SortDirection.DESCENDING;
		} else {
			direction = SortExpression.SortDirection.ASCENDING;
		}
		return SortExpression.newBuilder().setExpression(sortColumn).setDirection(direction).build();
	}

	protected SortOptions sortOptionsBuilder(String sortColumn, String sortDirection, int limit) {
		SortExpression sortExpression = sortExpressionBuilder(sortColumn, sortDirection);
		return SortOptions.newBuilder().addSortExpression(sortExpression).setLimit(limit).build();
	}

	protected Index indexBuilder(String indexName) {
		IndexSpec indexSpec = IndexSpec.newBuilder().setName(indexName).build();
		return SearchServiceFactory.getSearchService().getIndex(indexSpec);
	}

	protected Query queryBuilder(String queryString, Cursor cursor, SortOptions sortOptions) {

		QueryOptions queryOptions = QueryOptions.newBuilder().setCursor(cursor).setLimit(sortOptions.getLimit()).setNumberFoundAccuracy(10000).setSortOptions(sortOptions).build();

		return Query.newBuilder().setOptions(queryOptions).build(queryString);
	}

	/**
	 * This method is used to retrieve a total number of documents from the Index
	 * that match the Search Term.
	 * 
	 * @param searchText
	 *            The search term to find matching documents. By default, if you do
	 *            not use the Search Language Syntax, it will retrieve all the
	 *            records that contain a partial or full text match for all
	 *            attributes of a document
	 * @return A total count of Documents that were found
	 */
	public int getTotalCount(DocumentSearchDto documentSearchDto) {

		// Setup Index
		Index index = indexBuilder(documentSearchDto.getIndexName());

		// Build Sort Options
		SortOptions sortOptions = sortOptionsBuilder(documentSearchDto.getSortColumn(), documentSearchDto.getSortDirection(), documentSearchDto.getLimit());
		Cursor cursor_count = Cursor.newBuilder().build();
		int count = 0;
		// Retrieve the total count of Documents from the Index in 1000 slot
		do {
			// Build Cursor
			Query query = queryBuilder(documentSearchDto.getQueryString(), cursor_count, sortOptions);
			Results<ScoredDocument> results = index.search(query);
			log.info("resutl of doc"+results.getNumberReturned());
			for (ScoredDocument doc : results) {
				count++;
		
			}
			cursor_count = results.getCursor();
		} while (cursor_count != null);
		log.info("count*******************************"+count);
		return count;

	}

}
