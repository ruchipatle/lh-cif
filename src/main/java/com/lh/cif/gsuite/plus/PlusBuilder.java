package com.lh.cif.gsuite.plus;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;

public class PlusBuilder {

	private static PlusBuilder instance;

	private final HttpTransport transport;

	private final JsonFactory jsonFactory;

	private PlusBuilder() {
		this.transport = new NetHttpTransport();
		this.jsonFactory = new JacksonFactory();
	}

	public static PlusBuilder getInstance() {
		if (instance == null) {
			instance = new PlusBuilder();
		}
		return instance;
	}

	public Plus buildPlusService(final Credential credential) {
		return new Plus.Builder(this.transport, this.jsonFactory, credential).setApplicationName("CIF").build();
	}

}
