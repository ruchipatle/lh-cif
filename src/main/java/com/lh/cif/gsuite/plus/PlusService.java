package com.lh.cif.gsuite.plus;

import java.io.IOException;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.lh.cif.gsuite.util.RetrybleService;

/**
 * Plus Service to interact with Google Plus Low Level API
 *
 * @author anbharti
 *
 */
public class PlusService extends RetrybleService {

	private final Credential credential;

	protected Plus plus;

	public PlusService(final Credential credential) {
		this.credential = credential;
		initPlus();
	}

	private Plus initPlus() {
		if (this.plus == null)
			this.plus = PlusBuilder.getInstance().buildPlusService(this.credential);
		return this.plus;
	}

	public Person getPerson(final String userIdentity) throws IOException {
		return this.plus.people().get(userIdentity).execute();
	}

}
