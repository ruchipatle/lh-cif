package com.lh.cif.gsuite.script;

import java.io.IOException;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.script.Script;
import com.google.api.services.script.model.ExecutionRequest;
import com.google.api.services.script.model.Operation;
import com.lh.cif.gsuite.util.RetrybleService;

/**
 * Google AppScript Service to interact with Google Script Service Low Level API
 *
 * @author anbharti
 *
 */
public class ScriptService extends RetrybleService {

	private final Credential credential;

	protected Script script;

	public ScriptService(final Credential credential) {
		this.credential = credential;
		initScript();
	}

	private Script initScript() {
		if (this.script == null)
			this.script = ScriptBuilder.getInstance().buildScriptService(this.credential);
		return this.script;
	}

	public Operation execute(final String scriptId, final String functionName, final List<Object> params) throws IOException {
		ExecutionRequest request = new ExecutionRequest().setFunction(functionName).setParameters(params).setDevMode(true);
		return this.script.scripts().run(scriptId, request).execute();
	}

}
