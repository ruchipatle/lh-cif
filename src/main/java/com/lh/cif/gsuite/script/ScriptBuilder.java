package com.lh.cif.gsuite.script;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.script.Script;

public class ScriptBuilder {

	private static ScriptBuilder instance;

	private final HttpTransport transport;

	private final JsonFactory jsonFactory;

	private ScriptBuilder() {
		this.transport = new NetHttpTransport();
		this.jsonFactory = new JacksonFactory();
	}

	public static ScriptBuilder getInstance() {
		if (instance == null) {
			instance = new ScriptBuilder();
		}
		return instance;
	}

	public Script buildScriptService(final Credential credential) {
		return new Script.Builder(this.transport, this.jsonFactory, credential).setApplicationName("CIF").build();
	}

}
