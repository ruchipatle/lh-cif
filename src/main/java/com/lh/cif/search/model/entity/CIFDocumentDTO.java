package com.lh.cif.search.model.entity;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class CIFDocumentDTO {

    Long id;

    String mainCategory;

    String section;

    String plant;

    String name;

    String label;

    String description;

    String documentId;

    String documentLink;

    String docType;

    String tags;

    String keyTopics;

    int likesCount;

    int viewCounts;

    String owner;

    String dataSource;

    String previewURL;

    String thumbnailURL;

}