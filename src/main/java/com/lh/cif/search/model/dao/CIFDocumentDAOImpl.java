package com.lh.cif.search.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.lh.cif.gsuite.datastore.DatastoreWorker;
import com.lh.cif.search.model.entity.CIFDocument;

@Repository
class RecordDAOImpl implements CIFDocumentDAO {

	private DatastoreWorker<CIFDocument> record;
	// private DatastoreProjectionWorker datastoreRecord;

	public RecordDAOImpl() {
		super();
		NamespaceManager.set("CIF");

		this.record = new DatastoreWorker<>(CIFDocument.class);
	}

	@Override
	public void delete(Long id) {
		this.record.deleteById(id);
	}

	@Override
	public List<CIFDocument> findAll() {
		return this.record.list();
	}

	@Override
	public List<CIFDocument> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.record.list(columnValueMap);
	}

	@Override
	public List<CIFDocument> findAll(List<Long> ids) {
		return new ArrayList<CIFDocument>(this.record.getAllByIds(ids));
	}

	// repots module methods
	@Override
	public QueryResultIterator<CIFDocument> findByColumnAndValuePaginated(Map<String, Object> columnValueMap,
			Integer range, String startPosition) {
		return this.record.listEntitiesPaginated(columnValueMap, range, startPosition);
	}

	@Override
	public List<CIFDocument> findByRecordCreator(String creatorEmail) {
		return this.record.getBy("createdBy", creatorEmail);
	}

	@Override
	public List<Entity> fetchSelectedColumnValue(String kind, Map<String, Class<?>> columnTypeMap,
			Map<String, Object> columnValueMap, Boolean distinctFlag) {
		return this.record.fetchSelectedColumnValue(kind, columnTypeMap, columnValueMap, distinctFlag);
	}

	@Override
	public CIFDocument find(String id) {
		return this.record.get(id);
	}

	@Override
	public void delete(String id) {
		this.record.deleteById(id);
	}

	@Override
	public CIFDocument getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.record.get(columnValueMap);
	}

	@Override
	public CIFDocument saveOrUpdate(CIFDocument newInstance) {
		return this.record.save(newInstance);
	}

	@Override
	public CIFDocument find(Long id) {
		return this.record.get(id);
	}

}
