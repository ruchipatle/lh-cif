package com.lh.cif.search.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.lh.cif.gsuite.datastore.DatastoreWorker;
import com.lh.cif.search.model.entity.DocumentLikes;

@Repository
class DocumentLikesDAOImpl implements DocumentLikesDAO {

    private DatastoreWorker<DocumentLikes> record;
    // private DatastoreProjectionWorker datastoreRecord;

    public DocumentLikesDAOImpl() {
        super();
        NamespaceManager.set("CIF");

        this.record = new DatastoreWorker<>(DocumentLikes.class);
    }

    @Override
    public void delete(Long id) {
        this.record.deleteById(id);
    }

    @Override
    public List<DocumentLikes> findAll() {
        return this.record.list();
    }

    @Override
    public List<DocumentLikes> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap) {
        return this.record.list(columnValueMap);
    }

    @Override
    public List<DocumentLikes> findAll(List<Long> ids) {
        return new ArrayList<DocumentLikes>(this.record.getAllByIds(ids));
    }

    // repots module methods
    @Override
    public QueryResultIterator<DocumentLikes> findByColumnAndValuePaginated(Map<String, Object> columnValueMap, Integer range, String startPosition) {
        return this.record.listEntitiesPaginated(columnValueMap, range, startPosition);
    }

    @Override
    public List<DocumentLikes> findByRecordCreator(String creatorEmail) {
        return this.record.getBy("createdBy", creatorEmail);
    }

    @Override
    public List<Entity> fetchSelectedColumnValue(String kind, Map<String, Class<?>> columnTypeMap, Map<String, Object> columnValueMap, Boolean distinctFlag) {
        return this.record.fetchSelectedColumnValue(kind, columnTypeMap, columnValueMap, distinctFlag);
    }

    @Override
    public DocumentLikes find(String id) {
        return this.record.get(id);
    }

    @Override
    public void delete(String id) {
        this.record.deleteById(id);
    }

    @Override
    public DocumentLikes getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap) {
        return this.record.get(columnValueMap);
    }

    @Override
    public DocumentLikes saveOrUpdate(DocumentLikes newInstance) {
        return this.record.save(newInstance);
    }

    @Override
    public DocumentLikes find(Long id) {
        return this.record.get(id);
    }

}
