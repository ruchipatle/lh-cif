package com.lh.cif.search.model.entity;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class UploadFileSourceDataDTO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    String documentId;

    String documentLink;
    
    String documentName;

}
