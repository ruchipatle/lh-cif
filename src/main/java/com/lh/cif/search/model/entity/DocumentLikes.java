package com.lh.cif.search.model.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

@Entity
@ToString
@Data
public class DocumentLikes implements Serializable, Cloneable, Comparable<DocumentLikes> {

    private static final long serialVersionUID = 1L;

    @Id
    @Index
    Long id;

    @Index
    Long documentId;

    @Index
    String userId;

    Date createdDate;

    // only for view purpose
    transient String likedOn;

    public String getLikedOn() {
        if (this.createdDate == null) {
            this.likedOn = "";
        } else {
            this.likedOn = new SimpleDateFormat("MMM dd yyyy hh:mm:ss zzz").format(this.createdDate);
        }
        return this.likedOn;
    }

    @Override
    public int compareTo(DocumentLikes o) {
        if (o.getCreatedDate() == null) {
            return 1;
        }
        if (this.createdDate == null) {
            return -1;
        }
        return o.getCreatedDate().compareTo(this.getCreatedDate());
    }    

}
