package com.lh.cif.search.model.dao;

import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;
import com.lh.cif.core.repository.GenericDao;
import com.lh.cif.search.model.entity.DocumentLikes;

public interface DocumentLikesDAO extends GenericDao<DocumentLikes> {

    public List<DocumentLikes> findByRecordCreator(final String creatorEmail);

    public List<Entity> fetchSelectedColumnValue(String kind, Map<String, Class<?>> columnTypeMap, Map<String, Object> columnValueMap, Boolean distinctFlag);
}
