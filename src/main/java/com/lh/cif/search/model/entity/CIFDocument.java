package com.lh.cif.search.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

@Entity
@ToString
@Data
public class CIFDocument implements Serializable, Cloneable, Comparable<CIFDocument> {

    private static final long serialVersionUID = 1L;

    @Id
    @Index
    Long id;

    @Index
    String name;

    @Index
    String label;

    @Index
    String description;

    @Index
    String documentId;

    @Index
    String documentLink;

    @Index
    String docType;

    @Index
    List<String> tags;

    @Index
    List<String> keyTopics;

    @Index
    String plant;

    @Index
    String section;

    @Index
    String main;

    @Index
    int likesCount = 0;

    @Index
    int viewsCount = 0;

    @Index
    String dataSource;

    @Index
    String previewURL;

    @Index
    String thumbnailURL;

    String createdBy;

    Date createdDate;

    String modifiedBy;

    Date modifiedDate;

    public boolean contains(final String term) {
        if (this.toString().toLowerCase().contains(term.toLowerCase())) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(CIFDocument doc) {
        if (this.getLikesCount() == doc.getLikesCount()) {
            return (doc.getViewsCount() - this.getViewsCount());
        } else {
            return (doc.getLikesCount() - this.getLikesCount());
        }
    }

}
