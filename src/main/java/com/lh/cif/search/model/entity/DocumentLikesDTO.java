package com.lh.cif.search.model.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class DocumentLikesDTO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    Long id;

    Long documentId;
    
    boolean liked;

    String userId;

    Date createdDate;

}
