package com.lh.cif.search.model.entity;

import java.io.Serializable;
import java.util.Date;

import com.google.api.client.util.DateTime;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class DocumentCommentsDTO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    Long id;

    Long documentId;

    String comment;

    String userId;
    
    String userName;

    Date createdDate;

}