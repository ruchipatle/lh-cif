package com.lh.cif.search.web;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.DocumentComments;
import com.lh.cif.search.model.entity.DocumentCommentsDTO;
import com.lh.cif.search.model.entity.DocumentLikes;
import com.lh.cif.search.model.entity.DocumentLikesDTO;
import com.lh.cif.search.service.SearchService;
import com.lh.cif.search.service.SearchedCIFDocument;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin("*")
@RequestMapping("/emea")
public class CIFIntegrationController {
    
    @Autowired
    SearchService searchService;

    @Autowired
    MasterDataService masterService;
    
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> searchString(final ServletRequest request, final ServletResponse response, @RequestParam String searchTerm) {

        NamespaceManager.set("CIF");

        final Map<String, Object> map = new HashMap<String, Object>();

        final HttpSession session = ((HttpServletRequest) request).getSession();

        final List<MasterData> datasources;
        final List<MasterData> docTypes;

        final String dataSource = request.getParameter("dataSource");
        final String docType = request.getParameter("docType");

        if (session.getAttribute("CIFDatasource") == null) {
            datasources = masterService.getMasterDataByLookupType(MasterDataService.LOOKUP_TYPE_DATASOURCE);
            session.setAttribute("CIFDatasource", datasources);
        } else {
            datasources = (List<MasterData>) session.getAttribute("CIFDatasource");
        }
        if (session.getAttribute("CIFDocType") == null) {
            docTypes = masterService.getMasterDataByLookupType(MasterDataService.LOOKUP_TYPE_DOC_TYPE);
            session.setAttribute("CIFDocType", docTypes);
        } else {
            docTypes = (List<MasterData>) session.getAttribute("CIFDocType");
        }

        map.put("dataSource", datasources);
        map.put("documentType", docTypes);
        map.put("selectedDataSource", dataSource);
        map.put("selectedDocumentType", docType);

        Set<SearchedCIFDocument> searchResult = new TreeSet<SearchedCIFDocument>();
        if (searchTerm != null && !searchTerm.trim().isEmpty()) {
            final List<CIFDocument> srcData = searchService.getAllDocsOfGivenFilter(dataSource, docType);
            searchResult = searchService.search(srcData, searchTerm);
        }
        map.put("searchResult", searchResult);

        try {
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }
    
    public void sendMail(final String sendeerEmail, final String senderName, final List<String> recipientsTO,
            final List<String> recipientsCC, final List<String> recipientsBCC, final String subject,
            final String htmlBody) throws IOException, MessagingException {

        Properties props = new Properties();
        Session session = Session.getInstance(props,null);
        Multipart mp = new MimeMultipart(); 
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(htmlBody, "text/html");
        mp.addBodyPart(htmlPart);
        
        Message msg = new MimeMessage(session); 
        msg.setFrom(new InternetAddress(sendeerEmail, senderName));

        InternetAddress[] addressesTO = new InternetAddress[recipientsTO.size()];

        int indexTO = 0; for (String recipientTO : recipientsTO) {
            addressesTO[indexTO] = new InternetAddress(recipientTO, recipientTO);
            indexTO= indexTO + 1; 
        }

        if (recipientsCC != null && !recipientsCC.isEmpty()) {
            InternetAddress[] addressesCC = new InternetAddress[recipientsCC.size()];
            int indexCC = 0; 
            for (String recipientCC : recipientsCC) {
                addressesCC[indexCC] = new InternetAddress(recipientCC, recipientCC);
                indexCC= indexCC + 1; 
            } 

            msg.setRecipients(Message.RecipientType.CC, addressesCC); 
        }
        msg.setRecipients(Message.RecipientType.TO, addressesTO); String
        subjectEncoded = MimeUtility.encodeText(subject,StandardCharsets.UTF_8.name(), "Q"); 
        msg.setSubject(subjectEncoded);
        msg.setContent(mp);
        Transport.send(msg);
    }
    
    @RequestMapping(value = "/saveComment", method = RequestMethod.POST, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentComments>> saveComment(final ServletRequest request, final ServletResponse response, @RequestBody DocumentCommentsDTO docComment) {

        NamespaceManager.set("CIF");

        log.info("Entering save comment : " + docComment);

        HttpSession session = ((HttpServletRequest) request).getSession();
        /*
         * UserInfoSession user = (UserInfoSession)
         * session.getAttribute("userInfoSession");
         */
        
        log.info("docComment.getUserId()  "+docComment.getUserId());
        
        log.info("docComment.getUserName()  "+docComment.getUserName());

        docComment.setUserId(request.getParameter("email"));
        docComment.setUserName(request.getParameter("name"));

        DocumentComments doc = searchService.saveDocumentComments(docComment);
        List<DocumentComments> commentsList = searchService.getCommentsOfDocument(doc.getDocumentId());

        try {
            return new ResponseEntity<List<DocumentComments>>(commentsList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<DocumentComments>>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @RequestMapping(value = "/listLikedBy", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentLikes>> getLikedBy(final ServletRequest request, final ServletResponse response, @RequestParam Long docId) {

        NamespaceManager.set("CIF");

        if (docId != null) {

            List<DocumentLikes> likesList = searchService.getLikesOfDocument(docId);
            if (likesList != null & !likesList.isEmpty()) {
                Collections.sort(likesList);
            }
            try {
                return new ResponseEntity<List<DocumentLikes>>(likesList, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
            }

        } else {
            return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @RequestMapping(value = "/likeDocument", method = RequestMethod.POST, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentLikes>> saveLike(final ServletRequest request, final ServletResponse response, @RequestBody DocumentLikesDTO docLike) {

        NamespaceManager.set("CIF");
        /*
         * HttpSession session = ((HttpServletRequest) request).getSession();
         * 
         * UserInfoSession user = (UserInfoSession)
         * session.getAttribute("userInfoSession");
         */

        String userId = request.getParameter("email");

        if (docLike == null) {
            return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
        } else {
            searchService.saveDocumentLikes(docLike.getDocumentId(), docLike.isLiked(), userId);
            List<DocumentLikes> likesList = searchService.getLikesOfDocument(docLike.getDocumentId(), userId);

            try {
                return new ResponseEntity<List<DocumentLikes>>(likesList, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
            }
        }
    }
    
    @RequestMapping(value = "/viewDocument/{cifDocId}", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> getCIFDocumentDataForView(final ServletRequest request, final ServletResponse response, @PathVariable("cifDocId") Long id) {

        NamespaceManager.set("CIF");

        /*
         * HttpSession session = ((HttpServletRequest) request).getSession();
         * 
         * UserInfoSession user = (UserInfoSession)
         * session.getAttribute("userInfoSession");
         */

        String userId = request.getParameter("email");
        
        log.info("User ID "+userId);

        final Map<String, Object> documentMap = new HashMap<String, Object>();

        CIFDocument document = searchService.getCIFDocument(id);
        if (document == null) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        } else {
            List<DocumentComments> commentsList = searchService.getCommentsOfDocument(id);
            if (commentsList == null) {
                commentsList = new ArrayList<DocumentComments>();
            }

            final List<DocumentLikes> likes = searchService.getLikesOfDocument(id, userId);

            // Increase view count in CIFDocument entity
            document = searchService.increaseViewCountCIFDoc(id);

            documentMap.put("record", document);
            documentMap.put("comments", commentsList);
            documentMap.put("like", (likes != null && !likes.isEmpty())); // This user liked this document
        }

        try {
            return new ResponseEntity<Map<String, Object>>(documentMap, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }

}
