package com.lh.cif.search.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.DocumentComments;
import com.lh.cif.search.service.SearchService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/cif/library")
public class CIFDocumentUIController {

    @Autowired
    SearchService searchService;

    @Autowired
    MasterDataService masterService;

    @RequestMapping("/manage")
    public String getAllData(final ServletRequest request, final ServletResponse response, final Model map) {

        NamespaceManager.set("CIF");

        final List<CIFDocument> srcData = searchService.getAllDocuments();

        map.addAttribute("cifDataList", srcData);

        return "/WEB-INF/views/record/manage.jsp";
    }

    @RequestMapping("/create")
    public String createCifDocument(final ServletRequest request, final ServletResponse response, final Model map) {

        NamespaceManager.set("CIF");

        final List<MasterData> plants = masterService.getMasterDataByLookupType("Plant");
        final List<MasterData> main = masterService.getMasterDataByLookupType("Main");
        final List<MasterData> sections = masterService.getMasterDataByLookupType("Section");
        final List<MasterData> keyTopics = masterService.getMasterDataByLookupType("Key Topic");
        final List<MasterData> tags = masterService.getMasterDataByLookupType("Tag");
        final List<MasterData> docTypes = masterService.getMasterDataByLookupType("Document Type");
        final List<MasterData> datasource = masterService.getMasterDataByLookupType("DataSource");

        map.addAttribute("Plant", plants);
        map.addAttribute("DataSource", datasource);
        map.addAttribute("Main", main);
        map.addAttribute("Section", sections);
        map.addAttribute("KeyTopic", keyTopics);
        map.addAttribute("Tag", tags);
        map.addAttribute("DocumentType", docTypes);
        map.addAttribute("action", "CREATE");

        map.addAttribute("keyTopicsJson", "[]");
        map.addAttribute("tagsJson", "[]");

        return "/WEB-INF/views/record/create.jsp";
    }

    @RequestMapping(value = "/edit/{cifDocId}", params = { "mode=edit" }, method = RequestMethod.GET)
    public String editCIFDocument(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @PathVariable("cifDocId") Long id, Model map) {

        NamespaceManager.set("CIF");

        final List<MasterData> plants = masterService.getMasterDataByLookupType("Plant");
        final List<MasterData> main = masterService.getMasterDataByLookupType("Main");
        final List<MasterData> sections = masterService.getMasterDataByLookupType("Section");
        final List<MasterData> keyTopics = masterService.getMasterDataByLookupType("Key Topic");
        final List<MasterData> tags = masterService.getMasterDataByLookupType("Tag");
        final List<MasterData> docTypes = masterService.getMasterDataByLookupType("Document Type");
        final List<MasterData> datasource = masterService.getMasterDataByLookupType("DataSource");

        map.addAttribute("Plant", plants);
        map.addAttribute("DataSource", datasource);
        map.addAttribute("Main", main);
        map.addAttribute("Section", sections);
        map.addAttribute("KeyTopic", keyTopics);
        map.addAttribute("Tag", tags);
        map.addAttribute("DocumentType", docTypes);

        map.addAttribute("action", "EDIT");

        CIFDocument doc = searchService.getCIFDocument(id);

        map.addAttribute("cifDocRecord", doc);

        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(doc.getKeyTopics());
            if (json.equals("")) {
                json = "[]";
            }
            map.addAttribute("keyTopicsJson", json);

            String jsonTag = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(doc.getTags());
            if (jsonTag.equals("")) {
                jsonTag = "[]";
            }
            map.addAttribute("tagsJson", jsonTag);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "/WEB-INF/views/record/create.jsp";
    }

    /**
     * Opens the edit comment page
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * @param session  - Session
     * @param id       - comment ID
     * @param map      - Model
     * 
     * @return the view
     */
    @RequestMapping(value = "/editComment/{cifDocId}", method = RequestMethod.GET)
    public String editComment(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @PathVariable("cifDocId") Long id, Model map) {

        NamespaceManager.set("CIF");

        List<DocumentComments> docComments = searchService.getCommentsOfDocument(id);

        if (docComments == null) {
            docComments = new ArrayList<DocumentComments>();
        }

        map.addAttribute("docComments", docComments);

        return "/WEB-INF/views/record/editComments.jsp";
    }

    /**
     * Opens the upload Data page
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * @param session  - Session
     * @param map      - Model
     * 
     * @return the view
     */
    @RequestMapping(value = "/uploadDataView", method = RequestMethod.GET)
    public String uploadSourceData(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, Model map) {
        return "/WEB-INF/views/record/upload.jsp";
    }

}
