package com.lh.cif.search.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.DocumentComments;
import com.lh.cif.search.model.entity.DocumentCommentsDTO;
import com.lh.cif.search.model.entity.DocumentLikes;
import com.lh.cif.search.model.entity.DocumentLikesDTO;
import com.lh.cif.search.service.SearchService;
import com.lh.cif.search.service.SearchedCIFDocument;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin("*")
@RequestMapping("/cif/api/search")
public class SearchRest {

    @Autowired
    SearchService searchService;

    @Autowired
    MasterDataService masterService;

    /**
     * Gets the login User Data
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/userData", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity getUserData(final ServletRequest request, final ServletResponse response) {

        NamespaceManager.set("CIF");

        HttpSession session = ((HttpServletRequest) request).getSession();
        UserInfoSession user = (UserInfoSession) session.getAttribute("userInfoSession");
        log.info("User in userData : " + user);

        try {
            return new ResponseEntity(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves all the CIFDocument entity records in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<Map<String, Object>>> getAllData(final ServletRequest request, final ServletResponse response) {

        NamespaceManager.set("CIF");

        final List<MasterData> sections = masterService.getMasterDataByLookupType("Section");
        final List<CIFDocument> srcData = searchService.getAllDocuments();

        CIFDocument doc;
        String section;
        List<CIFDocument> records;

        final Iterator<CIFDocument> iterator = srcData.iterator();
        final Map<String, List<CIFDocument>> sectionMap = new HashMap<String, List<CIFDocument>>();

        while (iterator.hasNext()) {
            doc = iterator.next();
            section = doc.getSection();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);
            } else {
                records = new ArrayList<CIFDocument>();
            }
            records.add(doc);
            sectionMap.put(section, records);
        }

        MasterData data;
        Map<String, Object> dataMap;

        final Iterator<MasterData> iteratorM = sections.iterator();
        final List<Map<String, Object>> finalDataList = new ArrayList<Map<String, Object>>();
        while (iteratorM.hasNext()) {
            data = iteratorM.next();
            section = data.getDisplayValue();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);

                dataMap = new HashMap<String, Object>();
                dataMap.put("section", section);
                dataMap.put("records", records);

                finalDataList.add(dataMap);
            }
        }

        try {
            return new ResponseEntity<List<Map<String, Object>>>(finalDataList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Map<String, Object>>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves all the CIFDocument entity records in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/limitedSectionData", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<Map<String, Object>>> getLimitedSectionData(final ServletRequest request, final ServletResponse response) {

        NamespaceManager.set("CIF");

        final List<MasterData> sections = masterService.getMasterDataByLookupType("DataSource");
        final List<CIFDocument> srcData = searchService.getAllDocuments();

        CIFDocument doc;
        String section;
        List<CIFDocument> records;

        final Iterator<CIFDocument> iterator = srcData.iterator();
        final Map<String, List<CIFDocument>> sectionMap = new HashMap<String, List<CIFDocument>>();

        while (iterator.hasNext()) {
            doc = iterator.next();
            section = doc.getDataSource();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);
                if (records.size() == 5) {
                    continue;
                }
            } else {
                records = new ArrayList<CIFDocument>();
            }
            records.add(doc);
            sectionMap.put(section, records);
        }

        MasterData data;
        Map<String, Object> dataMap;

        final Iterator<MasterData> iteratorM = sections.iterator();
        final List<Map<String, Object>> finalDataList = new ArrayList<Map<String, Object>>();
        while (iteratorM.hasNext()) {
            data = iteratorM.next();
            section = data.getDisplayValue();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);

                dataMap = new HashMap<String, Object>();
                dataMap.put("section", section);
                dataMap.put("records", records);

                finalDataList.add(dataMap);
            }
        }

        try {
            return new ResponseEntity<List<Map<String, Object>>>(finalDataList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Map<String, Object>>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves all the CIFDocument entity records in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/sectionData", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> getSectionData(final ServletRequest request, final ServletResponse response, @RequestParam String section) {

        NamespaceManager.set("CIF");

        if (section != null && !section.isEmpty()) {
            List<CIFDocument> srcData = searchService.getAllDocumentsOfSection(section);

            if (srcData == null) {
                srcData = new ArrayList<CIFDocument>();
            }

            Map<String, Object> dataMap;
            dataMap = new HashMap<String, Object>();
            dataMap.put("section", section);
            dataMap.put("records", srcData);

            try {
                return new ResponseEntity<Map<String, Object>>(dataMap, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves the CIFDocument entity record in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * @param id       - CIF document ID
     * 
     * @return the CIFDocument entity record in specific JSON format as response
     */
    @RequestMapping(value = "/viewDocument/{cifDocId}", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> getCIFDocumentDataForView(final ServletRequest request, final ServletResponse response, @PathVariable("cifDocId") Long id) {

        NamespaceManager.set("CIF");

        HttpSession session = ((HttpServletRequest) request).getSession();

        UserInfoSession user = (UserInfoSession) session.getAttribute("userInfoSession");

        String userId = user.getUserEmail();

        final Map<String, Object> documentMap = new HashMap<String, Object>();

        CIFDocument document = searchService.getCIFDocument(id);
        if (document == null) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        } else {
            List<DocumentComments> commentsList = searchService.getCommentsOfDocument(id);
            if (commentsList == null) {
                commentsList = new ArrayList<DocumentComments>();
            }

            final List<DocumentLikes> likes = searchService.getLikesOfDocument(id, userId);

            // Increase view count in CIFDocument entity
            document = searchService.increaseViewCountCIFDoc(id);

            documentMap.put("record", document);
            documentMap.put("comments", commentsList);
            documentMap.put("like", (likes != null && !likes.isEmpty())); // This user liked this document
        }

        try {
            return new ResponseEntity<Map<String, Object>>(documentMap, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves all the CIFDocument entity records in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/listLikedBy", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentLikes>> getLikedBy(final ServletRequest request, final ServletResponse response, @RequestParam Long docId) {

        NamespaceManager.set("CIF");

        if (docId != null) {

            List<DocumentLikes> likesList = searchService.getLikesOfDocument(docId);
            if (likesList != null & !likesList.isEmpty()) {
                Collections.sort(likesList);
            }
            try {
                return new ResponseEntity<List<DocumentLikes>>(likesList, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
            }

        } else {
            return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Saves the Document comment of a specific CIF Document ID
     * 
     * @param request    - Servlet request
     * @param response   - Servlet response
     * @param docComment - the data coming from UI in DocumentCommentsDTO object
     * 
     * @return the list of DocumentComments of a specific CIF Document ID as
     *         response
     */
    @RequestMapping(value = "/saveComment", method = RequestMethod.POST, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentComments>> saveComment(final ServletRequest request, final ServletResponse response, @RequestBody DocumentCommentsDTO docComment) {

        NamespaceManager.set("CIF");

        log.info("Entering save comment : " + docComment);

        HttpSession session = ((HttpServletRequest) request).getSession();
        UserInfoSession user = (UserInfoSession) session.getAttribute("userInfoSession");

        docComment.setUserId(user.getUserEmail());
        docComment.setUserName(user.getUserName());

        DocumentComments doc = searchService.saveDocumentComments(docComment);
        List<DocumentComments> commentsList = searchService.getCommentsOfDocument(doc.getDocumentId());

        try {
            return new ResponseEntity<List<DocumentComments>>(commentsList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<DocumentComments>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Saves the Document like of a specific CIF Document ID
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * @param docLike  - the data coming from UI in DocumentLikesDTO object
     * 
     * @return the list of DocumentLikes of a specific CIF Document ID as response
     */
    @RequestMapping(value = "/likeDocument", method = RequestMethod.POST, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<DocumentLikes>> saveLike(final ServletRequest request, final ServletResponse response, @RequestBody DocumentLikesDTO docLike) {

        NamespaceManager.set("CIF");

        HttpSession session = ((HttpServletRequest) request).getSession();

        UserInfoSession user = (UserInfoSession) session.getAttribute("userInfoSession");

        String userId = user.getUserEmail();

        if (docLike == null) {
            return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
        } else {
            searchService.saveDocumentLikes(docLike.getDocumentId(), docLike.isLiked(), userId);
            List<DocumentLikes> likesList = searchService.getLikesOfDocument(docLike.getDocumentId(), userId);

            try {
                return new ResponseEntity<List<DocumentLikes>>(likesList, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<List<DocumentLikes>>(HttpStatus.BAD_REQUEST);
            }
        }
    }

    /**
     * Retrieves all the CIFDocument entity records which matches the search term in
     * specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> searchString(final ServletRequest request, final ServletResponse response, @RequestParam String searchTerm) {

        NamespaceManager.set("CIF");

        final Map<String, Object> map = new HashMap<String, Object>();

        final HttpSession session = ((HttpServletRequest) request).getSession();

        final List<MasterData> datasources;
        final List<MasterData> docTypes;

        final String dataSource = request.getParameter("dataSource");
        final String docType = request.getParameter("docType");

        if (session.getAttribute("CIFDatasource") == null) {
            datasources = masterService.getMasterDataByLookupType(MasterDataService.LOOKUP_TYPE_DATASOURCE);
            session.setAttribute("CIFDatasource", datasources);
        } else {
            datasources = (List<MasterData>) session.getAttribute("CIFDatasource");
        }
        if (session.getAttribute("CIFDocType") == null) {
            docTypes = masterService.getMasterDataByLookupType(MasterDataService.LOOKUP_TYPE_DOC_TYPE);
            session.setAttribute("CIFDocType", docTypes);
        } else {
            docTypes = (List<MasterData>) session.getAttribute("CIFDocType");
        }

        map.put("dataSource", datasources);
        map.put("documentType", docTypes);
        map.put("selectedDataSource", dataSource);
        map.put("selectedDocumentType", docType);

        Set<SearchedCIFDocument> searchResult = new TreeSet<SearchedCIFDocument>();
        if (searchTerm != null && !searchTerm.trim().isEmpty()) {
            final List<CIFDocument> srcData = searchService.getAllDocsOfGivenFilter(dataSource, docType);
            searchResult = searchService.search(srcData, searchTerm);
        }
        map.put("searchResult", searchResult);

        try {
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves all the master data in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/findAllMetadata", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> getAllMetaData(final ServletRequest request, final ServletResponse response) {

        NamespaceManager.set("CIF");

        final List<MasterData> plants = masterService.getMasterDataByLookupType("Plant");
        final List<MasterData> main = masterService.getMasterDataByLookupType("Main");
        final List<MasterData> sections = masterService.getMasterDataByLookupType("Section");
        final List<MasterData> keyTopics = masterService.getMasterDataByLookupType("Key Topic");
        final List<MasterData> tags = masterService.getMasterDataByLookupType("Tag");
        final List<MasterData> docTypes = masterService.getMasterDataByLookupType("Document Type");

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("Plant", plants);
        map.put("Main", main);
        map.put("Section", sections);
        map.put("KeyTopic", keyTopics);
        map.put("Tag", tags);
        map.put("DocumentType", docTypes);

        try {
            return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, Object>>(HttpStatus.BAD_REQUEST);
        }
    }

}