package com.lh.cif.search.web;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lh.cif.iam.domain.dto.UserInfoSession;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/")
    public ModelAndView index(final ServletRequest request, final ServletResponse response) {
        // All URLs under /cif/**/** goes through intercepter and that will make sure
        // the authentication happens
        try {
            ((HttpServletResponse) response).sendRedirect("/cif/search/home");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping("/cif/search/home")
    public ModelAndView authenticatedIndex(final ServletRequest request, final ServletResponse response) {
        // Intercepter completed the authentication, so redirect to angular UI URL
        try {
            ((HttpServletResponse) response).sendRedirect("/lh-cif-ui");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping("/lh-cif-ui")
    public ModelAndView authenticatedUIHome(final ServletRequest request, final ServletResponse response) {

        try {
            if (isUserLoggedIn(request)) {
                log.info("Logged In");
                // Redirect to angular UI index.html
                ((HttpServletResponse) response).sendRedirect("/lh-cif-ui/index.html");
            } else {
                log.info("Redirecting to /cif/search/home");
                // If the request comes directly to angular UI URL "/lh-cif-ui", the
                // authentication does not happen
                // So redirect to CIF search home for authentication
                ((HttpServletResponse) response).sendRedirect("/cif/search/home");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Checks if the user already went through authentication
     * 
     * @param request
     * 
     * @return if the user already went through authentication
     */
    private boolean isUserLoggedIn(final ServletRequest request) {

        HttpSession session = ((HttpServletRequest) request).getSession();
        UserInfoSession user = (UserInfoSession) session.getAttribute("userInfoSession");

        log.info("User in userData : " + user);

        request.setAttribute("user", user);

        return (user != null && user.getUserEmail() != null);

    }

}