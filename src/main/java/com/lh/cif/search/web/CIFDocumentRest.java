package com.lh.cif.search.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.core.domain.dto.Response;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.CIFDocumentDTO;
import com.lh.cif.search.model.entity.UploadFileSourceDataDTO;
import com.lh.cif.search.service.SearchService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/cif/api/library")
public class CIFDocumentRest {

    @Autowired
    SearchService searchService;

    @Autowired
    MasterDataService masterService;

    /**
     * Saves the CIFDocument
     * 
     * @param request     - Servlet request
     * @param response    - Servlet response
     * @param session     - Session
     * @param cifDocument - the data coming from UI in CIFDocumentDTO object
     * @param map         - Model
     * 
     * @return the response
     */
    @RequestMapping(value = "/saveCIFDoc", method = RequestMethod.POST)
    public Response saveCIFDocRecord(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @RequestBody CIFDocumentDTO cifDocument,
            Model map) {

        NamespaceManager.set("CIF");

        CIFDocument doc = searchService.saveCIFDocument(cifDocument);

        Response resp = new Response();
        if (doc != null) {
            resp.setMessage("SUCCESS");
            resp.setStatus(HttpStatus.OK);
            resp.setFlag(true);
        } else {
            resp.setFlag(false);
            resp.setMessage("FAIL");
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return resp;
    }

    /**
     * Deletes the CIFDocument
     * 
     * @param request     - Servlet request
     * @param response    - Servlet response
     * @param session     - Session
     * @param cifDocument - the data coming from UI in CIFDocumentDTO object
     * @param map         - Model
     * 
     * @return the response
     */
    @RequestMapping(value = "/deleteCIFDoc/{cifDocId}", method = RequestMethod.POST)
    public Response deleteCIFDocRecord(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @PathVariable("cifDocId") Long cifDocId,
            Model map) {

        NamespaceManager.set("CIF");

        boolean isDeleted = searchService.deleteRecord(cifDocId);

        Response resp = new Response();
        if (isDeleted) {
            resp.setMessage("SUCCESS");
            resp.setStatus(HttpStatus.OK);
            resp.setFlag(true);
        } else {
            resp.setFlag(false);
            resp.setMessage("FAIL");
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return resp;
    }

    /**
     * Retrieves all the CIFDocument entity records in specific JSON format
     * 
     * @param request  - Servlet request
     * @param response - Servlet response
     * 
     * @return the response
     */
    @RequestMapping(value = "/findall", method = RequestMethod.GET, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public ResponseEntity<List<Map<String, Object>>> getAllData(final ServletRequest request, final ServletResponse response) {

        NamespaceManager.set("CIF");

        final List<MasterData> sections = masterService.getMasterDataByLookupType("Section");
        final List<CIFDocument> srcData = searchService.getAllDocuments();

        CIFDocument doc;
        String section;
        List<CIFDocument> records;

        final Iterator<CIFDocument> iterator = srcData.iterator();
        final Map<String, List<CIFDocument>> sectionMap = new HashMap<String, List<CIFDocument>>();

        while (iterator.hasNext()) {
            doc = iterator.next();
            section = doc.getSection();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);
            } else {
                records = new ArrayList<CIFDocument>();
            }
            records.add(doc);
            sectionMap.put(section, records);
        }

        MasterData data;
        Map<String, Object> dataMap;

        final Iterator<MasterData> iteratorM = sections.iterator();
        final List<Map<String, Object>> finalDataList = new ArrayList<Map<String, Object>>();
        while (iteratorM.hasNext()) {
            data = iteratorM.next();
            section = data.getDisplayValue();

            if (sectionMap.containsKey(section)) {
                records = sectionMap.get(section);

                dataMap = new HashMap<String, Object>();
                dataMap.put("section", section);
                dataMap.put("records", records);

                finalDataList.add(dataMap);
            }
        }

        try {
            return new ResponseEntity<List<Map<String, Object>>>(finalDataList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Map<String, Object>>>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Deletes the comment
     * 
     * @param request     - Servlet request
     * @param response    - Servlet response
     * @param session     - Session
     * @param cifDocComId - the comment Id
     * @param map         - Model
     * 
     * @return the response
     */
    @RequestMapping(value = "/deleteComment/{cifDocComId}", method = RequestMethod.POST)
    public Response deleteComment(final HttpServletRequest request, final HttpServletResponse response, final HttpSession session, @PathVariable("cifDocComId") Long commentId,
            Model map) {

        NamespaceManager.set("CIF");

        boolean flag = searchService.deleteDocumentComment(commentId);

        Response resp = new Response();
        if (flag) {
            resp.setMessage("SUCCESS");
            resp.setStatus(HttpStatus.OK);
            resp.setFlag(true);
        } else {
            resp.setFlag(false);
            resp.setMessage("FAIL");
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return resp;
    }

    /**
     * Upload SourceData from a drive document
     * 
     * @param request     - Servlet request
     * @param response    - Servlet response
     * @param session     - Session
     * @param cifDocComId - the comment Id
     * @param map         - Model
     * 
     * @return the response
     */
    @RequestMapping(value = "/uploadSourceData", method = RequestMethod.POST, produces = { MimeTypeUtils.APPLICATION_JSON_VALUE }, headers = "Accept=application/json")
    public Response uploadSourceData(final ServletRequest request, final ServletResponse response, final HttpSession session, @RequestBody UploadFileSourceDataDTO uploadDTO) {

        NamespaceManager.set("CIF");

        UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");

        searchService.uploadSourceData(uploadDTO, userInfoSession.getUserEmail());

        boolean flag = true;

        Response resp = new Response();
        if (flag) {
            resp.setMessage("SUCCESS");
            resp.setStatus(HttpStatus.OK);
            resp.setFlag(true);
        } else {
            resp.setFlag(false);
            resp.setMessage("FAIL");
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return resp;
    }

}