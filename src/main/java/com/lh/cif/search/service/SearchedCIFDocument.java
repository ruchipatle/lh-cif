package com.lh.cif.search.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.lh.cif.search.model.entity.CIFDocument;

import lombok.Data;
import lombok.ToString;

@SuppressWarnings("serial")
@ToString
@Data
public class SearchedCIFDocument implements Cloneable, Comparable<SearchedCIFDocument>, Serializable {

    Long id;

    String name;

    String label;

    String description;

    String documentId;

    String documentLink;

    String docType;

    List<String> tags;

    List<String> keyTopics;

    String plant;

    String section;

    String main;

    int likesCount = 0;

    int viewsCount = 0;

    String dataSource;

    String previewURL;

    String thumbnailURL;

    int searchRelevance;

    List<String> searchableStrList;

    public SearchedCIFDocument() {
        super();
    }

    public SearchedCIFDocument(CIFDocument parent) {

        this.id = parent.getId();

        this.name = parent.getName();
        this.label = parent.getLabel();
        this.description = parent.getDescription();

        this.docType = parent.getDocType();
        this.documentId = parent.getDocumentId();
        this.documentLink = parent.getDocumentLink();
        this.previewURL = parent.getPreviewURL();
        this.thumbnailURL = parent.getThumbnailURL();

        this.dataSource = parent.getDataSource();
        this.main = parent.getMain();
        this.section = parent.getSection();
        this.plant = parent.getPlant();

        this.keyTopics = parent.getKeyTopics();
        this.tags = parent.getTags();

        this.viewsCount = parent.getViewsCount();
        this.likesCount = parent.getLikesCount();
        this.searchRelevance = 0;
    }

    public List<String> getSearchableStrList() {
        this.searchableStrList = new ArrayList<String>();

        addToSearchableStrList(this.name);
        addToSearchableStrList(this.label);
        addToSearchableStrList(this.description);
        addToSearchableStrList(this.docType);
        addToSearchableStrList(this.dataSource);
        addToSearchableStrList(this.main);
        addToSearchableStrList(this.section);
        addToSearchableStrList(this.dataSource);
        addToSearchableStrList(this.keyTopics);
        addToSearchableStrList(this.tags);

        return this.searchableStrList;
    }

    private void addToSearchableStrList(String object) {
        if (object != null) {
            this.searchableStrList.add(object);
        }
    }

    private void addToSearchableStrList(List<String> object) {
        if (object != null && !object.isEmpty()) {
            this.searchableStrList.addAll(object);
        }
    }

    public boolean contains(final String term) {
        if (this.toString().toLowerCase().contains(term.toLowerCase())) {
            return true;
        }
        return false;
    }

    public void containKeyFields(final String term) {

    }

    @Override
    public int compareTo(SearchedCIFDocument doc) {
        if (this.searchRelevance == doc.searchRelevance && this.getLikesCount() == doc.getLikesCount() && doc.getViewsCount() == this.getViewsCount()) {
            String thisLabel = this.getLabel();
            String otherLabel = doc.getLabel();
            if (thisLabel == null) {
                if (this.getName() == null) {
                    thisLabel = "";
                } else {
                    thisLabel = this.getName();
                }
            }
            if (otherLabel == null) {
                if (doc.getName() == null) {
                    otherLabel = "";
                } else {
                    otherLabel = doc.getName();
                }
            }
            return thisLabel.compareTo(otherLabel); // label - alphabetic ascending
        } else {
            if (this.searchRelevance == doc.searchRelevance) {
                if (this.getLikesCount() == doc.getLikesCount()) {
                    return (doc.getViewsCount() - this.getViewsCount()); // view count - descending (high on top)
                } else {
                    return (doc.getLikesCount() - this.getLikesCount()); // likes count - descending (high on top)
                }
            } else if (this.searchRelevance > doc.searchRelevance && (this.searchRelevance - doc.searchRelevance) < 15) { // only 15% relevance difference
                if (this.getLikesCount() == doc.getLikesCount()) {
                    if (this.getViewsCount() == doc.getViewsCount()) {
                        return (doc.searchRelevance - this.searchRelevance); // relevance - descending (high on top)
                    } else {
                        return (doc.getViewsCount() - this.getViewsCount()); // view count - descending (high on top)
                    }
                } else {
                    return (doc.getLikesCount() - this.getLikesCount()); // likes count - descending (high on top)
                }
            } else {
                return (doc.searchRelevance - this.searchRelevance); // relevance - descending (high on top)
            }
        }
    }

}
