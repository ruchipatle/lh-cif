package com.lh.cif.search.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.drive.model.File;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.admin.model.entity.MasterData;
import com.lh.cif.admin.service.MasterDataService;
import com.lh.cif.core.util.ConfigUtils;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.drive.DriveService;
import com.lh.cif.gsuite.spreadsheet.SpreadsheetReader;
import com.lh.cif.search.model.dao.CIFDocumentDAO;
import com.lh.cif.search.model.dao.DocumentCommentsDAO;
import com.lh.cif.search.model.dao.DocumentLikesDAO;
import com.lh.cif.search.model.entity.CIFDocument;
import com.lh.cif.search.model.entity.CIFDocumentDTO;
import com.lh.cif.search.model.entity.DocumentComments;
import com.lh.cif.search.model.entity.DocumentCommentsDTO;
import com.lh.cif.search.model.entity.DocumentLikes;
import com.lh.cif.search.model.entity.UploadFileSourceDataDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SearchService {

    @Autowired
    CIFDocumentDAO documentDao;

    @Autowired
    DocumentCommentsDAO commentsDao;

    @Autowired
    DocumentLikesDAO likesDao;

    @Autowired
    GsuiteManager googleService;

    @Autowired
    MasterDataService masterService;

    /**
     * Gets all the document source data
     * 
     * @return the list of document source data.
     */
    public List<CIFDocument> getAllDocuments() {
        return documentDao.findAll();
    }

    /**
     * Gets all the document source data for a specific section
     * 
     * @param section - a specific section
     * 
     * @return the list of document source data of a specific section
     */
    public List<CIFDocument> getAllDocumentsOfSection(final String section) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("dataSource", section);
        List<CIFDocument> documents = documentDao.getResultListfindByColumnAndValue(columnValueMap);

        if (documents != null) {
            Collections.sort(documents);
        }

        return documents;
    }

    /**
     * Gets all the document data for a specific filter
     * 
     * @param dataSource - a specific dataSource
     * @param docType    - a specific document Type
     * 
     * @return the list of document source data of a specific section
     */
    public List<CIFDocument> getAllDocsOfGivenFilter(final String dataSource, String docType) {

        List<CIFDocument> documents;

        if ((dataSource != null && !dataSource.isEmpty()) || (docType != null && !docType.isEmpty())) {
            Map<String, Object> columnValueMap = new HashMap<>();
            if (dataSource != null && !dataSource.isEmpty()) {
                columnValueMap.put("dataSource", dataSource);
            }
            if (docType != null && !docType.isEmpty()) {
                columnValueMap.put("docType", docType);
            }
            documents = documentDao.getResultListfindByColumnAndValue(columnValueMap);
        } else {
            documents = documentDao.findAll();
        }

        return documents;
    }

    /**
     * Gets all the document source data for a specific URL
     * 
     * @param url - a specific url
     * 
     * @return the list of document source data of a specific url
     */
    public List<CIFDocument> getAllDocumentsOfURL(final String url) {
        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("documentLink", url);
        List<CIFDocument> documents = documentDao.getResultListfindByColumnAndValue(columnValueMap);
        return documents;
    }

    /**
     * Gets a specific CIFDocument for the given ID
     * 
     * @param id - the id of the CIFDocument
     * 
     * @return a specific CIFDocument for the given ID
     */
    public CIFDocument getCIFDocument(Long id) {
        return documentDao.find(id);
    }

    /**
     * Saves the data coming from UI in CIFDocumentDTO object to the entity
     * CIFDocument
     * 
     * @param dto - CIFDocumentDTO object
     * 
     * @return the updated CIFDocument
     */
    public CIFDocument saveCIFDocument(final CIFDocumentDTO dto) {
        CIFDocument doc = createCIFDocument(dto);
        return documentDao.saveOrUpdate(doc);
    }

    /**
     * Deletes the document record
     * 
     * @param cifDocId - comment ID
     * 
     */
    public boolean deleteRecord(final Long cifDocId) {
        documentDao.delete(cifDocId);
        return true;
    }

    /**
     * Creates CIFDocument with the data coming from UI in CIFDocumentDTO object
     * 
     * @param dto - CIFDocumentDTO object
     * 
     * @return the CIFDocument
     */
    public CIFDocument createCIFDocument(final CIFDocumentDTO dto) {

        CIFDocument doc;

        if (dto.getId() == null) {
            doc = new CIFDocument();
        } else {
            doc = documentDao.find(dto.getId());
        }

        return copyDTOtoCIFDoc(doc, dto, "\\|");
    }

    /**
     * Creates CIFDocument with the data coming from UI in CIFDocumentDTO object
     * 
     * @param doc       - CIFDocument
     * @param dto       - CIFDocumentDTO object
     * @param delimiter - the delimiter to split the keytopics and tags
     * 
     * @return the CIFDocument
     */
    private CIFDocument copyDTOtoCIFDoc(CIFDocument doc, CIFDocumentDTO dto, String delimiter) {

        doc.setMain(dto.getMainCategory());
        doc.setPlant(dto.getPlant());
        doc.setSection(dto.getSection());
        doc.setDocumentLink(dto.getDocumentLink());
        doc.setDocumentId(dto.getDocumentId());
        doc.setDocType(dto.getDocType());
        doc.setName(dto.getName());
        doc.setLabel(dto.getLabel());
        doc.setDescription(dto.getDescription());
        doc.setDataSource(dto.getDataSource());
        doc.setPreviewURL(dto.getPreviewURL());
        doc.setThumbnailURL(dto.getThumbnailURL());

        List<String> keyTopicList = new ArrayList<String>();
        String keyTopicsStr = dto.getKeyTopics();
        if (keyTopicsStr != null) {
            if (delimiter.contentEquals(",")) {
                keyTopicsStr = keyTopicsStr.replaceAll(", ", ",");
                keyTopicsStr = keyTopicsStr.replaceAll(" ,", ",");
                String[] keyTopicStrArr = keyTopicsStr.split(",");
                keyTopicList = Arrays.asList(keyTopicStrArr);
            } else {
                String[] keyTopicStrArr = keyTopicsStr.split("\\|");
                keyTopicList = Arrays.asList(keyTopicStrArr);
            }
        }
        doc.setKeyTopics(keyTopicList);

        List<String> tagList = new ArrayList<String>();
        String tagStr = dto.getTags();
        if (tagStr != null) {
            if (delimiter.contentEquals(",")) {
                tagStr = tagStr.replaceAll(", ", ",");
                tagStr = tagStr.replaceAll(" ,", ",");
                String[] tagStrArr = tagStr.split(",");
                tagList = Arrays.asList(tagStrArr);
            } else {
                String[] tagStrArr = tagStr.split("\\|");
                tagList = Arrays.asList(tagStrArr);
            }
        }
        doc.setTags(tagList);

        if (null == doc.getCreatedDate()) {
            doc.setCreatedDate(new Date());
        }
        doc.setModifiedDate(new Date());

        return doc;
    }

    /**
     * Gets the list of comments related to a specific document ID
     * 
     * @param docId - the document ID
     * 
     * @return the list of comments related to a specific document ID
     */
    public List<DocumentComments> getCommentsOfDocument(final Long docId) {

        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("documentId", docId);
        List<DocumentComments> comments = commentsDao.getResultListfindByColumnAndValue(columnValueMap);

        if (comments != null) {
            Collections.sort(comments);
        }

        return comments;
    }

    /**
     * Gets the list of likes data related to a specific document ID and a specific
     * user ID
     * 
     * @param docId  - the document ID
     * @param userId - the user ID
     * 
     * @return the list of likes data related to a specific document ID and a
     *         specific user ID
     */
    public List<DocumentLikes> getLikesOfDocument(final Long docId, final String userId) {

        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("documentId", docId);
        columnValueMap.put("userId", userId);

        return likesDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    /**
     * Gets the list of likes data related to a specific document ID
     * 
     * @param docId - the document ID
     * 
     * @return the list of likes data related to a specific document ID
     */
    public List<DocumentLikes> getLikesOfDocument(final Long docId) {

        Map<String, Object> columnValueMap = new HashMap<>();
        columnValueMap.put("documentId", docId);

        return likesDao.getResultListfindByColumnAndValue(columnValueMap);
    }

    /**
     * Deletes the document comments
     * 
     * @param commentId - comment ID
     * 
     */
    public boolean deleteDocumentComment(final Long commentId) {
        commentsDao.delete(commentId);
        return true;
    }

    /**
     * Saves the document comments
     * 
     * @param documentId - document ID
     * @param comment    - comment related to the document
     * @param userId     - the user ID
     * 
     * @return the updated document comment
     */
    public DocumentComments saveDocumentComments(final Long documentId, final String comment, final String userId) {

        final DocumentComments docComment = new DocumentComments();
        docComment.setDocumentId(documentId);
        docComment.setComment(comment);
        docComment.setUserId(userId);
        docComment.setCreatedDate(new Date());

        return commentsDao.saveOrUpdate(docComment);
    }

    /**
     * Saves the document comments
     * 
     * @param commentDto - document ID, comment related to the document and the user
     *                   ID
     * 
     * @return the updated document comment
     */
    public DocumentComments saveDocumentComments(final DocumentCommentsDTO commentDto) {

        final DocumentComments docComment = new DocumentComments();
        docComment.setDocumentId(commentDto.getDocumentId());
        docComment.setComment(commentDto.getComment());
        docComment.setUserId(commentDto.getUserId());
        docComment.setUserName(commentDto.getUserName());
        docComment.setCreatedDate(new Date());

        return commentsDao.saveOrUpdate(docComment);
    }

    /**
     * Saves the document like details
     * 
     * @param documentId - document ID
     * @param isLiked    - the flag that states whether liked or disliked
     * @param userId     - the user ID
     * 
     */
    public void saveDocumentLikes(final Long documentId, final boolean isLiked, final String userId) {

        final List<DocumentLikes> likes = getLikesOfDocument(documentId, userId);

        boolean recordExist = false;
        if (likes != null && !likes.isEmpty()) {
            recordExist = true;
        }

        if (recordExist) {
            if (!isLiked) {
                // delete the like record & decrease the like count in CIFDocument
                for (DocumentLikes docLike : likes) { // Iteration not needed
                    likesDao.delete(docLike.getId());

                    decreaseLikeCountCIFDoc(documentId);
                }
            }
        } else {
            if (isLiked) {
                // insert the like record and increase the like count in CIFDocument
                DocumentLikes docLike = new DocumentLikes();
                docLike.setDocumentId(documentId);
                docLike.setUserId(userId);
                docLike.setCreatedDate(new Date());
                docLike = likesDao.saveOrUpdate(docLike);

                increaseLikeCountCIFDoc(documentId);
            }
        }
    }

    /**
     * Increase the views count of CIFDocument
     * 
     * @param documentId - the document ID
     * 
     * @return the updated CIFDocument
     * 
     */
    public CIFDocument increaseViewCountCIFDoc(final Long documentId) {
        final CIFDocument cifDocument = getCIFDocument(documentId);
        cifDocument.setViewsCount(cifDocument.getViewsCount() + 1);

        return documentDao.saveOrUpdate(cifDocument);
    }

    /**
     * Increase the likes count of CIFDocument
     * 
     * @param documentId - the document ID
     * 
     * @return the updated CIFDocument
     * 
     */
    public CIFDocument increaseLikeCountCIFDoc(final Long documentId) {
        final CIFDocument cifDocument = getCIFDocument(documentId);
        cifDocument.setLikesCount(cifDocument.getLikesCount() + 1);

        return documentDao.saveOrUpdate(cifDocument);
    }

    /**
     * Decrease the likes count of CIFDocument
     * 
     * @param documentId - the document ID
     * 
     * @return the updated CIFDocument
     * 
     */
    public CIFDocument decreaseLikeCountCIFDoc(final Long documentId) {
        final CIFDocument cifDocument = getCIFDocument(documentId);
        cifDocument.setLikesCount(cifDocument.getLikesCount() - 1);

        return documentDao.saveOrUpdate(cifDocument);
    }

    /**
     * Form the list with all relevant search terms from the search string
     * 
     * @param searchStr the search string
     * 
     * @return the list with all relevant search terms
     */
    private List<String> getFormedSearchTerms(final String searchStr) {

        // Remove double spaces and convert to lower cases
        String searchTerm = searchStr.toLowerCase().trim().replaceAll("  ", " ");

        // Convert all plural words to singular
        if (searchTerm.endsWith("ies")) {
            searchTerm = searchTerm.substring(0, searchTerm.length() - 3);
        } else if (searchTerm.endsWith("es")) {
            searchTerm = searchTerm.substring(0, searchTerm.length() - 2);
        } else if (searchTerm.endsWith("s")) {
            searchTerm = searchTerm.substring(0, searchTerm.length() - 1);
        } else if (searchTerm.endsWith("ing")) {
            searchTerm = searchTerm.substring(0, searchTerm.length() - 3);
        }
        searchTerm = searchTerm.replaceAll("ies ", " ").replaceAll("es ", " ").replaceAll("s ", " ").replaceAll("ing ", " ");

        // stop words list
        String[] stopWords = { "a", "an", "the", "is", "was", "am", "are", "were", "be", "been", "have", "has", "had", "of", "by", "it", "you", "i", "we", "in", "on", "as", "at",
                "and", "or" };
        List<String> stopWordsList = new ArrayList<String>(Arrays.asList(stopWords));

        // remove stop words from search term1
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(searchTerm.split(" ")));
        searchTerms.removeAll(stopWordsList);

        return searchTerms;
    }

    /**
     * Search a specific search term in the list of records
     * 
     * @param records    the records from datastore
     * @param searchTerm the search term
     */
    public Set<SearchedCIFDocument> search(List<CIFDocument> records, String searchTerm) {

        List<String> filteredSearchTerms = getFormedSearchTerms(searchTerm);

        Set<SearchedCIFDocument> searchResult = new TreeSet<SearchedCIFDocument>();

        int totalSearchWords = filteredSearchTerms.size();
        searchTerm = searchTerm.toLowerCase().trim().replaceAll("  ", " ");

        for (CIFDocument record : records) {
            SearchedCIFDocument sRecord = new SearchedCIFDocument(record);

            if (sRecord.contains(searchTerm)) {
                sRecord.setSearchRelevance(100);
            } else {
                int matchedWords = 0;

                String toBeSearchedStr = sRecord.getSearchableStrList().toString().toLowerCase();

                for (String str : filteredSearchTerms) {
                    if (toBeSearchedStr.contains(str)) {
                        matchedWords++;
                    }
                }

                if (matchedWords > 0) {
                    sRecord.setSearchRelevance(100 * matchedWords / totalSearchWords);
                }
            }

            if (sRecord.getSearchRelevance() > 30) {
                searchResult.add(sRecord);
            }
        }

        return searchResult;
    }

    /**
     * Uploads the content of the sheet to datastore
     * 
     * @param uploadDTO - the object contains the sheet details
     * @param userEmail - the user email ID
     * 
     * @return uploaded successfully or not
     */
    public boolean uploadSourceData(UploadFileSourceDataDTO uploadDTO, String userEmail) {

        log.info("uploadSourceData ... Sheet ID : " + uploadDTO.getDocumentId());

        HashMap<String, List<String>> lookupTypeListMap = getAllMasterData();

        List<CIFDocument> docsList = new ArrayList<CIFDocument>();

        int newRecs = 0;
        int updateRecs = 0;
        boolean isSheet = false;
        try {
            log.info("Dat upload Starts...");

            SpreadsheetReader reader = new SpreadsheetReader(googleService.getOauth(userEmail), uploadDTO.getDocumentId());
            List<WorksheetEntry> sheets = reader.listSheets();

            if (sheets != null && !sheets.isEmpty()) {

                WorksheetEntry sheet = null;
                for (WorksheetEntry sheetEntry : sheets) {
                    log.info("Sheet : " + sheetEntry.getTitle().getPlainText());
                    if ("CIF Bulk Upload".equalsIgnoreCase(sheetEntry.getTitle().getPlainText())) {
                        isSheet = true;
                        sheet = sheetEntry;
                    }
                }

                if (!isSheet) {
                    log.error("Unable to find the sheet named 'CIF Bulk Upload'");
                    return false;
                }

                // WorksheetEntry sheet = sheets.get(0);
                List<ListEntry> rows = reader.listRows(sheet);

                for (ListEntry row : rows) {

                    // log.info("TAGS ::::::" + row.getCustomElements().getTags());
                    // TAGS ::::::[datasource, name, url, id, label, description, preview, main,
                    // keytopic, section, tag, plant]

                    String url = SpreadsheetReader.getCellValue(row, "URL");
                    if (url == null || url.isEmpty()) {
                        continue;
                    }

                    CIFDocumentDTO docDTO = new CIFDocumentDTO();
                    docDTO.setDataSource(SpreadsheetReader.getCellValue(row, "datasource"));
                    docDTO.setName(SpreadsheetReader.getCellValue(row, "Name"));
                    docDTO.setDocumentLink(url);
                    docDTO.setDocumentId(SpreadsheetReader.getCellValue(row, "ID"));
                    docDTO.setLabel(SpreadsheetReader.getCellValue(row, "Label"));
                    docDTO.setDescription(SpreadsheetReader.getCellValue(row, "Description"));
                    docDTO.setThumbnailURL(SpreadsheetReader.getCellValue(row, "Preview"));
                    docDTO.setMainCategory(SpreadsheetReader.getCellValue(row, "Main"));
                    docDTO.setKeyTopics(SpreadsheetReader.getCellValue(row, "keytopic"));
                    docDTO.setSection(SpreadsheetReader.getCellValue(row, "Section"));
                    docDTO.setTags(SpreadsheetReader.getCellValue(row, "Tag"));
                    docDTO.setPlant(SpreadsheetReader.getCellValue(row, "Plant"));
                    docDTO.setDocType(SpreadsheetReader.getCellValue(row, "type"));

                    if (docDTO.getDocType() == null || docDTO.getDocType().isEmpty()) {
                        docDTO.setDocType("Other");
                    }

                    if (docDTO.getDescription() != null && docDTO.getDescription().length() > 500) {
                        docDTO.setDescription(docDTO.getDescription().substring(0, 499));
                    }

                    // To check if already same entry exist in datastore
                    List<CIFDocument> savedDocs = null;
                    try {
                        savedDocs = getAllDocumentsOfURL(docDTO.getDocumentLink());
                    } catch (Exception e1) {
                        log.error("Exception while fetching record during bulk upload : " + docDTO);
                        log.error(ConfigUtils.getStackTrace(e1));
                    }

                    CIFDocument cifDoc;
                    if (savedDocs == null || savedDocs.isEmpty()) {
                        cifDoc = new CIFDocument();
                        newRecs++;
                    } else {
                        cifDoc = savedDocs.get(0);
                        updateRecs++;
                    }

                    CIFDocument document = copyDTOtoCIFDoc(cifDoc, docDTO, ",");
                    try {
                        documentDao.saveOrUpdate(document);
                        docsList.add(document);
                    } catch (Exception e) {
                        log.error("Exception while inserting record from spreadsheet for bulk upload : " + document);
                        log.error(ConfigUtils.getStackTrace(e));
                    }

//                    if (document.getDocumentLink() != null
//                            && (document.getDocumentLink().startsWith("https://drive.google.com") || document.getDocumentLink().startsWith("https://docs.google.com"))
//                            && !gotType) {
//                        getDocumentType(document, userEmail);
//                        gotType = true;
//                    }
                }
            }

            log.info("Data upload completed...");

            if (docsList.size() > 0) {
                handleMasterData(lookupTypeListMap, docsList, userEmail);
            }

            log.info("Total Records : " + docsList.size() + ", New Records : " + newRecs + ", Updated Records : " + updateRecs);

        } catch (Throwable e) {
            log.error("exception while reading spreadsheet for bulk upload");
            log.error(ConfigUtils.getStackTrace(e));
        }

        return true;
    }

    /**
     * Gets all master data in a map
     * 
     * @return all master data in a map
     */
    public HashMap<String, List<String>> getAllMasterData() {
        List<MasterData> allData = masterService.getAllData();

        HashMap<String, List<String>> lookupTypeListMap = new HashMap<String, List<String>>();
        List<String> lTypeList;

        for (MasterData mdata : allData) {
            String lType = mdata.getLookupType();
            if (lookupTypeListMap.containsKey(lType)) {
                lTypeList = lookupTypeListMap.get(lType);
            } else {
                lTypeList = new ArrayList<String>();
            }
            lTypeList.add(mdata.getDisplayValue());

            lookupTypeListMap.put(lType, lTypeList);
        }

        return lookupTypeListMap;
    }

    /**
     * Create the master data if not exist already
     * 
     * @param lookupTypeListMap the map which contains the existing master data
     * @param docList           the list of CIFDocument objects
     * @param userEmail         the email ID
     */
    private void handleMasterData(HashMap<String, List<String>> lookupTypeListMap, List<CIFDocument> docList, String userEmail) {

        MasterData mData = null;

        for (CIFDocument cifDoc : docList) {

            if (cifDoc.getDataSource() != null && !cifDoc.getDataSource().isEmpty()) {
                if (!(lookupTypeListMap.containsKey(MasterDataService.LOOKUP_TYPE_DATASOURCE)
                        && lookupTypeListMap.get(MasterDataService.LOOKUP_TYPE_DATASOURCE).contains(cifDoc.getDataSource()))) {
                    mData = masterService.createMasterData(cifDoc.getDataSource(), MasterDataService.LOOKUP_TYPE_DATASOURCE, userEmail);
                    if (mData != null) {
                        setNewDataInMap(lookupTypeListMap, MasterDataService.LOOKUP_TYPE_DATASOURCE, cifDoc.getDataSource());
                    }
                }
            }

            if (cifDoc.getDataSource() != null && !cifDoc.getDataSource().isEmpty()) {
                if (!(lookupTypeListMap.containsKey(MasterDataService.LOOKUP_TYPE_MAIN) && lookupTypeListMap.get(MasterDataService.LOOKUP_TYPE_MAIN).contains(cifDoc.getMain()))) {
                    mData = masterService.createMasterData(cifDoc.getMain(), MasterDataService.LOOKUP_TYPE_MAIN, userEmail);
                    if (mData != null) {
                        setNewDataInMap(lookupTypeListMap, MasterDataService.LOOKUP_TYPE_MAIN, cifDoc.getMain());
                    }
                }
            }

            if (cifDoc.getDataSource() != null && !cifDoc.getDataSource().isEmpty()) {
                if (!(lookupTypeListMap.containsKey(MasterDataService.LOOKUP_TYPE_SECTION)
                        && lookupTypeListMap.get(MasterDataService.LOOKUP_TYPE_SECTION).contains(cifDoc.getSection()))) {
                    mData = masterService.createMasterData(cifDoc.getSection(), MasterDataService.LOOKUP_TYPE_SECTION, userEmail);
                    if (mData != null) {
                        setNewDataInMap(lookupTypeListMap, MasterDataService.LOOKUP_TYPE_SECTION, cifDoc.getSection());
                    }
                }
            }

            if (cifDoc.getTags() != null && !cifDoc.getTags().isEmpty()) {
                for (String tag : cifDoc.getTags()) {
                    if (!(lookupTypeListMap.containsKey(MasterDataService.LOOKUP_TYPE_TAG) && lookupTypeListMap.get(MasterDataService.LOOKUP_TYPE_TAG).contains(tag))) {
                        mData = masterService.createMasterData(tag, MasterDataService.LOOKUP_TYPE_TAG, userEmail);
                        if (mData != null) {
                            setNewDataInMap(lookupTypeListMap, MasterDataService.LOOKUP_TYPE_TAG, tag);
                        }
                    }
                }
            }

            if (cifDoc.getKeyTopics() != null && !cifDoc.getKeyTopics().isEmpty()) {
                for (String keytopic : cifDoc.getKeyTopics()) {
                    if (!(lookupTypeListMap.containsKey(MasterDataService.LOOKUP_TYPE_KEY_TOPIC)
                            && lookupTypeListMap.get(MasterDataService.LOOKUP_TYPE_KEY_TOPIC).contains(keytopic))) {
                        mData = masterService.createMasterData(keytopic, MasterDataService.LOOKUP_TYPE_KEY_TOPIC, userEmail);
                        if (mData != null) {
                            setNewDataInMap(lookupTypeListMap, MasterDataService.LOOKUP_TYPE_KEY_TOPIC, keytopic);
                        }
                    }
                }
            }

        }
    }

    /**
     * Add new data to the map
     * 
     * @param lookupTypeListMap
     * @param lType
     * @param displayValue
     */
    private void setNewDataInMap(HashMap<String, List<String>> lookupTypeListMap, String lType, String displayValue) {
        List<String> lTypeList;
        if (lookupTypeListMap.containsKey(lType)) {
            lTypeList = lookupTypeListMap.get(lType);
        } else {
            lTypeList = new ArrayList<String>();
        }

        if (!lTypeList.contains(displayValue)) {
            lTypeList.add(displayValue);
        }

        lookupTypeListMap.put(lType, lTypeList);
    }

    /**
     * Updates the master data parallel
     * 
     * @param cifDoc    the document object
     * @param userEmail the email ID
     */
    public void updateMasterData(CIFDocument cifDoc, String userEmail) {
        masterService.createAndSaveMaterData(cifDoc.getDataSource(), MasterDataService.LOOKUP_TYPE_DATASOURCE, userEmail);
        masterService.createAndSaveMaterData(cifDoc.getMain(), MasterDataService.LOOKUP_TYPE_MAIN, userEmail);
        masterService.createAndSaveMaterData(cifDoc.getSection(), MasterDataService.LOOKUP_TYPE_SECTION, userEmail);

        if (cifDoc.getTags() != null && !cifDoc.getTags().isEmpty()) {
            for (String tag : cifDoc.getTags()) {
                masterService.createAndSaveMaterData(tag, MasterDataService.LOOKUP_TYPE_TAG, userEmail);
            }
        }
        if (cifDoc.getKeyTopics() != null && !cifDoc.getKeyTopics().isEmpty()) {
            for (String keytopic : cifDoc.getKeyTopics()) {
                masterService.createAndSaveMaterData(keytopic, MasterDataService.LOOKUP_TYPE_KEY_TOPIC, userEmail);
            }
        }
    }

//    public void getDocumentType(CIFDocument doc, String userEmail) {
//        try {
//            DriveService driveService = googleService.getDrive(userEmail);
//            log.info("properties : " + driveService.getFileProperties(doc.getDocumentId()));
//        } catch (Throwable e) {
//            log.error(ConfigUtils.getStackTrace(e));
//        }
//    }

    /**
     * Sets the document type
     * 
     * @param doc       - CIFDocumentDTO
     * @param userEmail - user Email
     * 
     */
    public void setDocumentType(CIFDocumentDTO doc, String userEmail) {

        DriveService driveService = null;
        File file = null;

        if (doc.getDocumentLink() != null && (doc.getDocumentLink().startsWith("https://drive.google.com") || doc.getDocumentLink().startsWith("https://docs.google.com"))) {
            try {
                driveService = googleService.getDrive(userEmail);
                file = driveService.get(doc.getDocumentId());
                log.info("File .... " + file);
                if (file != null) {
                    String mimeType = file.getMimeType();
                    if (mimeType.equals("application/vnd.google-apps.document")) {
                        doc.setDocType("Document");
                    } else if (mimeType.equals("application/vnd.google-apps.spreadsheet")) {
                        doc.setDocType("Sheet");
                    } else if (mimeType.equals("application/vnd.google-apps.presentation")) {
                        doc.setDocType("Slide");
                    } else if (mimeType.equals("application/vnd.google-apps.site")) {
                        doc.setDocType("Site");
                    } else if (mimeType.contains("pdf")) {
                        doc.setDocType("Pdf");
                    } else if (mimeType.contains("image")) {
                        doc.setDocType("Image");
                    } else {
                        doc.setDocType("Other");
                    }
                }
            } catch (Throwable e) {
                log.error("exception while reading spreadsheet for bulk upload");
                log.error(ConfigUtils.getStackTrace(e));
            }
        } else {
            doc.setDocType("Site");
        }
    }

    // Sample Code
    public CIFDocument createDocument(String name) {
        CIFDocument doc = new CIFDocument();
        doc.setCreatedDate(new Date());
        doc.setDescription("Created through code");
        doc.setDocumentLink("http://jjsdkjah/askdjlk");
        doc.setDocumentId("sdkaskdkj");
        List<String> ktopics = new ArrayList<String>();
        doc.setKeyTopics(ktopics);
        doc.setName(name);
        return documentDao.saveOrUpdate(doc);
    }

}
