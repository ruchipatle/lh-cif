package com.lh.cif.iam.respository;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.appengine.api.datastore.QueryResultIterator;
import com.lh.cif.gsuite.datastore.DatastoreWorker;
import com.lh.cif.iam.domain.entity.OpcoLocalDetails;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class OpcoLocaleDAOImpl implements OpcoLocaleDAO {

	private DatastoreWorker<OpcoLocalDetails> opcoLocale;

	public OpcoLocaleDAOImpl() {
		super();
		this.opcoLocale = new DatastoreWorker<>(OpcoLocalDetails.class);
	}

	@Override
	public OpcoLocalDetails saveOrUpdate(OpcoLocalDetails newInstance) {
		return this.opcoLocale.save(newInstance);
	}

	@Override
	public OpcoLocalDetails find(Long id) {
		return this.opcoLocale.get(id);
	}

	@Override
	public List<OpcoLocalDetails> findAll() {
		return this.opcoLocale.list();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<OpcoLocalDetails> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.opcoLocale.list(columnValueMap);
	}

	@Override
	public QueryResultIterator<OpcoLocalDetails> findByColumnAndValuePaginated(Map<String, Object> columnValueMap, Integer range, String startPosition) {
		return this.opcoLocale.listEntitiesPaginated(columnValueMap, range, startPosition);
	}

	@Override
	public OpcoLocalDetails find(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public OpcoLocalDetails getSingleResultfindByColumnAndValue(Map<String, Object> columnValueMap) {
		return this.opcoLocale.get(columnValueMap);
	}

	@Override
	public List<OpcoLocalDetails> findAll(List<Long> ids) {
		return (List<OpcoLocalDetails>) this.opcoLocale.getAllByIds(ids);
	}

}
