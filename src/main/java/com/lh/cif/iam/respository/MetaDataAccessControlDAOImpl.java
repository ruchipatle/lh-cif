package com.lh.cif.iam.respository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.lh.cif.gsuite.datastore.DatastoreWorker;
import com.lh.cif.iam.domain.entity.MetaDataAccessControl;

@Repository
public class MetaDataAccessControlDAOImpl implements MetaDataAccessControlDAO {

	private DatastoreWorker<MetaDataAccessControl> restrictedMetaData;
	
	public MetaDataAccessControlDAOImpl () {
		super();
		this.restrictedMetaData = new DatastoreWorker<>(MetaDataAccessControl.class);
	}
	
	@Override
	public List<MetaDataAccessControl> getAllRestrictedMetaData() {
		// TODO Auto-generated method stub
		return restrictedMetaData.list();

	}

	@Override
	public void saveEntry(MetaDataAccessControl obj) {
		restrictedMetaData.save(obj);
	}
}
