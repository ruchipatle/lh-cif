package com.lh.cif.iam.respository;

import java.util.List;

public interface RoleVsActionDAO {

	public List<String> getActions(String role);

}
