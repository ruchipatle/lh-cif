package com.lh.cif.iam.respository;

import java.util.List;
import java.util.Map;

import com.lh.cif.core.repository.GenericDao;
import com.lh.cif.iam.domain.entity.OpcoLocalDetails;

public interface OpcoLocaleDAO extends GenericDao<OpcoLocalDetails> {

	@Override
	public List<OpcoLocalDetails> getResultListfindByColumnAndValue(Map<String, Object> columnValueMap);
}
