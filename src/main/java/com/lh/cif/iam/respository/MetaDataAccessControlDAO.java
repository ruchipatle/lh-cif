package com.lh.cif.iam.respository;

import java.util.List;

import com.lh.cif.iam.domain.entity.MetaDataAccessControl;

public interface MetaDataAccessControlDAO {

	public List<MetaDataAccessControl> getAllRestrictedMetaData();

	void saveEntry(MetaDataAccessControl obj);
}
