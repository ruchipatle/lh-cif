package com.lh.cif.iam.respository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.appengine.api.NamespaceManager;
import com.lh.cif.gsuite.datastore.DatastoreWorker;
import com.lh.cif.iam.domain.entity.RoleVsAction;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class RoleVsActionDAOImpl implements RoleVsActionDAO {

	@Override
	public List<String> getActions(final String role) {

		String oldNameSpace = NamespaceManager.get();
		List<String> actions = new ArrayList<String>();

		try {

			NamespaceManager.set("");
			DatastoreWorker<RoleVsAction> roleVsActionDetails = new DatastoreWorker<>(RoleVsAction.class);
			List<RoleVsAction> roles = roleVsActionDetails.getBy("Role =", role);
			if (roles.size() > 0) {
				RoleVsAction roleVsAction = roles.get(0);
				actions.addAll(roleVsAction.getActions());
			}
		} catch (Exception e) {
			log.error("RoleVsActionDAOImpl Exception " + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
		} finally {
			NamespaceManager.set(oldNameSpace);
		}
		return actions;
	}

}
