package com.lh.cif.iam.web;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.services.admin.directory.model.Member;
import com.lh.cif.core.domain.dto.GenericRequestDto;
import com.lh.cif.core.domain.dto.Response;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.service.CacheService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class IAMRest {

	@Autowired
	GsuiteManager gsuiteManager;


	@Autowired
	CacheService cacheService;
	
	Map<String, List<Member>> approverCache = new HashMap<String, List<Member>>();

	@RequestMapping(value = { "/ims/activeusercheck" }, method = RequestMethod.POST)
//	public Response activeUserCheck(final HttpServletRequest request, final HttpServletResponse response, @RequestBody GenericRequestDto genericRequestDto, final HttpSession session, Model map) {
//
//		Response resp = new Response();
//		UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");
//		try {
//			String[] approvers = genericRequestDto.getInput().split("\\|"); // different approval level members, separated by "|"
//			log.info("approvers" + approvers);
//			Map<String, List<String>> apprMap = StringUtil.getApproversMap(approvers);
//			Map<String, List<String>> inactiveApprovers = gsuiteManager.getInActiveApproversMap(apprMap);
//			boolean proceed = false;
//			StringBuffer message = new StringBuffer();
//
//			if (null != inactiveApprovers && inactiveApprovers.size() != 0) {
//				Set<String> levels = inactiveApprovers.keySet();
//				message.append("Following Approvers do not have active google account.");
//				for (String level : levels) {
//					message.append("\n");
//					message.append("Approver");
//					message.append(level + " : ");
//					message.append(inactiveApprovers.get(level).toString().replace("[", "").replace("]", ""));
//					message.append("\n");
//					proceed = false;
//				}
//			} else {
//				message.append("All approvers are active");
//				proceed = true;
//			}
//			resp.setMessage(message.toString());
//			resp.setFlag(proceed);
//			resp.setStatus(HttpStatus.OK);
//			resp.setCountryCode(userInfoSession.getCountryCode());
//		} catch (Exception e) {
//			printException("Report exception...", e);
//			resp.setMessage("FAIL");
//			resp.setStatus(HttpStatus.BAD_REQUEST);
//			resp.setCountryCode(userInfoSession.getCountryCode());
//		}
//		return resp;
//	}
//
//	@RequestMapping(value = { "/ims/loadApprovers" }, method = RequestMethod.POST)
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public Map getApproversList(final HttpServletRequest request, final HttpServletResponse response, @RequestBody GroupMembersDto groupMembersDto, final HttpSession session, Model map) {
//
//		Map resp = new HashMap();
//		UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");
//		try {
//			List<Member> approvers = cacheService.getApprovers(groupMembersDto.getGroupEmail());
//			//minor version
//			boolean flag = groupMembersDto.isCreatorCanApprove();
//			log.info("Disable creator approval : "+flag);
//			if(flag){/*
//			ListIterator<Member> iter = approvers.listIterator();
//			while(iter.hasNext()){
//					if(iter.next().getEmail().equals(groupMembersDto.getCreatedBy())){
//			        iter.remove();
//			    }
//			}
//			ListIterator<Member> iter1 = approvers.listIterator();
//			while(iter1.hasNext()){
//				if(groupMembersDto.getOwnerId() != null) {
//				if(iter1.next().getEmail().equals(groupMembersDto.getOwnerId())){
//					iter1.remove();
//		    }
//		}
//			}
//			
//			*/
//	
//			for (int i =0 ; i<approvers.size() ; i++) {
//				if (approvers.get(i).getEmail().equalsIgnoreCase(groupMembersDto.getCreatedBy()) || approvers.get(i).getEmail().equalsIgnoreCase(groupMembersDto.getOwnerId())
//						||userInfoSession.getUserEmail().equalsIgnoreCase(approvers.get(i).getEmail()) ) {
//					approvers.remove(approvers.get(i));
//					}
//				}
//			}
//
//			resp.put("activeUserList", approvers);
//			resp.put("countryCode", userInfoSession.getCountryCode());
//		} catch (Exception e) {
//			printException("Report exception...", e);
//			resp.put("countryCode", userInfoSession.getCountryCode());
//		}
//		return resp;
//	}
//
//	@RequestMapping(value = { "/ims/loadContentEditors" }, method = RequestMethod.POST)
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public Map getContentEditorsList(final HttpServletRequest request, final HttpServletResponse response, @RequestBody GroupMembersDto groupMembersDto, final HttpSession session, Model map) throws Throwable {
//
//		Map resp = new HashMap();
//		UserInfoSession userInfoSession = (UserInfoSession) session.getAttribute("userInfoSession");
//		log.info("groupMembersDto.getGroupEmail() ====> " + groupMembersDto.getGroupEmail());
//		try {
//			List<Map<String, String>> contentEditors = cacheService.getContentEditors(groupMembersDto.getGroupEmail());
//			resp.put("activeMemberList", contentEditors);
//			resp.put("countryCode", userInfoSession.getCountryCode());
//		} catch (Exception e) {
//			printException("Report exception...", e);
//			resp.put("countryCode", userInfoSession.getCountryCode());
//		}
//		return resp;
//	}
//	
	public void printException(String callerFunction, Exception e) {
		String errorMessage = e.getMessage();
		if (null == errorMessage || errorMessage.isEmpty()) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			log.error("\n " + callerFunction + "\n" + sw.toString());
		} else {
			log.error("\n " + callerFunction + "\n" + e.getMessage());
		}
	}
}
