package com.lh.cif.iam.domain.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
@Entity
public class MetaDataAccessControl implements Serializable {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	@Id
	Long id;
	@Index
	private String masterId ;
	@Index
	private String metaDataType;
	@Index
	private String groupId ;
}
