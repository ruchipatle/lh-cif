package com.lh.cif.iam.domain.entity;

import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import lombok.Data;
import lombok.ToString;

@ToString
@Entity
@Data
public class RoleVsAction {

	@Id
	private Long ID;

	private String Role;

	private List<String> Actions;
}
