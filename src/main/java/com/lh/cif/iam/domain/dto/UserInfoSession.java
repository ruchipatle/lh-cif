package com.lh.cif.iam.domain.dto;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.api.client.auth.oauth2.Credential;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
@Component
public class UserInfoSession implements Principal, Serializable {

	private static final long serialVersionUID = 1L;

	private String locale;

	private String userID;

	private String userName;

	private String userFirstName;

	private String userEmail;

	private String role;

	private String opcoName;

	private String photo;

	private String userCompanyGroupEmailId;

	private String countryCode;

	private boolean appAccess;

	private List<String> actions;

	private List<String> authorizedOpcos;

	private Map<String, String> opcoRoleMap;

	private Map<String, Map<String, String>> countryCodeLocaleMap;

	private Map<String, Map<String, String>> countryCodeDefaultLocaleMap;

	private Map<String, String> authorizedOpcoMap;

	@Override
	public String getName() {
		return null;
	}
	
	private boolean isRestricted ;
	
	private Map<String, String> restrictedMetaData ;

}
