package com.lh.cif.iam.domain.entity;

import java.io.Serializable;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;
import lombok.ToString;

/**
 *
 */
@ToString
@Data
@Entity /* (name="IMSConfigurationDetails") */
public class OpcoLocalDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	Long id;

	@Index
	private String opco = null;

	@Index
	private String locale = null;

	@Index
	private String language = null;

	@Index
	private String countryCode = null;

	@Index
	private List<String> companyGroupName = null;

	@Index
	private List<String> ceGroupName = null;

	@Index
	private List<String> adminGroupName = null;

	@Index
	private List<String> readersGroup = null;

	@Index
	private List<String> allGroup = null;

	@Index
	private String serviceOwnerId = null;

	@Index
	private int approvalLevel = 1;

	@Index
	private String editPermissionId = null;

	@Index
	private List<String> approverGroupName = null;

	@Index
	private String approvalMandatory = "Y";

	@Index
	private String overAllPermission = null;

	@Index
	private String readPermissionGroup = null;

	@Index
	private String defaultLocale = "en";

	/* added as part of footer update changes in versioning */
	@Index
	private boolean FooterFlag;

	/* added as part of footer update using app script and execution api */
	@Index
	private boolean AppScriptFlag;

	@Index
	private boolean isMetadataEditableAdmin;

	@Index
	private boolean isMetadataEditableEditor;

	@Index
	private boolean isParent; // value is true only for Labs(Europe) and CREST(LATAM)

	@Index
	private List<String> customCompanyGroupEmails;
	
	@Index
	private boolean showCloudUpload;
	@Index
	private boolean allowMinorVersion;
	@Index
	
	private boolean isRemoveFromApprovalWorkflowAdmin;
	@Index
	private boolean isRemoveFromApprovalWorkflowEditor;
	@Index
	private boolean creatorCanApprove;
	
	@Index
	private boolean distributionListInAudit ;
	
	
	@Index
	private boolean sendReviewMailToAprover ;
	@Index
	private boolean isMetaDataAccessControl ; 
	
	@Index
	private  List<String> controlledMetaData  ; 
	
	@Index
	private boolean isAcknowLedgeFeature ;
	
	@Index
	private boolean isDriveCommentsFeature ;
}
