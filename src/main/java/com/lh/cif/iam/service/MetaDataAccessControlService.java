package com.lh.cif.iam.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.lh.cif.iam.domain.entity.MetaDataAccessControl;

@Service
public interface MetaDataAccessControlService {

	public List<MetaDataAccessControl> getAllRestrictedMetaData(String opco);
	public Map<String, String> getMasterIdGrpIdMap(String opco);
	public List<String> getRestrictedGroups (String opco);
	public void saveEntity(MetaDataAccessControl obj);
	public void saveEntity(String groupId, String masterId);
}
