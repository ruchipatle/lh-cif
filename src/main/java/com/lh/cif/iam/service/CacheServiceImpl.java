package com.lh.cif.iam.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.Members;
import com.google.api.services.admin.directory.model.User;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.directory.MembersDirectoryService;
import com.lh.cif.gsuite.directory.UsersDirectoryService;

import lombok.extern.slf4j.Slf4j;

@Service("cacheService")
@Slf4j
public class CacheServiceImpl implements CacheService {

	@Autowired
	GsuiteManager gsuiteManager;

	@Autowired
	CacheManager cacheManager;

	@Override
	public List<Member> getApprovers(String approverGroupEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		log.info("approverGroupEmail ===========> " + approverGroupEmail);
		return (List<Member>) getApproversFromCache(approverGroupEmail);
	}

	@Override
	public List<Map<String, String>> getContentEditors(String contentEditorGroupEmail) throws Throwable {
		return (List<Map<String, String>>) getContentEditorsFromCache(contentEditorGroupEmail);
	}

	private Object getApproversFromCache(Object identifier) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		Object cachedObject = cacheManager.getCache(identifier);
		if (cachedObject == null) {
			List<Member> memberList = getMembers(identifier.toString());
			cacheManager.putCache(new CachedObject(memberList, identifier, 15));
			cachedObject = cacheManager.getCache(identifier);
		}
		return cachedObject;
	}

	private Object getContentEditorsFromCache(Object identifier) throws Throwable {
		Object cachedObject = cacheManager.getCache(identifier);
		if (cachedObject == null) {
			List<Member> contentEditors = getMembers(identifier.toString());
			// log.info("contentEditors ======> "+contentEditors);
			List<Map<String, String>> activeMembers = new ArrayList<>();
			if (null != contentEditors) {
				activeMembers = applyActiveUserCheck(contentEditors);
				log.info("activeMembers =====> " + activeMembers);
				cacheManager.putCache(new CachedObject(activeMembers, identifier, 15));
				cachedObject = cacheManager.getCache(identifier);
			}
		}
		return cachedObject;
	}

	private List<Map<String, String>> applyActiveUserCheck(List<Member> contentEditors) throws Throwable {
		List<Map<String, String>> activeMembers = new ArrayList<>();
		UsersDirectoryService userDirService = new UsersDirectoryService(gsuiteManager.getAdminOauth());
		for (Member member : contentEditors) {
			log.info("member.getEmail() === > " + member.getEmail());
			if (gsuiteManager.getActiveUserStatus(member.getEmail())) {
				log.info("user is active");
				User individualUser = userDirService.get(member.getEmail());
				Map map = new HashMap<String, String>();
				map.put("name", individualUser.getName().getFullName() + "|" + member.getEmail());
				map.put("role", member.getRole());
				activeMembers.add(map);
			} else {
				log.info("suspended user is " + member.getEmail());
			}
		}
		return activeMembers;
	}

	private List<Member> getMembers(String approverGroupEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException {
		log.info("GroupEmail " + approverGroupEmail);
		String paginationValue;
		List<Member> membersList = new ArrayList<Member>();
		Directory.Members.List res;
		res = new MembersDirectoryService(gsuiteManager.getAdminOauth()).list(approverGroupEmail);
		do {
			Members members = res.execute();
			List<Member> member = members.getMembers();
			if (member != null && member.size() > 0) {
				for (Member name : member) {
					membersList.add(name);
				}
			}
			paginationValue = members.getNextPageToken();
			res.setPageToken(paginationValue);
		} while (paginationValue != null);
		return membersList;
	}

}
