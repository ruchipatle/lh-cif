package com.lh.cif.iam.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import com.google.api.services.admin.directory.model.Member;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;

public interface CacheService {

	public List<Member> getApprovers(String approverGroupEmail) throws IOException, GeneralSecurityException, URISyntaxException, HolcimGoogleCredentialRetrievalException;

	public List<Map<String, String>> getContentEditors(String contentEditorGroupEmail) throws Throwable;

}
