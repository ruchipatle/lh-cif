package com.lh.cif.iam.service;

public interface Cacheable {

	/*
	 * This method will ensure expiration time for each object in the cache
	 */
	public boolean isExpired();

	/*
	 * This method will ensure that caching service is not responsible for uniquely
	 * identifying objects placed in cache
	 */
	public Object getIdentifier();

	/*
	 * This method will return the actual object
	 */
	public Object getCachedObject();

}
