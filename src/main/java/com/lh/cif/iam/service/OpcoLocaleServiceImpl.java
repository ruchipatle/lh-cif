package com.lh.cif.iam.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.iam.domain.entity.OpcoLocalDetails;
import com.lh.cif.iam.respository.OpcoLocaleDAO;

@Service
public class OpcoLocaleServiceImpl implements OpcoLocaleService {

	@Autowired
	OpcoLocaleDAO opcoLocaleDetailsDAO;

	@Override
	public List<OpcoLocalDetails> getEntityList() {
		String ns = NamespaceManager.get();
		try {
			NamespaceManager.set("");
			return this.opcoLocaleDetailsDAO.findAll();
		} finally {
			NamespaceManager.set(ns);
		}
	}

	@Override
	public OpcoLocalDetails getOpcoRecordByCompany(final String opco) {
		String ns = NamespaceManager.get();
		try {
			NamespaceManager.set("");
			Map<String, Object> columnValueMap = new HashMap<>();
			columnValueMap.put("countryCode", opco);
			return this.opcoLocaleDetailsDAO.getSingleResultfindByColumnAndValue(columnValueMap);
		} finally {
			NamespaceManager.set(ns);
		}
	}

	@Override
	public Map<String, String> getLanguagesByUserOpco(final String opco) {

		String ns = NamespaceManager.get();
		try {
			NamespaceManager.set("");

			Map<String, String> record = new HashMap<String, String>();
			Map<String, Object> columnValueMap = new HashMap<>();
			columnValueMap.put("countryCode", opco);
			List<OpcoLocalDetails> findByColumnAndValue = this.opcoLocaleDetailsDAO.getResultListfindByColumnAndValue(columnValueMap);
			for (OpcoLocalDetails opcoDetails : findByColumnAndValue) {
				record.put(opcoDetails.getLanguage(), opcoDetails.getLocale());
				record.put("defaultLocale", opcoDetails.getDefaultLocale());
			}
			return record;
		} finally {
			NamespaceManager.set(ns);
		}
	}

	// below method is pulled from admin module as part of merging
	@Override
	public List<OpcoLocalDetails> getOpcoRecordListByCompany(final String opco) {
		String ns = NamespaceManager.get();
		try {
			NamespaceManager.set("");
			Map<String, Object> columnValueMap = new HashMap<>();
			columnValueMap.put("countryCode", opco);
			return this.opcoLocaleDetailsDAO.getResultListfindByColumnAndValue(columnValueMap);
		} finally {
			NamespaceManager.set(ns);
		}
	}

}
