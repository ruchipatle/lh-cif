package com.lh.cif.iam.service;

import java.util.LinkedHashMap;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CacheManager {

	private LinkedHashMap<Object, Cacheable> cacheLinkedHashMap = new LinkedHashMap<Object, Cacheable>();

	public CacheManager() {

	}

	public void putCache(Cacheable object) {
		cacheLinkedHashMap.put(object.getIdentifier(), object);
	}

	public Object getCache(Object identifier) {

		Cacheable cachedObject = cacheLinkedHashMap.get(identifier);
		if (null == cachedObject) {
			log.info("Object not found in cache!!!!!!!!!!!!!!!");
			return null;
		} else if (null != cachedObject && cachedObject.isExpired()) {
			log.info("Object in the cache is expired!!!!!!!!!!!");
			return null;
		} else {
			return cachedObject.getCachedObject();
		}
	}
}
