package com.lh.cif.iam.service;

import java.util.List;

public interface RoleVsActionService {

	public List<String> getActions(String role);
}
