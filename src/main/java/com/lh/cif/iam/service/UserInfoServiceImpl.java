package com.lh.cif.iam.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.User;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.core.filter.security.util.UserInfoUtil;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.directory.DirectoryService;
import com.lh.cif.iam.domain.dto.UserInfoSession;
import com.lh.cif.iam.domain.entity.MetaDataAccessControl;
import com.lh.cif.iam.domain.entity.OpcoLocalDetails;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	OpcoLocaleService opcoLocaleService;

	@Autowired
	RoleVsActionService roleVsActionService;

	@Autowired
	IMSUserService googleUserService;

	@Autowired
	IMSGroupService googleGroupService;

	@Autowired
	GsuiteManager gsuiteManager;
	
	@Autowired
	MetaDataAccessControlService restrictedMetaDataService ;

	@Override
	public UserInfoSession getUserInfo(String userEmail, String requestLocale) {
		UserInfoSession userInfoSession = new UserInfoSession();

		List<String> userGroupEmailIds = new ArrayList<String>();
		try {
			// re-use this for Admin
			Credential credential = gsuiteManager.getAdminOauth();
			DirectoryService adminDirectory = gsuiteManager.getDirectory(credential);

			userInfoSession.setUserEmail(userEmail);
		//	userGroups = googleGroupService.getGroupsByUserEmail(userEmail, adminDirectory);
			User googleUser = googleUserService.getuserDetails(userEmail, adminDirectory);
			userInfoSession.setUserID(googleUser.getId());
			userInfoSession.setPhoto(googleUserService.getUserPhoto(userEmail, adminDirectory));
			userInfoSession.setUserName(WordUtils.capitalize(googleUser.getName().getGivenName()) + " " + WordUtils.capitalize(googleUser.getName().getFamilyName()));
			userInfoSession.setUserFirstName(WordUtils.capitalize(googleUser.getName().getGivenName()));

			userGroupEmailIds = UserInfoUtil.getUserGroups(userEmail,gsuiteManager,googleGroupService);
			
			// get user's OPCO and Role
//			List<OpcoLocalDetails> entityList = opcoLocaleService.getEntityList();
//
//			Map<String, List<String>> companyGroupMap = UserInfoUtil.getIMSCompanyGroupList(entityList);
//
//			/* find out all the opcos user can log in to */
//			UserInfoUtil.calculateAllowedOpcos(userGroupEmailIds, userInfoSession, companyGroupMap);
//			log.info("getUserInfo enter line 86---");
//
//			/* find out role in each opco for logged in user */
//			UserInfoUtil.findRoleInEachOpco(entityList, userInfoSession, userGroupEmailIds);
//
//			/* determine which opco to set in the session for first login */
//			UserInfoUtil.setUserOpco(userInfoSession, entityList);
//
//			// set the user locale
//			OpcoLocalDetails configObject = opcoLocaleService.getOpcoRecordByCompany(userInfoSession.getCountryCode());
//			log.info("getUserInfo enter line 96---");
//			
//
//			
//			Map<String, Map<String, String>> countryCodeLocaleMap = new HashMap<>();
//
//			for (String countryCode : userInfoSession.getAuthorizedOpcos()) {
//				countryCodeLocaleMap.put(countryCode, opcoLocaleService.getLanguagesByUserOpco(countryCode));
//			}
//			log.info("getUserInfo enter line 105---");
//
//			// get the actions allowed for the user
//			List<String> userActions = roleVsActionService.getActions(userInfoSession.getRole());
//
//			/*
//			 * compare locale from request(browser) with locale list if matches set it in userInfoSession else set default locale of the opco
//			 */
//			Set<Entry<String, String>> userLanguageListSet = countryCodeLocaleMap.get(userInfoSession.getCountryCode()).entrySet();
//			log.info("getUserInfo enter line 114---");
//
//			boolean flag = false;
//			for (Entry<String, String> ent : userLanguageListSet) {
//				if (ent.getValue().equals(requestLocale)) {
//					userInfoSession.setLocale(requestLocale);
//					flag = true;
//				}
//			}
//			if (!flag) {
//				userInfoSession.setLocale(configObject.getDefaultLocale());
//			}
//
//			userInfoSession.setCountryCodeLocaleMap(countryCodeLocaleMap);
//			userInfoSession.setActions(userActions);
			userInfoSession.setAppAccess(true);
			
			// IMS ZIMBABWE
//			boolean isRestrictedOpco = opcoLocaleService.getOpcoRecordByCompany(userInfoSession.getCountryCode()).isMetaDataAccessControl();
//				if(isRestrictedOpco) {
//				log.info("---------Restriction is enabled for opco: "+userInfoSession.getCountryCode());
//				userInfoSession.setRestricted(isRestrictedOpco);
//				List<MetaDataAccessControl> restrictedMetaData = restrictedMetaDataService.getAllRestrictedMetaData(userInfoSession.getCountryCode());
//				UserInfoUtil.setRestrictedMetaData(userInfoSession, userGroupEmailIds, restrictedMetaData);
//			 }
		} catch (Throwable e) {
			log.error("Get User Info IOException" + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
		}

		return userInfoSession;
	}

	@Override
	public Credential getUserCredential(String userEmail) {
		return googleUserService.getUserCredential(userEmail);
	}

	// Below method is not being used anywhere  yet
	@Override
	public void ifRestrcitionChanged(UserInfoSession userInfoSession, String sessionEmail)  {
		try {
	
		boolean isRestrictedOpco = opcoLocaleService.getOpcoRecordByCompany(userInfoSession.getCountryCode()).isMetaDataAccessControl();
		if(isRestrictedOpco !=userInfoSession.isRestricted()) {
			log.info("##########RESTRICTION HAS BEEN CHANGED############ ----- ");
			userInfoSession.setRestricted(isRestrictedOpco);
			if(isRestrictedOpco) {
			List<String> userGroupEmailIds = UserInfoUtil.getUserGroups(sessionEmail,gsuiteManager,googleGroupService);
			
			List<MetaDataAccessControl> restrictedMetaData = restrictedMetaDataService.getAllRestrictedMetaData(userInfoSession.getCountryCode());
			UserInfoUtil.setRestrictedMetaData(userInfoSession, userGroupEmailIds, restrictedMetaData);
			} else {
				userInfoSession.setRestrictedMetaData(null);
			}
		} else {
			log.info("Nothing needs to be changed --------------------------");
		}
		} catch (Throwable e ) {
			log.error("Error while checking if restriction was changed during session Or not " + e.getMessage() + " " + Throwables.getStackTraceAsString(e));
		}
	}
}
