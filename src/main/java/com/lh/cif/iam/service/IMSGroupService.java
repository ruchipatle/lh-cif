package com.lh.cif.iam.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.google.api.services.admin.directory.model.Groups;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.Members;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.gsuite.directory.DirectoryService;

@Service
public class IMSGroupService {

	public Groups getGroupsByUserEmail(String userEmail, DirectoryService adminDirectory) throws Throwable {
		return adminDirectory.getGroups().list(userEmail);
	}

	/**
	 * Get All Members by Group Email ID
	 *
	 * @param groupEmail
	 * @return
	 * @throws Throwable
	 */

	public List<Member> getMembersByGroupEmailId(String groupEmail, DirectoryService adminDirectory) {

		List<Member> allMembers = new ArrayList<Member>();
		String paginationValue = null;
		do {
			Members members = adminDirectory.getMembers().list(groupEmail, paginationValue);
			allMembers.addAll(members.getMembers());
			paginationValue = members.getNextPageToken();
		} while (paginationValue != null);

		return allMembers;
	}

	/**
	 * Adds the current user to a group
	 *
	 * @param googleGroupId
	 *            The group email address
	 * @param userEmail
	 *            The user email
	 * @return True if the user has been added. False if it is already present in the group
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws HolcimGoogleCredentialRetrievalException
	 */
	public boolean addMemberToGroup(String googleGroupId, String userEmail, DirectoryService adminDirectory) {

		Member m = new Member();
		m.setEmail(userEmail);
		m.setRole("MEMBER");
		m.setType("USER");

		// Add the user to the group if he is not already present
		Set<String> members = adminDirectory.getMembers().listAll(googleGroupId);

		if (!members.contains(m.getEmail())) {
			adminDirectory.getMembers().insert(googleGroupId, m);
			return true;
		}
		return false;
	}

	/**
	 * Remove the current user from the group
	 *
	 * @param googleGroupId
	 *            The group email address
	 * @param userEmail
	 *            The user email
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws HolcimGoogleCredentialRetrievalException
	 */
	public void removeMemberFromGroup(final String googleGroupId, final String userEmail, DirectoryService adminDirectory) {

		Member m = new Member();
		m.setEmail(userEmail);
		m.setRole("MEMBER");
		m.setType("USER");

		// Remove the user from the group
		adminDirectory.getMembers().delete(googleGroupId, userEmail);
	}
}
