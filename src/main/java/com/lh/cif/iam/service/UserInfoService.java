package com.lh.cif.iam.service;

import com.google.api.client.auth.oauth2.Credential;
import com.lh.cif.iam.domain.dto.UserInfoSession;

public interface UserInfoService {

	public UserInfoSession getUserInfo(String userEmail, String locale);

	public Credential getUserCredential(String userEmail);
	
	public void ifRestrcitionChanged (UserInfoSession userInfoSession, String sessionEmail);
}
