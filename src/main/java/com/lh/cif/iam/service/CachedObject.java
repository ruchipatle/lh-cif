package com.lh.cif.iam.service;

import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CachedObject implements Cacheable {

	private Date dateofExpiration = null;
	private Object identifier = null;
	private Object object = null;

	public CachedObject(Object obj, Object id, int minutesToLive) {
		this.object = obj;
		this.identifier = id;
		// minutesToLive of 0 means it lives on indefinitely.
		if (minutesToLive != 0) {
			dateofExpiration = new java.util.Date();
			java.util.Calendar cal = java.util.Calendar.getInstance();
			cal.setTime(dateofExpiration);
			cal.add(Calendar.MINUTE, minutesToLive);
			dateofExpiration = cal.getTime();
		}
	}

	@Override
	public boolean isExpired() {
		if (dateofExpiration != null) {
			if (dateofExpiration.before(new Date())) {
				log.info("Object is expired from cache");
				return true;
			} else {
				log.info("Object exists in the cache");
				return false;
			}
		} else { // This means it lives forever!
			return false;
		}
	}

	@Override
	public Object getIdentifier() {
		// TODO Auto-generated method stub
		return identifier;
	}

	@Override
	public Object getCachedObject() {
		return this.object;
	}

}
