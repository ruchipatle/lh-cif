package com.lh.cif.iam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.appengine.api.NamespaceManager;
import com.lh.cif.iam.domain.entity.MetaDataAccessControl;
import com.lh.cif.iam.respository.MetaDataAccessControlDAO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MetaDataAccessControlServiceImpl implements MetaDataAccessControlService {

	@Autowired
	MetaDataAccessControlDAO restrictedMetaDataService ;
	
	@Override
	public List<MetaDataAccessControl> getAllRestrictedMetaData(String opco) {
		String ns = NamespaceManager.get();
		try {
			NamespaceManager.set(opco);
		return restrictedMetaDataService.getAllRestrictedMetaData();
		} catch ( Exception e){
			log.error("error occured in getting MetaDataAccessControl details: "+e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			NamespaceManager.set(ns);
		}
		
	}
	
	// this will provide map of master id and restricted group email id 
	@Override
	public Map<String, String> getMasterIdGrpIdMap(String opco){
		List<MetaDataAccessControl>  metaList = getAllRestrictedMetaData(opco);
		Map<String, String> resTrictedMetaDataMap = new HashMap<String, String>();
		for (MetaDataAccessControl m : metaList) {
			resTrictedMetaDataMap.put(m.getMasterId().trim(), m.getGroupId().trim());
		}
		return resTrictedMetaDataMap;
	}

	@Override
	public List<String> getRestrictedGroups(String opco) {
		// TODO Auto-generated method stub
		List<MetaDataAccessControl>  metaList = getAllRestrictedMetaData(opco);
		List<String> restrictedGroups = new ArrayList<String>();
		for (MetaDataAccessControl obj : metaList) {
			restrictedGroups.add(obj.getGroupId());
		}
		return restrictedGroups;
	}
	@Override
	public void saveEntity(MetaDataAccessControl obj) {
		restrictedMetaDataService.saveEntry(obj);
	}

	@Override
	public void saveEntity(String groupId, String masterId) {
		MetaDataAccessControl obj = new MetaDataAccessControl();
		obj.setGroupId(groupId);
		obj.setMasterId(masterId);
		obj.setMetaDataType("Department");
		saveEntity(obj);
	}
}
