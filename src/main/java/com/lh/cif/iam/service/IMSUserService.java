package com.lh.cif.iam.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.UserPhoto;
import com.holcim.cc.framework.credentials.HolcimGoogleCredentialRetrievalException;
import com.lh.cif.gsuite.GsuiteManager;
import com.lh.cif.gsuite.directory.DirectoryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IMSUserService {

	@Autowired
	GsuiteManager gsuiteManager;

	public User getuserDetails(String userEmail, DirectoryService adminDirectory) throws Throwable {
		return adminDirectory.getUsers().get(userEmail);
	}

	public String getUserPhoto(String userEmail, DirectoryService adminDirectory) {

		// Default Photo
		String userPhotoData = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAoACgDASIAAhEBAxEB/8QAGwABAAEFAQAAAAAAAAAAAAAAAAcBAgUGCAP/xAAYAQADAQEAAAAAAAAAAAAAAAADBAUGB//aAAwDAQACEAMQAAABn6jUukZHar+YpneFuwmHrgc1ELY8JLERZqwrM7wZ17w5yLqd1C0r0CMXU//EACEQAAEEAQQDAQAAAAAAAAAAAAUBAgQGEAADEzUSFRYx/9oACAEBAAEFAscjeXNyK7goV4IuqQW3CUDFjDe8GrVyDd2rglBQdOejGk75JlP+jnqo+9TIr48hsqOR61PzFd6L/8QAHhEAAgICAgMAAAAAAAAAAAAAAQIAAxARBBIiMUH/2gAIAQMBAT8BZtDcrv7HUMtZQPKVsinHK+YX0J//xAAiEQABAwMDBQAAAAAAAAAAAAADAAECBAUREBJBITEycYH/2gAIAQIBAT8BENySaDcqrtzhhvznSkCWc8i4VWCqJDb0Tqz9pfE6L5v7X//EACsQAAECBAIJBQEAAAAAAAAAAAECAwAEERIQMRMUICFBUVJzkWFxobGy0f/aAAgBAQAGPwLCy5N/TXfsDRG1x5VgV0xlDrbyitcuQLjmQcvrEtXBLiTegnnFmqOV+PMFKyFPOm5dMh6YFSjRKRUnlBEsBLtcDSqz/Irrj9feBpqTLfEHcrzCHUGqHBcIme0v8nYk+0I//8QAIhABAAECBgIDAAAAAAAAAAAAAREAIRAxQVFxgWHBIKGx/9oACAEBAAE/IcEAweUE+s8YqUePmYhVPNo7pWyFWVbq70WCcky9zqkr8YwxoWwZs+EU7q9TLMh+6KlMxb9AG8b7rgThhGQLrWsSAPNZtwK/NAeqBNuB1D2RxTai1SLJhIyuPgn/2gAMAwEAAgADAAAAEETMiAQAAAP/xAAcEQACAgIDAAAAAAAAAAAAAAAAARFBECExgaH/2gAIAQMBAT8QQ5qEaJCyF2onqns09o5YeJH/xAAdEQACAwACAwAAAAAAAAAAAAAAAREhMXHRobHw/9oACAECAQE/ENKaECqlJ1G1OuSzNDtLxdk3NlrSlNxz2UcMy/NKHnvZ/8QAIBABAQACAgEFAQAAAAAAAAAAAREAITFBYRBRcYGRof/aAAgBAQABPxDF1gMBqyvcuvzDn7nGHOMe35jkUdHfZ1ADyNGzFGCg7Cl2rzXa+cVAkYAwm0HW0a3VDEuGTAAUgaNiAlm01lw0izfJwPN+sAoBigK3QX2KjWXBAIZolHgBfrDRBQoOkpHoGWKuNEG3BPzT+ZvIRDJ2kl8IuLVLoC+EiU6epvdOvQQZ8LOfQT5D+Z//2Q==";
		;
		String mimeType = "image/jpeg";

		StringBuilder sb = new StringBuilder();

		try {
			UserPhoto googleUserPhoto = adminDirectory.getUsers().getUserPhoto(userEmail);
			if (null != googleUserPhoto) {
				userPhotoData = googleUserPhoto.getPhotoData().replaceAll("_", "\\/").replaceAll("-", "\\+").replaceAll("\\*", "=");
				mimeType = googleUserPhoto.getMimeType();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		sb.append("data:");
		sb.append(mimeType);
		sb.append(";base64,");
		sb.append(userPhotoData);

		return sb.toString();
	}

	public Credential getUserCredential(String email) {

		boolean isCredentialNotAcquired = true;

		Credential userCredential = null;

		do {
			try {
				userCredential = gsuiteManager.getOauth(email);
				if (null != userCredential && null != userCredential.getAccessToken())
					isCredentialNotAcquired = false;
			} catch (IOException | GeneralSecurityException | URISyntaxException | HolcimGoogleCredentialRetrievalException e) {
				log.error("************* Error in getting User Credential");
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					log.error("************* Error in making thread wait for 1 seconds");
					e1.printStackTrace();
				}
			}

		} while (isCredentialNotAcquired);

		return userCredential;

	}

	public Map<String, List<User>> getUserStatus(List<Member> listOfUsers, DirectoryService adminDirectory) throws Throwable {

		Map<String, List<User>> userStatusMap = new HashMap<>();

		List<User> activeUsers = new ArrayList<>();
		List<User> suspendedUsers = new ArrayList<>();

		for (Member member : listOfUsers) {
			User userDetail = getuserDetails(member.getEmail(), adminDirectory);
			if (!userDetail.getSuspended()) {
				activeUsers.add(userDetail);
			} else {
				suspendedUsers.add(userDetail);
			}
		}

		userStatusMap.put("activeUserList", activeUsers);
		userStatusMap.put("suspendedUserList", suspendedUsers);

		return userStatusMap;
	}

}
