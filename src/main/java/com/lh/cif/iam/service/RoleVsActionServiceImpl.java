package com.lh.cif.iam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lh.cif.iam.respository.RoleVsActionDAO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoleVsActionServiceImpl implements RoleVsActionService {

	@Autowired
	RoleVsActionDAO roleVsActionDAO;

	@Override
	public List<String> getActions(final String role) {
		return this.roleVsActionDAO.getActions(role);
	}

}
