package com.lh.cif.iam.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.lh.cif.iam.domain.entity.OpcoLocalDetails;

/**
 * ONLY to replicate current functionality . Will be depracated with new
 * Authorization Service
 *
 * @author anbharti
 *
 */
@Service
public interface OpcoLocaleService {

	public List<OpcoLocalDetails> getEntityList();

	public OpcoLocalDetails getOpcoRecordByCompany(String opco); // this gets only 1 record to fetch
																	// lcoale only here...

	public Map<String, String> getLanguagesByUserOpco(String opco); // this gets list of languages
																	// for a particular opco...

	public List<OpcoLocalDetails> getOpcoRecordListByCompany(String opco);// this gets list records

}
