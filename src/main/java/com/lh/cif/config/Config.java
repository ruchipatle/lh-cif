package com.lh.cif.config;

import java.util.logging.Logger;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.appengine.api.NamespaceManager;
import com.google.common.collect.ImmutableSet;
import com.holcim.cc.framework.config.HolcimConfigHelper;
import com.holcim.cc.framework.config.HolcimConfigRetrievalException;

/**
 * Created by Ankit Bharti 11-June-2016
 */
public class Config {

	private static Logger LOG = Logger.getLogger(Config.class.getName());

	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	public static HolcimConfigHelper holcimConfigHelper;

	private static Config config = null;

	public Config() {
		String curNS = NamespaceManager.get();
		try {
			NamespaceManager.set("");
			Config.holcimConfigHelper = HolcimConfigHelper.getInstance("DEFAULT_CONFIG_3.X");
		} catch (HolcimConfigRetrievalException e) {
			LOG.severe(e.getMessage() + " " + Throwables.getStackTraceAsString(e));
		} finally {
			NamespaceManager.set(curNS);
		}
	}

	public static Config getConfig() {
		return getConfig(false);
	}

	public static Config getConfig(final Boolean force) {
		if (config != null && force == false)
			return config;
		return new Config();
	}

	public enum Keys {
		googleAdminUser, driveUser;

		public String get() {
			getConfig();
			return Config.holcimConfigHelper.getValue(this.name());
		}

		public boolean isTrue() {
			getConfig();
			return Config.holcimConfigHelper.getValue(this.name()).equals("true");
		}

		public Integer getInt() {
			getConfig();
			return Integer.parseInt(Config.holcimConfigHelper.getValue(this.name()));
		}

		public ImmutableSet<String> getSet() {
			getConfig();
			return ImmutableSet.copyOf(Config.holcimConfigHelper.getValue(this.name()).split(","));
		}
	}

}
