package com.lh.cif.config;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum AppConfig {

	DEV("search-doc-library-267015"), QA("lh-ims-qa"), PROD("corp-search-doc-library");

	public final static String APPNAME = "Corporate Search Doc Library";

	public final static int BATCH_SIZE = 500;

	public final static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	public final static JsonFactory JSON_FACTORY = new JacksonFactory();

	public final String applicationId;

	public final String serviceAccountEmail;

	public final String clientId;


	public final static String DEV_APP = "search-doc-library-267015";
	public final static String DEV_CLIENT_ID = "117989646618942680513";
	public final static String DEV_EMAIL_SERVICE_ACCOUNT = "service-account-1@search-doc-library-267015.iam.gserviceaccount.com";

	public final static String QA_APP = "lh-cif-qa";
	public final static String QA_CLIENT_ID = "100553229013433036117";
	public final static String QA_EMAIL_SERVICE_ACCOUNT = "lh-ims-qa@lh-ims-qa.iam.gserviceaccount.com";

	public final static String PROD_APP = "corp-search-doc-library";
	public final static String PROD_CLIENT_ID = "112665705378403409859";
	public final static String PROD_EMAIL_SERVICE_ACCOUNT = "service-account1@corp-search-doc-library.iam.gserviceaccount.com";

	public static AppConfig get(final String applicationId) {
	    log.info("AppConfig....... get.....applicationId : "+applicationId);
		for (AppConfig config : AppConfig.values()) {
			if (config.applicationId.equals(applicationId))
				return config;
		}
		log.info("No applicationId in environment, using ", DEV);
		return DEV;
	}

	AppConfig(final String applicationId) {
		this.applicationId = applicationId;

		switch (applicationId) {
		
		case DEV_APP:
			this.serviceAccountEmail = DEV_EMAIL_SERVICE_ACCOUNT;
			this.clientId = DEV_CLIENT_ID;
			break;
		case QA_APP:
			this.serviceAccountEmail = QA_EMAIL_SERVICE_ACCOUNT;
			this.clientId = QA_CLIENT_ID;
			break;
		case PROD_APP:
			this.serviceAccountEmail = PROD_EMAIL_SERVICE_ACCOUNT;
			this.clientId = PROD_CLIENT_ID;
			break;
		default:
			this.serviceAccountEmail = DEV_EMAIL_SERVICE_ACCOUNT;
			this.clientId = DEV_CLIENT_ID;
		}
	}
}
