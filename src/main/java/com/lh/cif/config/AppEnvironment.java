package com.lh.cif.config;

import com.google.appengine.api.utils.SystemProperty;

public class AppEnvironment {

	public static final String APP_NAME = "CIF";

	public static final String APP_SENDER_NAME = "[" + APP_NAME + "]";

	public static final AppConfig CONFIG;

	static {
		CONFIG = AppConfig.get(SystemProperty.applicationId.get());
	}

	public static boolean isStandalone() {
		return SystemProperty.environment.value() == null;
	}

	public static boolean isDev() {
		return SystemProperty.environment.value() == SystemProperty.Environment.Value.Development;
	}

	public static String getSenderEmail() {
		return "noreply@" + CONFIG.applicationId + ".appspotmail.com";
	}

	public static String getApplicationURL() {
		if (isStandalone() || isDev())
			return "http://localhost:8080/";
		return "https://" + CONFIG.applicationId + ".appspot.com";
	}

	public static String getRemoteUrl() {
		return "remote-dot-" + CONFIG.applicationId + ".appspot.com";

	}

	public static String getAppId() {
		if (isStandalone() || isDev())
			return "localhost";
		return CONFIG.applicationId;
	}

}
