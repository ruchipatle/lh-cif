var deleteDiv;
var addDeleteKeywordDiv;
var selectedComp = '';

var customElement = $("<div>", {
	id : "countdown",
	css : {
		"font-size" : "20px"
	},
	text : _PLEASE_WAIT
});

$.LoadingOverlaySetup({
	color : "rgba(255, 255, 255, 0.53)",
	/*
	 * image : "img/custom_loading.gif", maxSize : "80px", minSize : "20px", resizeInterval : 0, size : "50%", custom : customElement,
	 */
	fade : [ 500, 500 ],
	image : "",
	/*fontawesome : "fa fa-spinner fa-pulse fa-3x fa-fw",*/
	fontawesome : "fa fa-circle-o-notch fa-spin fa-3x fa-fw",
	maxSize : "50px",
	minSize : "20px"
});

$.LoadingOverlay("show", true);

var urlOpco = location.pathname.split("/")[3];
var urlLocale = location.pathname.split("/")[5];

/* alert("URL opco " + urlOpco + "\n URL locale " + urlLocale + "\n Request Country Code " + _COUNTRY_CODE + "\n Request locale " + _LOCALE); */

$(document).ready(
		function() {

			if ("" !== urlOpco && _COUNTRY_CODE !== urlOpco) {
				console.log("User manually entered OPCO Code")
				changeOpcoRestApi(_COUNTRY_CODE);
			}

			if ("" !== urlLocale && _LOCALE !== urlLocale) {
				console.log("User manually entered LOCALE Code")
				changeLangRestApi(_LOCALE);
			}

			// Initialize Select2 Elements
			$(".select2").select2();

			var d = new Date();
			d = d.getTime();
			if (jQuery('#reloadValue').val().length == 0) {
				jQuery('#reloadValue').val(d);
			} else {
				jQuery('#reloadValue').val('');
				location.reload();
			}
			$('select[name="langSel"]').val(_LOCALE);
			$('select[name="opcoSel"]').val(_COUNTRY_CODE);
			$("#selectedOpco").val(_COUNTRY_CODE);
			$("#selectedUser").val(_USER_EMAIL);

			// temp list of users
			var tempUserist = [ "ce1.ce@dev.holcim.com", "aprov1.ce@dev.holcim.com", "admin1.ce@dev.holcim.com", "ce1.br@dev.holcim.com", "aprov1.br@dev.holcim.com", "admin1.br@dev.holcim.com",
					"ce1.aiuk@dev.holcim.com", "aprov1.aiuk@dev.holcim.com", "admin1.aiuk@dev.holcim.com" ];
			$('select[name="user-select"]').append($('<option></option>').val(_USER_EMAIL).html(_USER_EMAIL));
			$.each(tempUserist, function(i, p) {
				if (_USER_EMAIL !== p) {
					$('select[name="user-select"]').append($('<option>' + p + '</option>').val(p).html(p));
				}
			});

			/* $('.sidebar-div a[href^="/ims/' + location.pathname.split("/")[2] + '"]').addClass('custom-link-highlight'); */
			$('.menubar a[href^="' + location.pathname + '"]').addClass('custom-link-highlight');

			$('select[name="langSel"]').change(function() {
				$.LoadingOverlay("show", true);
				var locale = $(this).val().replace("\"", "");
				changeLangRestApi(locale);
			});

			function changeLangRestApi(locale) {
				$.ajax({
					url : "/ims/change-language",
					type : "post",
					data : {
						"selectedLanguage" : locale
					},
					success : function(data) {
						var path = window.location.pathname.replace(urlLocale, locale);
						window.location.replace(path);
					},
					error : function(err) {
						console.log("There was an error " + err.responseText);
					}
				});
			}

			$('select[name="opcoSel"]').change(function() {
				// $.LoadingOverlay("show", true);
				var opco = $(this).val().replace("\"", "");
				changeOpcoRestApi(opco);
			});

			function changeOpcoRestApi(opco) {
				$.ajax({
					url : "/ims/change-opco",
					type : "post",
					data : {
						"selectedOpco" : opco,
						"pageURL" : window.location.href
					},
					success : function(data) {
						if (data == "OK_VIEW") {
						} else {
						}
						var path = window.location.pathname.replace(urlOpco, opco);
						window.location.replace(path);
					},
					error : function(err) {
						console.log("There was an error " + err.responseText);
					}
				});
			}

			$('select[name="user-select"]').on('click', function(event) {
				event.stopPropagation();
			});

			$('select[name="langSel"]').on('click', function(event) {
				event.stopPropagation();
			});

			$('select[name="opcoSel"]').on('click', function(event) {
				event.stopPropagation();
			});

			$('select[name="user-select"]').on('change', function() {
				var user = $(this).val().replace("\"", "");
				$.ajax({
					url : "/ims/change-user",
					type : "post",
					data : {
						"selectedUser" : user
					},
					success : function(data) {
						window.location.reload();
					},
					error : function(err) {
						console.log("There was an error " + err.responseText);
					}
				});
			});

			// Google Analytics
			var _gaq = _gaq || [];
			_gaq.push([ '_setAccount', _GOOGLE_ANALYTICS ]);
			_gaq.push([ '_trackPageview' ]);
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);

			$.LoadingOverlay("hide", true);

		});

/*
 * $(document).ajaxSend(function(event, jqxhr, settings){ $(".content-wrapper").LoadingOverlay("show", true); }); $(document).ajaxComplete(function(event, jqxhr, settings){ $(".content-wrapper").LoadingOverlay("hide",
 * true); });
 */

// ALL Ajax Request will call this
$(document).ajaxStart(function() {
	$(".content-wrapper").LoadingOverlay("show", true);
});
$(document).ajaxStop(function() {
	$(".content-wrapper").LoadingOverlay("hide", true);
});
