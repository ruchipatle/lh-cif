function resetFields() {
    document.getElementById("errMsg").innerHTML = "";
    
    $("#lookUpList").val("");
    $("#lookupId").val("");    
    $("#id").val("");
    $('#sortOrder').val("");
    $('#displayValue').val("");
}

function addLink() {
	validateForm();
}

function validateForm() {
    var isError = false;
    if (document.getElementById("lookUpList").value == -1 || document.getElementById("lookUpList").value == "") {
        swal({
            title: "",
            text: _SELECT_LOOUPLIST,
            type: "error"
        });
        isError = true;
        return false;
    }

    if (null == $('#displayValue').val() || $('#displayValue').val() == "") {
        swal({
            title: "",
            text: _DISPLAY_VALUE,
            type: "error"
        });
        isError = true;
        return false;
    }

    if (null == $('#sortOrder').val() || $('#sortOrder').val() == "") {
        swal({
            title: "",
            text: _SORT_ORDER,
            type: "error"
        });
        isError = true;
        return false;
    } else {
        var onlyNumeric = $('#sortOrder').val();
        if (!onlyNumeric.match(/^\d+$/) || !onlyNumeric.match(/[1-9]+/)) {
            swal({
                title: "",
                text: _NUMREIC_CHK_ALERT,
                type: "error"
            });
            isError = true;
            return false;
        }
    }

    if (!isError) {
    	var id = $('#id').val();
        var jsonLookupId = $('#lookupId').val();
        var jsonLookUpType = $('#lookUpList').val();
        var jsonSortValue = $('#sortOrder').val();
        var jsonDisplayValue = $('#displayValue').val();
        var jsonCreateData = {
        	"id":id,
            "lookupId": jsonLookupId,
            "lookupType": jsonLookUpType,
            "sortOrder": jsonSortValue,
            "displayValue": jsonDisplayValue
        };
        var createData = JSON.stringify(jsonCreateData);
        swal({
                title: _SAVE_RECORD_CONFRIM_BOX_TITLE,
                text: _CONFIRM_TO_SAVE_RECORD_MSG,
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                cancelButtonText: _CANCEL_BUTTON
            },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/cif/api/admin/masterdata",
                    data: createData,
                    dataType: "json",
                    contentType: "application/json",
                    success: function (response) {
                        if (response.flag) {
                            swal({
                                    title: _SAVE_MASTER_DATA_SUCCESS,
                                    text: _PLEASE_WAIT,
                                    type: "success",
                                    timer: 500,
                                    showConfirmButton: false
                                },
                                function () {
                                    window.location.replace("/cif/admin/masterdata/manage");
                                });
                        } else {
                            swal({
                                title: _PLEASE_TRY_AGAIN,
                                text: _SAVE_MASTER_DATA_FAIL,
                                type: "error"
                            });
                        }

                    },
                    error: function (
                        xhr,
                        status,
                        errorThrown) {
                        swal({
                            title: _PLEASE_TRY_AGAIN,
                            text: _SAVE_MASTER_DATA_FAIL,
                            type: "error"
                        });
                    }
                });

            });
    }
}


$(document).ready(function () {
//    if (document.getElementById("lookUpList").value == "Folder") {
//        $("#section").show();
//        $('#section option').each(function () {
//            if ($(this).val() == _SECTION_NAME_FOLDER_EDIT_ACTION) {
//                $(this).attr('selected', 'selected');
//            }
//        });
//        $("#sectionVal").attr("disabled", "disabled");
//    } else {
//        $("#section").hide();
//    }
});
