var asInitVals = new Array();
var oSettings = null;

$(document).ready(function () {	
	
	$(function () {

    $('.navTitle').html(title);

    $("thead input").each(function (i) {
        asInitVals[i] = this.value;
    });

    $("thead input").focus(function () {
        if (this.className == "search_init") {
            this.className = "disp";
            this.value = "";
        }
    });

    $("thead input").blur(function (i) {
        if (this.value == "") {
            this.className = "search_init";
            this.value = asInitVals[$("thead input").index(this)];
        }
    });

    oTable = $('#cifMasterDataTable').dataTable({
    	responsive: true,
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "aoColumns": [
            {
                "asSorting": ["asc", "desc"]
            },
            {
                "asSorting": ["asc", "desc"]
            },
            {
                "asSorting": ["asc", "desc"]
            },
            {
                "asSorting": ["asc", "desc"]
            },
            {
                "asSorting": ["asc", "desc"]
            },
            {
                "asSorting": ["asc", "desc"]
            }
            
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false,
                     }],
        "oLanguage": {
        	 "sSearch": _SEARCH_DT,
             "sLengthMenu": _LENGTH_DT,
             "sInfoFiltered": _FILTERINFO_DT,
             "sInfoEmpty": _EMPTYINFO_DT,
             "sInfo": _INFO_DT
        },
        "sDom": "<'row'<'col-md-3 col-xs-12'<'totalEntries'i>><'col-md-2 col-xs-12 dataTables_menu_padding'l><'col-md-3 col-xs-12'f><'col-md-4 col-xs-12'<'nextData pull-right'>p<'clearfix'>>>t<'row'<'col-md-6'<'totalEntries'i>><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "bStateSave": true,
        "bSortCellsTop": true,
        "iDisplayLength": 10,
        "aaSorting": [],
        "fnInitComplete": function () {
            oSettings = $('#cifMasterDataTable').dataTable().fnSettings();

            for (var i = 0; i < (oSettings.aoPreSearchCols.length -1); i++) {
                if (oSettings.aoPreSearchCols[i].sSearch.length > 0) {
                    $("thead input")[i].value = oSettings.aoPreSearchCols[i].sSearch;

                    $("thead input")[i].className = "";                    

                }
            }
            
        }
        
    });

    $('.dataTables_filter input').attr('placeholder',_FILTER_DT);
    $('.dataTables_length select').addClass("form-control pull-right");

    $("thead input").keyup(function () {
        oTable.fnFilter(this.value, $("thead input").index(this));

        	if($('#cifMasterDataTable').dataTable().fnGetColumnData(5).join() == ""){
        		$("#exportButton").hide();
        	}else {
        		$("#exportButton").show();
        	}
    });
    oTable.fnSetColumnVis(5, false);
    
	});


	$("#exportButton").click(function () {
	    var ids = $('#cifMasterDataTable').dataTable().fnGetColumnData(5).join();
	    if (ids == ""){
	    	swal({
	            title: "",
	            text: 'unable to export data due to no matching records found',
	            type: "error"
	        });	
	    } else{
		    var inputData = {
		    		"input":ids,
		    		"countryCode": _COUNTRY_CODE,
			        "locale": _LOCALE,
			        "userEmail": _USER_EMAIL
		    };
		    var json = JSON.stringify(inputData);
		    $.ajax({
		    	url: "/cif/api/admin/export",
		        method: 'POST',
		        data: json,
		        dataType : "json",
				contentType : "application/json",
		        success: function (response) {
		            if (response.flag) {
		            	var html = getHeaderBootstrapHtmlExport("Export Data");
						html += _EXPORT_MSG;
						html += getFooterBootstrapHtmlExport();
						$('#exportDiv').html(html);
						$('#exportModal').modal('show');
		            } else {
		                swal({
		                    title: "",
		                    text: response.message,
		                    type: "error"
		                });
		            }
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            swal({
		                title: "",
		                text: 'unable to fetch data due to the error:::' + errorThrown,
		                type: "error"
		            });
		        }
		    });
	    }
	});
	
	
	function getHeaderBootstrapHtmlExport(title) {
		var html = '<div id="exportModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
				+ '<div class="modal-dialog">'
				+ '<div class="modal-content col-md-6 col-md-offset-3">'
				+ '  <div class="modal-header modal-header-danger">'
				+ ' <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">'
				+ _CLOSE_BUTTON
				+ '</span></button>'
				+ ' <div class="modal-title">'
				+ title + '</div>' + '  </div>' + ' <div class="modal-body">';
		return html;
	}
	
	function getFooterBootstrapHtmlExport() {
		var html = '</div>' + ' <div class="modal-footer">' + ' </div>' + ' </div>'
				+ ' </div>';
		return html;
	}

});
