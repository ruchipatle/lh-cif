function resetFields() {
    document.getElementById("errMsg").innerHTML = "";
    
    $("#lookUpList").val("");
    $("#lookupId").val("");    
    $("#id").val("");
    $('#sortOrder').val("");
    $('#displayValue').val("");
}

function cancelRecord() {
	window.location = "/cif/library/manage";
}

function saveRecord() {
	validateForm();
}

function validateForm() {
    var isError = false;

    if (null == $('#dataSource').val() || $('#dataSource').val() == "" || $('#dataSource').val() == "-1") {
        swal({
            title: "",
            text: "Please select the Data Source",
            type: "error"
        });
        isError = true;
        return false;
    }

    if (null == $('#mainCategory').val() || $('#mainCategory').val() == "" || $('#mainCategory').val() == "-1") {
        swal({
            title: "",
            text: "Please select Main Category",
            type: "error"
        });
        isError = true;
        return false;
    }

    if (null == $('#section').val() || $('#section').val() == "" || $('#section').val() == "-1") {
        swal({
            title: "",
            text: "Please select Section",
            type: "error"
        });
        isError = true;
        return false;
    }
    
    if (null == $('#docType').val() || $('#docType').val() == "" || $('#docType').val() == "-1") {
        swal({
            title: "",
            text: "Please select File Type",
            type: "error"
        });
        isError = true;
        return false;
    }    
    if (null == $('#documentLink').val() || $('#documentLink').val() == "") {
        swal({
            title: "",
            text: "Please select / enter the Link",
            type: "error"
        });
        isError = true;
        return false;
    }
    if (null == $('#name').val() || $('#name').val() == "") {
        swal({
            title: "",
            text: "Please enter the name",
            type: "error"
        });
        isError = true;
        return false;
    }
    if (null == $('#label').val() || $('#label').val() == "") {
        swal({
            title: "",
            text: "Please enter the label",
            type: "error"
        });
        isError = true;
        return false;
    }      

    if (!isError) {

    	var id = $('#id').val();
        var mainCategory = $('#mainCategory').val();
        var section = $('#section').val();

        var plant = $('#plant').val();
        if(plant == "-1"){
        	plant = "";
        }

        var name = $('#name').val();
        var label = $('#label').val();
        var description = $('#description').val();
        var documentId = $('#documentId').val();
        var documentLink = $('#documentLink').val();
        var docType = $('#docType').val();
        var dataSource = $('#dataSource').val();
        var previewURL = $('#previewURL').val();
        var thumbnailURL = $('#thumbnailURL').val();

    	if( documentId != "" && thumbnailURL == ""){
    		thumbnailURL = 'https://drive.google.com/a/lafargeholcim.com/thumbnail?id=' + documentId + '&sz=w1000&-h1000%';
    		$('#thumbnailURL').val(thumbnailURL);
    	}

        var keyTopics = $('#keyTopics').chosen().val();
        if(keyTopics != null && keyTopics != ""){
        	keyTopics = keyTopics.join('|');
        }

        var tags = $('#tags').chosen().val();
        if(tags != null && tags != ""){
        	tags = tags.join('|');
        }

        var jsonCreateData = {
        	"id":id,
            "mainCategory": mainCategory,
            "section": section,
            "plant": plant,
            "name": name,
            "label": label,
            "description": description,
            "documentId": documentId,
            "documentLink": documentLink,
            "docType": docType,
            "tags": tags,
            "keyTopics": keyTopics,
            "dataSource":dataSource,
            "previewURL":previewURL,
            "thumbnailURL":thumbnailURL
        };
        var createData = JSON.stringify(jsonCreateData);
        swal({
                title: _SAVE_RECORD_CONFRIM_BOX_TITLE,
                text: _CONFIRM_TO_SAVE_RECORD_MSG,
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                cancelButtonText: _CANCEL_BUTTON
            },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/cif/api/library/saveCIFDoc",
                    data: createData,
                    dataType: "json",
                    contentType: "application/json",
                    success: function (response) {
                        if (response.flag) {
                            swal({
                                    title: _SAVE_MASTER_DATA_SUCCESS,
                                    text: _PLEASE_WAIT,
                                    type: "success",
                                    timer: 500,
                                    showConfirmButton: false
                                },
                                function () {
                                    window.location.replace("/cif/library/manage");
                                });
                        } else {
                            swal({
                                title: _PLEASE_TRY_AGAIN,
                                text: _SAVE_MASTER_DATA_FAIL,
                                type: "error"
                            });
                        }

                    },
                    error: function (
                        xhr,
                        status,
                        errorThrown) {
                        swal({
                            title: _PLEASE_TRY_AGAIN,
                            text: _SAVE_MASTER_DATA_FAIL,
                            type: "error"
                        });
                    }
                });

            });
    }
}


$(document).ready(function () {

	$('#pick').on("click", function() {
		if ($('#linkType0').val() == "") {
			swal({
				title : "",
				text : _LINKS_EMPTY_ALERT,
				type : "error"
			});
			return false;
		}
		onApiLoad();
	});
	
	var documentId = $('#documentId').val();
    var thumbnailURL = $('#thumbnailURL').val();

	if( documentId != "" && thumbnailURL == ""){
		thumbnailURL = 'https://drive.google.com/a/lafargeholcim.com/thumbnail?id=' + documentId + '&sz=w1000&-h1000%';
		$('#thumbnailURL').val(thumbnailURL);
	}
	
	// Configure multi select
	var config = {
		'.chosen-select-width' : {
			width : "100%"
		},
		'.chosen-select' : {
			width : "100%"
		}
	}	
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
	
	preselectMultiSelect("tags", _TAGS_JSON);
	preselectMultiSelect("keyTopics", _KEYTOPICS_JSON);
	
});

function preselectMultiSelect(key, dataList){
	if (null != dataList && dataList != "") {
		$.each(dataList, function(index, data) {
			$('#' + key + ' option').each(function() {
				if ($(this).val() == data) {
					$(this).attr('selected', 'selected');
				}
			});
		});
		$('#' + key).trigger("chosen:updated");
	}
}

var pickerApiLoaded = false;

function onApiLoad() {
	gapi.load('picker', {
		'callback' : onPickerApiLoad
	});
}

function onPickerApiLoad() {
	pickerApiLoaded = true;
	createPicker();
}

function createPicker() {
	if (pickerApiLoaded) {
		var view = new google.picker.DocsView(google.picker.ViewId.DOCS);
		view.setIncludeFolders(true);
		view.setOwnedByMe(true);
		var uploadView = new google.picker.DocsUploadView().setIncludeFolders(true)	
		var picker = new google.picker.PickerBuilder().addView(view).addView(uploadView).enableFeature(google.picker.Feature.MULTISELECT_ENABLED).setOAuthToken(_ACCESS_TOKEN).setCallback(pickerCallback).build();
		picker.setVisible(true);
	}
}

// A simple callback implementation.
function pickerCallback(data) {
	driveFlag = true;
	var url = 'nothing';
	var name = '';
	var id = '';
	var descr = '';
	var previewURL = '';
	var thumbnailURL = '';
	if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
		var doc = data[google.picker.Response.DOCUMENTS];
		for ( var i in doc) {
			console.log(doc[i]);
			url = doc[i][google.picker.Document.URL];
			id = doc[i][google.picker.Document.ID];
			name = doc[i][google.picker.Document.NAME];
			descr = doc[i][google.picker.Document.DESCRIPTION];
			previewURL = doc[i][google.picker.Document.EMBEDDABLE_URL];

			if (url != 'nothing') {
				linkCheck = true;
				$('#name').val(name);
				$('#label').val(name);
				$('#documentLink').val(url);
				$('#documentId').val(id);
				if(previewURL){
					$('#previewURL').val(previewURL);
				}
				if(descr){
					$('#description').val(descr);
				}
				//thumbnailURL = 'https://drive.google.com/a/dev.lafargeholcim.com/thumbnail?id='+id+'&sz=w500&-h500%';			
	    		thumbnailURL = 'https://drive.google.com/a/lafargeholcim.com/thumbnail?id=' + id + '&sz=w1000&-h1000%';
	    		$('#thumbnailURL').val(thumbnailURL);
				
				driveFlag = false;
			}
		}
	}
}

// Multi select toggler
function toggleSelection(id, selection) {
	$('#' + id + ' option').prop('selected', selection);
	$('#' + id).trigger("chosen:updated");
}
