
function cancelRecord() {
	window.location = "/cif/library/manage";
}

function deleteComment(temp1) {
	
	var commentId = temp1.attr('id').substring("deleteComment_".length,temp1.attr('id').length);
	var documentId = temp1.attr('documentId');	
	
    swal({
            title: _DELETE_RECORD_CONFRIM_BOX_TITLE,
            text: _CONFIRM_TO_DELETE_RECORD_MSG,
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            cancelButtonText: _CANCEL_BUTTON
        },
        function () {
            $.ajax({
                type: "POST",
                url: "/cif/api/library/deleteComment/"+commentId,
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    if (response.flag) {
                        swal({
                                title: _DELETE_MASTER_DATA_SUCCESS,
                                text: _PLEASE_WAIT,
                                type: "success",
                                timer: 500,
                                showConfirmButton: false
                            },
                            function () {
                                window.location.replace("/cif/library/editComment/"+documentId);
                            });
                    } else {
                        swal({
                            title: _PLEASE_TRY_AGAIN,
                            text: _DELETE_MASTER_DATA_FAIL,
                            type: "error"
                        });
                    }

                },
                error: function (
                    xhr,
                    status,
                    errorThrown) {
                    swal({
                        title: _PLEASE_TRY_AGAIN,
                        text: _DELETE_MASTER_DATA_FAIL,
                        type: "error"
                    });
                }
            });

        });
}


$(document).ready(function () {

	$(".deleteComment").on("click", function() {
		deleteComment($(this));
	});

});
