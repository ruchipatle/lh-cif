function cancelRecord() {
	window.location = "/cif/library/manage";
}

function saveRecord() {
	validateForm();
}

function validateForm() {
    var isError = false;
    if (null == $('#documentLink').val() || $('#documentLink').val() == "") {
        swal({
            title: "",
            text: "Please select / enter the Link",
            type: "error"
        });
        isError = true;
        return false;
    }  

    if (!isError) {
        var documentId = $('#documentId').val();
        var documentLink = $('#documentLink').val();
        var documentName = $('#name').val();

        var jsonCreateData = {
            "documentId": documentId,
            "documentLink": documentLink,
            "documentName": documentName
        };
        var createData = JSON.stringify(jsonCreateData);
        swal({
                title: _SAVE_RECORD_CONFRIM_BOX_TITLE,
                text: _CONFIRM_TO_SAVE_RECORD_MSG,
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                cancelButtonText: _CANCEL_BUTTON
            },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/cif/api/library/uploadSourceData",
                    data: createData,
                    dataType: "json",
                    contentType: "application/json",
                    success: function (response) {
                        if (response.flag) {
                            swal({
                                    title: _SAVE_MASTER_DATA_SUCCESS,
                                    text: _PLEASE_WAIT,
                                    type: "success",
                                    timer: 500,
                                    showConfirmButton: false
                                },
                                function () {
                                    window.location.replace("/cif/library/manage");
                                });
                        } else {
                            swal({
                                title: _PLEASE_TRY_AGAIN,
                                text: _SAVE_MASTER_DATA_FAIL,
                                type: "error"
                            });
                        }

                    },
                    error: function (
                        xhr,
                        status,
                        errorThrown) {
                        swal({
                            title: _PLEASE_TRY_AGAIN,
                            text: _SAVE_MASTER_DATA_FAIL,
                            type: "error"
                        });
                    }
                });

            });
    }
}

$(document).ready(function () {

	$('#pick').on("click", function() {
		if ($('#linkType0').val() == "") {
			swal({
				title : "",
				text : _LINKS_EMPTY_ALERT,
				type : "error"
			});
			return false;
		}
		onApiLoad();
	});
	
});

var pickerApiLoaded = false;

function onApiLoad() {
	gapi.load('picker', {
		'callback' : onPickerApiLoad
	});
}

function onPickerApiLoad() {
	pickerApiLoaded = true;
	createPicker();
}

function createPicker() {
	if (pickerApiLoaded) {
		var view = new google.picker.DocsView(google.picker.ViewId.DOCS).setMimeTypes("application/vnd.google-apps.spreadsheet");
		view.setIncludeFolders(true);
		view.setOwnedByMe(true);
		var uploadView = new google.picker.DocsUploadView().setIncludeFolders(true)	
		var picker = new google.picker.PickerBuilder().addView(view).addView(uploadView).enableFeature(google.picker.Feature.MULTISELECT_ENABLED).setOAuthToken(_ACCESS_TOKEN).setCallback(pickerCallback).build();
		picker.setVisible(true);
	}
}

// A simple callback implementation.
function pickerCallback(data) {
	driveFlag = true;
	var url = 'nothing';
	var name = '';
	var id = '';
	if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
		var doc = data[google.picker.Response.DOCUMENTS];
		for ( var i in doc) {
			console.log(doc[i]);
			url = doc[i][google.picker.Document.URL];
			id = doc[i][google.picker.Document.ID];
			name = doc[i][google.picker.Document.NAME];

			if (url != 'nothing') {
				linkCheck = true;
				$('#name').val(name);
				$('#documentLink').val(url);
				$('#documentId').val(id);
				driveFlag = false;
			}
		}
	}
}
