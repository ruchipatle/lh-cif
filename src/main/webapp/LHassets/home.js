
$(document).ready(function() {	
	$("#getData").click(function() {
		getData();
	});
});

function getData() {
	var testText = $("#testText").val();
	
	$("#testText").val("");
	$.ajax({
		url : "/cif/search/create",
		contentType : "application/json",
    	type : "GET",
    	data : {
			"testText" : testText
		}
	}).done(function (message) {
		processSuccessfulAjaxRequest(message)
	}).fail(function(jqXHR) {
		processFailedAjaxRequest(jqXHR)
	});
}