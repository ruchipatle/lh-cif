<%@taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn'%>

<!DOCTYPE html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="shortcut icon" type="image/ico" href="/LHassets/core/latest/img/favicon/favicon.ico" />

<title>CIF : Manage Library</title>

<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen-bootstrap.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-dialog.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-editable.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.custom.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/sweetalert.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-tokenfield.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/select2.min.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/AdminLTE.css"/>
<link rel="stylesheet" href="/LHassets/core/css/custom.css"/>

<script src="/LHassets/core/latest/js/jquery.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-dialog.js"></script>
<script src="/LHassets/core/latest/js/bootstrap.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.dataTables.js"></script>
<script src="/LHassets/core/latest/js/DT_bootstrap.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-editable.js"></script>
<script src="/LHassets/core/latest/js/chosen.jquery.js" ></script>
<script src="/LHassets/core/latest/js/jquery-ui.min.js"></script>
<script src="/LHassets/core/latest/js/typeahead.bundle.js"></script>
<script src="/LHassets/core/latest/js/jquery.toaster.js"></script>
<script src="/LHassets/core/latest/js/fnLengthChange.js"></script>
<script src="/LHassets/core/latest/js/sweetalert.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-tokenfield.min.js"></script>
<script src="/LHassets/core/latest/js/select2.full.min.js"></script>
<script src="/LHassets/core/latest/js/fastclick.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.slimscroll.min.js"></script>
<script src="/LHassets/core/latest/js/fileinput.min.js"></script>
<script src="/LHassets/core/latest/js/moment.min.js"></script>
<script src="/LHassets/core/latest/js/datetime-moment.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay.min.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay_progress.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.ui.touch-punch.min.js"></script>
<script src="/LHassets/core/latest/js/app.min.js"></script>

<style>
	.box-body {
	    border: 2px !important;
	    border-style: groove !important;
	    border-radius: 15px !important;
	}
</style>

<c:set var="USER_EMAIL" value="${userInfoSession.userEmail}" />
<c:set var="USER_NAME" value="${userInfoSession.userName}" />
<c:set var="USER_ROLE" value="${USER_ROLE}" />
<c:set var="accessToken" value="${accessToken}" />

<script type="text/javascript">
	var _OPCO_NAME = "CIF";
	var _USER_EMAIL = "${USER_EMAIL}";
	var _USER_NAME = "${USER_NAME}";
	var _USER_ROLE = "${USER_ROLE}";
	var _SELECT_LOOUPLIST="Please select the look up type";
	var _DISPLAY_VALUE="Please enter the display value";
	var _SORT_ORDER="Please enter sort Order";
	var _NUMREIC_CHK_ALERT="Please select the section";
	var _SELECT_SECTION_MSG="";
	var _SAVE_MASTER_DATA_SUCCESS="Record created successfully";
	var _SAVE_MASTER_DATA_FAIL="Failed to create record";
	var _CONFIRM_TO_SAVE_RECORD_MSG="Do you want to save record?";
	var _SAVE_RECORD_CONFRIM_BOX_TITLE="Save Record";
	var _CANCEL_BUTTON="Cancel";
	var _SECTION_NAME_FOLDER_EDIT_ACTION = "";
	var _CLOSE_BUTTON="Close";
	var _PLEASE_WAIT="please wait";
	var _PLEASE_TRY_AGAIN="please try again";	
	
	var _ADD_DELETE_KEYWORD_PAGE_TITLE="Add / remove keywords";
	var _BOOTSTRAPDIALOG_LINK_TITTLE="link";
	var _OK_BUTTON="OK";
	var _CANCEL_BUTTON="Cancel";
	var _LINK_NAME_LBL='Link name';
	var _LINK_URL_LBL='URL link';
	var _LINKS_EMPTY_ALERT='Please Select link';
	var _REMOVE_ALERT_FOR_LINK="Do you want to delete link ?";
	var _FULL_NAME="Full Name";
	var _ROLE="Role";
	var _LINK_VALIDN_MSG="Please enter Name and URL";
	var _VALDN_EXT_URL="Please enter proper a URL (format : https://www.google.com)";
	var _ADD_OWNER_ALERT="Select a user for Registration.";
	var _SAVE_RECORD_TITLE="Save Record";
	var _SAVE_RECORD_SUCCESS="Record saved successfully";
	var _SAVE_RECORD_FAILURE="Cannot save record";
	var _CONFIRM_TO_SAVE_RECORD="Do you want to save record?";
	var _CLOUD_FILE_UPLOAD_ALERT="File Upload Successfully";	
	var _ACCESS_TOKEN='${accessToken}';
	
	var _KEYTOPICS_JSON = ${keyTopicsJson};
	var _TAGS_JSON = ${tagsJson};
	
</script>

<script src="//apis.google.com/js/client.js?onload=initPicker"></script>
<script type="text/javascript" src="/LHassets/core/latest/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="/LHassets/record/create.js"></script>

</head>

<body class="hold-transition skin-brown layout-top-nav fixed">

	<div class="wrapper">

		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<div class="navbar-header">
						<a href="/cif/search/home" class="navbar-brand">
							<img src="/LHassets/core/latest/img/logo.png" class="hidden-xs logo img-responsive">
							<img src="/LHassets/core/latest/img/48x48.png" class="visible-xs logo img-responsive">
						</a>
						<label class="nav-title"><strong>CIF</strong></label>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Laptop/Desktop -->
					<div class="hidden-xs">
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">

							<ul class="nav navbar-nav menubar">

								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
										<span class="hidden-xs pull-right">${USER_NAME}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>${USER_NAME}</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>

										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Laptop/Desktop -->
					
					<!-- Mobile/Tablet -->
					<div class="visible-xs">
					
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">	
							
								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>
												${USER_NAME}
											</p></li>
										<!-- Menu Body -->
										<li class="user-body">									
											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Mobile/Tablet -->
					
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>

		<div class="content-wrapper">


<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">

					<div class="box-header">
						<div class="row">
							<div class="col-md-3">
								<h3 class="box-title main-title"><strong>Source Data</strong></h3>
							</div>
						</div>
					</div>

					<div class="box-body">
						<form id="cifDocument" name="cifDocument" action="/cif/api/library/saveCIFDoc" method="post">

<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">

								<div class="col-lg-4 col-md-4 col-sm-8">
								 	<div class="form-group">
										<label>Data Source&nbsp;<span style="color: red">*</span></label> 
										<select id="dataSource" name="dataSource" class="form-control">
											<option value="-1"></option>
										<c:if test="${not empty requestScope.DataSource}"><c:forEach var="list" items="${requestScope.DataSource}">
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											<option value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:when><c:otherwise>
											<option value="${list.displayValue}" id="${list.id}" <c:if test="${requestScope.cifDocRecord.dataSource eq list.displayValue}">selected</c:if>>${list.displayValue}</option>
											</c:otherwise>
										</c:choose>
										</c:forEach></c:if>
										</select>
									</div>
								</div>

								<div class="col-lg-4 col-md-4 col-sm-8">
								 	<div class="form-group">
										<label>Main Category&nbsp;<span style="color: red">*</span></label> 
										<select id="mainCategory" name="mainCategory" class="form-control">
											<option value="-1"></option>
										<c:if test="${not empty requestScope.Main}"><c:forEach var="list" items="${requestScope.Main}">
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											<option value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:when><c:otherwise>
											<option value="${list.displayValue}" id="${list.id}" <c:if test="${requestScope.cifDocRecord.main eq list.displayValue}">selected</c:if>>${list.displayValue}</option>
											</c:otherwise>
										</c:choose>
										</c:forEach></c:if>
										</select>
									</div>
								</div>
								
							</div>
							
							<div class="row">

								<div class="col-lg-4 col-md-4 col-sm-8">
								 	<div class="form-group">
										<label>Section&nbsp;<span style="color: red">*</span></label>
										<select id="section" name="section" class="form-control">
											<option value="-1"></option>
										<c:if test="${not empty requestScope.Section}"><c:forEach var="list" items="${requestScope.Section}">
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											<option value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:when><c:otherwise>
											<option value="${list.displayValue}" id="${list.id}" <c:if test="${requestScope.cifDocRecord.section eq list.displayValue}">selected</c:if>>${list.displayValue}</option>
											</c:otherwise>
										</c:choose>
										</c:forEach></c:if>
										</select>
									</div>
								</div>
								
								<div class="col-lg-4 col-md-4 col-sm-8">
								 	<div class="form-group">
										<label>Plant</label>
										<select id="plant" name="plant" class="form-control">
											<option value="-1"></option>
										<c:if test="${not empty requestScope.Plant}"><c:forEach var="list" items="${requestScope.Plant}">
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											<option value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:when><c:otherwise>
											<option value="${list.displayValue}" id="${list.id}" <c:if test="${requestScope.cifDocRecord.plant eq list.displayValue}">selected</c:if>>${list.displayValue}</option>
											</c:otherwise>
										</c:choose>
										</c:forEach></c:if>
										</select>
									</div>
								</div>

							</div>
<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">
 							
								<div class="col-lg-4 col-md-4 col-sm-12">
								 	<div class="form-group">
										<label>File Type&nbsp;<span style="color: red">*</span></label> 
										<select id="docType" name="docType" class="form-control">
											<option value="-1"></option>
										<c:if test="${not empty requestScope.DocumentType}"><c:forEach var="list" items="${requestScope.DocumentType}">
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											<option value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:when><c:otherwise>
											<option value="${list.displayValue}" id="${list.id}" <c:if test="${requestScope.cifDocRecord.docType eq list.displayValue}">selected</c:if>>${list.displayValue}</option>
											</c:otherwise>
										</c:choose>
										</c:forEach></c:if>
										</select>
									</div>
								</div>
								
								<div class="col-lg-7 col-md-7 col-sm-12">
								 	<div class="form-group">
										<label>Link / URL&nbsp;<span style="color: red">*</span></label>
										<input class="form-control" type="text" name="documentLink" id="documentLink" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.documentLink}"
											</c:otherwise>
										</c:choose>
										data-toggle="tooltip" data-placement="top" title= "Document URL / Link" />
									</div>
								</div>

								<div class="col-lg-1 col-md-1 col-sm-12">
									<div class="form-group upload-button">
										 <div class="col-xs-3 col-md-1">
											<a href="javascript:void(0)" id="pick" title="${LINK_DRIVE_TITLE}"> 
												<img title="Drive" src="/LHassets/core/latest/img/Google-Drive-icon.png" />
											</a> 
										 </div>

									</div>
								</div>								
							</div>
<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12">
								 	<div class="form-group">
										<label>Name&nbsp;<span style="color: red">*</span></label>
										<input class="form-control" type="text" name="name" id="name" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.name}"
											</c:otherwise>
										</c:choose>
										data-toggle="tooltip" data-placement="top" title= "Name" />
									</div>
								</div>

								<div class="col-lg-4 col-md-4 col-sm-12">
								 	<div class="form-group">
										<label>Label&nbsp;<span style="color: red">*</span></label>
										<input class="form-control" type="text" name="label" id="label" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.label}"
											</c:otherwise>
										</c:choose>
										data-toggle="tooltip" data-placement="top" title= "Label" />
									</div>
								</div>

								<div class="col-lg-4 col-md-4 col-sm-12">
								 	<div class="form-group">
										<label>Document ID</label>
										<input class="form-control" type="text" name="documentId" id="documentId" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.documentId}"
											</c:otherwise>
										</c:choose>
										data-toggle="tooltip" data-placement="top" title= "Document ID" />
									</div>								
								</div>	
																
							</div>
<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12">
								 	<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" name="description" id="description" data-toggle="tooltip" data-placement="top" title= "Description"><c:if test="${requestScope.action eq 'EDIT'}">${requestScope.cifDocRecord.description}</c:if></textarea>
									</div>
								</div>
								
								<div class="col-lg-4 col-md-4 col-sm-12">
								 	<div class="form-group">
										<label>Preview URL</label>
										<input class="form-control" type="text" name="thumbnailURL" id="thumbnailURL" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.thumbnailURL}"
											</c:otherwise>
										</c:choose>
										data-toggle="tooltip" data-placement="top" title= "Thumbnail URL" />

										<input type="hidden" id="previewURL" name="previewURL" 
										<c:choose><c:when test="${requestScope.action eq 'CREATE'}">
											value=""
											</c:when><c:otherwise>
											value="${requestScope.cifDocRecord.previewURL}"
											</c:otherwise>
										</c:choose>
										/>
									</div>
								</div>
							</div>
<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12">
								 	<div class="form-group">
										<label>Key Topics&nbsp;&nbsp;</label>
										<div class="txtLeg" style="display: inline;">
											<a href="javascript:void(0)" onclick="toggleSelection('keyTopics',false)">Clear</a>
										</div>
										<select id="keyTopics" name ="keyTopics" data-placeholder="Key Topics" multiple class="form-control chosen-select-width ">
											<c:forEach items="${requestScope.KeyTopic}" var="list">
												<option title="${list.displayValue}" value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:forEach>
										</select>

									</div>
								</div>
							</div>
<!-- ----------------------------------------------------------------------------- -->
 							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12">
								 	<div class="form-group">
										<label>Tags&nbsp;&nbsp;</label>
										<div class="txtLeg" style="display: inline;">
											<a href="javascript:void(0)" onclick="toggleSelection('tags',false)">Clear</a>
										</div>
										<select id="tags" name ="tags" data-placeholder="Tags" multiple class="form-control chosen-select-width ">
											<c:forEach items="${requestScope.Tag}" var="list">
												<option title="${list.displayValue}" value="${list.displayValue}" id="${list.id}">${list.displayValue}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
<!-- ----------------------------------------------------------------------------- -->
							<div class="row" id="secondtDiv" style="display:none;">
								<div class="col-lg-3 col-md-3 col-sm-6">
								 	<div class="form-group">
										<label>ID</label>
											<p><c:choose>
												<c:when test="${requestScope.action eq 'CREATE'}">
													<input name="id" class="form-control" value="" type="text" id="id" readonly />
												</c:when>
												<c:otherwise>
													<input name="id" class="form-control" value="${requestScope.cifDocRecord.id}" type="text" id="id" readonly/>
												</c:otherwise>
											</c:choose>
										</p>
										</div>
								</div>
							</div>
<!-- ----------------------------------------------------------------------------- -->
							<div class="row">
								<div class="col-lg-10 col-md-10">
									<c:choose>
										<c:when test="${requestScope.action eq 'CREATE'}">
											<div class="pull-left">
												<button type="button" id="submitButton" name="show" class="btn btn-primary" value="Save" onclick="saveRecord()">Save</button>
											</div>
										</c:when>
										<c:otherwise>
											<div class="pull-left" style="margin-left: 25px;">
												<button type="button" id="updatetButton" name="Same" class="btn btn-primary" value="Update" onclick="saveRecord()">Update</button>
											</div>
										</c:otherwise>
									</c:choose>

										<div class="pull-left" style="margin-left: 25px;">
											<button type="button" id="cancelButton" name="cancel" class="btn btn-primary" value="Cancel" onclick="cancelRecord()">Back</button>
										</div>									
								</div>
							</div>

							<input type="hidden" id="flag" name="flag" value="" />
							<div id="errMsg" style="color: red"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>

	</div>
</body>

</html>
