<%@taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn'%>

<!DOCTYPE html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="shortcut icon" type="image/ico" href="/LHassets/core/latest/img/favicon/favicon.ico" />

<title>CIF : Manage Library</title>

<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen-bootstrap.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-dialog.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-editable.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.custom.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/sweetalert.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-tokenfield.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/select2.min.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/AdminLTE.css"/>
<link rel="stylesheet" href="/LHassets/core/css/custom.css"/>

<script src="/LHassets/core/latest/js/jquery.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-dialog.js"></script>
<script src="/LHassets/core/latest/js/bootstrap.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.dataTables.js"></script>
<script src="/LHassets/core/latest/js/DT_bootstrap.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-editable.js"></script>
<script src="/LHassets/core/latest/js/chosen.jquery.js" ></script>
<script src="/LHassets/core/latest/js/jquery-ui.min.js"></script>
<script src="/LHassets/core/latest/js/typeahead.bundle.js"></script>
<script src="/LHassets/core/latest/js/jquery.toaster.js"></script>
<script src="/LHassets/core/latest/js/fnLengthChange.js"></script>
<script src="/LHassets/core/latest/js/sweetalert.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-tokenfield.min.js"></script>
<script src="/LHassets/core/latest/js/select2.full.min.js"></script>
<script src="/LHassets/core/latest/js/fastclick.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.slimscroll.min.js"></script>
<script src="/LHassets/core/latest/js/fileinput.min.js"></script>
<script src="/LHassets/core/latest/js/moment.min.js"></script>
<script src="/LHassets/core/latest/js/datetime-moment.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay.min.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay_progress.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.ui.touch-punch.min.js"></script>
<script src="/LHassets/core/latest/js/app.min.js"></script>

<style>
	.box-body {
	    border: 2px !important;
	    border-style: groove !important;
	    border-radius: 15px !important;
	}
</style>

<c:set var="USER_EMAIL" value="${userInfoSession.userEmail}" />
<c:set var="USER_NAME" value="${userInfoSession.userName}" />
<c:set var="USER_ROLE" value="${USER_ROLE}" />
<c:set var="accessToken" value="${accessToken}" />

<script type="text/javascript">

	var _CONFIRM_TO_DELETE_RECORD_MSG="Do you want to delete this comment?";
	var _DELETE_RECORD_CONFRIM_BOX_TITLE="Delete Comment";
	var _CANCEL_BUTTON="Cancel";	
	var _SECTION_NAME_FOLDER_EDIT_ACTION = "";
	var _CLOSE_BUTTON="Close";
	var _PLEASE_WAIT="please wait";
	var _PLEASE_TRY_AGAIN="please try again";	
	var _DELETE_MASTER_DATA_SUCCESS="Comment removed successfully";
	var _DELETE_MASTER_DATA_FAIL="Failed to remove the comment";	
	var _OPCO_NAME = "CIF";
	var _USER_EMAIL = "${USER_EMAIL}";
	var _USER_NAME = "${USER_NAME}";
	var _USER_ROLE = "${USER_ROLE}";
	
	var _SELECT_LOOUPLIST="Please select the look up type";
	var _DISPLAY_VALUE="Please enter the display value";
	var _SORT_ORDER="Please enter sort Order";
	var _NUMREIC_CHK_ALERT="Please select the section";
	var _SELECT_SECTION_MSG="";
	var _ADD_DELETE_KEYWORD_PAGE_TITLE="Add / remove keywords";
	var _BOOTSTRAPDIALOG_LINK_TITTLE="link";
	var _OK_BUTTON="OK";
	var _CANCEL_BUTTON="Cancel";
	var _LINK_NAME_LBL='Link name';
	var _LINK_URL_LBL='URL link';
	var _LINKS_EMPTY_ALERT='Please Select link';
	var _REMOVE_ALERT_FOR_LINK="Do you want to delete link ?";
	var _FULL_NAME="Full Name";
	var _ROLE="Role";
	var _LINK_VALIDN_MSG="Please enter Name and URL";
	var _VALDN_EXT_URL="Please enter proper a URL (format : https://www.google.com)";
	var _ADD_OWNER_ALERT="Select a user for Registration.";
	var _DELETE_RECORD_TITLE="Delete Comment";
	var _DELETE_RECORD_SUCCESS="Comment saved successfully";
	var _DELETE_RECORD_FAILURE="Cannot save comment";
	var _CONFIRM_TO_DELETE_RECORD="Do you want to delete the comment?";
	var _CLOUD_FILE_UPLOAD_ALERT="File Upload Successfully";	
	var _ACCESS_TOKEN='${accessToken}';
	
</script>

<script type="text/javascript" src="/LHassets/record/editComments.js?v=1.0.000.1"></script>

<style>
	.comment-block .user-div .user-profile{
	    padding: 13px 18px;
	    background: #f3f3f3;
	    border-radius: 50%;
	    border: 1px solid #adb5bd;
	}
	.comment-block .user-div{
	    text-align: center;
	}
	
	.comment-block .right-section{
	    padding-left: 0px;
	}
	.comment-block .right-section .para1{
	    font-size: 14px;
	    font-weight: 700;
	    letter-spacing: 0.2px;
	    margin: 0px;
	    padding-bottom: 3px;
	}
	
	.comment-block .right-section .para2{
	    margin: 0px;
	    font-size: 12px;
	    letter-spacing: 0.2px;
	    font-weight: 700;
	    padding-bottom: 3px;
	    
	}
	
	.comment-block .right-section .para3{
	    font-size: 11px;
	    padding-bottom: 3px;    
	    letter-spacing: 0.2px;
	    color: grey;
	    margin: 0px;
	}
	.comment-block .comment-section{
	    padding: 10px 5px;
	    margin-bottom: 10px;
	}
	.comment-block .comment-heading{
	    margin: 5px 0px;
	}
	
	.comment-block .comment-heading .p1{
	    font-size: 25px;
	    font-weight: 700;
	    letter-spacing: 0.4px;
	    margin-bottom: 10px;
	}
	
	.comment-block .comment-heading .p2{
	    font-size: 17px;
	    font-weight: 500;
	    letter-spacing: 0.4px;
	    margin-bottom: 15px;
	}
	
	.comment-block .comment-block{
	    padding: 2em 3em;
	    border: 1px solid #979797;
	    border-radius: 6px;
	}
	
	p.para1 {
	    font-size: 20px;
	    color: teal;
	}
	
	.card {
	    position: relative;
	    display: -ms-flexbox;
	    display: flex;
	    -ms-flex-direction: column;
	    flex-direction: column;
	    min-width: 0;
	    word-wrap: break-word;
	    background-color: #fff;
	    background-clip: border-box;
	    border: 1px solid rgba(0,0,0,.125);
	    border-radius: .25rem;
	}
	
	span.for_icon {
    	padding: 5px;
    	font-size: 20px;
	}
	
	div.forButton {
	    padding-bottom: 15px;
	}	

</style>

</head>

<body class="hold-transition skin-brown layout-top-nav fixed">

	<div class="wrapper">

		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<div class="navbar-header">
						<a href="/cif/search/home" class="navbar-brand">
							<img src="/LHassets/core/latest/img/logo.png" class="hidden-xs logo img-responsive">
							<img src="/LHassets/core/latest/img/48x48.png" class="visible-xs logo img-responsive">
						</a>
						<label class="nav-title"><strong>CIF</strong></label>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Laptop/Desktop -->
					<div class="hidden-xs">
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">

							<ul class="nav navbar-nav menubar">

								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
										<span class="hidden-xs pull-right">${USER_NAME}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>${USER_NAME}</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>

										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Laptop/Desktop -->
					
					<!-- Mobile/Tablet -->
					<div class="visible-xs">
					
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">	
							
								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>
												${USER_NAME}
											</p></li>
										<!-- Menu Body -->
										<li class="user-body">									
											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Mobile/Tablet -->
					
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>

<div class="content-wrapper">
	<section class="content">
		<div class="container-fluid card comment-block">
		
		<div class="row">
			<div class="col-12 col-sm-12 comment-heading">
			<p class="p1">Comments</p></div>
		</div>
		
		<c:if test="${empty requestScope.docComments}">
			<div class="row">
				<div class="col-12 col-sm-12 comment-heading">
				<p class="para2">No Comments added yet !!</p>
			</div>
		</c:if>
		
		<c:if test="${not empty requestScope.docComments}">
		<c:forEach var="comment" items="${requestScope.docComments}">
			<div>
				<div class="comment-section card">
					<div class="row">
						<div class="col-1 col-sm-1 col-md-1"><div class="user-div"><i aria-hidden="true" class="fa fa-user user-profile"></i></div></div>
						<div class="col-10 col-sm-10 col-md-10 right-section">
							<p class="para1">${comment.userName}
								<a title="Delete Comment" class="deleteComment" href="#" documentId="${comment.documentId}" id="deleteComment_${comment.id}">
									<span class="for_icon"><i class="fa fa-trash"></i></span>
								</a>
							</p>
							<p class="para3">${comment.createdOnStr}</p>
							<p class="para2">${comment.comment}</p>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
		</c:if>
		
		<div class="forButton">
			<div class="pull-left" style="margin-left: 25px;">
				<button type="button" id="cancelButton" name="cancel" class="btn btn-primary" value="Cancel" onclick="cancelRecord()">Back</button>
			</div>
		</div>
		
		</div>	
	</section>
</div>

</div>

</body>

</html>
