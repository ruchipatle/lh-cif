<%@page import="com.holcim.openid_connect.utils.OpenIdConnectConstants"%>
<%@page import="com.holcim.openid_connect.utils.OpenIdConnectHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" isErrorPage="true"%>
<%@ page import="com.holcim.cc.framework.config.HolcimConfigHelper"%>
<script>
	$("#logout").hide();
</script>
<title>IMS</title>
<body>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-body">
						<div class="col-xs-12">
							<div class="error-page">
								<h2 class="headline text-red">500</h2>
								<div class="error-content">
									<h3><i class="fa fa-warning text-red"></i> &nbsp;Oops! Something went wrong.</h3>
									<p>We will work on fixing that right away. Please contact <a href= "mailto:<GlobalServiceDesk@lafargeholcim.com>" target="_blank">GSD</a></p>
									<p>Message : ${msg}</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.error-page -->
	</section>
</body>
</body>