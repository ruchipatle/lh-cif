<%@page import="com.holcim.openid_connect.utils.OpenIdConnectConstants"%>
<%@page import="com.holcim.openid_connect.utils.OpenIdConnectHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" isErrorPage="true"%>
<%@ page import="com.holcim.cc.framework.config.HolcimConfigHelper"%>
<script>
	$("#logout").hide();
</script>
<title>IMS</title>
<body>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-body">
						<div class="col-xs-12">
							<div class="error-page">
								<h2 class="headline text-blue">404</h2>
								<div class="error-content">
									<h3><i class="fa fa-warning text-red"></i>&nbsp;Not Found</h3>
									<p>You may return to  <a href="/ims/search">Home page</a></p>
									<p>${message}</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.error-page -->
	</section>
</body>
</body>