<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="shortcut icon" type="image/ico" href="/LHassets/core/latest/img/favicon/favicon.ico" />

<title>CIF : Master Data</title>

<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen-bootstrap.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-dialog.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-editable.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.custom.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/sweetalert.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-tokenfield.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/select2.min.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/AdminLTE.css"/>
<link rel="stylesheet" href="/LHassets/core/css/custom.css"/>

<script src="/LHassets/core/latest/js/jquery.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-dialog.js"></script>
<script src="/LHassets/core/latest/js/bootstrap.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.dataTables.js"></script>
<script src="/LHassets/core/latest/js/DT_bootstrap.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-editable.js"></script>
<script src="/LHassets/core/latest/js/chosen.jquery.js" ></script>
<script src="/LHassets/core/latest/js/jquery-ui.min.js"></script>
<script src="/LHassets/core/latest/js/typeahead.bundle.js"></script>
<script src="/LHassets/core/latest/js/jquery.toaster.js"></script>
<script src="/LHassets/core/latest/js/fnLengthChange.js"></script>
<script src="/LHassets/core/latest/js/sweetalert.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-tokenfield.min.js"></script>
<script src="/LHassets/core/latest/js/select2.full.min.js"></script>
<script src="/LHassets/core/latest/js/fastclick.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.slimscroll.min.js"></script>
<script src="/LHassets/core/latest/js/fileinput.min.js"></script>
<script src="/LHassets/core/latest/js/moment.min.js"></script>
<script src="/LHassets/core/latest/js/datetime-moment.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay.min.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay_progress.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.ui.touch-punch.min.js"></script>
<script src="/LHassets/core/latest/js/app.min.js"></script>

<c:set var="USER_EMAIL" value="${userInfoSession.userEmail}" />
<c:set var="USER_NAME" value="${userInfoSession.userName}" />
<c:set var="USER_ROLE" value="${USER_ROLE}" />

<script type="text/javascript">
	var _OPCO_NAME = "CIF";
	var _USER_EMAIL = "${USER_EMAIL}";
	var _USER_NAME = "${USER_NAME}";
	var _USER_ROLE = "${USER_ROLE}";
	var _LOCALE = "en";
	var _COUNTRY_CODE = "cif";

	var title = "CIF : Master Data";
	var _SEARCH_DT = 'Search';
	var _LENGTH_DT = 'Show _MENU_';
	var _FILTERINFO_DT = '(filtered from _MAX_ rows)';
	var _EMPTYINFO_DT = 'Showing 0 to 0';
	var _INFO_DT = 'Showing _START_ to _END_';
	var _CALCULATING_DT = 'of Calculating..';
	var _OF_DT = 'of';
	var _FILTER_DT = '(Filter)';
	var _EXPORT_MSG = "Please check your mail after few minutes for the exported data";
</script>

<script type="text/javascript"
	src="/LHassets/admin/masterdata/manage.js">
</script>

</head>

<body class="hold-transition skin-brown layout-top-nav fixed">

	<div class="wrapper">

		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<div class="navbar-header">
						<a href="/cif/search/home" class="navbar-brand">
							<img src="/LHassets/core/latest/img/logo.png" class="hidden-xs logo img-responsive">
							<img src="/LHassets/core/latest/img/48x48.png" class="visible-xs logo img-responsive">
						</a>
						<label class="nav-title"><strong>CIF</strong></label>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Laptop/Desktop -->
					<div class="hidden-xs">
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">

							<ul class="nav navbar-nav menubar">

								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
										<span class="hidden-xs pull-right">${USER_NAME}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>${USER_NAME}</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>

										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Laptop/Desktop -->
					
					<!-- Mobile/Tablet -->
					<div class="visible-xs">
					
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">	
							
								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>
												${USER_NAME}
											</p></li>
										<!-- Menu Body -->
										<li class="user-body">									
											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Mobile/Tablet -->
					
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>

		<div class="content-wrapper">

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-header">
						<div class="row">
							<div class="col-md-3">
								<h3 class="box-title main-title">
									<strong>CIF : Master Data Management</strong>
								</h3>
							</div>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group pull-right">
									<a
										href="/cif/admin/masterdata/create"
										class="btn btn-primary">Add Master Data</a>
								</div>
							</div>
						</div>
						<c:if test="${not empty requestScope.masterDataList}">
						<div id="exportDiv"></div>
							<div id="searchResult">
								<div class="row">
									<div class="col-xs-12 table-responsive">
										<table class="table table-striped table-hover table-bordered"
											id="cifMasterDataTable">
											<thead>
												<tr class="withBorder">
													<th>Lookup Id</th>
													<th>Lookup Type</th>
													<th>Display Value</th>
													<th>Sort Id</th>
													<th>${ACTION_LBL}</th>
													<th style="display: none;"></th>
												</tr>
												<tr>									
													<td><div>
															<input type="text" placeholder="(Filter)"
																class="search_init" size="4" />
														</div></td>
													<td><div>
															<input type="text" placeholder="(Filter)"
																class="search_init" size="4" />
														</div></td>
													<td><div>
															<input type="text" placeholder="(Filter)"
																class="search_init" size="4" />
														</div></td>
													<td><div>
															<input type="text" placeholder="(Filter)"
																class="search_init" size="4" />
														</div></td>
													<td style="width: 7%;"><div>
														<!-- <button type="button" id="exportButton"
															class="btn btn-primary btn-sm">Export
														</button> -->
													    </div></td>
													<td style="display: none;"></td>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="list" items="${requestScope.masterDataList}">
													<tr>										
														<td><input type="hidden" name="imsRecordId"
															value="${list.id}" /> ${list.lookupId}<br /></td>
														<td><input type="hidden" name="imsRecordId"
															value="${list.id}" /> ${list.lookupType}<br /></td>
														<td style="width: 25%"><input type="hidden"
															name="RecordId" value="${list.id}" />
															${list.displayValue}<br /></td>															
														<td><input type="hidden" name="imsRecordId"
															value="${list.id}" /> ${list.sortOrder}<br /></td>
														<td><a
															href="/cif/admin/masterdata/${list.id}?mode=edit"
															id="editDetail1">Edit</a></td>
														<td id="listId">${list.id}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
	
	</div>
</body>
