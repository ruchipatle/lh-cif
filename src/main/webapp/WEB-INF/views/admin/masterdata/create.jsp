<%@taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn'%>

<!DOCTYPE html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="shortcut icon" type="image/ico" href="/LHassets/core/latest/img/favicon/favicon.ico" />

<title>CIF : Master Data</title>

<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen-bootstrap.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-dialog.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-editable.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.custom.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/sweetalert.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-tokenfield.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/select2.min.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/AdminLTE.css"/>
<link rel="stylesheet" href="/LHassets/core/css/custom.css"/>

<script src="/LHassets/core/latest/js/jquery.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-dialog.js"></script>
<script src="/LHassets/core/latest/js/bootstrap.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.dataTables.js"></script>
<script src="/LHassets/core/latest/js/DT_bootstrap.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-editable.js"></script>
<script src="/LHassets/core/latest/js/chosen.jquery.js" ></script>
<script src="/LHassets/core/latest/js/jquery-ui.min.js"></script>
<script src="/LHassets/core/latest/js/typeahead.bundle.js"></script>
<script src="/LHassets/core/latest/js/jquery.toaster.js"></script>
<script src="/LHassets/core/latest/js/fnLengthChange.js"></script>
<script src="/LHassets/core/latest/js/sweetalert.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-tokenfield.min.js"></script>
<script src="/LHassets/core/latest/js/select2.full.min.js"></script>
<script src="/LHassets/core/latest/js/fastclick.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.slimscroll.min.js"></script>
<script src="/LHassets/core/latest/js/fileinput.min.js"></script>
<script src="/LHassets/core/latest/js/moment.min.js"></script>
<script src="/LHassets/core/latest/js/datetime-moment.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay.min.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay_progress.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.ui.touch-punch.min.js"></script>
<script src="/LHassets/core/latest/js/app.min.js"></script>

<c:set var="USER_EMAIL" value="${userInfoSession.userEmail}" />
<c:set var="USER_NAME" value="${userInfoSession.userName}" />
<c:set var="USER_ROLE" value="${USER_ROLE}" />

<script type="text/javascript">
	var _OPCO_NAME = "CIF";
	var _USER_EMAIL = "${USER_EMAIL}";
	var _USER_NAME = "${USER_NAME}";
	var _USER_ROLE = "${USER_ROLE}";
	var _SELECT_LOOUPLIST="Please select the look up type";
	var _DISPLAY_VALUE="Please enter the display value";
	var _SORT_ORDER="Please enter sort Order";
	var _NUMREIC_CHK_ALERT="Please select the section";
	var _SELECT_SECTION_MSG="";
	var _SAVE_MASTER_DATA_SUCCESS="Master Data created successfully";
	var _SAVE_MASTER_DATA_FAIL="Fail to create master data";
	var _CONFIRM_TO_SAVE_RECORD_MSG="Do you want to save record?";
	var _SAVE_RECORD_CONFRIM_BOX_TITLE="Save Record";
	var _CANCEL_BUTTON="Cancel";
	var _SECTION_NAME_FOLDER_EDIT_ACTION = "";
	var _CLOSE_BUTTON="Close";
	var _PLEASE_WAIT="please wait";
	var _PLEASE_TRY_AGAIN="please try again";	
</script>

<script type="text/javascript" src="/LHassets/admin/masterdata/create.js"></script>

</head>

<body class="hold-transition skin-brown layout-top-nav fixed">

	<div class="wrapper">

		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<div class="navbar-header">
						<a href="/cif/search/home" class="navbar-brand">
							<img src="/LHassets/core/latest/img/logo.png" class="hidden-xs logo img-responsive">
							<img src="/LHassets/core/latest/img/48x48.png" class="visible-xs logo img-responsive">
						</a>
						<label class="nav-title"><strong>CIF</strong></label>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Laptop/Desktop -->
					<div class="hidden-xs">
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">

							<ul class="nav navbar-nav menubar">

								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
										<span class="hidden-xs pull-right">${USER_NAME}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>${USER_NAME}</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>

										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Laptop/Desktop -->
					
					<!-- Mobile/Tablet -->
					<div class="visible-xs">
					
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">	
							
								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="${userInfoSession.photo}" class="user-image" alt="User Image">
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="${userInfoSession.photo}" class="img-circle" alt="User Image">
											<p>
												${USER_NAME}
											</p></li>
										<!-- Menu Body -->
										<li class="user-body">									
											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="https://search-doc-library-ui.appspot.com/">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Mobile/Tablet -->
					
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>

		<div class="content-wrapper">


<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">

					<div class="box-header">
						<div class="row">
							<div class="col-md-3">
								<h3 class="box-title main-title"><strong>Master Data Management</strong></h3>
							</div>
						</div>
					</div>

					<div class="box-body">
						<form id="newMasterdata" name="newMasterdata" action="/cif/api/admin/masterdata" method="post">
							
 						<div class="row" id="firstDiv">
								<div class="col-lg-3 col-md-3 col-sm-6">
								 	<div class="form-group">
										<label>Lookup Type</label> 
										<select id="lookUpList" name="lookUpList" class="form-control">
											<c:choose>
												<c:when test="${requestScope.action eq 'EDIT'}">
													<option value="${requestScope.masterRecord.lookupType}" id="${requestScope.masterRecord.lookupType}">
													${requestScope.masterRecord.lookupType}
												</option>
												</c:when>
												<c:otherwise>
													<c:if test="${not empty requestScope.lookTypeList}">
														<option value="-1"></option>
														<c:forEach var="list" items="${requestScope.lookTypeList}">
															<option value="${list}" id="${list}">${list}</option>
														</c:forEach>
													</c:if>
												</c:otherwise>
											</c:choose>
										</select>
									</div>
								</div>

								<div class="col-lg-3 col-md-3 col-sm-6">
								 	<div class="form-group">
										<label>Display Value</label>

										<p><c:choose>
											<c:when test="${requestScope.action eq 'CREATE'}">
												<input name="displayValue" class="form-control" value="" type="text" id="displayValue"/>
											</c:when>
											<c:otherwise>
												<input name="displayValue" class="form-control" value="${requestScope.masterRecord.displayValue}" type="text" id="displayValue" tabindex="5" />
											</c:otherwise>
										</c:choose></p>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-3 col-sm-6">
								 	<div class="form-group">
										<label>Sort Order</label>

										<p><c:choose>
											<c:when test="${requestScope.action eq 'CREATE'}">
												<input name="sortOrder" class="form-control" value="" type="text" maxlength="4" id="sortOrder"/>
											</c:when>
											<c:otherwise>
												<input name="sortOrder" class="form-control" value="${requestScope.masterRecord.sortOrder}" type="text" maxlength="4" id="sortOrder" tabindex="5" />
											</c:otherwise>
										</c:choose></p>
									</div>
								</div>
							</div>

							<div class="row" id="secondtDiv" style="display:none;">
								<div class="col-lg-3 col-md-3 col-sm-6">
								 	<div class="form-group">
										<label>Id</label>
											<p><c:choose>
												<c:when test="${requestScope.action eq 'CREATE'}">
													<input name="id" class="form-control" value="" type="text" id="id" readonly />
												</c:when>
												<c:otherwise>
													<input name="id" class="form-control" value="${requestScope.masterRecord.id}" type="text" id="id" readonly/>
												</c:otherwise>
											</c:choose>
										</p>
										</div>
								</div>							
								<div class="col-lg-3 col-md-3 col-sm-6">
									 <div class="form-group">
										<label>Lookup Id</label>
										<p><c:choose>
											<c:when test="${requestScope.action eq 'CREATE'}">
												<input name="lookupId" class="form-control" value="" type="text" id="lookupId" readonly/>
											</c:when>
											<c:otherwise>
												<input name="lookupId" class="form-control" value="${requestScope.masterRecord.lookupId}" type="text" id="lookupId" tabindex="5" readonly />
											</c:otherwise>
										</c:choose>
										</p>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-10 col-md-10">
									<c:choose>
										<c:when test="${requestScope.action eq 'CREATE'}">
											<div class="pull-left">
												<button type="button" id="submitButton" name="show" class="btn btn-primary" value="Save" onclick="addLink()">Create New</button>
											</div>
											<div class="col-xs-2 pull-left">
												<button type="button" id="resetButton" name="reset" class="btn btn-primary" value="Reset" tabindex="16" onclick="resetFields()">Reset</button>
											</div>
										</c:when>
										<c:otherwise>
											<div class="pull-left">
												<button type="button" id="updatetButton" name="Same" class="btn btn-primary" value="Update" onclick="addLink()">Update</button>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>

							<input type="hidden" id="flag" name="flag" value="" />
							<div id="errMsg" style="color: red"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>

	</div>
</body>
</html>