<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="shortcut icon" type="image/ico" href="/LHassets/core/latest/img/favicon/favicon.ico" />

<title>CIF : Manage Library</title>

<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen-bootstrap.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/chosen.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-dialog.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-editable.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/jquery-ui.custom.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/sweetalert.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/font-awesome.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/bootstrap-tokenfield.min.css" />
<link rel="stylesheet" href="/LHassets/core/latest/css/select2.min.css">
<link rel="stylesheet" href="/LHassets/core/latest/css/AdminLTE.css"/>
<link rel="stylesheet" href="/LHassets/core/css/custom.css"/>

<script src="/LHassets/core/latest/js/jquery.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-dialog.js"></script>
<script src="/LHassets/core/latest/js/bootstrap.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.dataTables.js"></script>
<script src="/LHassets/core/latest/js/DT_bootstrap.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-editable.js"></script>
<script src="/LHassets/core/latest/js/chosen.jquery.js" ></script>
<script src="/LHassets/core/latest/js/jquery-ui.min.js"></script>
<script src="/LHassets/core/latest/js/typeahead.bundle.js"></script>
<script src="/LHassets/core/latest/js/jquery.toaster.js"></script>
<script src="/LHassets/core/latest/js/fnLengthChange.js"></script>
<script src="/LHassets/core/latest/js/sweetalert.min.js"></script>
<script src="/LHassets/core/latest/js/bootstrap-tokenfield.min.js"></script>
<script src="/LHassets/core/latest/js/select2.full.min.js"></script>
<script src="/LHassets/core/latest/js/fastclick.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.slimscroll.min.js"></script>
<script src="/LHassets/core/latest/js/fileinput.min.js"></script>
<script src="/LHassets/core/latest/js/moment.min.js"></script>
<script src="/LHassets/core/latest/js/datetime-moment.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay.min.js"></script>
<script src="/LHassets/core/latest/js/loadingoverlay_progress.min.js"></script>
<script src="/LHassets/core/latest/js/jquery.ui.touch-punch.min.js"></script>
<script src="/LHassets/core/latest/js/app.min.js"></script>

<script type="text/javascript">
	var _OPCO_NAME = "CIF";
	var _USER_EMAIL = "test.user@dev.lafargeholcim.com";
	var _USER_NAME = "Test User";
	var _USER_ROLE = "TEST USER";
	var _LOCALE = "en";
	var _COUNTRY_CODE = "cif";

	var title = "CIF : Manage Library";
	var _SEARCH_DT = 'Search';
	var _LENGTH_DT = 'Show _MENU_';
	var _FILTERINFO_DT = '(filtered from _MAX_ rows)';
	var _EMPTYINFO_DT = 'Showing 0 to 0';
	var _INFO_DT = 'Showing _START_ to _END_';
	var _CALCULATING_DT = 'of Calculating..';
	var _OF_DT = 'of';
	var _FILTER_DT = '(Filter)';
	var _EXPORT_MSG = "Please check your mail after few minutes for the exported data";
</script>

<script type="text/javascript"
	src="/LHassets/home.js">
</script>

</head>

<body class="hold-transition skin-brown layout-top-nav fixed">

	<div class="wrapper">

		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<div class="navbar-header">
						<a href="/cif/search/home" class="navbar-brand">
							<img src="/LHassets/core/latest/img/logo.png" class="hidden-xs logo img-responsive">
							<img src="/LHassets/core/latest/img/48x48.png" class="visible-xs logo img-responsive">
						</a>
						<label class="nav-title"><strong>CIF</strong></label>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Laptop/Desktop -->
					<div class="hidden-xs">
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">

							<ul class="nav navbar-nav menubar">

								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAoACgDASIAAhEBAxEB/8QAGwABAAEFAQAAAAAAAAAAAAAAAAcBAgUGCAP/xAAYAQADAQEAAAAAAAAAAAAAAAADBAUGB//aAAwDAQACEAMQAAABn6jUukZHar+YpneFuwmHrgc1ELY8JLERZqwrM7wZ17w5yLqd1C0r0CMXU//EACEQAAEEAQQDAQAAAAAAAAAAAAUBAgQGEAADEzUSFRYx/9oACAEBAAEFAscjeXNyK7goV4IuqQW3CUDFjDe8GrVyDd2rglBQdOejGk75JlP+jnqo+9TIr48hsqOR61PzFd6L/8QAHhEAAgICAgMAAAAAAAAAAAAAAQIAAxARBBIiMUH/2gAIAQMBAT8BZtDcrv7HUMtZQPKVsinHK+YX0J//xAAiEQABAwMDBQAAAAAAAAAAAAADAAECBAUREBJBITEycYH/2gAIAQIBAT8BENySaDcqrtzhhvznSkCWc8i4VWCqJDb0Tqz9pfE6L5v7X//EACsQAAECBAIJBQEAAAAAAAAAAAECAwAEERIQMRMUICFBUVJzkWFxobGy0f/aAAgBAQAGPwLCy5N/TXfsDRG1x5VgV0xlDrbyitcuQLjmQcvrEtXBLiTegnnFmqOV+PMFKyFPOm5dMh6YFSjRKRUnlBEsBLtcDSqz/Irrj9feBpqTLfEHcrzCHUGqHBcIme0v8nYk+0I//8QAIhABAAECBgIDAAAAAAAAAAAAAREAIRAxQVFxgWHBIKGx/9oACAEBAAE/IcEAweUE+s8YqUePmYhVPNo7pWyFWVbq70WCcky9zqkr8YwxoWwZs+EU7q9TLMh+6KlMxb9AG8b7rgThhGQLrWsSAPNZtwK/NAeqBNuB1D2RxTai1SLJhIyuPgn/2gAMAwEAAgADAAAAEETMiAQAAAP/xAAcEQACAgIDAAAAAAAAAAAAAAAAARFBECExgaH/2gAIAQMBAT8QQ5qEaJCyF2onqns09o5YeJH/xAAdEQACAwACAwAAAAAAAAAAAAAAAREhMXHRobHw/9oACAECAQE/ENKaECqlJ1G1OuSzNDtLxdk3NlrSlNxz2UcMy/NKHnvZ/8QAIBABAQACAgEFAQAAAAAAAAAAAREAITFBYRBRcYGRof/aAAgBAQABPxDF1gMBqyvcuvzDn7nGHOMe35jkUdHfZ1ADyNGzFGCg7Cl2rzXa+cVAkYAwm0HW0a3VDEuGTAAUgaNiAlm01lw0izfJwPN+sAoBigK3QX2KjWXBAIZolHgBfrDRBQoOkpHoGWKuNEG3BPzT+ZvIRDJ2kl8IuLVLoC+EiU6epvdOvQQZ8LOfQT5D+Z//2Q==" class="user-image" alt="User Image">
										<span class="hidden-xs pull-right">Test User</span>
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAoACgDASIAAhEBAxEB/8QAGwABAAEFAQAAAAAAAAAAAAAAAAcBAgUGCAP/xAAYAQADAQEAAAAAAAAAAAAAAAADBAUGB//aAAwDAQACEAMQAAABn6jUukZHar+YpneFuwmHrgc1ELY8JLERZqwrM7wZ17w5yLqd1C0r0CMXU//EACEQAAEEAQQDAQAAAAAAAAAAAAUBAgQGEAADEzUSFRYx/9oACAEBAAEFAscjeXNyK7goV4IuqQW3CUDFjDe8GrVyDd2rglBQdOejGk75JlP+jnqo+9TIr48hsqOR61PzFd6L/8QAHhEAAgICAgMAAAAAAAAAAAAAAQIAAxARBBIiMUH/2gAIAQMBAT8BZtDcrv7HUMtZQPKVsinHK+YX0J//xAAiEQABAwMDBQAAAAAAAAAAAAADAAECBAUREBJBITEycYH/2gAIAQIBAT8BENySaDcqrtzhhvznSkCWc8i4VWCqJDb0Tqz9pfE6L5v7X//EACsQAAECBAIJBQEAAAAAAAAAAAECAwAEERIQMRMUICFBUVJzkWFxobGy0f/aAAgBAQAGPwLCy5N/TXfsDRG1x5VgV0xlDrbyitcuQLjmQcvrEtXBLiTegnnFmqOV+PMFKyFPOm5dMh6YFSjRKRUnlBEsBLtcDSqz/Irrj9feBpqTLfEHcrzCHUGqHBcIme0v8nYk+0I//8QAIhABAAECBgIDAAAAAAAAAAAAAREAIRAxQVFxgWHBIKGx/9oACAEBAAE/IcEAweUE+s8YqUePmYhVPNo7pWyFWVbq70WCcky9zqkr8YwxoWwZs+EU7q9TLMh+6KlMxb9AG8b7rgThhGQLrWsSAPNZtwK/NAeqBNuB1D2RxTai1SLJhIyuPgn/2gAMAwEAAgADAAAAEETMiAQAAAP/xAAcEQACAgIDAAAAAAAAAAAAAAAAARFBECExgaH/2gAIAQMBAT8QQ5qEaJCyF2onqns09o5YeJH/xAAdEQACAwACAwAAAAAAAAAAAAAAAREhMXHRobHw/9oACAECAQE/ENKaECqlJ1G1OuSzNDtLxdk3NlrSlNxz2UcMy/NKHnvZ/8QAIBABAQACAgEFAQAAAAAAAAAAAREAITFBYRBRcYGRof/aAAgBAQABPxDF1gMBqyvcuvzDn7nGHOMe35jkUdHfZ1ADyNGzFGCg7Cl2rzXa+cVAkYAwm0HW0a3VDEuGTAAUgaNiAlm01lw0izfJwPN+sAoBigK3QX2KjWXBAIZolHgBfrDRBQoOkpHoGWKuNEG3BPzT+ZvIRDJ2kl8IuLVLoC+EiU6epvdOvQQZ8LOfQT5D+Z//2Q==" class="img-circle" alt="User Image">
											<p>Test User <small> TEST USER</small></p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="https://search-doc-library-ui.appspot.com/">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>

										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Laptop/Desktop -->
					
					<!-- Mobile/Tablet -->
					<div class="visible-xs">
					
						<!-- Navbar Right Menu -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">	
							
								<!-- User Account Menu -->
								<li class="dropdown user user-menu">
									<!-- Menu Toggle Button --> 
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
										<!-- The user image in the navbar--> 
										<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAoACgDASIAAhEBAxEB/8QAGwABAAEFAQAAAAAAAAAAAAAAAAcBAgUGCAP/xAAYAQADAQEAAAAAAAAAAAAAAAADBAUGB//aAAwDAQACEAMQAAABn6jUukZHar+YpneFuwmHrgc1ELY8JLERZqwrM7wZ17w5yLqd1C0r0CMXU//EACEQAAEEAQQDAQAAAAAAAAAAAAUBAgQGEAADEzUSFRYx/9oACAEBAAEFAscjeXNyK7goV4IuqQW3CUDFjDe8GrVyDd2rglBQdOejGk75JlP+jnqo+9TIr48hsqOR61PzFd6L/8QAHhEAAgICAgMAAAAAAAAAAAAAAQIAAxARBBIiMUH/2gAIAQMBAT8BZtDcrv7HUMtZQPKVsinHK+YX0J//xAAiEQABAwMDBQAAAAAAAAAAAAADAAECBAUREBJBITEycYH/2gAIAQIBAT8BENySaDcqrtzhhvznSkCWc8i4VWCqJDb0Tqz9pfE6L5v7X//EACsQAAECBAIJBQEAAAAAAAAAAAECAwAEERIQMRMUICFBUVJzkWFxobGy0f/aAAgBAQAGPwLCy5N/TXfsDRG1x5VgV0xlDrbyitcuQLjmQcvrEtXBLiTegnnFmqOV+PMFKyFPOm5dMh6YFSjRKRUnlBEsBLtcDSqz/Irrj9feBpqTLfEHcrzCHUGqHBcIme0v8nYk+0I//8QAIhABAAECBgIDAAAAAAAAAAAAAREAIRAxQVFxgWHBIKGx/9oACAEBAAE/IcEAweUE+s8YqUePmYhVPNo7pWyFWVbq70WCcky9zqkr8YwxoWwZs+EU7q9TLMh+6KlMxb9AG8b7rgThhGQLrWsSAPNZtwK/NAeqBNuB1D2RxTai1SLJhIyuPgn/2gAMAwEAAgADAAAAEETMiAQAAAP/xAAcEQACAgIDAAAAAAAAAAAAAAAAARFBECExgaH/2gAIAQMBAT8QQ5qEaJCyF2onqns09o5YeJH/xAAdEQACAwACAwAAAAAAAAAAAAAAAREhMXHRobHw/9oACAECAQE/ENKaECqlJ1G1OuSzNDtLxdk3NlrSlNxz2UcMy/NKHnvZ/8QAIBABAQACAgEFAQAAAAAAAAAAAREAITFBYRBRcYGRof/aAAgBAQABPxDF1gMBqyvcuvzDn7nGHOMe35jkUdHfZ1ADyNGzFGCg7Cl2rzXa+cVAkYAwm0HW0a3VDEuGTAAUgaNiAlm01lw0izfJwPN+sAoBigK3QX2KjWXBAIZolHgBfrDRBQoOkpHoGWKuNEG3BPzT+ZvIRDJ2kl8IuLVLoC+EiU6epvdOvQQZ8LOfQT5D+Z//2Q==" class="user-image" alt="User Image">
									</a>
									<ul class="dropdown-menu">
										<!-- The user image in the menu -->
										<li class="user-header"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAoACgDASIAAhEBAxEB/8QAGwABAAEFAQAAAAAAAAAAAAAAAAcBAgUGCAP/xAAYAQADAQEAAAAAAAAAAAAAAAADBAUGB//aAAwDAQACEAMQAAABn6jUukZHar+YpneFuwmHrgc1ELY8JLERZqwrM7wZ17w5yLqd1C0r0CMXU//EACEQAAEEAQQDAQAAAAAAAAAAAAUBAgQGEAADEzUSFRYx/9oACAEBAAEFAscjeXNyK7goV4IuqQW3CUDFjDe8GrVyDd2rglBQdOejGk75JlP+jnqo+9TIr48hsqOR61PzFd6L/8QAHhEAAgICAgMAAAAAAAAAAAAAAQIAAxARBBIiMUH/2gAIAQMBAT8BZtDcrv7HUMtZQPKVsinHK+YX0J//xAAiEQABAwMDBQAAAAAAAAAAAAADAAECBAUREBJBITEycYH/2gAIAQIBAT8BENySaDcqrtzhhvznSkCWc8i4VWCqJDb0Tqz9pfE6L5v7X//EACsQAAECBAIJBQEAAAAAAAAAAAECAwAEERIQMRMUICFBUVJzkWFxobGy0f/aAAgBAQAGPwLCy5N/TXfsDRG1x5VgV0xlDrbyitcuQLjmQcvrEtXBLiTegnnFmqOV+PMFKyFPOm5dMh6YFSjRKRUnlBEsBLtcDSqz/Irrj9feBpqTLfEHcrzCHUGqHBcIme0v8nYk+0I//8QAIhABAAECBgIDAAAAAAAAAAAAAREAIRAxQVFxgWHBIKGx/9oACAEBAAE/IcEAweUE+s8YqUePmYhVPNo7pWyFWVbq70WCcky9zqkr8YwxoWwZs+EU7q9TLMh+6KlMxb9AG8b7rgThhGQLrWsSAPNZtwK/NAeqBNuB1D2RxTai1SLJhIyuPgn/2gAMAwEAAgADAAAAEETMiAQAAAP/xAAcEQACAgIDAAAAAAAAAAAAAAAAARFBECExgaH/2gAIAQMBAT8QQ5qEaJCyF2onqns09o5YeJH/xAAdEQACAwACAwAAAAAAAAAAAAAAAREhMXHRobHw/9oACAECAQE/ENKaECqlJ1G1OuSzNDtLxdk3NlrSlNxz2UcMy/NKHnvZ/8QAIBABAQACAgEFAQAAAAAAAAAAAREAITFBYRBRcYGRof/aAAgBAQABPxDF1gMBqyvcuvzDn7nGHOMe35jkUdHfZ1ADyNGzFGCg7Cl2rzXa+cVAkYAwm0HW0a3VDEuGTAAUgaNiAlm01lw0izfJwPN+sAoBigK3QX2KjWXBAIZolHgBfrDRBQoOkpHoGWKuNEG3BPzT+ZvIRDJ2kl8IuLVLoC+EiU6epvdOvQQZ8LOfQT5D+Z//2Q==" class="img-circle" alt="User Image">
											<p>
												Test User <small> TEST USER</small>
											</p></li>
										<!-- Menu Body -->
										<li class="user-body">									
											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/search/home">Search library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/library/manage">Manage library</a>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 text-center">
													<a href="/cif/admin/masterdata/manage">Master Data Management</a>
												</div>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="/logout" class="btn btn-primary btn-flat">Close App Session</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<!-- /.navbar-custom-menu -->
					</div>
					<!-- /. Mobile/Tablet -->
					
				</div>
				<!-- /.container-fluid -->
			</nav>
		</header>

		<div class="content-wrapper">
		
		In Progress .....
		
		</div>
	</div>
</body>

</html>