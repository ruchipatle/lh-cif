import { Component, OnInit } from '@angular/core';
import { CIFDataService } from '../serivce/cif-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  content: any;

  constructor(private cifDataService: CIFDataService) { }

  ngOnInit() {

    this.cifDataService.retrieveSection_ListData().subscribe(data => {
      this.content = data;
    },
    error => {
      console.log("eror in dashboard load");
    });

  }

}
