
import { Component, OnInit ,Input} from '@angular/core';
import { CIFDataService } from 'src/app/serivce/cif-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {

  content:any;

  sectionName:string;

  pageOfItems:Array<any>;
  
  items=[];

  constructor(private cifDataService: CIFDataService,private route: ActivatedRoute,private routeParams: ActivatedRoute,) {  }
  
  ngOnInit() {

    this.fetchSectionData(); 

  }

  fetchSectionData(){   

    this.sectionName = this.routeParams.snapshot.params['sectionName'];

    this.cifDataService.retrieveSectionData(this.sectionName).subscribe(data => {
      this.content = data;
      this.items = this.content.records;
    },

    error => {
      console.log('error in fatching the data');
    });

  }

  ngOnChange(){

    this.fetchSectionData();

  }
  
  onChangePage(pageOfItems: Array<any>) {

    this.pageOfItems = pageOfItems;

  }

}
