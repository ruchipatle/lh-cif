import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DocumentDetailsComponent } from './document-details/document-details.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SerachItemComponent } from './serach-item/serach-item.component';
import { SectionComponent } from './dashboard/section/section.component';


const routes: Routes = [
 
  { path: '', component:DashboardComponent },
  { path: 'doc_details/:id', component:DocumentDetailsComponent},
  { path: 'search_item/:item', component:SerachItemComponent},
  { path: 'dashboard',component:DashboardComponent},
  { path: 'section/:sectionName',component:SectionComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
