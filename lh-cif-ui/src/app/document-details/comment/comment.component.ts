import { Component, Input, OnInit, ɵɵNgOnChangesFeature } from '@angular/core';
import { CIFDataService } from 'src/app/serivce/cif-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DocumentDetailsComponent } from '../document-details.component';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})

export class CommentComponent implements OnInit {
 
  @Input() comments: any;
  comment_on_doc:any;
  id:any;
  content:any;
  obj={documentId: '', comment: '', userId: ''};
  constructor(private cifDataService: CIFDataService, private router:Router,private route: ActivatedRoute) { }

  post_comment(documentId, comment, userId) {
    

console.log(comment)

    if(comment.trim()!==''){

      console.log(comment);
    this.obj.documentId=documentId;
    this.obj.comment=comment;
    this.obj.userId=userId;

    this.cifDataService.post_comment(this.obj).subscribe(

    data =>{
      this.comment_on_doc='';
      this.ngOnInit();
    },
    error=>{
      console.log("eror in comments post comment");
    });}


    else{


alert("Comment can not be empty");

    }
  }

  ngOnInit() {
  
     this.id = this.route.snapshot.params.id;

    this.cifDataService.retrieve_Documente_Data(this.id).subscribe(data => {
      this.content = data;
    },
    error => {
      console.log("eror in comments init");
    });
   }

}