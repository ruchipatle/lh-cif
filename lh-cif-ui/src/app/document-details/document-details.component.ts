import { Component, OnInit, Input } from '@angular/core';
import { CIFDataService } from 'src/app/serivce/cif-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-document-details',
  templateUrl: './document-details.component.html',
  styleUrls: ['./document-details.component.css']
})

export class DocumentDetailsComponent implements OnInit {

  constructor(private cifDataService: CIFDataService,private route: ActivatedRoute) { }

  src :any;
  id: any;
  obj = {documentId: '', liked:false, userId: ''};
  content: any;
  likedBy: number
  status: boolean = false;
  contentOfpeople: any;
  isCliked = false;
  showComment = false;
  isLiked = false;
  receivedSearchString: string;

  show_Hide_Comment(){

    if(this.showComment)
      this.showComment=false;   
    else
      this.showComment= true;
    
  }

  likedByPeople(){
    
    this.cifDataService.retrieveDocumentLikedBy(this.id).subscribe(
      data =>{
        this.contentOfpeople = data;
        this.isCliked=true;
      },
      error=>{
       console.log('error during like...');
      }
    );

  }

  likeBtn(documentId, liked) {

    this.obj.documentId=documentId;
    this.obj.liked=liked;

    this.cifDataService.document_likes(this.obj).subscribe(
      data =>{
        this.isLiked = liked;
      },
      error=>{
        this.isLiked = !liked;
      }
    );

  }

  backToSearch(theLink){
    console.log(theLink);
  }

  ngOnInit() {

    console.log('search : '+ this.cifDataService.csdlSearchString);
    this.receivedSearchString = this.cifDataService.csdlSearchString;
    this.id = this.route.snapshot.params.id;

    this.cifDataService.retrieve_Documente_Data(this.id).subscribe(data => {
      this.content = data;
      this.isLiked = this.content.like;
    },
    error => {
      console.log("eror in retriveTodo");
    });

  }

}