import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerachItemComponent } from './serach-item.component';

describe('SerachItemComponent', () => {
  let component: SerachItemComponent;
  let fixture: ComponentFixture<SerachItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerachItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerachItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
