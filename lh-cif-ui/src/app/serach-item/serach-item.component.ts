import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { CIFDataService } from '../serivce/cif-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-serach-item',
  templateUrl: './serach-item.component.html',
  styleUrls: ['./serach-item.component.css']
})
export class SerachItemComponent implements OnInit {

  toSearch = "";
  content: any[];  
  pageOfItems:Array<any>;  
  items=[];
  dataSources=[];
  docTypes=[];
  selDataSource="";
  selDocType="";

  constructor(private cifDataService: CIFDataService, private routeParams: ActivatedRoute, private router: Router) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

  }

  ngOnInit() {

    this.fetchData_service();

  }

  fetchData_service() {
  
    this.toSearch = this.routeParams.snapshot.params['item'];
    this.cifDataService.csdlDTypeFilter

    if(this.toSearch.trim()!=''){

      this.cifDataService.searchCIFDocuments(this.toSearch,this.cifDataService.csdlDSrcFilter,this.cifDataService.csdlDTypeFilter).subscribe(data => {
        if(data.selectedDataSource && data.selectedDataSource !=null){
          this.selDataSource = data.selectedDataSource;
        }
        if(data.selectedDocumentType && data.selectedDocumentType !=null){
          this.selDocType = data.selectedDocumentType;
        } 
        this.dataSources = data.dataSource;
        this.docTypes = data.documentType;
        this.content = data.searchResult;
        this.items = this.content;
      },

      error => {
        console.log('error in fatching the data');
      });
    }
    else{
     alert("Please enter something to search");
    }

  }
  
  onChangePage(pageOfItems: Array<any>) {

    this.pageOfItems = pageOfItems;

  }

  onFilterSearch(){

    if(this.toSearch.trim()!=''){

      this.cifDataService.searchCIFDocuments(this.toSearch, this.selDataSource, this.selDocType).subscribe(data => {
        if(data.selectedDataSource && data.selectedDataSource !=null){
          this.selDataSource = data.selectedDataSource;
        }
        if(data.selectedDocumentType && data.selectedDocumentType !=null){
          this.selDocType = data.selectedDocumentType;
        } 
        this.dataSources = data.dataSource;
        this.docTypes = data.documentType;
        this.content = data.searchResult;
        this.items = this.content;
      },

      error => {
        console.log('error in fatching the data');
      });
    }
    else{
     alert("Please enter something to search");
    }
  }

  onDataSrcChange(event: any){
    this.selDataSource = event.target.value;
  }

  onDocTypeChange(event: any){
    this.selDocType = event.target.value;
  }  
}