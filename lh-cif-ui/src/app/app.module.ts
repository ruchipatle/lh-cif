import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule, FormsModule} from'@angular/forms'
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { CommentComponent } from './document-details/comment/comment.component';
import { DocumentDetailsComponent } from './document-details/document-details.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SerachItemComponent } from './serach-item/serach-item.component';
import { SectionComponent } from './dashboard/section/section.component';
import { ChildComponent } from './component/child/child.component';
//import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SubmenuComponent } from './header/submenu/submenu.component'; // <- import PdfViewerModule
import { JwPaginationComponent } from 'jw-angular-pagination';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    ItemListComponent,
    ItemDetailComponent,
    CommentComponent,
    DocumentDetailsComponent,
    LoginPageComponent,
    SerachItemComponent,
    SectionComponent,
    ChildComponent,
    SubmenuComponent,
    JwPaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    PdfViewerModule
    //ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }) 
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
