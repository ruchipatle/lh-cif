import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CIFDataService } from '../serivce/cif-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  value = "";
  showSubmenu = false;
  content: any;

  constructor( private router: Router, private cifDataService: CIFDataService ) { }

  profileClick() {

    this.showSubmenu = !this.showSubmenu;

  }

  ngOnInit() {

    this.cifDataService.retrieveUserData().subscribe(data => {
        this.content = data;
        console.log(data)
      },
      error => {
        console.log("eror in retriveTodo");
      }
    );

  }

  onEnter(){

    if(this.value.trim()!='') {
      console.log(this.value + "Inside")
      this.router.navigate(['search_item',this.value]);
    }

  }

  homePage(){

    this.router.navigate(['']);
    this.value='';

  }

}