import { TestBed } from '@angular/core/testing';

import { CIFDataService } from './cif-data.service';

describe('CIFDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CIFDataService = TestBed.get(CIFDataService);
    expect(service).toBeTruthy();
  });
});
