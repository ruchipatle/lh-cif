import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CIFDataService {

  constructor(private http: HttpClient) { }

  public csdlSearchString:string = "";
  public csdlDSrcFilter:string = "";
  public csdlDTypeFilter:string = "";

  retrieveUserData() {

    return this.http.get<any>(window['cifAPIBaseURL'] + '/userData');

  }  

  retrieveSection_ListData() {

    //return this.http.get<any>('../../assets/data/data.json');
    // return this.http.get<any>('https://testdriveui-dot-search-doc-library-267015.appspot.com/cif/api/search/limitedSectionData');
    return this.http.get<any>(window['cifAPIBaseURL'] + '/limitedSectionData');

  }

  retrieveDocumentLikedBy(id:number) {

    return this.http.get<any>(window['cifAPIBaseURL'] + '/listLikedBy?docId='+id);

  }

  retrieveSectionData(sectionName:string) {

    return this.http.get<any>(window['cifAPIBaseURL'] + '/sectionData?section='+sectionName);

  }

  retrieve_Documente_Data(id:any) {

    return this.http.get<any>(window['cifAPIBaseURL'] + '/viewDocument/' + id);

  }

  retrieveSearchItem(name:string) {

    this.csdlSearchString = name;
    return this.http.get<any>(window['cifAPIBaseURL'] + '/search?searchTerm='+name);

  }

  searchCIFDocuments(name:string, dataSrc:string, docType:string) {

    this.csdlSearchString = name;
    this.csdlDSrcFilter = dataSrc;
    this.csdlDTypeFilter = docType;

    return this.http.get<any>(window['cifAPIBaseURL'] + '/search?searchTerm='+name+"&dataSource="+dataSrc+"&docType="+docType);

  }

  post_comment(Comment_Data) {

    return this.http.post(window['cifAPIBaseURL'] + '/saveComment',Comment_Data);

  }

  document_likes(like_data) {

    return this.http.post(window['cifAPIBaseURL'] + '/likeDocument',like_data);

  }

}