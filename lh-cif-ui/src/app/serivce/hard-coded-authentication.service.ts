import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HardCodedAuthenticationService {
 
  authenticate(username, password) {
    console.log('before ' + this.isUserLoggedIn());

    if (username === "Shubham" && password === "") {
      sessionStorage.setItem('authenticator', username);
      console.log('after ' + this.isUserLoggedIn());

      return true
    }
    else
      return false;


  }

  isUserLoggedIn() {

    let user = sessionStorage.getItem('authenticator');

    return !(user === null);


  }

  loggedOut() {

    sessionStorage.removeItem('authenticator');

  }
}
