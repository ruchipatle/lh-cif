import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, ChangeDetectionStrategy, DoCheck } from '@angular/core';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  //private myNewNumber: number
  @Input() myNewNumber: string
  constructor() {

    console.log('child contructor');

  }

  //   @Input()
  //   set myNewNumber(number: number) {

  //     this.MyNumber = number;
  //     console.log('One time set')
  //   }

  //   get myNewNumber(){
  //     console.log('int time get')

  // return this.MyNumber;

  //   }

  ngOnInit() {
    // debugger
    console.log("ngOninit called of child component");

  }

  // It will execute whenever value gets change in parent side as it is recieving value from parent, so it gets notified as soemthing is cahnged there ...so as we are using @input 
  // tag it is somehow related to the ngOnChange  
  ngOnChanges(change: SimpleChanges) {
    //debugger
    console.log(change);
    console.log("ngOnChange called of child component");

  }



  // it will called when we get the value as pass by reference like by changing the value of the object
  ngDoCheck() {

    // debugger
    console.log('ngdocheck called of child component');

  }


  //Executes only one time like we use this hook only to write some special code after all init change doCheck called 
  ngAfterContentInit() {

    //debugger
    console.log('ngAfterContentInit of child component')

  }
  ngAfterContentChecked() {

    // debugger
    console.log('ngAfterContentChecked of child component');

  }

  ngAfterViewInit(){



    console.log('ngAfterViewInit of child component');




  }

  ngAfterViewChecked(){


    console.log('ngAfterViewChecked of child component');


  }

  ngOnDestroy(){

    console.log('ngDestroy of child component');


  }




}
